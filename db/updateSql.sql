-- 绩效计算  20210328

DROP TABLE IF EXISTS `hr_assessment_task_result_summary`;
CREATE TABLE `hr_assessment_task_result_summary` (
`id`  varchar(64) NOT NULL ,
`create_by`  varchar(64) NULL DEFAULT NULL ,
`create_time`  datetime NULL DEFAULT NULL ,
`update_by`  varchar(64) NULL DEFAULT NULL ,
`update_time`  datetime NULL DEFAULT NULL ,
`instance_code`  varchar(64) NULL DEFAULT NULL ,
`username`  varchar(64) NULL DEFAULT NULL ,
`score`  decimal(20,6) NULL DEFAULT NULL ,
`final_score`  decimal(20,6) NULL DEFAULT NULL ,
`assessment_score`  decimal(20,6) NULL DEFAULT NULL ,
`level_desc`  varchar(255) NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

ALTER TABLE `hr_assessment_rule` ADD COLUMN `score_super`  decimal(20,6) NULL DEFAULT NULL AFTER `plan_code`;
ALTER TABLE `hr_assessment_rule` ADD COLUMN `score_lower`  decimal(20,6) NULL DEFAULT NULL AFTER `score_super`;
ALTER TABLE `hr_assessment_rule` DROP COLUMN `score`;
ALTER TABLE `hr_assessment_instance` ADD COLUMN `plan_code` varchar(64) NULL DEFAULT NULL AFTER `update_time`;

DROP TABLE IF EXISTS `sys_tenant`;
CREATE TABLE `sys_tenant` (
`id`  int(5) NOT NULL COMMENT '租户编码' ,
`name`  varchar(100) NULL DEFAULT NULL COMMENT '租户名称' ,
`create_time`  datetime NULL DEFAULT NULL COMMENT '创建时间' ,
`create_by`  varchar(100) NULL DEFAULT NULL COMMENT '创建人' ,
`begin_date`  datetime NULL DEFAULT NULL COMMENT '开始时间' ,
`end_date`  datetime NULL DEFAULT NULL COMMENT '结束时间' ,
`status`  int(1) NULL DEFAULT NULL COMMENT '状态 1正常 0冻结' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

-- 绩效升级SQL 20210321

DROP TABLE IF EXISTS `hr_assessment_index`;
CREATE TABLE `hr_assessment_index` (
  `id` varchar(64) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `plan_code` varchar(64) DEFAULT NULL,
  `index_name` varchar(255) DEFAULT NULL,
  `index_desc` varchar(4000) DEFAULT NULL,
  `weight` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `hr_assessment_instance`;
CREATE TABLE `hr_assessment_instance` (
  `id` varchar(64) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `instance_code` varchar(64) DEFAULT NULL,
  `instance_name` varchar(255) DEFAULT NULL,
  `instance_status` varchar(64) DEFAULT NULL,
  `remark` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `hr_assessment_plan`;
CREATE TABLE `hr_assessment_plan` (
  `id` varchar(64) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `plan_code` varchar(64) DEFAULT NULL,
  `plan_name` varchar(255) DEFAULT NULL,
  `plan_type` varchar(64) DEFAULT NULL,
  `account_cycle` varchar(64) DEFAULT NULL,
  `self_able` tinyint(1) DEFAULT NULL,
  `remark` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `hr_assessment_proc`;
CREATE TABLE `hr_assessment_proc` (
  `id` varchar(64) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `plan_code` varchar(64) DEFAULT NULL,
  `proc_name` varchar(255) DEFAULT NULL,
  `assignee` varchar(255) DEFAULT NULL,
  `weight` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `hr_assessment_rule`;
CREATE TABLE `hr_assessment_rule` (
  `id` varchar(64) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `plan_code` varchar(64) DEFAULT NULL,
  `score` varchar(255) DEFAULT NULL,
  `level_desc` varchar(255) DEFAULT NULL,
  `assessment_score` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `hr_assessment_task`;
CREATE TABLE `hr_assessment_task` (
  `id` varchar(64) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `instance_code` varchar(64) DEFAULT NULL,
  `task_code` varchar(64) DEFAULT NULL,
  `username` varchar(64) DEFAULT NULL,
  `leader` varchar(64) DEFAULT NULL,
  `weight` decimal(20,6) DEFAULT NULL,
  `task_status` varchar(64) DEFAULT NULL,
  `task_complete_time` datetime DEFAULT NULL,
  `scale` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `hr_assessment_task_result`;
CREATE TABLE `hr_assessment_task_result` (
  `id` varchar(64) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `task_code` varchar(64) DEFAULT NULL,
  `index_name` varchar(255) DEFAULT NULL,
  `index_desc` varchar(4000) DEFAULT NULL,
  `weight` decimal(20,6) DEFAULT NULL,
  `score` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `hr_assessment_user`;
CREATE TABLE `hr_assessment_user` (
  `id` varchar(64) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `plan_code` varchar(64) DEFAULT NULL,
  `username` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 菜单
INSERT INTO `sys_permission` (`id`, `parent_id`, `name`, `url`, `component`, `component_name`, `redirect`, `menu_type`, `perms`, `perms_type`, `sort_no`, `always_show`, `icon`, `is_route`, `is_leaf`, `keep_alive`, `hidden`, `description`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `rule_flag`, `status`, `internal_or_external`) VALUES ('1373606643710836738', '1373511790884855809', '绩效任务管理', '/assessment/AssessmentInstance', 'personnel/assessment/AssessmentInstance', NULL, NULL, '1', NULL, '1', '2.00', '0', NULL, '1', '1', '0', '0', NULL, 'admin', '2021-03-21 20:05:19', 'admin', '2021-03-21 20:06:36', '0', '0', '1', '0');
INSERT INTO `sys_permission` (`id`, `parent_id`, `name`, `url`, `component`, `component_name`, `redirect`, `menu_type`, `perms`, `perms_type`, `sort_no`, `always_show`, `icon`, `is_route`, `is_leaf`, `keep_alive`, `hidden`, `description`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `rule_flag`, `status`, `internal_or_external`) VALUES ('1373595260726292481', '1373594259348795394', '绩效考核审批权限', NULL, NULL, NULL, NULL, '2', 'AssessmentInstance:check', '1', '1.00', '0', NULL, '1', '1', '0', '0', NULL, 'admin', '2021-03-21 19:20:05', 'admin', '2021-03-21 20:07:02', '0', '0', '1', '0');
INSERT INTO `sys_permission` (`id`, `parent_id`, `name`, `url`, `component`, `component_name`, `redirect`, `menu_type`, `perms`, `perms_type`, `sort_no`, `always_show`, `icon`, `is_route`, `is_leaf`, `keep_alive`, `hidden`, `description`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `rule_flag`, `status`, `internal_or_external`) VALUES ('1373594259348795394', '1373511790884855809', '绩效考核执行', '/assessment/AssessmentTask', 'personnel/assessment/AssessmentTask', NULL, NULL, '1', NULL, '1', '3.00', '0', NULL, '1', '0', '0', '0', NULL, 'admin', '2021-03-21 19:16:06', 'admin', '2021-03-21 20:06:53', '0', '0', '1', '0');
INSERT INTO `sys_permission` (`id`, `parent_id`, `name`, `url`, `component`, `component_name`, `redirect`, `menu_type`, `perms`, `perms_type`, `sort_no`, `always_show`, `icon`, `is_route`, `is_leaf`, `keep_alive`, `hidden`, `description`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `rule_flag`, `status`, `internal_or_external`) VALUES ('1373511990877659138', '1373511790884855809', '考核计划', '/assessment/AssessmentPlan', 'personnel/assessment/AssessmentPlan', NULL, NULL, '1', NULL, '1', '1.00', '0', NULL, '1', '1', '0', '0', NULL, 'admin', '2021-03-21 13:49:12', NULL, NULL, '0', '0', '1', '0');
INSERT INTO `sys_permission` (`id`, `parent_id`, `name`, `url`, `component`, `component_name`, `redirect`, `menu_type`, `perms`, `perms_type`, `sort_no`, `always_show`, `icon`, `is_route`, `is_leaf`, `keep_alive`, `hidden`, `description`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `rule_flag`, `status`, `internal_or_external`) VALUES ('1373511790884855809', NULL, '绩效考核', '/assessment', 'layouts/RouteView', NULL, NULL, '0', NULL, '1', '5.00', '0', 'reconciliation', '1', '0', '0', '0', NULL, 'admin', '2021-03-21 13:48:24', NULL, NULL, '0', '0', '1', '0');

-- 字典
INSERT INTO `sys_dict` (`id`, `dict_name`, `dict_code`, `description`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `type`) VALUES ('1373612603313508353', '绩效节点执行状态', 'hr_task_status', '0-未执行；1-已执行', '0', 'admin', '2021-03-21 20:29:00', 'admin', '2021-03-21 20:29:25', '0');
INSERT INTO `sys_dict` (`id`, `dict_name`, `dict_code`, `description`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `type`) VALUES ('1373600748805304321', '绩效考核状态', 'hr_instance_status', '0-已立项;1-执行中；2-待审批；3-已审批', '0', 'admin', '2021-03-21 19:41:53', NULL, NULL, '0');
INSERT INTO `sys_dict` (`id`, `dict_name`, `dict_code`, `description`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `type`) VALUES ('1373555975977476097', '绩效考核得分级别', 'hr_attendance_level', '1-优；2-良；3-中；4-差；', '0', 'admin', '2021-03-21 16:43:59', 'admin', '2021-03-21 16:44:52', '0');
INSERT INTO `sys_dict` (`id`, `dict_name`, `dict_code`, `description`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `type`) VALUES ('1373514054953377794', '绩效考核方式', 'hr_assessment_type', '1-按得分；2-按排名', '0', 'admin', '2021-03-21 13:57:24', NULL, NULL, '0');

-- 字典项
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373514094895734786', '1373514054953377794', '按得分', '1', '', '1', '1', 'admin', '2021-03-21 13:57:34', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373514126084579329', '1373514054953377794', '按排名', '2', '', '1', '1', 'admin', '2021-03-21 13:57:41', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373556031480700929', '1373555975977476097', '优', '1', '', '1', '1', 'admin', '2021-03-21 16:44:12', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373556077416718337', '1373555975977476097', '良', '2', '', '1', '1', 'admin', '2021-03-21 16:44:23', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373556116281139202', '1373555975977476097', '中', '3', '', '1', '1', 'admin', '2021-03-21 16:44:32', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373556149290311681', '1373555975977476097', '差', '4', '', '1', '1', 'admin', '2021-03-21 16:44:40', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373600799426359297', '1373600748805304321', '已立项', '0', '', '1', '1', 'admin', '2021-03-21 19:42:06', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373600834121641985', '1373600748805304321', '执行中', '1', '', '1', '1', 'admin', '2021-03-21 19:42:14', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373600868959531010', '1373600748805304321', '待审批', '2', '', '1', '1', 'admin', '2021-03-21 19:42:22', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373600900651692034', '1373600748805304321', '已审批', '3', '', '1', '1', 'admin', '2021-03-21 19:42:30', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373612646187683842', '1373612603313508353', '未执行', '0', '', '1', '1', 'admin', '2021-03-21 20:29:10', NULL, NULL);
INSERT INTO `sys_dict_item` (`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1373612674721533953', '1373612603313508353', '已执行', '1', '', '1', '1', 'admin', '2021-03-21 20:29:17', NULL, NULL);


-- 项目管理-工作日报菜单及权限 2021-12-21
INSERT INTO `sys_permission`(`id`, `parent_id`, `name`, `url`, `component`, `component_name`, `redirect`, `menu_type`, `perms`, `perms_type`, `sort_no`, `always_show`, `icon`, `is_route`, `is_leaf`, `keep_alive`, `hidden`, `description`, `create_by`, `create_time`, `update_by`, `update_time`, `del_flag`, `rule_flag`, `status`, `internal_or_external`) VALUES ('1471370974535356417', '1358404217768800258', '工作日报', '/project/dailyWork', 'project/DailyWork', NULL, NULL, 1, NULL, '1', 1.00, 0, NULL, 1, 1, 0, 0, NULL, 'admin', '2021-12-16 14:45:52', 'admin', '2021-12-16 14:51:13', 0, 0, '1', 0);
INSERT INTO `sys_role_permission`(`id`, `role_id`, `permission_id`, `data_rule_ids`, `operate_date`, `operate_ip`) VALUES ('1471371793993306114', 'f6817f48af4fb3af11b9e8bf182f618b', '1471370974535356417', NULL, '2021-12-16 14:49:07', '127.0.0.1');
-- 项目管理 -工作日报完成比计算所需字段 2021-12-31
alter table pms_task_time add (task_progress DECIMAL(20,6));
alter table pms_task add (parent_progress_part DECIMAL(20,6));
alter table pms_project add (plan_man_hours DECIMAL(20,6));

-- 项目管理 -工作日报:通用日报所需字段 2022-01-08
alter table pms_task add (task_type VARCHAR(60));
INSERT INTO `sys_dict`(`id`, `dict_name`, `dict_code`, `description`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `type`) VALUES ('1478976872946610177', '项目任务类别', 'pms_task_type', 'project:项目；common:通用', 0, 'admin', '2022-01-06 14:28:59', NULL, NULL, 0);
INSERT INTO `sys_dict`(`id`, `dict_name`, `dict_code`, `description`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `type`) VALUES ('1479299956610195457', '项目通用任务类别', 'pms_common_task_type', '会议/培训等', 0, 'admin', '2022-01-07 11:52:48', 'admin', '2022-01-07 11:52:58', 0);
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478976932182765570', '1478976872946610177', '项目', 'project', '项目/专案', 1, 1, 'admin', '2022-01-06 14:29:13', NULL, NULL);
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478977002424774658', '1478976872946610177', '通用', 'common', '会议/培训等', 1, 1, 'admin', '2022-01-06 14:29:30', NULL, NULL);
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1479300301079994370', '1479299956610195457', '会议', 'meeting', '', 1, 1, 'admin', '2022-01-07 11:54:11', NULL, NULL);
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1479300376359362562', '1479299956610195457', '培训', 'train', '', 2, 1, 'admin', '2022-01-07 11:54:28', NULL, NULL);


-- 用户信息 -新增资位，技术支撑等字段 2022-01-08
alter table sys_user add (tech_post VARCHAR(60));
alter table sys_user add (class_post VARCHAR(60));
INSERT INTO `sys_dict`(`id`, `dict_name`, `dict_code`, `description`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `type`) VALUES ('1478620101711241217', '资位', 'sys_class_position', '人员资位', 0, 'admin', '2022-01-05 14:51:18', 'admin', '2022-01-06 09:56:20', 0);
INSERT INTO `sys_dict`(`id`, `dict_name`, `dict_code`, `description`, `del_flag`, `create_by`, `create_time`, `update_by`, `update_time`, `type`) VALUES ('1478976158467895298', '技术/行政职位', 'sys_tech_position', '技术/行政职位', 0, 'admin', '2022-01-06 14:26:09', NULL, NULL, 0);
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478620416166600706', '1478620101711241217', '师1', 'cadre1', '师级干部一', 1, 1, 'admin', '2022-01-05 14:52:33', 'admin', '2022-01-06 15:50:58');
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478620477223084033', '1478620101711241217', '师2', 'cadre2', '', 2, 1, 'admin', '2022-01-05 14:52:48', 'admin', '2022-01-06 15:51:02');
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478620532147494914', '1478620101711241217', '师3', 'cadre3', '', 3, 1, 'admin', '2022-01-05 14:53:01', 'admin', '2022-01-06 15:51:06');
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478907160183287809', '1478620101711241217', '师4', 'cadre4', '', 4, 1, 'admin', '2022-01-06 09:51:58', 'admin', '2022-01-06 15:51:10');
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478907207427928066', '1478620101711241217', '师5', 'cadre5', '', 5, 1, 'admin', '2022-01-06 09:52:10', 'admin', '2022-01-06 15:51:15');
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478907264394964993', '1478620101711241217', '师6', 'cadre6', '', 6, 1, 'admin', '2022-01-06 09:52:23', 'admin', '2022-01-06 15:51:19');
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478907335022850049', '1478620101711241217', '师7', 'cadre7', '', 7, 1, 'admin', '2022-01-06 09:52:40', 'admin', '2022-01-06 15:51:24');
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478907379339866114', '1478620101711241217', '师8', 'cadre8', '', 8, 1, 'admin', '2022-01-06 09:52:51', 'admin', '2022-01-06 15:51:28');
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478907435203801090', '1478620101711241217', '师9', 'cadre9', '', 9, 1, 'admin', '2022-01-06 09:53:04', 'admin', '2022-01-06 15:51:32');
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478907482087731201', '1478620101711241217', '师10', 'cadre10', '', 10, 1, 'admin', '2022-01-06 09:53:15', 'admin', '2022-01-06 15:51:36');
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478907537779699714', '1478620101711241217', '师11', 'cadre11', '', 11, 1, 'admin', '2022-01-06 09:53:28', 'admin', '2022-01-06 15:51:43');
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478907583136903170', '1478620101711241217', '师12', 'cadre12', '', 12, 1, 'admin', '2022-01-06 09:53:39', 'admin', '2022-01-06 15:51:47');
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478907637969039362', '1478620101711241217', '师13', 'cadre13', '', 13, 1, 'admin', '2022-01-06 09:53:52', 'admin', '2022-01-06 15:51:52');
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478907682319609858', '1478620101711241217', '师14', 'cadre14', '', 14, 1, 'admin', '2022-01-06 09:54:03', 'admin', '2022-01-06 15:51:57');
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478907735113314305', '1478620101711241217', '师15', 'cadre15', '', 15, 1, 'admin', '2022-01-06 09:54:15', 'admin', '2022-01-06 15:52:01');
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478907775848394753', '1478620101711241217', '师16', 'cadre16', '', 16, 1, 'admin', '2022-01-06 09:54:25', 'admin', '2022-01-06 15:52:06');
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478907823109812226', '1478620101711241217', '师17', 'cadre17', '', 17, 1, 'admin', '2022-01-06 09:54:36', 'admin', '2022-01-06 15:52:10');
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478907863891030017', '1478620101711241217', '师18', 'cadre18', '', 18, 1, 'admin', '2022-01-06 09:54:46', 'admin', '2022-01-06 15:52:15');
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478907903359430658', '1478620101711241217', '师19', 'cadre19', '', 19, 1, 'admin', '2022-01-06 09:54:56', 'admin', '2022-01-06 15:52:19');
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478907948678885377', '1478620101711241217', '师20', 'cadre20', '', 20, 1, 'admin', '2022-01-06 09:55:06', 'admin', '2022-01-06 15:52:23');
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478995969939927042', '1478976158467895298', '1-1專員', '1-1專員', '', 1, 1, 'admin', '2022-01-06 15:44:52', NULL, NULL);
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478995998083706882', '1478976158467895298', '1-2工程師', '1-2工程師', '', 1, 1, 'admin', '2022-01-06 15:44:59', NULL, NULL);
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478996024197443586', '1478976158467895298', '1-2專員', '1-2專員', '', 1, 1, 'admin', '2022-01-06 15:45:05', NULL, NULL);
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478996053582737409', '1478976158467895298', '2-1研發工程師', '2-1研發工程師', '', 1, 1, 'admin', '2022-01-06 15:45:12', NULL, NULL);
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478996083643314178', '1478976158467895298', '2-1資深專員', '2-1資深專員', '', 1, 1, 'admin', '2022-01-06 15:45:19', NULL, NULL);
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478996114819575809', '1478976158467895298', '2-1資深銷售專員', '2-1資深銷售專員', '', 1, 1, 'admin', '2022-01-06 15:45:27', NULL, NULL);
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478996139247202306', '1478976158467895298', '2-2研發工程師', '2-2研發工程師', '', 1, 1, 'admin', '2022-01-06 15:45:33', NULL, NULL);
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478996162450092034', '1478976158467895298', '3-1高級專員', '3-1高級專員', '', 1, 1, 'admin', '2022-01-06 15:45:38', NULL, NULL);
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478996200173662210', '1478976158467895298', '3-1資深研發工程師', '3-1資深研發工程師', '', 1, 1, 'admin', '2022-01-06 15:45:47', NULL, NULL);
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478996228128698369', '1478976158467895298', '3-2高級專員', '3-2高級專員', '', 1, 1, 'admin', '2022-01-06 15:45:54', NULL, NULL);
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478996259539841025', '1478976158467895298', '3-2產品經理', '3-2產品經理', '', 1, 1, 'admin', '2022-01-06 15:46:01', NULL, NULL);
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478996289231319042', '1478976158467895298', '3-2資深研發工程師', '3-2資深研發工程師', '', 1, 1, 'admin', '2022-01-06 15:46:08', NULL, NULL);
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478996314896265218', '1478976158467895298', '4-1系統架構師', '4-1系統架構師', '', 1, 1, 'admin', '2022-01-06 15:46:15', NULL, NULL);
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478996344155729921', '1478976158467895298', '4-1產品副總監', '4-1產品副總監', '', 1, 1, 'admin', '2022-01-06 15:46:22', NULL, NULL);
INSERT INTO `sys_dict_item`(`id`, `dict_id`, `item_text`, `item_value`, `description`, `sort_order`, `status`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('1478996370198163458', '1478976158467895298', '5-2專案經理', '5-2專案經理', '', 1, 1, 'admin', '2022-01-06 15:46:28', NULL, NULL);

