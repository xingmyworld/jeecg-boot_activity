package org.jeecg.autoconfigure.autopoi;

import org.jeecg.autopoi.AutopoiConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import lombok.extern.slf4j.Slf4j;

@Configuration
@ConditionalOnClass(AutopoiConfiguration.class)
@Import(AutopoiConfiguration.class)
@Slf4j
public class AutopoiAutoConfiguration {
	public AutopoiAutoConfiguration() {
		log.info("[AutoConfiguration==>]:Autopoi Module Loading");
	}
}
