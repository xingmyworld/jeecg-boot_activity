package org.jeecg.autoconfigure.drools;

import org.jeecg.autoconfigure.system.SystemAutoConfiguration;
import org.jeecg.drools.DroolsConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import lombok.extern.slf4j.Slf4j;

@Configuration
@ConditionalOnClass(DroolsConfiguration.class)
@AutoConfigureBefore({SystemAutoConfiguration.class})
@Import(DroolsConfiguration.class)
@Slf4j
public class DroolsAutoConfiguration {

	public DroolsAutoConfiguration() {
		log.info("[AutoConfiguration==>]:Drools Module Loading");
	}
}
