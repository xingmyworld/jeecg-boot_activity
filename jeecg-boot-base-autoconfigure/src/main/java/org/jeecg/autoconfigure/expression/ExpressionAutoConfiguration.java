package org.jeecg.autoconfigure.expression;

import org.jeecg.expression.ExpressionConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import lombok.extern.slf4j.Slf4j;

@Configuration
@ConditionalOnClass(ExpressionConfiguration.class)
@Import(ExpressionConfiguration.class)
@Slf4j
public class ExpressionAutoConfiguration {

	public ExpressionAutoConfiguration() {
		log.info("[AutoConfiguration==>]:Expression Module Loading");
	}
}
