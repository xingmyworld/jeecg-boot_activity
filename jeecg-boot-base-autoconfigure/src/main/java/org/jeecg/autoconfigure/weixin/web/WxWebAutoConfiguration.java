package org.jeecg.autoconfigure.weixin.web;

import org.jeecg.autoconfigure.weixin.cp.WxCpAutoConfiguration;
import org.jeecg.weixin.web.WxWebConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import lombok.extern.slf4j.Slf4j;

@Configuration
@ConditionalOnClass(WxWebConfiguration.class)
@AutoConfigureAfter({WxCpAutoConfiguration.class})
@Import(WxWebConfiguration.class)
@Slf4j
public class WxWebAutoConfiguration {

	public WxWebAutoConfiguration() {
		log.info("[AutoConfiguration==>]:WxWeb Module Loading");
	}
}
