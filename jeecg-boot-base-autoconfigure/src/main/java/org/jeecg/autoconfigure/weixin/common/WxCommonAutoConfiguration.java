package org.jeecg.autoconfigure.weixin.common;

import org.jeecg.autoconfigure.system.SystemAutoConfiguration;
import org.jeecg.weixin.common.WxCommonConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import lombok.extern.slf4j.Slf4j;

@Configuration
@ConditionalOnClass(WxCommonConfiguration.class)
@AutoConfigureAfter({SystemAutoConfiguration.class})
@Import(WxCommonConfiguration.class)
@Slf4j
public class WxCommonAutoConfiguration {

	public WxCommonAutoConfiguration() {
		log.info("[AutoConfiguration==>]:WxCommon Module Loading");
	}
}
