package org.jeecg.autoconfigure.pms;

import org.jeecg.autoconfigure.system.SystemAutoConfiguration;
import org.jeecg.pms.PmsConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import lombok.extern.slf4j.Slf4j;

@Configuration
@ConditionalOnClass(PmsConfiguration.class)
@AutoConfigureAfter({SystemAutoConfiguration.class})
@Import(PmsConfiguration.class)
@Slf4j
public class PmsAutoConfiguration {

	public PmsAutoConfiguration() {
		log.info("[AutoConfiguration==>]:Pms Module Loading");
	}
}
