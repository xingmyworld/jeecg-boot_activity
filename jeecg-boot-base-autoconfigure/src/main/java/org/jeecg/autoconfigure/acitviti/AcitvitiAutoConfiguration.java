package org.jeecg.autoconfigure.acitviti;

import org.jeecg.activiti.AcitvitiConfiguration;
import org.jeecg.autoconfigure.system.SystemAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import lombok.extern.slf4j.Slf4j;

@Configuration
@ConditionalOnClass(AcitvitiConfiguration.class)
@AutoConfigureAfter({SystemAutoConfiguration.class})
@Import(AcitvitiConfiguration.class)
@Slf4j
public class AcitvitiAutoConfiguration {
	
	public AcitvitiAutoConfiguration() {
		log.info("[AutoConfiguration==>]:Acitviti Module Loading");
	}

}
