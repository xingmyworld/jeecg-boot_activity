package org.jeecg.autoconfigure.system;

import org.apache.shiro.spring.boot.autoconfigure.ShiroAnnotationProcessorAutoConfiguration;
import org.apache.shiro.spring.boot.autoconfigure.ShiroAutoConfiguration;
import org.jeecg.autoconfigure.common.CommonAutoConfiguration;
import org.jeecg.modules.SystemConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import lombok.extern.slf4j.Slf4j;

@Configuration
@ConditionalOnClass(SystemConfiguration.class)
@AutoConfigureBefore({ShiroAutoConfiguration.class,ShiroAnnotationProcessorAutoConfiguration.class})
@AutoConfigureAfter({CommonAutoConfiguration.class})
@Import(SystemConfiguration.class)
@Slf4j
public class SystemAutoConfiguration {

	public SystemAutoConfiguration() {
		log.info("[AutoConfiguration==>]:System Module Loading");
	}
}
