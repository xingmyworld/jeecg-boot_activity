package org.jeecg.pms.service.impl;

import org.jeecg.pms.entity.PmsTaskDeliver;
import org.jeecg.pms.mapper.PmsTaskDeliverMapper;
import org.jeecg.pms.service.IPmsTaskDeliverService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class PmsTaskDeliverService extends ServiceImpl<PmsTaskDeliverMapper, PmsTaskDeliver> implements IPmsTaskDeliverService {


}
