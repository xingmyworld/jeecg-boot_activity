package org.jeecg.pms.service;

import org.jeecg.common.system.base.service.JeecgService;
import org.jeecg.pms.entity.PmsProjectLog;

public interface IPmsProjectLogService extends JeecgService<PmsProjectLog> {

}
