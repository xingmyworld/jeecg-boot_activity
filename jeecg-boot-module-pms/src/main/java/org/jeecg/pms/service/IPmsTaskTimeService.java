package org.jeecg.pms.service;

import org.jeecg.common.system.base.service.JeecgService;
import org.jeecg.modules.online.config.exception.BusinessException;
import org.jeecg.pms.entity.PmsTaskTime;

import java.util.List;

public interface IPmsTaskTimeService extends JeecgService<PmsTaskTime> {

    /**
     * 完成工时的记录以及总工时的计算
     * @param pmsTaskTime
     */
    void addWorkHours(PmsTaskTime pmsTaskTime) throws BusinessException;

    /**
     *
     * @param pmsTaskTimeList
     * @throws BusinessException
     */
    void addWorkHoursBatch(List<PmsTaskTime> pmsTaskTimeList)throws BusinessException;
}
