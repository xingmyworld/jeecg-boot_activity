package org.jeecg.pms.service.impl;

import org.jeecg.pms.entity.PmsProjectLog;
import org.jeecg.pms.mapper.PmsProjectLogMapper;
import org.jeecg.pms.service.IPmsProjectLogService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class PmsProjectLogService extends ServiceImpl<PmsProjectLogMapper, PmsProjectLog> implements IPmsProjectLogService {


}
