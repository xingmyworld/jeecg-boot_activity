package org.jeecg.pms.service;

import org.jeecg.common.system.base.service.JeecgService;
import org.jeecg.pms.entity.PmsTaskDeliver;

public interface IPmsTaskDeliverService extends JeecgService<PmsTaskDeliver> {

}
