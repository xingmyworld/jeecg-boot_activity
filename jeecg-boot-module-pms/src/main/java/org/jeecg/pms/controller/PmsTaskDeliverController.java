package org.jeecg.pms.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.pms.entity.PmsTaskDeliver;
import org.jeecg.pms.service.IPmsTaskDeliverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
* 项目管理模块-任务成果
* @author: MxpIO
* @version: V1.0
*/
@Api(tags="项目管理模块-任务成果")
@RestController
@RequestMapping("/pms/taskdeliver")
@Slf4j
public class PmsTaskDeliverController extends JeecgController<PmsTaskDeliver, IPmsTaskDeliverService> {
	@Autowired
	private IPmsTaskDeliverService pmsTaskDeliverService;
	
	/**
	 * 分页列表查询
	 *
	 * @param pmsTaskDeliver
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "项目管理-任务成果分页查询")
	@ApiOperation(value="项目管理模块-任务成果-分页列表查询", notes="项目管理模块-任务成果-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(PmsTaskDeliver pmsTaskDeliver,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<PmsTaskDeliver> queryWrapper = QueryGenerator.initQueryWrapper(pmsTaskDeliver, req.getParameterMap());
		Page<PmsTaskDeliver> page = new Page<PmsTaskDeliver>(pageNo, pageSize);
		IPage<PmsTaskDeliver> pageList = pmsTaskDeliverService.page(page, queryWrapper);
		log.info("查询当前页：" + pageList.getCurrent());
        log.info("查询当前页数量：" + pageList.getSize());
        log.info("查询结果数量：" + pageList.getRecords().size());
        log.info("数据总数：" + pageList.getTotal());
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param pmsTaskDeliver
	 * @return
	 */
	@AutoLog(value = "项目管理-任务成果添加")
	@ApiOperation(value="项目管理模块-任务成果-添加", notes="项目管理模块-任务成果-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody PmsTaskDeliver pmsTaskDeliver) {
		pmsTaskDeliverService.save(pmsTaskDeliver);
		return Result.OK("添加成功！",pmsTaskDeliver);
	}
	
	/**
	 *  编辑
	 *
	 * @param pmsTaskDeliver
	 * @return
	 */
	@AutoLog(value = "项目管理-任务成果编辑")
	@ApiOperation(value="项目管理模块-任务成果-编辑", notes="项目管理模块-任务成果-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody PmsTaskDeliver pmsTaskDeliver) {
		pmsTaskDeliverService.updateById(pmsTaskDeliver);
		return Result.OK("编辑成功!",pmsTaskDeliver);
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "项目管理-通过id删除任务成果")
	@ApiOperation(value="项目管理-通过id删除任务成果", notes="项目管理-通过id删除任务成果")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		pmsTaskDeliverService.removeById(id);
		return Result.OK("删除成功!",null);
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "项目管理-批量删除任务成果")
	@ApiOperation(value="项目管理-批量删除任务成果", notes="项目管理-批量删除任务成果")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.pmsTaskDeliverService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!",null);
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "项目管理-通过id查询任务成果")
	@ApiOperation(value="项目管理-通过id查询任务成果", notes="项目管理-通过id查询任务成果")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		PmsTaskDeliver pmsTaskDeliver = pmsTaskDeliverService.getById(id);
		if(pmsTaskDeliver==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(pmsTaskDeliver);
	}

   /**
   * 导出excel
   *
   * @param request
   * @param pmsTaskDeliver
   */
   @RequestMapping(value = "/exportXls")
   public ModelAndView exportXls(HttpServletRequest request, PmsTaskDeliver pmsTaskDeliver) {
       return super.exportXls(request, pmsTaskDeliver, PmsTaskDeliver.class, "项目管理模块-任务成果");
   }

   /**
     * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
   @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
   public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
       return super.importExcel(request, response, PmsTaskDeliver.class);
   }
}
