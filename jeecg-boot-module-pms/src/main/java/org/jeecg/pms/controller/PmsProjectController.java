package org.jeecg.pms.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.pms.aspect.annotation.PmsLog;
import org.jeecg.pms.entity.PmsProject;
import org.jeecg.pms.service.IPmsProjectService;
import org.jeecg.pms.service.impl.PmsProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;


/**
 * 项目管理模块-项目管理
 *
 * @author MxpIO
 *
 */
@Slf4j
@Api(tags = "项目管理")
@RestController
@RequestMapping("/pms/project")
public class PmsProjectController extends JeecgController<PmsProject, PmsProjectService> {

	@Autowired
	private IPmsProjectService pmsProjectService;


	/**
     * 分页列表查询
     *
     * @param pmsProject
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @ApiOperation(value = "获取项目数据列表", notes = "获取项目数据列表")
    @GetMapping(value = "/list")
    public Result<?> list(PmsProject pmsProject, @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo, @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                          HttpServletRequest req) {
        QueryWrapper<PmsProject> queryWrapper = QueryGenerator.initQueryWrapper(pmsProject, req.getParameterMap());
        Page<PmsProject> page = new Page<PmsProject>(pageNo, pageSize);

        IPage<PmsProject> pageList = pmsProjectService.page(page, queryWrapper);
//        pmsProjectService.handleTransientVariable(pageList.getRecords());
        log.info("查询当前页：" + pageList.getCurrent());
        log.info("查询当前页数量：" + pageList.getSize());
        log.info("查询结果数量：" + pageList.getRecords().size());
        log.info("数据总数：" + pageList.getTotal());
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param pmsProject
     * @return
     */
    @PostMapping(value = "/add")
    @AutoLog(value = "添加项目数据")
    @ApiOperation(value = "添加项目数据", notes = "添加项目数据")
    public Result<?> add(@RequestBody PmsProject pmsProject) {
    	pmsProjectService.save(pmsProject);
        return Result.OK("添加成功！",pmsProject);
    }

    /**
     * 编辑
     *
     * @param pmsProject
     * @return
     */
    @PutMapping(value = "/edit")
    @ApiOperation(value = "编辑项目数据", notes = "编辑项目数据")
    @AutoLog(value = "编辑项目数据", operateType = CommonConstant.OPERATE_TYPE_EDIT)
    @PmsLog
    public Result<?> edit(@RequestBody PmsProject pmsProject) {
    	pmsProjectService.updateById(pmsProject);
        return Result.OK("更新成功！",pmsProject);
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "删除项目数据")
    @DeleteMapping(value = "/delete")
    @ApiOperation(value = "通过ID删除项目数据", notes = "通过ID删除项目数据")
    public Result<?> delete(@ApiParam(name = "id", value = "项目id", required = true) @RequestParam(name = "id", required = true) String id) {
    	pmsProjectService.removeById(id);
        return Result.OK("删除成功!",null);
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @DeleteMapping(value = "/deleteBatch")
    @ApiOperation(value = "批量删除项目数据", notes = "批量删除项目数据")
    public Result<?> deleteBatch(@ApiParam(name = "ids", value = "项目id数组", required = true) @RequestParam(name = "ids", required = true) String ids) {
        pmsProjectService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功！",null);
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/queryById")
    @ApiOperation(value = "通过ID查询项目数据", notes = "通过ID查询项目数据")
    public Result<?> queryById(@ApiParam(name = "id", value = "项目id", required = true) @RequestParam(name = "id", required = true) String id) {
    	PmsProject pmsProject = pmsProjectService.getById(id);
        return Result.OK(pmsProject);
    }

    /**
     * 导出excel
     *
     * @param request
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, PmsProject pmsProject) {
        return super.exportXls(request, pmsProject, PmsProject.class, "项目数据");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, PmsProject.class);
    }

}
