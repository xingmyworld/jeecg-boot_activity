package org.jeecg.pms.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.pms.entity.PmsTaskDeliver;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface PmsTaskDeliverMapper extends BaseMapper<PmsTaskDeliver> {

}
