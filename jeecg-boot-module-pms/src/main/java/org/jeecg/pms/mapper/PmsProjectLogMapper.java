package org.jeecg.pms.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.pms.entity.PmsProjectLog;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface PmsProjectLogMapper extends BaseMapper<PmsProjectLog> {

}
