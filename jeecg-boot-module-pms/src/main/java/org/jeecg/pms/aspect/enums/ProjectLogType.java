package org.jeecg.pms.aspect.enums;

public enum ProjectLogType {
	
	/**
     * PROJECT
     */
    PROJECT,

    /**
     * TASK
     */
    TASK;
}
