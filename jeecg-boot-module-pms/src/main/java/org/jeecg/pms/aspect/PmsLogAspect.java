package org.jeecg.pms.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.jeecg.common.system.base.service.BaseCommonService;
import org.jeecg.pms.aspect.annotation.PmsLog;
import org.jeecg.pms.aspect.enums.CompareStatus;
import org.jeecg.pms.aspect.enums.ProjectLogType;
import org.jeecg.pms.entity.PmsProject;
import org.jeecg.pms.entity.PmsProjectLog;
import org.jeecg.pms.entity.PmsTask;
import org.jeecg.pms.service.IPmsProjectLogService;
import org.jeecg.pms.service.IPmsProjectService;
import org.jeecg.pms.service.IPmsTaskService;
import org.jeecg.pms.utils.CompareObejct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


@Aspect
@Component
public class PmsLogAspect {
	
	@Autowired
	private IPmsProjectService pmsProjectService;
	
	@Autowired
	private IPmsProjectLogService pmsProjectLogService;
	
	@Autowired
	private IPmsTaskService pmsTaskService;

    @Resource
    private BaseCommonService baseCommonService;

    @Pointcut("@annotation(org.jeecg.pms.aspect.annotation.PmsLog)")
    public void logPointCut() {

    }

    @Around("logPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        //保存日志
        saveProjectLog(point);
        //执行方法
        Object result = point.proceed();
        return result;
    }

    private void saveProjectLog(ProceedingJoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();

        PmsLog pmsLog = method.getAnnotation(PmsLog.class);
        ProjectLogType projectLogType = ProjectLogType.PROJECT;
        if(pmsLog != null){
        	projectLogType = pmsLog.projectLogType();
        }
        //获取参数
        Object[] args = joinPoint.getArgs();
        if(args !=null){
        	Object o = args[0];
        	if(projectLogType == ProjectLogType.PROJECT){
        		PmsProject entity = (PmsProject) o;
        		PmsProject oldEntity = pmsProjectService.getById(entity.getId());
        		
        		CompareObejct<PmsProject> compare = new CompareObejct<>();
        		compare.setCurrent(entity);
        		compare.setOriginal(oldEntity);
        		List<String> ignoreFields = Arrays.asList("id","createBy","createTime","updateBy","updateTime","taskDesc","remarks");
        		Map<String, Map<String, Object>> result = compare.contrastObj(PmsProject.class, ignoreFields);
        		if(compare.getStatus() == CompareStatus.CHANGE){
        			List<PmsProjectLog> logs = new ArrayList<>();
        			for(Entry<String, Map<String, Object>> entry : result.entrySet()){
        				PmsProjectLog log = new PmsProjectLog();
        				log.setProjectCode(entity.getProjectCode());
                		log.setProjectName(entity.getProjectName());
                		log.setFieldName(entry.getKey());
                		log.setOldValue(entry.getValue().get("oldValue") == null ? "" : entry.getValue().get("oldValue").toString());
                		log.setNewValue(entry.getValue().get("newValue") == null ? "" : entry.getValue().get("newValue").toString());
                		logs.add(log);
        			}
        			pmsProjectLogService.saveBatch(logs);
        		}
        	}else if(projectLogType == ProjectLogType.TASK){
        		PmsTask entity = (PmsTask) o;
        		PmsTask oldEntity = pmsTaskService.getById(entity.getId());
        		
        		CompareObejct<PmsTask> compare = new CompareObejct<>();
        		compare.setCurrent(entity);
        		compare.setOriginal(oldEntity);
        		List<String> ignoreFields = Arrays.asList("id","createBy","createTime","updateBy","updateTime","taskDesc","remarks");
        		Map<String, Map<String, Object>> result = compare.contrastObj(PmsTask.class, ignoreFields);
        		if(compare.getStatus() == CompareStatus.CHANGE){
        			List<PmsProjectLog> logs = new ArrayList<>();
        			for(Entry<String, Map<String, Object>> entry : result.entrySet()){
        				PmsProjectLog log = new PmsProjectLog();
        				log.setProjectCode(entity.getProjectCode());
        				log.setTaskCode(entity.getTaskCode());
        				log.setTaskName(entity.getTaskName());
                		log.setFieldName(entry.getKey());
                		log.setOldValue(entry.getValue().get("oldValue") == null ? "" : entry.getValue().get("oldValue").toString());
                		log.setNewValue(entry.getValue().get("newValue") == null ? "" : entry.getValue().get("newValue").toString());
                		logs.add(log);
        			}
        			pmsProjectLogService.saveBatch(logs);
        		}
        	}
        }
    }
}
