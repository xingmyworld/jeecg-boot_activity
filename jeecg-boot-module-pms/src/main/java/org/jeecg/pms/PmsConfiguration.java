package org.jeecg.pms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@AutoConfigurationPackage
@MapperScan({"org.jeecg.pms.mapper"})
public class PmsConfiguration {
	
}