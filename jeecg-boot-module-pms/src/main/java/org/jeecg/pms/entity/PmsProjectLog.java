package org.jeecg.pms.entity;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("pms_project_log")
public class PmsProjectLog extends JeecgEntity {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "项目编号")
	@Excel(name="项目编号",width=15)
	private String projectCode;
	
	@ApiModelProperty(value = "项目名称")
	@Excel(name="项目名称",width=15)
	private String projectName;
	
	@ApiModelProperty(value = "任务编号")
	@Excel(name="任务编号",width=15)
	private String taskCode;
	
	@ApiModelProperty(value = "任务名称")
	@Excel(name="任务名称",width=15)
	private String taskName;
	
	@ApiModelProperty(value = "字段名称")
	private String fieldName;
	
	@ApiModelProperty(value = "修改前")
	private String oldValue;
	
	@ApiModelProperty(value = "修改后")
	private String newValue;

}
