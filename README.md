Jeecg-Boot-PLUS 快速开发平台
===============

	> 如有功能需求可在issus中提交，或加入QQ群：905442757（入群请备注）

JEECG-BOOT 基础版本号： 2.2.1（发布日期：20200721）
基础代码同步至20200713

注：请勿在生产系统更新项目代码。当前项目为开发版本，更新不考虑兼容性。新功能对应的SQL更新会直接在db/mysql8.0-xxxxxxxx.sql文件中修改和追加（非增量更新，不向前兼容，同步更新oracle/MSSQL/pg等文件,暂无精力验证可用性）。

## 分支特性
#### 结构变更
	> 分支模块重新分隔，业务代码与框架代码及示例分开。业务系统只需依赖对应模块即可使用平台功能，降低多系统时代码管理的复杂度。
#### 功能变更
- 【BUG修复】富文本光标异常，更换富文本控件（已完成）
- 【仿商业版功能】在线开发-Online组合报表（已完成）
- 【仿商业版功能】在线开发-Online图表功能（已完成）
- 【开发计划】在线开发-Online图表功能-表达式支持（已完成）
	> 基于Aviator引擎实现表达式支持，可按照Aviator语法编写表达式，当前行的字段名可直接作为变量。如需扩展自定义函数可继承AbstractSpringAviatorFunction(固定数量参数)、AbstractSpringAviatorVariadicFunction（可变参数）抽象类并注册到IOC中即可
- 【开发计划】在线开发-Online图表功能-Y轴字段聚合支持（已完成）
	> 可根据X轴进行汇总聚合计算，聚合计算实现方式是对SQL结果遍历累加，此功能主要解决一部分SQL在图表的复用，涉及大量数据计算不建议使用。后期会考虑重做Online报表功能（JEECG未开源），使用报表作为图表数据源解决SQL复用问题。
- 【开发计划】基于activiti7工作流（已完成）
- 【开发计划】自定义表单（已完成）
- 【开发计划】Drools规则引擎（未启动）
- 【开发计划】基于Kettle数据集成支持（未启动）
- 【开发计划】后台配置项目信息（已完成）
	> 通过配置jeecg/jeecg.properties实现站点信息的加载（约定jeecg.properties中的配置项会自动装载，每个模块都可定义jeecg.properties文件，根据依赖关系同属性值覆盖。例：webapp模块中相同配置会覆盖其他模块的配置。）

```
jeecg.site.appName=JEECG_BOOT快速开发平台
jeecg.site.appAbbr=JEECG_BOOT
jeecg.site.appDesc=JEECG_BOOT专注于企业级快速开发平台
jeecg.site.appLogo=
jeecg.site.allowRegister=false
jeecg.site.allowPhoneLogin=false
jeecg.site.allowThirdLogin=false
jeecg.site.helpPath=http://doc.jeecg.com
```

- 【开发计划】站内邮件
- 【开发计划】考勤管理
- 【开发计划】项目管理（开发中）


## 启动方式

```

cd /var/lib/jenkins/workspace/jeecg_boot
mvn install
cd /var/lib/jenkins/workspace/jeecg_boot/jeecg-boot-business-webapp
mvn clean package spring-boot:repackage
cd target/
java -jar jeecg-boot-business-webapp-2.2.1.jar

```

## 配套前端项目地址
https://gitee.com/i_mxpio/ant-design-vue-jeecg

*** 
以下是Jeecg-Boot信息
***

## 后端技术架构
- 基础框架：Spring Boot 2.1.3.RELEASE

- 持久层框架：Mybatis-plus_3.3.2

- 安全框架：Apache Shiro 1.4.0，Jwt_3.7.0

- 数据库连接池：阿里巴巴Druid 1.1.17

- 缓存框架：redis

- 日志打印：logback

- 其他：fastjson，poi，Swagger-ui，quartz, lombok（简化代码）等。



## 开发环境

- 语言：Java 8

- IDE(JAVA)： Eclipse安装lombok插件 或者 IDEA

- 依赖管理：Maven

- 数据库：MySQL5.0  &  Oracle 11g

- 缓存：Redis


## 技术文档


- 在线演示 ：  [http://boot.jeecg.com](http://boot.jeecg.com)

- 在线文档：  [http://doc.jeecg.com/1273927](http://doc.jeecg.com/1273927)

- 常见问题：  [入门常见问题大全](http://bbs.jeecg.com/forum.php?mod=viewthread&tid=7816&extra=page%3D1)

- QQ交流群 ：  ①284271917、②769925425


## 专项文档

#### 一、查询过滤器用法

```
QueryWrapper<?> queryWrapper = QueryGenerator.initQueryWrapper(?, req.getParameterMap());
```

代码示例：

```

	@GetMapping(value = "/list")
	public Result<IPage<JeecgDemo>> list(JeecgDemo jeecgDemo, @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo, 
	                                     @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			HttpServletRequest req) {
		Result<IPage<JeecgDemo>> result = new Result<IPage<JeecgDemo>>();
		
		//调用QueryGenerator的initQueryWrapper
		QueryWrapper<JeecgDemo> queryWrapper = QueryGenerator.initQueryWrapper(jeecgDemo, req.getParameterMap());
		
		Page<JeecgDemo> page = new Page<JeecgDemo>(pageNo, pageSize);
		IPage<JeecgDemo> pageList = jeecgDemoService.page(page, queryWrapper);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}

```



- 查询规则 (本规则不适用于高级查询,高级查询有自己对应的查询类型可以选择 )

| 查询模式           | 用法    | 说明                         |
|---------- |-------------------------------------------------------|------------------|
| 模糊查询     | 支持左右模糊和全模糊  需要在查询输入框内前或后带\*或是前后全部带\* |    |
| 取非查询     | 在查询输入框前面输入! 则查询该字段不等于输入值的数据(数值类型不支持此种查询,可以将数值字段定义为字符串类型的) |    |
| \>  \>= < <=     | 同取非查询 在输入框前面输入对应特殊字符即表示走对应规则查询 |    |
| in查询     | 若传入的数据带,(逗号) 则表示该查询为in查询 |    |
| 多选字段模糊查询     | 上述4 有一个特例，若某一查询字段前后都带逗号 则会将其视为走这种查询方式 ,该查询方式是将查询条件以逗号分割再遍历数组 将每个元素作like查询 用or拼接,例如 现在name传入值 ,a,b,c, 那么结果sql就是 name like '%a%' or name like '%b%' or name like '%c%' |    |


#### 二、AutoPoi(EXCEL工具类-EasyPOI衍变升级重构版本）
 
  [在线文档](https://github.com/zhangdaiscott/autopoi)
  


#### 三、代码生成器

> 功能说明：   一键生成的代码（包括：controller、service、dao、mapper、entity、vue）
 
 - 模板位置： src/main/resources/jeecg/code-template
 - 技术文档： http://doc.jeecg.com



#### 四、编码排重使用示例

重复校验效果：
![输入图片说明](https://static.oschina.net/uploads/img/201904/19191836_eGkQ.png "在这里输入图片标题")

1.引入排重接口,代码如下:  
 
```
import { duplicateCheck } from '@/api/api'
  ```
2.找到编码必填校验规则的前端代码,代码如下:  
  
```
<a-input placeholder="请输入编码" v-decorator="['code', validatorRules.code ]"/>

code: {
            rules: [
              { required: true, message: '请输入编码!' },
              {validator: this.validateCode}
            ]
          },
  ```
3.找到rules里validator对应的方法在哪里,然后使用第一步中引入的排重校验接口.  
  以用户online表单编码为示例,其中四个必传的参数有:  
    
```
  {tableName:表名,fieldName:字段名,fieldVal:字段值,dataId:表的主键},
  ```
 具体使用代码如下:  
 
```
    validateCode(rule, value, callback){
        let pattern = /^[a-z|A-Z][a-z|A-Z|\d|_|-]{0,}$/;
        if(!pattern.test(value)){
          callback('编码必须以字母开头，可包含数字、下划线、横杠');
        } else {
          var params = {
            tableName: "onl_cgreport_head",
            fieldName: "code",
            fieldVal: value,
            dataId: this.model.id
          };
          duplicateCheck(params).then((res)=>{
            if(res.success){
             callback();
            }else{
              callback(res.message);
            }
          })
        }
      },
```

## docker镜像用法
 ``` 
注意： 如果本地安装了mysql和redis,启动容器前先停掉本地服务，不然会端口冲突。
       net stop redis
       net stop mysql
 
# 1.修改项目配置文件 application.yml
    active: docker

# 2.先进JAVA项目根路径 maven打包
    mvn clean package
 

# 3.构建镜像__容器组（当你改变本地代码，也可重新构建镜像）
    docker-compose build

# 4.配置host

    # jeecgboot
    127.0.0.1   jeecg-boot-redis
    127.0.0.1   jeecg-boot-mysql
    127.0.0.1   jeecg-boot-system

# 5.启动镜像__容器组（也可取代运行中的镜像）
    docker-compose up -d

# 6.访问后台项目（注意要开启swagger）
    http://localhost:8080/jeecg-boot/doc.html
``` 