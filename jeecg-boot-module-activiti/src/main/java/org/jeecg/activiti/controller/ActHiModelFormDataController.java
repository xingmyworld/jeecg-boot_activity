package org.jeecg.activiti.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.jeecg.activiti.entity.ActHiModelFormData;
import org.jeecg.activiti.service.IActHiModelFormDataService;
import org.jeecg.common.api.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * TODO 流程表单历史数据
 *
 * @author dousw
 * @version 1.0
 */
@RestController
@RequestMapping("/activiti/hiFormData")
@Api(tags = "流程表单历史数据管理")
public class ActHiModelFormDataController {

    @Autowired
    private IActHiModelFormDataService iActHiModelFormDataService;

    @GetMapping("/getFormDataByProcessInstanceId")
    @ApiOperation(value = "根据流程实例ID查询流程历史表单数据", notes = "根据流程实例ID查询流程历史表单数据")
    public Result<Object> getFormDataByProcessInstanceId(@ApiParam(name="processInstanceId", value="流程实例ID", required = true) @RequestParam(name="processInstanceId",required=true) String processInstanceId,
                                                         String taskId) {
        ActHiModelFormData actReModelFormData = iActHiModelFormDataService.getOne(new QueryWrapper<ActHiModelFormData>().eq("process_instance_id", processInstanceId));
        if(actReModelFormData == null) {
            return Result.error("未找到数据 [ processInstanceId:" + processInstanceId + " ]");
        }
        if (!org.springframework.util.StringUtils.isEmpty(taskId)) {
            List<Map<String, Object>> appendFormList = iActHiModelFormDataService.handleNodeAppendForm(taskId, processInstanceId);
            actReModelFormData.setAppendFormList(appendFormList);
        }
        return Result.OK(actReModelFormData);
    }
}
