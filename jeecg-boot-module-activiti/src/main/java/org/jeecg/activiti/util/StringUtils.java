package org.jeecg.activiti.util;

import java.util.Date;

/**
 * TODO
 * 字符串工具类
 * @author dousw
 * @version 1.0
 */

public class StringUtils {

    public static String getTimeDiff(Date startTime, Date endTime) {

        long between = endTime.getTime() - startTime.getTime();
        long day = between / (24 * 60 * 60 * 1000);
        long hour = (between / (60 * 60 * 1000) - day * 24);
        long min = ((between / (60 * 1000)) - day * 24 * 60 - hour * 60);
        long s = (between / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        return day + "天" + hour + "小时" + min + "分" + s + "秒";
    }

    public static String objToString(Object object) {
        return object != null ? object.toString() : "";
    }
}
