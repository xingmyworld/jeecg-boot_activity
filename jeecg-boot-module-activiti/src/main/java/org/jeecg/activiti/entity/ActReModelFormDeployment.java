package org.jeecg.activiti.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("act_re_model_form_depl")
public class ActReModelFormDeployment extends ActReModelForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2259904422483853173L;

	/** 流程定义ID */
	private String processDefinitionId;
	
	/** 流程部署ID */
	private String deploymentId;
	
	/** 流程版本号 */
	private Integer version;

}
