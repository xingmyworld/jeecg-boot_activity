package org.jeecg.activiti.entity;

import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * TODO
 *
 * @author dousw
 * @version 1.0
 */
@Data
public class ModelExtend implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;

    private String name;

    private String key;

    private String category;

    private Date createTime;

    private Date lastUpdateTime;

    private Integer version;

    private String metaInfo;

    private String deploymentId;

    private String tenantId;

    private String businessPlate;

    private String icon;

    private Boolean isShow;
    
    /**
     * 表单类型（0、自定义 1、Onl表单）
     */
    private String formType;
    
    /**
     * tableName
     */
    private String tableName;
    
    /**
     * 流程状态字段
     */
    private String flowStatusCol;

	private String description;
	
	/** 流程定义ID */
	private String processDefinitionId;
	
	/**
	 * 流程发布时间
	 */
	private Date publishTime;
	
	/**
     * 报表模板编码
     */
    private String reportCode;
    
    
    private String title;
    
    private String profiles;

}
