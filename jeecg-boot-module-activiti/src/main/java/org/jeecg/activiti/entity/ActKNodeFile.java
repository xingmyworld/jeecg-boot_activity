package org.jeecg.activiti.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import org.jeecg.common.system.base.entity.JeecgEntity;

/**
 * 流程节点管理表
 *
 * @author dousw
 * @version 1.0
 */

@Data
@EqualsAndHashCode(callSuper = false)
@TableName("act_k_node_file")
public class ActKNodeFile extends JeecgEntity implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    /**
     * 节点ID
     */
    private String nodeId;

    /**
     * 文件路径或文件ID
     */
    private String filePath;

    /**
     * 流程模型ID
     */
    private String modelId;

    /**
     * 流程实例ID
     */
    private String processInstanceId;

    /**
     * 任务ID
     */
    private String taskId;

    /**
     * 执行ID
     */
    private String executionId;
}
