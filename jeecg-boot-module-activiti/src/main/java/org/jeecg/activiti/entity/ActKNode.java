package org.jeecg.activiti.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 流程节点管理表
 *
 * @author dousw
 * @version 1.0
 */

@Data
@TableName("act_k_node")
public class ActKNode implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
    /**
     * 流程节点ID
     */
    private String nodeId;
    /**
     * 流程节点名称
     */
    private String nodeName;

    /**
     * 附加表单ID
     */
    private String appendFormId;
    /**
     * 流程模型ID
     */
    private String modelId;
    /**
     * 流程定义ID
     */
    private String processDefinitionId;
    /**
     * 优先级
     */
    private String priority;
    /**
     * 进口
     */
    private String incoming;
    /**
     * 出口
     */
    private String outgoing;
    /**
     * 审批用户（多个）
     */
    private String candidateUsers;
    /**
     * 审批用户组
     */
    private String candidateGroup;
    /**
     * 审批人
     */
    private String assignee;

    /**
     * 节点类型（0:工作流、1:业务流）
     */
    private int nodeType;

    /**
     * 是否显示附加表单
     */
    @TableField(exist = false)
    private Boolean isShowAppendForm;

    /**
     * 节点脚本
     */
    @TableField(exist = false)
    private String nodeScript;

}
