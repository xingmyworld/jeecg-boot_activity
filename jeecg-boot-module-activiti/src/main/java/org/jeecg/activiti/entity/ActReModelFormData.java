package org.jeecg.activiti.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.annotation.TableField;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 流程模型扩展表
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("act_re_model_form_data")
public class ActReModelFormData  extends JeecgEntity implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 流程模型ID
     */
    private String modelId;

    /**
     * 表单ID
     */
    private String formId;

    /**
     * 表单模型
     */
    private String formData;
    
    /**
     * 数据ID
     */
    private String dataId;
    
    /**
     * Onl表单ID
     */
    private String tableId;
    
    /**
     * Onl表单名称
     */
    private String tableName;

    /**
     * 流程状态（0：未提交、1、处理中、2、已完成、3、已退回、4、未通过、5、已撤销、6、已作废、7、已挂起）
     */
    private Integer status;

    /**
     * 流程实例ID
     */
    private String processInstanceId;

	/** 流程定义ID */
	private String processDefinitionId;

    /**
     * 提交类型（1、流程提交 2、工单提交 ）
     */
	private int type;

	@TableField(exist = false)
	private String modelKey;

    @TableField(exist = false)
	private String modelName;

    @TableField(exist = false)
    private List<Map<String, Object>> appendFormList;
}
