package org.jeecg.activiti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.activiti.entity.ActKNodeFile;

/**
 * TODO
 *
 * @author dousw
 * @version 1.0
 */

public interface ActKNodeFileMapper extends BaseMapper<ActKNodeFile> {
}
