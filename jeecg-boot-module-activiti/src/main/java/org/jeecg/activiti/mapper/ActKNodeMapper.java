package org.jeecg.activiti.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.activiti.entity.ActKNode;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * TODO
 * 流程节点Mapper
 * @author dousw
 * @version 1.0
 */
@Mapper
public interface ActKNodeMapper extends BaseMapper<ActKNode> {
}
