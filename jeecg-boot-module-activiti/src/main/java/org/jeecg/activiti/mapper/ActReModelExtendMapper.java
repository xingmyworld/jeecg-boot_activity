package org.jeecg.activiti.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.activiti.entity.ActReModelExtend;
import org.jeecg.activiti.entity.ModelExtend;

@Mapper
public interface ActReModelExtendMapper extends BaseMapper<ActReModelExtend> {

    IPage<ModelExtend> selectListByPage(@Param("page") Page<ActReModelExtend> page, @Param("model") ModelExtend modelE);
}
