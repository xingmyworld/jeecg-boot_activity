package org.jeecg.activiti.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.activiti.entity.ActKAppendForm;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * TODO 流程节点附加表单Mapper
 *
 * @author dousw
 * @version 1.0
 */
@Mapper
public interface ActKAppendFormMapper extends BaseMapper<ActKAppendForm> {
}
