package org.jeecg.activiti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.activiti.entity.ActKNodeFile;
import org.jeecg.activiti.mapper.ActKNodeFileMapper;
import org.jeecg.activiti.service.IActKNodeFileService;
import org.springframework.stereotype.Service;

/**
 * TODO
 *
 * @author dousw
 * @version 1.0
 */
@Service
public class ActKNodeFileServiceImpl extends ServiceImpl<ActKNodeFileMapper, ActKNodeFile> implements IActKNodeFileService {

}
