package org.jeecg.activiti.service.impl;

import org.jeecg.activiti.entity.ActKAppendFormDeployment;
import org.jeecg.activiti.mapper.ActKAppendFormDeploymentMapper;
import org.jeecg.activiti.service.IActKAppendFormDeploymentService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class ActKAppendFormDeploymentServiceImpl extends ServiceImpl<ActKAppendFormDeploymentMapper, ActKAppendFormDeployment> implements IActKAppendFormDeploymentService {

}
