package org.jeecg.activiti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.activiti.entity.ActKHandleResult;

/**
 * 任务节点处理结果service
 */
public interface IActKHandleResultService extends IService<ActKHandleResult> {
}
