package org.jeecg.activiti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.activiti.entity.ActKHandleResult;
import org.jeecg.activiti.mapper.ActKHandleResultMapper;
import org.jeecg.activiti.service.IActKHandleResultService;
import org.springframework.stereotype.Service;

/**
 * TODO 任务节点处理结果service实现类
 *
 * @author dousw
 * @version 1.0
 */

@Service
public class ActKHandleResultServiceImpl extends ServiceImpl<ActKHandleResultMapper,ActKHandleResult> implements IActKHandleResultService {
}
