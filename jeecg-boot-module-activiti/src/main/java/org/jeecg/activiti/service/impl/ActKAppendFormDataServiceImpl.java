package org.jeecg.activiti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.activiti.entity.ActKAppendFormData;
import org.jeecg.activiti.mapper.ActKAppendFormDataMapper;
import org.jeecg.activiti.service.IActKAppendFormDataService;
import org.springframework.stereotype.Service;

/**
 * TODO 流程节点附加表单数据service实现类
 *
 * @author dousw
 * @version 1.0
 */

@Service
public class ActKAppendFormDataServiceImpl extends ServiceImpl<ActKAppendFormDataMapper, ActKAppendFormData> implements IActKAppendFormDataService {
}
