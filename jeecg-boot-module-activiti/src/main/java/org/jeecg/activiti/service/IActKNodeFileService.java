package org.jeecg.activiti.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.activiti.entity.ActKNodeFile;

public interface IActKNodeFileService extends IService<ActKNodeFile> {
}
