package org.jeecg.activiti.service;

import org.jeecg.activiti.entity.ActKNodeDesign;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.online.config.exception.ActScriptException;

import java.util.Map;

public interface IActKNodeDesignService extends IService<ActKNodeDesign> {

    /**
     * 根据表达式任务后置处理表单数据
     * @param modelId
     * @param taskId
     */
    void postProcessFormdataByScript(String modelId, String taskId, Map<String,Object> variables) throws ActScriptException;

}
