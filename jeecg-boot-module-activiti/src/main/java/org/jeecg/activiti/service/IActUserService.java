package org.jeecg.activiti.service;

public interface IActUserService {
	
	/**
	 * 根据用户获取所在部门及上级部门特定职级人员
	 * @param username
	 * @param positionRank
	 * @return
	 */
	public String getDepartLeaders(String username, String positionRank);
	
	/**
	 * 根据用户获取所在部门及上级部门特定职级人员
	 * @param deptCode
	 * @param positionRank
	 * @return
	 */
	public String getDepartLeadersByDept(String deptCode, String positionRank);
	
	/**
	 * 检查用户职级是否为特定职级
	 * @param username
	 * @param positionRank
	 * @return
	 */
	public boolean checkPositionRank(String username, String positionRank);
	
	/**
	 * 检查用户是否属于特定部门
	 * @param username
	 * @param deptCode
	 * @return
	 */
	public boolean checkDept(String username, String deptCode);
	
	public void sendMessage(String fromUser, String toUser, String title, String content);
}
