package org.jeecg.activiti.service;

import org.activiti.engine.runtime.ProcessInstance;
import org.jeecg.activiti.entity.ActReModelFormData;
import org.jeecg.activiti.entity.ProcessNodeVo;
import org.jeecg.common.system.vo.LoginUser;

import java.util.Map;

/**
 * 流程实例service
 */
public interface IActivitiInstanceService {

    /**
     * 查询首届点
     * @param processDefinitionId
     * @return
     */
    ProcessNodeVo getFirstNode(String processDefinitionId);

    /**
     * 根据流程实例Id,获取实时流程图片
     * @param processInstanceId
     */
    String getFlowImgByInstanceId(String processInstanceId);

    /**
     * 根据流程实例ID，获取实时流程xml
     * @param processInstanceId
     * @return
     */
    String getFlowXmlByInstanceId(String processInstanceId);

    /**
     * 处理发起流程抄送
     * @param actReModelFormData
     */
    void handleProcessDuplicate(ActReModelFormData actReModelFormData, Map<String, Object> variables, String procInstId, LoginUser sysUser);

    /**
     * 发送待办消息
     * @param pi
     * @param sysUser
     */
    void handleBacklogMsg(ProcessInstance pi, LoginUser sysUser);
}
