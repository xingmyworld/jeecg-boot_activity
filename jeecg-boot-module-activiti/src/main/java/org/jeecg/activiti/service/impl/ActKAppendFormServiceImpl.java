package org.jeecg.activiti.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.activiti.entity.ActKAppendForm;
import org.jeecg.activiti.mapper.ActKAppendFormMapper;
import org.jeecg.activiti.service.IActKAppendFormService;
import org.springframework.stereotype.Service;

/**
 * TODO 流程节点附加表单service实现类
 *
 * @author dousw
 * @version 1.0
 */
@Service
public class ActKAppendFormServiceImpl extends ServiceImpl<ActKAppendFormMapper, ActKAppendForm> implements IActKAppendFormService {
}
