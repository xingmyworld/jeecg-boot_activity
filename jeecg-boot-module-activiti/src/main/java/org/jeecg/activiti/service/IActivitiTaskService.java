package org.jeecg.activiti.service;

import org.activiti.bpmn.model.FlowElement;
import org.activiti.engine.task.Task;
import org.jeecg.activiti.entity.ActReModelFormData;
import org.jeecg.activiti.entity.ActivitiTaskVo;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.vo.LoginUser;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;
import java.util.Map;

public interface IActivitiTaskService {

    /**
     * 根据当前任务或许下一个任务的信息
     * @param task
     * @return
     * @throws Exception
     */
    FlowElement getNextUserFlowElement(Task task) throws Exception;

    /**
     * 获取组任务
     * @return
     */
    List<Map<String,Object>> getGroupTaskList(String modelKey, String modelName);

    /**
     * 处理抄送
     * @param usernames
     */
    void handelDuplicate(String usernames,String procInstId,String modelId,Task task, LoginUser sysUser);

    /**
     * 查询我的任务列表
     * @param pageNo
     * @param pageSize
     * @return
     */
    Map<String, Object> getMyTaskList(String modelName, String modelKey, String creator, Integer pageNo, Integer pageSize);

    /**
     * 保存附加单据数据
     */
    void saveActKAppendFormData(String taskId, String username, String formData, String appendFormId,
                                String procInstId, String modelId, String nodeId, String appendFormDataId, String executionId);

    /**
     * 处理意见
     * @param taskId
     * @param procInstId
     * @param flag
     * @param comment
     * @param taskDefinitionKey
     * @param username
     */
    void handleResult(String taskId, String procInstId, int flag, String comment, String taskDefinitionKey, String username, String executionId);

    /**
     * 处理流程状态
     * @param formDataId
     * @param flag
     * @return 
     */
    boolean handleProcessStatus(String procInstId, String formDataId,int flag);

    /**
     * 驳回到指定节点
     * @param tableName 
     */
    Result<Object> rejectTargetNode(String taskId, String nodeId, String record, Map<String, Object> variables, Map<String, Object> transientVariables, LoginUser sysUser, String tableName);

    /**
     * 处理作废
     * @param procInstId
     */
    void handleCancellation(String procInstId);

    /**
     * 完成流程任务
     * @param taskId
     * @param procInstId
     * @param comment
     * @param variables
     * @param transientVariables
     */
    void handleCompleteTask(String taskId, String procInstId, String comment, Map<String, Object> variables, Map<String, Object> transientVariables,Task task, ActReModelFormData actReModelFormData );

    /**
     * 保存上传的附件
     * @param myTask
     * @param procInstId
     * @param files
     */
    void saveFiles(Task myTask, String procInstId, String files, String username);

    /**
     * 撤销流程
     * @param procInstId
     */
    void revocation(String procInstId);

    /**
     * 退回到上一步
     * @param taskId
     */
    void backProcess(String taskId);

    /**
     * 查询我发起的流程
     * @return
     */
    List<Map<String, Object>> getMyCreateTask(String processInstanceId, String active);

    /**
     * 查询流程审批历史
     * @param dataId 
     * @param tableId 
     * @return
     */
    List<Map<String, Object>> getApprovalRecord(String tableId, String dataId);
    /**
     * 获取下个任务节点
     * @param taskId
     * @return
     */
    Map<String, Object> getNextTaskInfo(String taskId) throws Exception;

    /**
     * 带分页查询我的任务
     * @param page
     * @param activitiTaskVo
     * @return
     */
    IPage<ActivitiTaskVo> getMyTaskList(Page<ActivitiTaskVo> page, ActivitiTaskVo activitiTaskVo);

    /**
     * 查询所有任务列表
     * @return
     */
	List<Map<String, Object>> getAllTask();

	IPage<Map<String, Object>> getAllTaskWithPage(String creator, String modelName, String modelKey, Page<Map<String,Object>> page);

	/**
     * 催办
     * @param procInstId
     */
	void urging(String procInstId);
}
