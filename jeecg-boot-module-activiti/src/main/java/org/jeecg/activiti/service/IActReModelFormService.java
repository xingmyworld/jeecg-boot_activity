package org.jeecg.activiti.service;

import org.jeecg.activiti.entity.ActReModelForm;

import com.baomidou.mybatisplus.extension.service.IService;

public interface IActReModelFormService extends IService<ActReModelForm> {

}
