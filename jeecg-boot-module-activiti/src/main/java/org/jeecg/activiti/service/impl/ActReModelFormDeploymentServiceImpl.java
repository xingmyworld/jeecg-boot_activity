package org.jeecg.activiti.service.impl;

import org.jeecg.activiti.entity.ActReModelFormDeployment;
import org.jeecg.activiti.mapper.ActReModelFormDeploymentMapper;
import org.jeecg.activiti.service.IActReModelFormDeploymentService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class ActReModelFormDeploymentServiceImpl extends ServiceImpl<ActReModelFormDeploymentMapper, ActReModelFormDeployment> implements IActReModelFormDeploymentService {

}
