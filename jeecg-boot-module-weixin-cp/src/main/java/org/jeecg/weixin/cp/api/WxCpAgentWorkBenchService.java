package org.jeecg.weixin.cp.api;

import org.jeecg.weixin.common.error.WxErrorException;
import org.jeecg.weixin.cp.bean.WxCpAgentWorkBench;

/**
 * 工作台自定义展示：https://work.weixin.qq.com/api/doc/90000/90135/92535
 * @author songshiyu
 */
public interface WxCpAgentWorkBenchService {

	void setWorkBenchTemplate(WxCpAgentWorkBench wxCpAgentWorkBench) throws WxErrorException;

	String getWorkBenchTemplate(Long agentid) throws WxErrorException;

	void setWorkBenchData(WxCpAgentWorkBench wxCpAgentWorkBench) throws WxErrorException;
}
