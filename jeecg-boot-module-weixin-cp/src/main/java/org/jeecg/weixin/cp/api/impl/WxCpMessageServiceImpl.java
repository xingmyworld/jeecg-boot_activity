package org.jeecg.weixin.cp.api.impl;

import com.google.common.collect.ImmutableMap;
import lombok.RequiredArgsConstructor;
import org.jeecg.weixin.common.error.WxErrorException;
import org.jeecg.weixin.cp.api.WxCpMessageService;
import org.jeecg.weixin.cp.api.WxCpService;
import org.jeecg.weixin.cp.bean.message.WxCpLinkedCorpMessage;
import org.jeecg.weixin.cp.bean.message.WxCpMessage;
import org.jeecg.weixin.cp.bean.message.WxCpMessageSendResult;
import org.jeecg.weixin.cp.bean.message.WxCpMessageSendStatistics;
import org.jeecg.weixin.cp.constant.WxCpApiPathConsts.Message;
import org.jeecg.weixin.cp.util.json.WxCpGsonBuilder;

/**
 * 消息推送接口实现类.
 *
 * @author <a href="https://github.com/binarywang">Binary Wang</a>
 */
@RequiredArgsConstructor
public class WxCpMessageServiceImpl implements WxCpMessageService {
	private final WxCpService cpService;

	@Override
	public WxCpMessageSendResult send(WxCpMessage message) throws WxErrorException {
		Integer agentId = message.getAgentId();
		if (null == agentId) {
			message.setAgentId(this.cpService.getWxCpConfigStorage().getAgentId());
		}

		return WxCpMessageSendResult.fromJson(this.cpService
				.post(this.cpService.getWxCpConfigStorage().getApiUrl(Message.MESSAGE_SEND), message.toJson()));
	}

	@Override
	public WxCpMessageSendStatistics getStatistics(int timeType) throws WxErrorException {
		return WxCpMessageSendStatistics
				.fromJson(this.cpService.post(this.cpService.getWxCpConfigStorage().getApiUrl(Message.GET_STATISTICS),
						WxCpGsonBuilder.create().toJson(ImmutableMap.of("time_type", timeType))));
	}

	@Override
	public WxCpMessageSendResult sendLinkedCorpMessage(WxCpLinkedCorpMessage message) throws WxErrorException {
		Integer agentId = message.getAgentId();
		if (null == agentId) {
			message.setAgentId(this.cpService.getWxCpConfigStorage().getAgentId());
		}

		return WxCpMessageSendResult.fromJson(this.cpService.post(
				this.cpService.getWxCpConfigStorage().getApiUrl(Message.LINKEDCORP_MESSAGE_SEND), message.toJson()));
	}
}
