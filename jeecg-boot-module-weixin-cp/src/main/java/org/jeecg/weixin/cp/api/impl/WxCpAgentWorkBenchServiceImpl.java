package org.jeecg.weixin.cp.api.impl;

import com.google.gson.JsonObject;
import lombok.RequiredArgsConstructor;
import org.jeecg.weixin.common.error.WxErrorException;
import org.jeecg.weixin.cp.api.WxCpAgentWorkBenchService;
import org.jeecg.weixin.cp.api.WxCpService;
import org.jeecg.weixin.cp.bean.WxCpAgentWorkBench;

import static org.jeecg.weixin.cp.constant.WxCpApiPathConsts.WorkBench.WORKBENCH_DATA_SET;
import static org.jeecg.weixin.cp.constant.WxCpApiPathConsts.WorkBench.WORKBENCH_TEMPLATE_GET;
import static org.jeecg.weixin.cp.constant.WxCpApiPathConsts.WorkBench.WORKBENCH_TEMPLATE_SET;

/**
 * 工作台自定义展示实现
 * @author songshiyu
 */
@RequiredArgsConstructor
public class WxCpAgentWorkBenchServiceImpl implements WxCpAgentWorkBenchService {
	private final WxCpService mainService;

	@Override
	public void setWorkBenchTemplate(WxCpAgentWorkBench wxCpAgentWorkBench) throws WxErrorException {
		final String url = String.format(this.mainService.getWxCpConfigStorage().getApiUrl(WORKBENCH_TEMPLATE_SET));
		this.mainService.post(url, wxCpAgentWorkBench.toTemplateString());
	}

	@Override
	public String getWorkBenchTemplate(Long agentId) throws WxErrorException {
		final String url = String.format(this.mainService.getWxCpConfigStorage().getApiUrl(WORKBENCH_TEMPLATE_GET));
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("agentid", agentId);
		return this.mainService.post(url, jsonObject.toString());
	}

	@Override
	public void setWorkBenchData(WxCpAgentWorkBench wxCpAgentWorkBench) throws WxErrorException {
		final String url = String.format(this.mainService.getWxCpConfigStorage().getApiUrl(WORKBENCH_DATA_SET));
		this.mainService.post(url, wxCpAgentWorkBench.toUserDataString());
	}
}
