package org.jeecg.autopoi.poi.handler.inter;

/**
 * 接口自定义处理类
 * 
 * @author JEECG
 * @param <T>
 */
public interface IExcelReadRowHanlder<T> {
	/**
	 * 处理解析对象
	 * 
	 * @param t
	 */
	public void hanlder(T t);

}
