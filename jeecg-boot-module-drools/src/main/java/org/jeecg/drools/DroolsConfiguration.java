package org.jeecg.drools;

import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@AutoConfigurationPackage
@ComponentScan({"org.jeecg.drools.**.mapper"})
public class DroolsConfiguration {
}
