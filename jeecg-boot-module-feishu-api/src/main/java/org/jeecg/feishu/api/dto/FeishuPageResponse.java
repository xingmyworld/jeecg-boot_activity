package org.jeecg.feishu.api.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * 用于V3接口, 分页查询结果
 * @param <T>
 */
@Data
@ToString
public class FeishuPageResponse<T> implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JSONField(name = "has_more")
    private Boolean hasMore;
    @JSONField(name = "page_token")
    private String pageToken;
    @JSONField(name = "items")
    private List<T> items;
}
