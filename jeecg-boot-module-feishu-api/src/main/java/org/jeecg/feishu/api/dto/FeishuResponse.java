package org.jeecg.feishu.api.dto;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class FeishuResponse<T> implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int code;
    private String msg;
    private T data;
}
