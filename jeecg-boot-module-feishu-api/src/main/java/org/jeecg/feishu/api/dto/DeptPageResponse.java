package org.jeecg.feishu.api.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * 用于V1接口，批量获取部门返回结果
 */
@Data
@ToString
public class DeptPageResponse implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * 部门列表
     */
    @JSONField(name = "department_infos")
    private List<DeptInfo> departmentInfos;

    @Data
    @ToString
    public static class DeptInfo implements Serializable {
        /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		/**
         * 部门群 ID
         */
        @JSONField(name = "chat_id")
        private String chatId;
        /**
         * 部门 ID
         */
        private String id;
        /**
         * 部门名称
         */
        private String name;
        /**
         * 部门成员数量
         */
        @JSONField(name = "member_count")
        private int memberCount;
        /**
         * 父部门 ID
         */
        @JSONField(name = "parent_id")
        private String parentId;
        /**
         * 部门状态，0 无效，1 有效
         */
        private int status;
        /**
         * 部门负责人 employee_id，申请了"获取用户 user_id"权限的应用返回该字段
         */
        @JSONField(name = "leader_employee_id")
        private String leaderEmployeeId;
        /**
         * 部门负责人 open_id
         */
        @JSONField(name = "leader_open_id")
        private String leaderOpenId;
    }
}
