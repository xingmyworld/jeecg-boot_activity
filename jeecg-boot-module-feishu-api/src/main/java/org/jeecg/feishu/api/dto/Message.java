package org.jeecg.feishu.api.dto;

import java.io.Serializable;

import com.alibaba.fastjson.annotation.JSONField;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Message implements Serializable {
	
	private static final long serialVersionUID = 1L;

	/**
     * message_id
     */
    @JSONField(name = "message_id")
    private String message_id;
    
    /**
     * invalid_department_ids
     */
    @JSONField(name = "invalid_department_ids")
    private String invalidDepartmentIds;
    
    /**
     * invalid_open_ids
     */
    @JSONField(name = "invalid_open_ids")
    private String invalidOpenIds;
    
    /**
     * invalid_user_ids
     */
    @JSONField(name = "invalid_user_ids")
    private String invalidUserIds;

}
