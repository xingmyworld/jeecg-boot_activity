package org.jeecg.feishu.api.constant;

public interface FeishuApiConstants {

    /*
     * access_toke获取路径
     */
    String APP_ACCESS_TOKEN_INTERNAL_URL_PATH = "auth/v3/app_access_token/internal";
    String APP_ACCESS_TOKEN_ISV_URL_PATH = "auth/v3/app_access_token";
    String TENANT_ACCESS_TOKEN_INTERNAL_URL_PATH = "auth/v3/tenant_access_token/internal/";
    String TENANT_ACCESS_TOKEN_ISV_URL_PATH = "auth/v3/tenant_access_token";
    String APPLY_APP_TICKET_PATH = "auth/v3/app_ticket/resend";
    
    String OAUTH_AUTHORIZE = "https://passport.feishu.cn/suite/passport/oauth/authorize";

    /**
     * 根据code获取登录用户身份
     */
    String APP_ACCESS_TOKEN_BY_CODE_PATH = "authen/v1/access_token";

    /**
     * 刷新 access_token
     */
    String APP_ACCESS_TOKEN_REFRESH_PATH = "authen/v1/refresh_access_token";
    
    /**
     * 发送消息
     */
    String MESSAGE_BATCH_SEND_PATH = "message/v4/batch_send/";



    /**
     * 网站根据code获取用户登陆身份授权类型
     */
    String WEB_TOKEN_GRANT_TYPE = "authorization_code";

    /**
     * 刷新 access_token
     * 网站根据code获取用户登陆身份授权类型
     */
    String WEB_TOKEN_GRANT_TYPE_REFRESH = "refresh_token";

    /**
     * 获取列表/创建 用户
     */
    String CONTACT_V3_USERS_PATH = "contact/v3/users/";

    /**
     * 用户数据批量获取, 用户中心用于用户信息全量对比
     */
    String CONTACT_V1_USERS_PATH = "contact/v1/user/batch_get/";

    /**
     * 用户id_Mapping查询, 用户用户中心和飞书id转换
     */
    String CONTACT_V1_USER_MAPPING_PATH = "contact/v1/user/usermapping/:user_id";

    /**
     * 获取单个/修改/删除 用户
     */
    String CONTACT_V3_USER_PATH = "contact/v3/users/:user_id";

    /**
     * 创建 部门
     */
    String CONTACT_V3_DEPARTMENTS_PATH = "contact/v3/departments";
    /**
     * 部门信息批量获取,部门中心用于用户信息全量对比
     */
    String CONTACT_V1_DEPARTMENTS_PATH = "contact/v1/department/detail/batch_get";

    /**
     * 获取单个/修改/删除 部门
     */
    String CONTACT_V3_DEPARTMENT_PATH = "contact/v3/departments/:department_id";
    /**
     * 获取单个/修改/删除 部门
     */
    String CONTACT_V1_DEPARTMENT_MAPPING_PATH = "contact/v1/department/deptmapping/:department_id";

    /**
     * 获取单个审批实例详情
     */
    String WORKFLOW_APPROVAL_INSTANCE = "approval/openapi/v2/instance/get";

    /**
     * tenant_access_token not valid
     */
    int TOKEN_NOT_VALID_CODE = 9999166;

}