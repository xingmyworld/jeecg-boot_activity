package org.jeecg.feishu.api.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * 用于V3接口，获取部门返回结果
 */
@Data
@ToString
public class DeptV3Response implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 部门名称
     */
    @JSONField(name = "name")
    private String name;

    /**
     * 国际化的部门名称
     */
    @JSONField(name = "i18n_name")
    private DeptI18nName i18nName;

    /**
     * 父部门的ID,创建根部门，该参数值为 “0”
     */
    @JSONField(name = "parent_department_id")
    private String parentDepartmentId;

    /**
     * 本部门的自定义部门ID,
     * 数据校验规则：
     * <p>
     * 最大长度：128 字符
     * <p>
     * 正则校验：^0|[^od][A-Za-z0-9]*
     */
    @JSONField(name = "department_id")
    private String departmentId;
    /**
     * 部门负责人 employee_id，申请了"获取用户 user_id"权限的应用返回该字段
     */
    @JSONField(name = "leader_user_id")
    private String leaderUserId;
    /**
     * 部门群 ID
     */
    @JSONField(name = "chat_id")
    private String chatId;
    /**
     * 部门的排序，即部门在其同级部门的展示顺序
     */
    @JSONField(name = "order")
    private String order;
    /**
     * 部门单位自定义ID列表，当前只支持一个
     */
    @JSONField(name = "unit_ids")
    private String[] unitIds;
    /**
     * 部门成员数量
     */
    @JSONField(name = "member_count")
    private int memberCount;
    /**
     * 部门成员数量
     */
    @JSONField(name = "status")
    private DeptStatus status;

    /**
     * 部门状态
     */
    @Data
    @ToString
    public static class DeptStatus {
        @JSONField(name = "is_deleted")
        private boolean isDeleted;
    }

    /**
     * 国际化的部门名称
     */
    @Data
    @ToString
    public static class DeptI18nName {
        /**
         * 部门的中文名
         */
        @JSONField(name = "zh_cn")
        private String zhCn;
        /**
         * 部门的日文名
         */
        @JSONField(name = "ja_jp")
        private String jaJp;
        /**
         * 部门的英文名
         */
        @JSONField(name = "en_us")
        private String enUs;
    }
}
