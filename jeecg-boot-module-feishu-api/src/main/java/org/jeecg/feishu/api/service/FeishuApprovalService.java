package org.jeecg.feishu.api.service;

import org.jeecg.feishu.api.dto.WorkflowApproval;
import org.jeecg.feishu.api.dto.WorkflowApprovalCondition;

/**
 * 飞书审批接口
 */
public interface FeishuApprovalService {

    /**
     * 获取单个审批实例详情
     * @param condition
     * @return
     */
    WorkflowApproval getWorkApprovalInstance(WorkflowApprovalCondition condition);

}
