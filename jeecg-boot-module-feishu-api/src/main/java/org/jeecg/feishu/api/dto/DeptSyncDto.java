package org.jeecg.feishu.api.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Objects;

/**
 * 部门同步信息
 */
@Data
@ToString
public class DeptSyncDto implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * 部门名称
     */
    @JSONField(name = "name")
    private String name;
    /**
     * 父部门的ID
     */
    @JSONField(name = "parent_department_id")
    private String parentDepartmentId;
    /**
     * 本部门的自定义部门ID
     */
    @JSONField(name = "department_id")
    private String departmentId;
    /**
     * 部门主管用户ID
     */
    @JSONField(name = "leader_user_id")
    private String leaderUserId;
    /**
     * 部门的排序
     */
    @JSONField(name = "order")
    private String order;
    /**
     * 是否创建部门群，默认不创建
     */
    @JSONField(name = "create_group_chat")
    private boolean createGroupChat = false;

    /**
     * 是否被删除
     */
    @JSONField(name = "status", serialize = false)
    private DepartmentStatus status;

    /**
     * 部门的open_id
     */
    @JSONField(name = "open_department_id", serialize = false)
    private String openDepartmentId;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DeptSyncDto that = (DeptSyncDto) o;
        return createGroupChat == that.createGroupChat &&
                Objects.equals(name, that.name) &&
                Objects.equals(parentDepartmentId, that.parentDepartmentId) &&
                Objects.equals(departmentId, that.departmentId) &&
                Objects.equals(leaderUserId, that.leaderUserId) &&
                Objects.equals(order, that.order) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, parentDepartmentId, departmentId, leaderUserId, order, createGroupChat, status);
    }

    @Data
    @ToString
    public static class DepartmentStatus {
        @JSONField(name = "is_deleted")
        private boolean isDeleted = false;

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            DepartmentStatus that = (DepartmentStatus) o;
            return isDeleted == that.isDeleted;
        }

        @Override
        public int hashCode() {
            return Objects.hash(isDeleted);
        }
    }

}
