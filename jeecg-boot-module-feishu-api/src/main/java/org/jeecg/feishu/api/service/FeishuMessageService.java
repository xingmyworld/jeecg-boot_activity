package org.jeecg.feishu.api.service;

import org.jeecg.feishu.api.dto.Message;
import org.jeecg.feishu.api.dto.MessageCondition;

public interface FeishuMessageService {
	
	public Message send(MessageCondition condition);

}
