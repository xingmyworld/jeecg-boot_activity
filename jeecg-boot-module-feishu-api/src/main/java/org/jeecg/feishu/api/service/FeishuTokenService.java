package org.jeecg.feishu.api.service;

import org.jeecg.feishu.api.dto.WebAccessToken;
import org.jeecg.feishu.api.dto.WebAccessTokenCondition;
import org.jeecg.feishu.api.dto.WebRefreshTokenCondition;

/**
 * 飞书：身份验证（免登）服务
 */
public interface FeishuTokenService {
	
	/**
     * 获取权限URL
     * @param url
     * @return
     */
    String getAuthUrl(String url);

    /**
     * 根据code获取登录用户身份
     * @param condition
     * @return
     */
    WebAccessToken getWebAccessToken(WebAccessTokenCondition condition);

    /**
     * 刷新token
     * @param refreshToken 
     * @return
     */
    WebAccessToken refreshWebAccessToken(String refreshToken);
}
