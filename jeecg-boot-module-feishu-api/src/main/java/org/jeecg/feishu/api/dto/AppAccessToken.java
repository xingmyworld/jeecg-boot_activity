package org.jeecg.feishu.api.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class AppAccessToken {
    @JSONField(name = "expire")
    private int expire;
    @JSONField(name = "app_access_token")
    private String appAccessToken;
    @JSONField(name = "tenant_access_token")
    private String tenantAccessToken;
}