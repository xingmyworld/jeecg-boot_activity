package org.jeecg.feishu.api.service;

import java.util.List;

import org.jeecg.feishu.api.dto.DeptCondition;
import org.jeecg.feishu.api.dto.DeptSyncDto;
import org.jeecg.feishu.api.dto.EmployeeCondition;
import org.jeecg.feishu.api.dto.EmployeeSyncDto;
import org.jeecg.feishu.common.exception.FeishuException;

public interface FeishuContractService {

    /**
     * 查询用户详情, 获取单个用户详情
     * @param employeeId
     * @return
     */
    EmployeeSyncDto getEmployee(String employeeId);

    /**
     * 创建员工
     *
     * @param employeeSyncDto
     */
    void createEmployee(EmployeeSyncDto employeeSyncDto) throws FeishuException;

    /**
     * 更新员工信息
     *
     * @param employeeSyncDto
     */
    void updateEmployee(EmployeeSyncDto employeeSyncDto) throws FeishuException;

    /**
     * 员工离职
     *
     * @param userId
     */
    void employeeLeave(String userId) throws FeishuException;

    /**
     * V1接口，获取员工信息列表
     *
     * @param condition
     * @return
     */
    List<EmployeeSyncDto> pageEmployee(EmployeeCondition condition) throws FeishuException;

    /**
     * V1接口，获取部门列表
     *
     * @param condition
     * @return
     */
    List<DeptSyncDto> pageDept(DeptCondition condition) throws FeishuException;

    /**
     * V3接口，获取部门列表
     *
     * @param condition
     * @return
     */
    List<DeptSyncDto> pageDeptV3(DeptCondition condition) throws FeishuException;

    /**
     * 获取单个部门详情
     *
     * @param deptCondition
     */
    DeptSyncDto getDept(DeptCondition deptCondition) throws FeishuException;

    /**
     * 创建部门
     *
     * @param deptSyncDto
     */
    void createDept(DeptSyncDto deptSyncDto) throws FeishuException;

    /**
     * 更新部门
     *
     * @param deptSyncDto
     */
    void updateDept(DeptSyncDto deptSyncDto) throws FeishuException;

    /**
     * 删除部门
     *
     * @param deptId
     */
    void deleteDept(String deptId) throws FeishuException;

    /**
     * 部门与用户中心和飞书id转换
     * @param deptId
     * @return 飞书id
     */
    String deptIdMapping(String deptId);

    /**
     * 用户id_Mapping查询, 用户用户中心和飞书id转换
     * @param employeeId 用户中心员工ID
     * @return 飞书id
     */
    String employeeIdMapping(String employeeId);
}
