package org.jeecg.feishu.api.enums;

import org.jeecg.feishu.api.dto.AppAccessToken;
import lombok.Getter;

import java.util.function.Function;

public enum AccessTokenType {

    None("none_access_token"), App("app_access_token", AppAccessToken::getAppAccessToken), Tenant("tenant_access_token", AppAccessToken::getTenantAccessToken), User("user_access_token");

    @Getter
    private final String value;

    @Getter
    private Function<AppAccessToken, String> tokenFn;

    AccessTokenType(String value) {
        this.value = value;
    }
    AccessTokenType(String value,Function<AppAccessToken, String> tokenFn) {
        this.value = value;
        this.tokenFn = tokenFn;
    }

    @Override
    public String toString() {
        return value;
    }
}
