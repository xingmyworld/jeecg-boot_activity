package org.jeecg.feishu.api.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * 根据code获取登录用户身份返回结果
 */
@Data
public class WebAccessToken implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * user_access_token，用于获取用户资源
     */
    @JSONField(name = "access_token")
    private String accessToken;
    /**
     * token 类型
     */
    @JSONField(name = "token_type")
    private String tokenType;
    /**
     * access_token 的有效期，单位: 秒
     */
    @JSONField(name = "expires_in")
    private int expiresIn;
    /**
     * 用户姓名
     */
    @JSONField(name = "name")
    private String name;
    /**
     * 用户英文名称
     */
    @JSONField(name = "en_name")
    private String enName;
    /**
     * 用户头像
     */
    @JSONField(name = "avatar_url")
    private String avatarUrl;
    /**
     * 用户头像 72x72
     */
    @JSONField(name = "avatar_thumb")
    private String avatarThum;
    /**
     * 用户头像 240x240
     */
    @JSONField(name = "avatar_middle")
    private String avatarMiddle;
    /**
     * 用户头像 640x640
     */
    @JSONField(name = "avatar_big")
    private String avatartBig;
    /**
     * 用户在应用内的唯一标识
     */
    @JSONField(name = "open_id")
    private String openId;
    /**
     * 用户统一ID
     */
    @JSONField(name = "union_id")
    private String unionId;
    /**
     * 用户邮箱
     */
    @JSONField(name = "email")
    private String email;
    /**
     * user_id
     */
    @JSONField(name = "user_id")
    private String userId;
    /**
     * 用户手机号
     */
    @JSONField(name = "mobile")
    private String mobile;
    /**
     * 当前企业标识
     */
    @JSONField(name = "tenant_key")
    private String tenantKey;
    /**
     * refresh_token 的有效期，单位: 秒
     */
    @JSONField(name = "refresh_expires_in")
    private String refreshExpiresIn;
    /**
     * 刷新用户 access_token 时使用的 token
     */
    @JSONField(name = "refresh_token")
    private String refreshToken;
}
