package org.jeecg.feishu.api.dto.message;

import java.io.Serializable;

import com.alibaba.fastjson.annotation.JSONField;

import lombok.Data;

@Data
public class TextContent implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
     * text
     */
    @JSONField(name = "text")
	private String text;

}
