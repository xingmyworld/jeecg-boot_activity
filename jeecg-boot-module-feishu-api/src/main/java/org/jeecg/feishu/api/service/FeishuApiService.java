package org.jeecg.feishu.api.service;

import java.util.List;

import org.jeecg.feishu.api.config.FeishuConfig;
import org.jeecg.feishu.api.dto.FeishuRequest;
import org.jeecg.feishu.api.enums.AccessTokenType;
import org.jeecg.feishu.common.exception.FeishuException;

public interface FeishuApiService {
    /**
     * 发送请求
     *
     * @param config  飞书api配置
     * @param request 请求参数
     * @param <I>     请求参数类型
     * @param <O>     返回参数类型
     * @return 请求结果
     * @throws FeishuException
     */
    <I, O> O send(FeishuConfig config, FeishuRequest<I, O> request) throws FeishuException;

    /**
     * 获取accessToken
     *
     * @param config
     * @param refresh 是否刷新缓存
     * @return
     */
    String accessToken(FeishuConfig config, AccessTokenType accessTokenType, boolean refresh);

    /**
     * 获取列表信息
     *
     * @param config  飞书api配置
     * @param request 请求参数
     * @param <I>     请求参数类型
     * @param <O>     返回参数类型
     * @return 查询列表
     */
    <I, O> List<O> pageQuery(FeishuConfig config, FeishuRequest<I, O> request);
}
