package org.jeecg.community.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.community.entity.XyParkHouse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.community.vo.HouseTreeNode;
import org.jeecg.community.vo.XyParkHouseWithContractInfoVo;

import java.util.List;
import java.util.Map;

/**
 * 房源信息
 * @author: jeecg-boot
 * @version: V1.0
 */
@Mapper
public interface XyParkHouseMapper extends BaseMapper<XyParkHouse> {
    List<HouseTreeNode> getNodeTree(@Param("currentId")Long currentId);
    List<HouseTreeNode> getNextNodeTree(Map<String, Object> parameterMap);
    List<XyParkHouseWithContractInfoVo> queryHouseContractListByTreeNode(Page<XyParkHouseWithContractInfoVo> page,@Param("entity") XyParkHouseWithContractInfoVo entity);
}
