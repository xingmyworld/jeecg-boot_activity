package org.jeecg.community.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkContractHouse;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface XyParkContractHouseMapper extends BaseMapper<XyParkContractHouse> {

}
