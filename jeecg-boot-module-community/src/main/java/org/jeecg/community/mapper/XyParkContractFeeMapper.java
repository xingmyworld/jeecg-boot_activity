package org.jeecg.community.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkContractFee;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface XyParkContractFeeMapper extends BaseMapper<XyParkContractFee> {

}
