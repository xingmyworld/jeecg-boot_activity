package org.jeecg.community.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkCustomer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 客户信息表
 * @author: jeecg-boot
 * @version: V1.0
 */
@Mapper
public interface XyParkCustomerMapper extends BaseMapper<XyParkCustomer> {

}
