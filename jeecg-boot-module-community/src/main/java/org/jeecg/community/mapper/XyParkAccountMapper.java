package org.jeecg.community.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 账户管理
 * @author: MxpIO
 * @version: V1.0
 */
@Mapper
public interface XyParkAccountMapper extends BaseMapper<XyParkAccount> {

}
