package org.jeecg.community.mapper;

import java.util.List;
import org.jeecg.community.entity.XyParkOwnercontact;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 业主联系人信息
 * @author: jeecg-boot
 * @version: V1.0
 */
@Mapper
public interface XyParkOwnercontactMapper extends BaseMapper<XyParkOwnercontact> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<XyParkOwnercontact> selectByMainId(@Param("mainId") String mainId);
}
