package org.jeecg.community.mapper;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.community.entity.XyParkBill;
import org.jeecg.community.vo.XyParkBillVo;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface XyParkBillMapper extends BaseMapper<XyParkBill> {

	public List<XyParkBillVo> selectBillAmount(@Param("cashType") String cashType, @Param("ownerCode") String ownerCode, @Param("costType") String costType, @Param("customerAccount") String customerAccount);

	public BigDecimal selectSumCheckAmount(@Param("billCode") String billCode);
}
