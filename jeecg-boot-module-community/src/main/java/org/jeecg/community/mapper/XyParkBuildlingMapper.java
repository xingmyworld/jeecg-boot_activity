package org.jeecg.community.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkBuildling;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 楼宇信息
 * @author: jeecg-boot
 * @version: V1.0
 */
@Mapper
public interface XyParkBuildlingMapper extends BaseMapper<XyParkBuildling> {

}
