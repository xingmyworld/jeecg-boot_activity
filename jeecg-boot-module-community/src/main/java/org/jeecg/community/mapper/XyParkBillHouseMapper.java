package org.jeecg.community.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.community.entity.XyParkBillHouse;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface XyParkBillHouseMapper extends BaseMapper<XyParkBillHouse> {

}
