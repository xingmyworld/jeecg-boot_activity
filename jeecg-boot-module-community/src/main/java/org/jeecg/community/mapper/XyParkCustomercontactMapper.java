package org.jeecg.community.mapper;

import java.util.List;
import org.jeecg.community.entity.XyParkCustomercontact;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 客户联系人
 * @author: jeecg-boot
 * @version: V1.0
 */
@Mapper
public interface XyParkCustomercontactMapper extends BaseMapper<XyParkCustomercontact> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<XyParkCustomercontact> selectByMainId(@Param("mainId") String mainId);
}
