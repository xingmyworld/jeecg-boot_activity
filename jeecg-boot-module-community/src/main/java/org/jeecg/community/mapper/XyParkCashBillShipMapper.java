package org.jeecg.community.mapper;

import java.math.BigDecimal;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.community.entity.XyParkCashBillShip;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface XyParkCashBillShipMapper extends BaseMapper<XyParkCashBillShip> {
	
	public BigDecimal sumCheckAmountByCashCode(@Param("cashCode") String cashCode);
	
	public BigDecimal sumCheckAmountByBillCode(@Param("billCode") String billCode);

}
