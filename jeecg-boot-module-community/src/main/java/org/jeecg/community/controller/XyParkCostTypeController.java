package org.jeecg.community.controller;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import org.apache.shiro.SecurityUtils;
import org.jeecg.autopoi.poi.excel.def.NormalExcelConstants;
import org.jeecg.autopoi.poi.excel.entity.ExportParams;
import org.jeecg.autopoi.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.community.entity.XyParkCostType;
import org.jeecg.community.service.IXyParkCostTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 园区物业-费项类型
 * @author: MxpIO
 */
@Api(tags = "园区物业-费项类型")
@RestController
@RequestMapping("/community/costtype")
public class XyParkCostTypeController {
	
	@Autowired
	private IXyParkCostTypeService xyParkCostTypeService;
	
	/**
	 * 费项类型-分页列表查询
	 * @param cash
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "费项类型-分页列表查询")
	@ApiOperation(value = "费项类型-分页列表查询", notes = "费项类型-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(XyParkCostType cash,
			@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize, HttpServletRequest req) {
		QueryWrapper<XyParkCostType> queryWrapper = QueryGenerator.initQueryWrapper(cash,
				req.getParameterMap());
		Page<XyParkCostType> page = new Page<XyParkCostType>(pageNo, pageSize);
		IPage<XyParkCostType> pageList = xyParkCostTypeService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 * 添加
	 * 
	 * @param cash
	 * @return
	 */
	@AutoLog(value = "费项类型-添加")
	@ApiOperation(value = "费项类型-添加", notes = "费项类型-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody XyParkCostType cash) {
		xyParkCostTypeService.save(cash);
		return Result.OK("添加成功！", cash);
	}

	/**
	 * 编辑
	 * 
	 * @param cash
	 * @return
	 */
	@AutoLog(value = "费项类型-编辑")
	@ApiOperation(value = "费项类型-编辑", notes = "费项类型-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody XyParkCostType cash) {
		xyParkCostTypeService.updateById(cash);
		return Result.OK("编辑成功!", cash);
	}
	
	/**
	 * 通过id删除
	 * 
	 * @param id
	 * @return
	 */
	@AutoLog(value = "费项类型-通过id删除")
	@ApiOperation(value = "费项类型-通过id删除", notes = "费项类型-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
		xyParkCostTypeService.removeById(id);
		return Result.OK("删除成功!", id);
	}

	/**
	 * 批量删除
	 * 
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "费项类型-批量删除")
	@ApiOperation(value = "费项类型-批量删除", notes = "费项类型-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
		xyParkCostTypeService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!", null);
	}
	
	/**
	 * 导出
	 * 
	 * @return
	 */
	@RequestMapping(value = "/export")
	public ModelAndView export(HttpServletRequest request,
			XyParkCostType cash) {
		// Step.1 组装查询条件
		QueryWrapper<XyParkCostType> queryWrapper = QueryGenerator.initQueryWrapper(cash,
				request.getParameterMap());
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		// Step.2 获取导出数据
		List<XyParkCostType> pageList = xyParkCostTypeService.list(queryWrapper);
		List<XyParkCostType> exportList = null;

		// 过滤选中数据
		String selections = request.getParameter("selections");
		if (oConvertUtils.isNotEmpty(selections)) {
			List<String> selectionList = Arrays.asList(selections.split(","));
			exportList = pageList.stream().filter(item -> selectionList.contains(item.getId()))
					.collect(Collectors.toList());
		} else {
			exportList = pageList;
		}

		// Step.3 AutoPoi 导出Excel
		ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		mv.addObject(NormalExcelConstants.FILE_NAME, "费项类型"); // 此处设置的filename无效
																// ,前端会重更新设置一下
		mv.addObject(NormalExcelConstants.CLASS, XyParkCostType.class);
		mv.addObject(NormalExcelConstants.PARAMS,
				new ExportParams("费项类型", "导出人:" + sysUser.getRealname(), "费项类型"));
		mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		return mv;
	}
}
