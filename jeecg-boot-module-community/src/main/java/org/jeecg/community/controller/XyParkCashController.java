package org.jeecg.community.controller;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import org.apache.shiro.SecurityUtils;
import org.jeecg.autopoi.poi.excel.def.NormalExcelConstants;
import org.jeecg.autopoi.poi.excel.entity.ExportParams;
import org.jeecg.autopoi.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.community.entity.XyParkCash;
import org.jeecg.community.entity.XyParkCashBillShip;
import org.jeecg.community.service.IXyParkBillService;
import org.jeecg.community.service.IXyParkCashBillShipService;
import org.jeecg.community.service.IXyParkCashService;
import org.jeecg.community.vo.XyParkBillVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 园区物业-收支管理
 * @author: MxpIO
 */
@Api(tags = "园区物业-收支管理")
@RestController
@RequestMapping("/community/cash")
public class XyParkCashController {
	
	@Autowired
	private IXyParkCashService cashService;
	
	@Autowired
	private IXyParkBillService xyParkBillService;
	
	@Autowired
	private IXyParkCashBillShipService xyParkCashBillShipService;
	
	/**
	 * 收支管理-分页列表查询
	 * @param cash
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "收支管理-分页列表查询")
	@ApiOperation(value = "收支管理-分页列表查询", notes = "收支管理-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(XyParkCash cash,
			@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize, HttpServletRequest req) {
		QueryWrapper<XyParkCash> queryWrapper = QueryGenerator.initQueryWrapper(cash,
				req.getParameterMap());
		Page<XyParkCash> page = new Page<XyParkCash>(pageNo, pageSize);
		IPage<XyParkCash> pageList = cashService.page(page, queryWrapper);
		cashService.handleDetails(pageList);
		return Result.OK(pageList);
	}
	
	/**
	 * 收支管理-已匹配账单列表
	 */
	@AutoLog(value = "收支管理-已匹配账单列表")
	@ApiOperation(value = "收支管理-已匹配账单列表", notes = "收支管理-分页列表查询")
	@GetMapping(value = "/billlist")
	public Result<?> billlist(@RequestParam(name = "cashCode", required = true) String cashCode) {
		List<XyParkCashBillShip> list = xyParkCashBillShipService.list(new QueryWrapper<XyParkCashBillShip>().eq("cash_code", cashCode));
		xyParkCashBillShipService.handleDetails(list);
		return Result.OK(list);
	}
	
	/**
	 * 收支管理-自动匹配
	 */
	@AutoLog(value = "收支管理-自动匹配")
	@ApiOperation(value = "收支管理-自动匹配", notes = "收支管理-自动匹配")
	@GetMapping(value = "/autoAmount")
	public Result<?> autoAmount(@RequestParam(name = "cashCode", required = true) String cashCode) {
		XyParkCash cash = cashService.getOne(new QueryWrapper<XyParkCash>().eq("cash_code", cashCode));
		List<XyParkBillVo> result = xyParkBillService.getCashBillShipByAmount(cash);
		return Result.OK(result);
	}
	
	/**
	 * 添加
	 * 
	 * @param ships
	 * @return
	 */
	@AutoLog(value = "收支管理-添加账单匹配")
	@ApiOperation(value = "收支管理-添加账单匹配", notes = "收支管理-添加账单匹配")
	@PostMapping(value = "/addAmount")
	public Result<?> addAmount(@RequestBody List<XyParkCashBillShip> ships){
		xyParkCashBillShipService.saveBatch(ships);
		xyParkBillService.updateStatus(ships);
		return Result.OK("添加成功！", ships);
	}
	
	@AutoLog(value = "收支管理-删除账单匹配")
	@ApiOperation(value = "收支管理-删除账单匹配", notes = "收支管理-删除账单匹配")
	@PostMapping(value = "/deleteAmount")
	public Result<?> deleteAmount(@RequestBody XyParkCashBillShip ship){
		xyParkCashBillShipService.removeById(ship.getId());
		xyParkBillService.updateStatus(ship);
		return Result.OK("删除成功！", ship);
	}
	
	
	/**
	 * 添加
	 * 
	 * @param cash
	 * @return
	 */
	@AutoLog(value = "收支管理-添加")
	@ApiOperation(value = "收支管理-添加", notes = "收支管理-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody XyParkCash cash) {
		cashService.save(cash);
		return Result.OK("添加成功！", cash);
	}

	/**
	 * 编辑
	 * 
	 * @param cash
	 * @return
	 */
	@AutoLog(value = "收支管理-编辑")
	@ApiOperation(value = "收支管理-编辑", notes = "收支管理-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody XyParkCash cash) {
		cashService.updateById(cash);
		return Result.OK("编辑成功!", cash);
	}
	
	/**
	 * 通过id删除
	 * 
	 * @param id
	 * @return
	 */
	@AutoLog(value = "收支管理-通过id删除")
	@ApiOperation(value = "收支管理-通过id删除", notes = "收支管理-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
		cashService.removeById(id);
		return Result.OK("删除成功!", id);
	}

	/**
	 * 批量删除
	 * 
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "收支管理-批量删除")
	@ApiOperation(value = "收支管理-批量删除", notes = "收支管理-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
		cashService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!", null);
	}
	
	/**
	 * 导出
	 * 
	 * @return
	 */
	@RequestMapping(value = "/export")
	public ModelAndView export(HttpServletRequest request,
			XyParkCash cash) {
		// Step.1 组装查询条件
		QueryWrapper<XyParkCash> queryWrapper = QueryGenerator.initQueryWrapper(cash,
				request.getParameterMap());
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		// Step.2 获取导出数据
		List<XyParkCash> pageList = cashService.list(queryWrapper);
		List<XyParkCash> exportList = null;

		// 过滤选中数据
		String selections = request.getParameter("selections");
		if (oConvertUtils.isNotEmpty(selections)) {
			List<String> selectionList = Arrays.asList(selections.split(","));
			exportList = pageList.stream().filter(item -> selectionList.contains(item.getId()))
					.collect(Collectors.toList());
		} else {
			exportList = pageList;
		}

		// Step.3 AutoPoi 导出Excel
		ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		mv.addObject(NormalExcelConstants.FILE_NAME, "收支管理"); // 此处设置的filename无效
																// ,前端会重更新设置一下
		mv.addObject(NormalExcelConstants.CLASS, XyParkCash.class);
		mv.addObject(NormalExcelConstants.PARAMS,
				new ExportParams("收支管理", "导出人:" + sysUser.getRealname(), "收支管理"));
		mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		return mv;
	}
}
