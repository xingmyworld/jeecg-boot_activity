package org.jeecg.community.controller;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import org.apache.shiro.SecurityUtils;
import org.jeecg.autopoi.poi.excel.def.NormalExcelConstants;
import org.jeecg.autopoi.poi.excel.entity.ExportParams;
import org.jeecg.autopoi.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.community.entity.XyParkBill;
import org.jeecg.community.entity.XyParkBillHouse;
import org.jeecg.community.service.IXyParkBillHouseService;
import org.jeecg.community.service.IXyParkBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 园区物业-账单管理
 * @author: MxpIO
 */
@Api(tags = "园区物业-账单管理")
@RestController
@RequestMapping("/community/bill")
public class XyParkBillController {
	
	@Autowired
	private IXyParkBillService billService;
	
	@Autowired
	private IXyParkBillHouseService xyParkBillHouseService;
	
	/**
	 * 账单管理-分页列表查询
	 * @param bill
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "账单管理-分页列表查询")
	@ApiOperation(value = "账单管理-分页列表查询", notes = "账单管理-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(XyParkBill bill,
			@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize, HttpServletRequest req) {
		QueryWrapper<XyParkBill> queryWrapper = QueryGenerator.initQueryWrapper(bill,
				req.getParameterMap());
		Page<XyParkBill> page = new Page<XyParkBill>(pageNo, pageSize);
		IPage<XyParkBill> pageList = billService.page(page, queryWrapper);
		billService.handleDetails(pageList);
		return Result.OK(pageList);
	}
	
	/**
	 * 添加
	 * 
	 * @param bill
	 * @return
	 */
	@AutoLog(value = "账单管理-添加")
	@ApiOperation(value = "账单管理-添加", notes = "账单管理-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody XyParkBill bill) {
		List<XyParkBillHouse> houses = bill.getHouses();
		if(houses != null){
			for(XyParkBillHouse house : houses){
				house.setBillCode(bill.getBillCode());
				xyParkBillHouseService.save(house);
			}
		}
		billService.save(bill);
		return Result.OK("添加成功！", bill);
	}

	/**
	 * 编辑
	 * 
	 * @param bill
	 * @return
	 */
	@AutoLog(value = "账单管理-编辑")
	@ApiOperation(value = "账单管理-编辑", notes = "账单管理-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody XyParkBill bill) {
		xyParkBillHouseService.remove(new QueryWrapper<XyParkBillHouse>().eq("bill_code", bill.getBillCode()));
		List<XyParkBillHouse> houses = bill.getHouses();
		if(houses != null){
			for(XyParkBillHouse house : houses){
				house.setBillCode(bill.getBillCode());
				xyParkBillHouseService.save(house);
			}
		}
		billService.updateById(bill);
		return Result.OK("编辑成功!", bill);
	}
	
	/**
	 * 通过id删除
	 * 
	 * @param id
	 * @return
	 */
	@AutoLog(value = "账单管理-通过id删除")
	@ApiOperation(value = "账单管理-通过id删除", notes = "账单管理-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
		XyParkBill bill = billService.getById(id);
		xyParkBillHouseService.remove(new QueryWrapper<XyParkBillHouse>().eq("bill_code", bill.getBillCode()));
		billService.removeById(id);
		return Result.OK("删除成功!", id);
	}

	/**
	 * 批量删除
	 * 
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "账单管理-批量删除")
	@ApiOperation(value = "账单管理-批量删除", notes = "账单管理-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
		List<String> idList = Arrays.asList(ids.split(","));
		for(String id : idList){
			XyParkBill bill = billService.getById(id);
			xyParkBillHouseService.remove(new QueryWrapper<XyParkBillHouse>().eq("bill_code", bill.getBillCode()));
		}
		billService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!", null);
	}
	
	/**
	 * 批量审批
	 * 
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "账单管理-批量审批")
	@ApiOperation(value = "账单管理-批量审批", notes = "账单管理-批量审批")
	@GetMapping(value = "/approvalBatch")
	public Result<?> approvalBatch(@RequestParam(name = "ids", required = true) String ids,
			@RequestParam(name = "status", required = true) String status) {
		List<String> idList = Arrays.asList(ids.split(","));
		billService.approvalByIds(idList,status);
		return Result.OK("批量审批成功!", null);
	}
	
	/**
	 * 导出
	 * 
	 * @return
	 */
	@RequestMapping(value = "/export")
	public ModelAndView export(HttpServletRequest request,
			XyParkBill bill) {
		// Step.1 组装查询条件
		QueryWrapper<XyParkBill> queryWrapper = QueryGenerator.initQueryWrapper(bill,
				request.getParameterMap());
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		// Step.2 获取导出数据
		List<XyParkBill> pageList = billService.list(queryWrapper);
		List<XyParkBill> exportList = null;

		// 过滤选中数据
		String selections = request.getParameter("selections");
		if (oConvertUtils.isNotEmpty(selections)) {
			List<String> selectionList = Arrays.asList(selections.split(","));
			exportList = pageList.stream().filter(item -> selectionList.contains(item.getId()))
					.collect(Collectors.toList());
		} else {
			exportList = pageList;
		}

		// Step.3 AutoPoi 导出Excel
		ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		mv.addObject(NormalExcelConstants.FILE_NAME, "账单管理"); // 此处设置的filename无效
																// ,前端会重更新设置一下
		mv.addObject(NormalExcelConstants.CLASS, XyParkBill.class);
		mv.addObject(NormalExcelConstants.PARAMS,
				new ExportParams("账单管理", "导出人:" + sysUser.getRealname(), "账单管理"));
		mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		return mv;
	}
}
