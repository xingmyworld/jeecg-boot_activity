package org.jeecg.community.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.autopoi.poi.excel.ExcelImportUtil;
import org.jeecg.autopoi.poi.excel.def.NormalExcelConstants;
import org.jeecg.autopoi.poi.excel.entity.ExportParams;
import org.jeecg.autopoi.poi.excel.entity.ImportParams;
import org.jeecg.autopoi.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.vo.LoginUser;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.community.entity.XyParkFloor;
import org.jeecg.community.entity.XyParkBuildling;
import org.jeecg.community.vo.XyParkBuildlingPage;
import org.jeecg.community.service.IXyParkBuildlingService;
import org.jeecg.community.service.IXyParkFloorService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * 楼宇信息
 * @author: jeecg-boot
 * @version: V1.0
 */
@Api(tags="园区物业-楼宇信息")
@RestController
@RequestMapping("/community/xyParkBuildling")
@Slf4j
public class XyParkBuildlingController {
	@Autowired
	private IXyParkBuildlingService xyParkBuildlingService;
	@Autowired
	private IXyParkFloorService xyParkFloorService;
	
	/**
	 * 分页列表查询
	 *
	 * @param xyParkBuildling
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "楼宇信息-分页列表查询")
	@ApiOperation(value="楼宇信息-分页列表查询", notes="楼宇信息-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(XyParkBuildling xyParkBuildling,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<XyParkBuildling> queryWrapper = QueryGenerator.initQueryWrapper(xyParkBuildling, req.getParameterMap());
		Page<XyParkBuildling> page = new Page<XyParkBuildling>(pageNo, pageSize);
		IPage<XyParkBuildling> pageList = xyParkBuildlingService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param xyParkBuildlingPage
	 * @return
	 */
	@AutoLog(value = "楼宇信息-添加")
	@ApiOperation(value="楼宇信息-添加", notes="楼宇信息-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody XyParkBuildlingPage xyParkBuildlingPage) {
		XyParkBuildling xyParkBuildling = new XyParkBuildling();
		BeanUtils.copyProperties(xyParkBuildlingPage, xyParkBuildling);
		xyParkBuildlingService.saveMain(xyParkBuildling, xyParkBuildlingPage.getXyParkFloorList());
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param xyParkBuildlingPage
	 * @return
	 */
	@AutoLog(value = "楼宇信息-编辑")
	@ApiOperation(value="楼宇信息-编辑", notes="楼宇信息-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody XyParkBuildlingPage xyParkBuildlingPage) {
		XyParkBuildling xyParkBuildling = new XyParkBuildling();
		BeanUtils.copyProperties(xyParkBuildlingPage, xyParkBuildling);
		XyParkBuildling xyParkBuildlingEntity = xyParkBuildlingService.getById(xyParkBuildling.getId());
		if(xyParkBuildlingEntity==null) {
			return Result.error("未找到对应数据");
		}
		xyParkBuildlingService.updateMain(xyParkBuildling, xyParkBuildlingPage.getXyParkFloorList());
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "楼宇信息-通过id删除")
	@ApiOperation(value="楼宇信息-通过id删除", notes="楼宇信息-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		xyParkBuildlingService.delMain(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "楼宇信息-批量删除")
	@ApiOperation(value="楼宇信息-批量删除", notes="楼宇信息-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.xyParkBuildlingService.delBatchMain(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "楼宇信息-通过id查询")
	@ApiOperation(value="楼宇信息-通过id查询", notes="楼宇信息-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		XyParkBuildling xyParkBuildling = xyParkBuildlingService.getById(id);
		if(xyParkBuildling==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(xyParkBuildling);

	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "楼层信息通过主表ID查询")
	@ApiOperation(value="楼层信息主表ID查询", notes="楼层信息-通主表ID查询")
	@GetMapping(value = "/queryXyParkFloorByMainId")
	public Result<?> queryXyParkFloorListByMainId(@RequestParam(name="id",required=true) String id) {
		List<XyParkFloor> xyParkFloorList = xyParkFloorService.selectByMainId(id);
		return Result.OK(xyParkFloorList);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param xyParkBuildling
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, XyParkBuildling xyParkBuildling) {
      // Step.1 组装查询条件查询数据
      QueryWrapper<XyParkBuildling> queryWrapper = QueryGenerator.initQueryWrapper(xyParkBuildling, request.getParameterMap());
      LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

      //Step.2 获取导出数据
      List<XyParkBuildling> queryList = xyParkBuildlingService.list(queryWrapper);
      // 过滤选中数据
      String selections = request.getParameter("selections");
      List<XyParkBuildling> xyParkBuildlingList = new ArrayList<XyParkBuildling>();
      if(oConvertUtils.isEmpty(selections)) {
          xyParkBuildlingList = queryList;
      }else {
          List<String> selectionList = Arrays.asList(selections.split(","));
          xyParkBuildlingList = queryList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
      }

      // Step.3 组装pageList
      List<XyParkBuildlingPage> pageList = new ArrayList<XyParkBuildlingPage>();
      for (XyParkBuildling main : xyParkBuildlingList) {
          XyParkBuildlingPage vo = new XyParkBuildlingPage();
          BeanUtils.copyProperties(main, vo);
          List<XyParkFloor> xyParkFloorList = xyParkFloorService.selectByMainId(main.getId());
          vo.setXyParkFloorList(xyParkFloorList);
          pageList.add(vo);
      }

      // Step.4 AutoPoi 导出Excel
      ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
      mv.addObject(NormalExcelConstants.FILE_NAME, "楼宇信息列表");
      mv.addObject(NormalExcelConstants.CLASS, XyParkBuildlingPage.class);
      mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("楼宇信息数据", "导出人:"+sysUser.getRealname(), "楼宇信息"));
      mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
      return mv;
    }

    /**
    * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
      Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
      for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
          MultipartFile file = entity.getValue();// 获取上传文件对象
          ImportParams params = new ImportParams();
          params.setTitleRows(2);
          params.setHeadRows(1);
          params.setNeedSave(true);
          try {
              List<XyParkBuildlingPage> list = ExcelImportUtil.importExcel(file.getInputStream(), XyParkBuildlingPage.class, params);
              for (XyParkBuildlingPage page : list) {
                  XyParkBuildling po = new XyParkBuildling();
                  BeanUtils.copyProperties(page, po);
                  xyParkBuildlingService.saveMain(po, page.getXyParkFloorList());
              }
              return Result.OK("文件导入成功！数据行数:" + list.size());
          } catch (Exception e) {
              log.error(e.getMessage(),e);
              return Result.error("文件导入失败:"+e.getMessage());
          } finally {
              try {
                  file.getInputStream().close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
      }
      return Result.OK("文件导入失败！");
    }

}
