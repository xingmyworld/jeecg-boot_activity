package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkHousestay;
import org.jeecg.community.mapper.XyParkHousestayMapper;
import org.jeecg.community.service.IXyParkHousestayService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class XyParkHousestayServiceImpl extends ServiceImpl<XyParkHousestayMapper, XyParkHousestay> implements IXyParkHousestayService {
	
	@Autowired
	private XyParkHousestayMapper xyParkHousestayMapper;
	
	@Override
	public List<XyParkHousestay> selectByMainId(String mainId) {
		return xyParkHousestayMapper.selectByMainId(mainId);
	}
}
