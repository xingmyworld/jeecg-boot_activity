package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkIntentionhouse;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * 客户意向房源
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface IXyParkIntentionhouseService extends IService<XyParkIntentionhouse> {

	public List<XyParkIntentionhouse> selectByMainId(String mainId);
}
