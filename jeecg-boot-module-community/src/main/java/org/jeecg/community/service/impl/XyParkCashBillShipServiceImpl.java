package org.jeecg.community.service.impl;

import java.util.List;

import org.jeecg.community.entity.XyParkBill;
import org.jeecg.community.entity.XyParkCashBillShip;
import org.jeecg.community.mapper.XyParkBillMapper;
import org.jeecg.community.mapper.XyParkCashBillShipMapper;
import org.jeecg.community.service.IXyParkCashBillShipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class XyParkCashBillShipServiceImpl extends ServiceImpl<XyParkCashBillShipMapper,XyParkCashBillShip> implements IXyParkCashBillShipService {

	@Autowired
	private XyParkBillMapper xyParkBillMapper;
	
	@Override
	public void handleDetails(List<XyParkCashBillShip> list) {
		for(XyParkCashBillShip ship : list){
			ship.setXyParkBill(xyParkBillMapper.selectOne(new QueryWrapper<XyParkBill>().eq("bill_code", ship.getBillCode())));
		}
	}
	
}
