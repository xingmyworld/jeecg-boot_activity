package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkAccount;
import org.jeecg.community.mapper.XyParkAccountMapper;
import org.jeecg.community.service.IXyParkAccountService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class XyParkAccountServiceImpl extends ServiceImpl<XyParkAccountMapper, XyParkAccount> implements IXyParkAccountService {

}
