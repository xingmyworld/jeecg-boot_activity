package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkOwnerbillinginfo;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * 业主开票信息表
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface IXyParkOwnerbillinginfoService extends IService<XyParkOwnerbillinginfo> {

	public List<XyParkOwnerbillinginfo> selectByMainId(String mainId);
}
