package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkContractHouse;
import org.jeecg.community.mapper.XyParkContractHouseMapper;
import org.jeecg.community.service.IXyParkContractHouseService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class XyParkContractHouseServiceImpl extends ServiceImpl<XyParkContractHouseMapper,XyParkContractHouse> implements IXyParkContractHouseService {

}
