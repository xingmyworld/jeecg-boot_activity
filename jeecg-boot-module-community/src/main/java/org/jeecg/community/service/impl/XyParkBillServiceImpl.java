package org.jeecg.community.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.jeecg.community.entity.XyParkBill;
import org.jeecg.community.entity.XyParkBillHouse;
import org.jeecg.community.entity.XyParkCash;
import org.jeecg.community.entity.XyParkCashBillShip;
import org.jeecg.community.entity.XyParkContract;
import org.jeecg.community.entity.XyParkCostType;
import org.jeecg.community.entity.XyParkOwner;
import org.jeecg.community.mapper.XyParkBillHouseMapper;
import org.jeecg.community.mapper.XyParkBillMapper;
import org.jeecg.community.mapper.XyParkCashBillShipMapper;
import org.jeecg.community.mapper.XyParkContractMapper;
import org.jeecg.community.mapper.XyParkCostTypeMapper;
import org.jeecg.community.mapper.XyParkOwnerMapper;
import org.jeecg.community.service.IXyParkBillService;
import org.jeecg.community.vo.XyParkBillVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class XyParkBillServiceImpl extends ServiceImpl<XyParkBillMapper,XyParkBill> implements IXyParkBillService {
	
	@Autowired
	private XyParkBillHouseMapper xyParkBillHouseMapper;
	
	@Autowired
	private XyParkBillMapper xyParkBillMapper;
	
	@Autowired
	private XyParkContractMapper xyParkContractMapper;
	
	@Autowired
	private XyParkOwnerMapper xyParkOwnerMapper;
	
	@Autowired
	private XyParkCostTypeMapper xyParkCostTypeMapper;
	
	@Autowired
	private XyParkCashBillShipMapper xyParkCashBillShipMapper;

	@Override
	public void handleDetails(IPage<XyParkBill> pageList) {
		for(XyParkBill bill : pageList.getRecords()){
			
			bill.setHouses(xyParkBillHouseMapper.selectList(new QueryWrapper<XyParkBillHouse>().eq("bill_code", bill.getBillCode())));
			bill.setXyParkContract(xyParkContractMapper.selectOne(new QueryWrapper<XyParkContract>().eq("contract_code", bill.getContractCode())));
			bill.setXyParkCostType(xyParkCostTypeMapper.selectOne(new QueryWrapper<XyParkCostType>().eq("cost_type", bill.getCostType())));
			bill.setXyParkOwner(xyParkOwnerMapper.selectOne(new QueryWrapper<XyParkOwner>().eq("owner_code", bill.getOwnerCode())));;
			bill.setCheckAmount(xyParkBillMapper.selectSumCheckAmount(bill.getBillCode()));
		}
	}

	@Override
	public void approvalByIds(List<String> idList, String status) {
		List<XyParkBill> list = xyParkBillMapper.selectBatchIds(idList);
		if(list != null){
			for(XyParkBill bill : list){
				bill.setBillStatus(status);
				xyParkBillMapper.updateById(bill);
			}
		}		
	}

	@Override
	public List<XyParkBillVo> getCashBillShipByAmount(XyParkCash cash) {
		BigDecimal checkAmount = xyParkCashBillShipMapper.sumCheckAmountByCashCode(cash.getCashCode());
		if(checkAmount == null){
			checkAmount = BigDecimal.ZERO;
		}
		BigDecimal amount = cash.getAmount().subtract(checkAmount);
		List<XyParkBillVo> bills = xyParkBillMapper.selectBillAmount(cash.getCashType(), cash.getOwnerCode(),cash.getCostType(),cash.getCustomerAccount());
		List<XyParkBillVo> result = new ArrayList<>();
		for(XyParkBillVo billVo : bills){
			amount = amount.subtract(billVo.getSurplusAmount());
			if(amount.compareTo(BigDecimal.ZERO) <= 0){
				billVo.setPlanAmount(amount.add(billVo.getSurplusAmount()));
				result.add(billVo);
				break;
			}else{
				billVo.setPlanAmount(billVo.getSurplusAmount());
				result.add(billVo);
			}
		}
		return result;
	}

	@Override
	public void updateStatus(List<XyParkCashBillShip> ships) {
		for(XyParkCashBillShip cashBillShip : ships){
			updateStatus(cashBillShip);
		}
	}

	@Override
	public void updateStatus(XyParkCashBillShip ship) {
		XyParkBill bill = xyParkBillMapper.selectOne(new QueryWrapper<XyParkBill>().eq("bill_code", ship.getBillCode()));
		BigDecimal amount = xyParkCashBillShipMapper.sumCheckAmountByBillCode(bill.getBillCode());
		if(BigDecimal.ZERO.compareTo(amount) == 0){
			bill.setClearStatus("1");
		}else if(bill.getAmount().compareTo(amount) <= 0){
			bill.setClearStatus("3");
		}else{
			bill.setClearStatus("2");
		}
		xyParkBillMapper.updateById(bill);
	}

}
