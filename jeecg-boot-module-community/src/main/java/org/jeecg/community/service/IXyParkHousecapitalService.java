package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkHousecapital;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * 房源配属资产
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface IXyParkHousecapitalService extends IService<XyParkHousecapital> {

	public List<XyParkHousecapital> selectByMainId(String mainId);
}
