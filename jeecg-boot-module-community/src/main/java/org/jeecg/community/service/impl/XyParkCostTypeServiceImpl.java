package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkCostType;
import org.jeecg.community.mapper.XyParkCostTypeMapper;
import org.jeecg.community.service.IXyParkCostTypeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class XyParkCostTypeServiceImpl extends ServiceImpl<XyParkCostTypeMapper,XyParkCostType> implements IXyParkCostTypeService {
	
}
