package org.jeecg.community.service;

import java.util.List;

import org.jeecg.community.entity.XyParkCashBillShip;

import com.baomidou.mybatisplus.extension.service.IService;

public interface IXyParkCashBillShipService extends IService<XyParkCashBillShip> {

	void handleDetails(List<XyParkCashBillShip> list);

}
