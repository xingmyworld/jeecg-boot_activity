package org.jeecg.community.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.community.entity.XyParkHousecapital;
import org.jeecg.community.entity.XyParkHousestay;
import org.jeecg.community.entity.XyParkHouse;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.community.vo.HouseTreeNode;
import org.jeecg.community.vo.XyParkHouseWithContractInfoVo;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * 房源信息
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface IXyParkHouseService extends IService<XyParkHouse> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(XyParkHouse xyParkHouse,List<XyParkHousecapital> xyParkHousecapitalList,List<XyParkHousestay> xyParkHousestayList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(XyParkHouse xyParkHouse,List<XyParkHousecapital> xyParkHousecapitalList,List<XyParkHousestay> xyParkHousestayList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);

    List<HouseTreeNode> getHouseTree(Long currentId);

    Page<XyParkHouseWithContractInfoVo> queryHouseContractListByTreeNode(Page<XyParkHouseWithContractInfoVo> page, XyParkHouseWithContractInfoVo entity);
}
