package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkOwner;
import org.jeecg.community.entity.XyParkOwnercontact;
import org.jeecg.community.entity.XyParkOwnerbusiness;
import org.jeecg.community.entity.XyParkOwnerbillinginfo;
import org.jeecg.community.mapper.XyParkOwnercontactMapper;
import org.jeecg.community.mapper.XyParkOwnerbusinessMapper;
import org.jeecg.community.mapper.XyParkOwnerbillinginfoMapper;
import org.jeecg.community.mapper.XyParkOwnerMapper;
import org.jeecg.community.service.IXyParkOwnerService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

@Service
public class XyParkOwnerServiceImpl extends ServiceImpl<XyParkOwnerMapper, XyParkOwner> implements IXyParkOwnerService {

	@Autowired
	private XyParkOwnerMapper xyParkOwnerMapper;
	@Autowired
	private XyParkOwnercontactMapper xyParkOwnercontactMapper;
	@Autowired
	private XyParkOwnerbusinessMapper xyParkOwnerbusinessMapper;
	@Autowired
	private XyParkOwnerbillinginfoMapper xyParkOwnerbillinginfoMapper;
	
	@Override
	@Transactional
	public void saveMain(XyParkOwner xyParkOwner, List<XyParkOwnercontact> xyParkOwnercontactList,List<XyParkOwnerbusiness> xyParkOwnerbusinessList,List<XyParkOwnerbillinginfo> xyParkOwnerbillinginfoList) {
		xyParkOwnerMapper.insert(xyParkOwner);
		if(xyParkOwnercontactList!=null && xyParkOwnercontactList.size()>0) {
			for(XyParkOwnercontact entity:xyParkOwnercontactList) {
				//外键设置
				entity.setOwnerCode(xyParkOwner.getOwnerCode());
				xyParkOwnercontactMapper.insert(entity);
			}
		}
		if(xyParkOwnerbusinessList!=null && xyParkOwnerbusinessList.size()>0) {
			for(XyParkOwnerbusiness entity:xyParkOwnerbusinessList) {
				//外键设置
				entity.setOwnerCode(xyParkOwner.getOwnerCode());
				xyParkOwnerbusinessMapper.insert(entity);
			}
		}
		if(xyParkOwnerbillinginfoList!=null && xyParkOwnerbillinginfoList.size()>0) {
			for(XyParkOwnerbillinginfo entity:xyParkOwnerbillinginfoList) {
				//外键设置
				entity.setOwnerCode(xyParkOwner.getOwnerCode());
				xyParkOwnerbillinginfoMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(XyParkOwner xyParkOwner,List<XyParkOwnercontact> xyParkOwnercontactList,List<XyParkOwnerbusiness> xyParkOwnerbusinessList,List<XyParkOwnerbillinginfo> xyParkOwnerbillinginfoList) {
		xyParkOwnerMapper.updateById(xyParkOwner);
		
		//1.先删除子表数据
		xyParkOwnercontactMapper.deleteByMainId(xyParkOwner.getOwnerCode());
		xyParkOwnerbusinessMapper.deleteByMainId(xyParkOwner.getOwnerCode());
		xyParkOwnerbillinginfoMapper.deleteByMainId(xyParkOwner.getOwnerCode());
		
		//2.子表数据重新插入
		if(xyParkOwnercontactList!=null && xyParkOwnercontactList.size()>0) {
			for(XyParkOwnercontact entity:xyParkOwnercontactList) {
				//外键设置
				entity.setOwnerCode(xyParkOwner.getOwnerCode());
				xyParkOwnercontactMapper.insert(entity);
			}
		}
		if(xyParkOwnerbusinessList!=null && xyParkOwnerbusinessList.size()>0) {
			for(XyParkOwnerbusiness entity:xyParkOwnerbusinessList) {
				//外键设置
				entity.setOwnerCode(xyParkOwner.getOwnerCode());
				xyParkOwnerbusinessMapper.insert(entity);
			}
		}
		if(xyParkOwnerbillinginfoList!=null && xyParkOwnerbillinginfoList.size()>0) {
			for(XyParkOwnerbillinginfo entity:xyParkOwnerbillinginfoList) {
				//外键设置
				entity.setOwnerCode(xyParkOwner.getOwnerCode());
				xyParkOwnerbillinginfoMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		xyParkOwnercontactMapper.deleteByMainId(id);
		xyParkOwnerbusinessMapper.deleteByMainId(id);
		xyParkOwnerbillinginfoMapper.deleteByMainId(id);
		xyParkOwnerMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			xyParkOwnercontactMapper.deleteByMainId(id.toString());
			xyParkOwnerbusinessMapper.deleteByMainId(id.toString());
			xyParkOwnerbillinginfoMapper.deleteByMainId(id.toString());
			xyParkOwnerMapper.deleteById(id);
		}
	}
	
}
