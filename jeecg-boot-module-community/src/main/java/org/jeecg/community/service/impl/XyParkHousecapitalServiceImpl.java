package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkHousecapital;
import org.jeecg.community.mapper.XyParkHousecapitalMapper;
import org.jeecg.community.service.IXyParkHousecapitalService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class XyParkHousecapitalServiceImpl extends ServiceImpl<XyParkHousecapitalMapper, XyParkHousecapital> implements IXyParkHousecapitalService {
	
	@Autowired
	private XyParkHousecapitalMapper xyParkHousecapitalMapper;
	
	@Override
	public List<XyParkHousecapital> selectByMainId(String mainId) {
		return xyParkHousecapitalMapper.selectByMainId(mainId);
	}
}
