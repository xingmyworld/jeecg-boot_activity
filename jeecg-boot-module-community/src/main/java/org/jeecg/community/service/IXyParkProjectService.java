package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkProject;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 园区项目信息
 * @author: MxpIO
 * @version: V1.0
 */
public interface IXyParkProjectService extends IService<XyParkProject> {

}
