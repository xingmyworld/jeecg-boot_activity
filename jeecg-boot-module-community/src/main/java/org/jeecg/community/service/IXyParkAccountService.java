package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkAccount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 账户管理
 * @author: MxpIO
 * @version: V1.0
 */
public interface IXyParkAccountService extends IService<XyParkAccount> {

}
