package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkCustomercontact;
import org.jeecg.community.entity.XyParkIntentionhouse;
import org.jeecg.community.entity.XyParkCustomer;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * 客户信息表
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface IXyParkCustomerService extends IService<XyParkCustomer> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(XyParkCustomer xyParkCustomer,List<XyParkCustomercontact> xyParkCustomercontactList,List<XyParkIntentionhouse> xyParkIntentionhouseList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(XyParkCustomer xyParkCustomer,List<XyParkCustomercontact> xyParkCustomercontactList,List<XyParkIntentionhouse> xyParkIntentionhouseList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
