package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkFloor;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * 楼层信息
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface IXyParkFloorService extends IService<XyParkFloor> {

	public List<XyParkFloor> selectByMainId(String mainId);
}
