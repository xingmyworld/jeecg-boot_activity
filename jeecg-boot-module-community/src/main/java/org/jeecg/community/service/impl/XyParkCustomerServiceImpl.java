package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkCustomer;
import org.jeecg.community.entity.XyParkCustomercontact;
import org.jeecg.community.entity.XyParkIntentionhouse;
import org.jeecg.community.mapper.XyParkCustomercontactMapper;
import org.jeecg.community.mapper.XyParkIntentionhouseMapper;
import org.jeecg.community.mapper.XyParkCustomerMapper;
import org.jeecg.community.service.IXyParkCustomerService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

@Service
public class XyParkCustomerServiceImpl extends ServiceImpl<XyParkCustomerMapper, XyParkCustomer> implements IXyParkCustomerService {

	@Autowired
	private XyParkCustomerMapper xyParkCustomerMapper;
	@Autowired
	private XyParkCustomercontactMapper xyParkCustomercontactMapper;
	@Autowired
	private XyParkIntentionhouseMapper xyParkIntentionhouseMapper;
	
	@Override
	@Transactional
	public void saveMain(XyParkCustomer xyParkCustomer, List<XyParkCustomercontact> xyParkCustomercontactList,List<XyParkIntentionhouse> xyParkIntentionhouseList) {
		xyParkCustomerMapper.insert(xyParkCustomer);
		if(xyParkCustomercontactList!=null && xyParkCustomercontactList.size()>0) {
			for(XyParkCustomercontact entity:xyParkCustomercontactList) {
				//外键设置
				entity.setCustomerCode(xyParkCustomer.getCustomerCode());
				xyParkCustomercontactMapper.insert(entity);
			}
		}
		if(xyParkIntentionhouseList!=null && xyParkIntentionhouseList.size()>0) {
			for(XyParkIntentionhouse entity:xyParkIntentionhouseList) {
				//外键设置
				entity.setCustomerCode(xyParkCustomer.getCustomerCode());
				xyParkIntentionhouseMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(XyParkCustomer xyParkCustomer,List<XyParkCustomercontact> xyParkCustomercontactList,List<XyParkIntentionhouse> xyParkIntentionhouseList) {
		xyParkCustomerMapper.updateById(xyParkCustomer);
		
		//1.先删除子表数据
		xyParkCustomercontactMapper.deleteByMainId(xyParkCustomer.getCustomerCode());
		xyParkCustomercontactMapper.deleteByMainId(xyParkCustomer.getCustomerCode());
		xyParkIntentionhouseMapper.deleteByMainId(xyParkCustomer.getCustomerCode());
		xyParkIntentionhouseMapper.deleteByMainId(xyParkCustomer.getCustomerCode());
		
		//2.子表数据重新插入
		if(xyParkCustomercontactList!=null && xyParkCustomercontactList.size()>0) {
			for(XyParkCustomercontact entity:xyParkCustomercontactList) {
				//外键设置
				entity.setCustomerCode(xyParkCustomer.getCustomerCode());
				xyParkCustomercontactMapper.insert(entity);
			}
		}
		if(xyParkIntentionhouseList!=null && xyParkIntentionhouseList.size()>0) {
			for(XyParkIntentionhouse entity:xyParkIntentionhouseList) {
				//外键设置
				entity.setCustomerCode(xyParkCustomer.getCustomerCode());
				xyParkIntentionhouseMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		xyParkCustomercontactMapper.deleteByMainId(id);
		xyParkIntentionhouseMapper.deleteByMainId(id);
		xyParkCustomerMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			xyParkCustomercontactMapper.deleteByMainId(id.toString());
			xyParkIntentionhouseMapper.deleteByMainId(id.toString());
			xyParkCustomerMapper.deleteById(id);
		}
	}
	
}
