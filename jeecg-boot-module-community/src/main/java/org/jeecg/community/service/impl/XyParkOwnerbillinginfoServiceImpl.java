package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkOwnerbillinginfo;
import org.jeecg.community.mapper.XyParkOwnerbillinginfoMapper;
import org.jeecg.community.service.IXyParkOwnerbillinginfoService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class XyParkOwnerbillinginfoServiceImpl extends ServiceImpl<XyParkOwnerbillinginfoMapper, XyParkOwnerbillinginfo> implements IXyParkOwnerbillinginfoService {
	
	@Autowired
	private XyParkOwnerbillinginfoMapper xyParkOwnerbillinginfoMapper;
	
	@Override
	public List<XyParkOwnerbillinginfo> selectByMainId(String mainId) {
		return xyParkOwnerbillinginfoMapper.selectByMainId(mainId);
	}
}
