package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkHousestay;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * 房源住宿信息
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface IXyParkHousestayService extends IService<XyParkHousestay> {

	public List<XyParkHousestay> selectByMainId(String mainId);
}
