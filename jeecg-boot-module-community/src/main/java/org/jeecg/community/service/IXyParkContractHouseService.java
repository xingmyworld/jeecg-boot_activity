package org.jeecg.community.service;

import org.jeecg.community.entity.XyParkContractHouse;

import com.baomidou.mybatisplus.extension.service.IService;

public interface IXyParkContractHouseService extends IService<XyParkContractHouse> {

}
