package org.jeecg.community.service.impl;

import org.jeecg.community.entity.XyParkContractFee;
import org.jeecg.community.mapper.XyParkContractFeeMapper;
import org.jeecg.community.service.IXyParkContractFeeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class XyParkContractFeeServiceImpl extends ServiceImpl<XyParkContractFeeMapper,XyParkContractFee> implements IXyParkContractFeeService {

}
