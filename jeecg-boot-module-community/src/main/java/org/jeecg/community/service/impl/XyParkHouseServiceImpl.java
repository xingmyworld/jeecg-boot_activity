package org.jeecg.community.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.community.entity.XyParkHouse;
import org.jeecg.community.entity.XyParkHousecapital;
import org.jeecg.community.entity.XyParkHousestay;
import org.jeecg.community.mapper.XyParkHousecapitalMapper;
import org.jeecg.community.mapper.XyParkHousestayMapper;
import org.jeecg.community.mapper.XyParkHouseMapper;
import org.jeecg.community.service.IXyParkHouseService;
import org.jeecg.community.vo.HouseTreeNode;
import org.jeecg.community.vo.XyParkHouseWithContractInfoVo;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

@Service
public class XyParkHouseServiceImpl extends ServiceImpl<XyParkHouseMapper, XyParkHouse> implements IXyParkHouseService {

	@Autowired
	private XyParkHouseMapper xyParkHouseMapper;
	@Autowired
	private XyParkHousecapitalMapper xyParkHousecapitalMapper;
	@Autowired
	private XyParkHousestayMapper xyParkHousestayMapper;
	
	@Override
	@Transactional
	public void saveMain(XyParkHouse xyParkHouse, List<XyParkHousecapital> xyParkHousecapitalList,List<XyParkHousestay> xyParkHousestayList) {
		xyParkHouseMapper.insert(xyParkHouse);
		if(xyParkHousecapitalList!=null && xyParkHousecapitalList.size()>0) {
			for(XyParkHousecapital entity:xyParkHousecapitalList) {
				//外键设置
				entity.setHouseCode(xyParkHouse.getHouseCode());
				xyParkHousecapitalMapper.insert(entity);
			}
		}
		if(xyParkHousestayList!=null && xyParkHousestayList.size()>0) {
			for(XyParkHousestay entity:xyParkHousestayList) {
				//外键设置
				entity.setHouseCode(xyParkHouse.getHouseCode());
				xyParkHousestayMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(XyParkHouse xyParkHouse,List<XyParkHousecapital> xyParkHousecapitalList,List<XyParkHousestay> xyParkHousestayList) {
		xyParkHouseMapper.updateById(xyParkHouse);
		
		//1.先删除子表数据
		xyParkHousecapitalMapper.deleteByMainId(xyParkHouse.getHouseCode());
		xyParkHousecapitalMapper.deleteByMainId(xyParkHouse.getHouseCode());
		xyParkHousestayMapper.deleteByMainId(xyParkHouse.getHouseCode());
		xyParkHousestayMapper.deleteByMainId(xyParkHouse.getHouseCode());
		
		//2.子表数据重新插入
		if(xyParkHousecapitalList!=null && xyParkHousecapitalList.size()>0) {
			for(XyParkHousecapital entity:xyParkHousecapitalList) {
				//外键设置
				entity.setHouseCode(xyParkHouse.getHouseCode());
				xyParkHousecapitalMapper.insert(entity);
			}
		}
		if(xyParkHousestayList!=null && xyParkHousestayList.size()>0) {
			for(XyParkHousestay entity:xyParkHousestayList) {
				//外键设置
				entity.setHouseCode(xyParkHouse.getHouseCode());
				xyParkHousestayMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		xyParkHousecapitalMapper.deleteByMainId(id);
		xyParkHousestayMapper.deleteByMainId(id);
		xyParkHouseMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			xyParkHousecapitalMapper.deleteByMainId(id.toString());
			xyParkHousestayMapper.deleteByMainId(id.toString());
			xyParkHouseMapper.deleteById(id);
		}
	}
	@Override
	public List<HouseTreeNode> getHouseTree(Long currentId){
		return xyParkHouseMapper.getNodeTree(currentId);
	}

	@Override
	public Page<XyParkHouseWithContractInfoVo> queryHouseContractListByTreeNode(Page<XyParkHouseWithContractInfoVo> page,XyParkHouseWithContractInfoVo entity) {
		return page.setRecords(xyParkHouseMapper.queryHouseContractListByTreeNode(page,
				entity));
	}
}
