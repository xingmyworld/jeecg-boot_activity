package org.jeecg.community.vo;

import java.util.List;
import org.jeecg.community.entity.XyParkOwnercontact;
import org.jeecg.community.entity.XyParkOwnerbusiness;
import org.jeecg.community.entity.XyParkOwnerbillinginfo;
import lombok.Data;
import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.autopoi.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 业主信息表
 * @author: jeecg-boot
 * @version: V1.0
 */
@Data
@ApiModel(value="xy_park_ownerPage对象", description="业主信息表")
public class XyParkOwnerPage {

	/**主键*/
	@ApiModelProperty(value = "主键")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**业主编码*/
	@Excel(name = "业主编码", width = 15)
	@ApiModelProperty(value = "业主编码")
	private java.lang.String ownerCode;
	/**业主名称*/
	@Excel(name = "业主名称", width = 15)
	@ApiModelProperty(value = "业主名称")
	private java.lang.String ownerName;
	/**业主身份证*/
	@Excel(name = "身份证", width = 15)
    @ApiModelProperty(value = "身份证")
    private java.lang.String idCard;
	/**业主类型*/
	@Excel(name = "业主类型", width = 15)
	@ApiModelProperty(value = "业主类型")
	private java.lang.String ownerType;
	/**所属行业*/
	@Excel(name = "所属行业", width = 15)
	@ApiModelProperty(value = "所属行业")
	private java.lang.String ownerTrade;
	/**年收入额度*/
	@Excel(name = "年收入额度", width = 15)
	@ApiModelProperty(value = "年收入额度")
	private java.lang.String annualIncome;
	/**是否注册园区*/
	@Excel(name = "是否注册园区", width = 15)
	@ApiModelProperty(value = "是否注册园区")
	private java.lang.String isLogon;
	/**是否科技型企业*/
	@Excel(name = "是否科技型企业", width = 15)
	@ApiModelProperty(value = "是否科技型企业")
	private java.lang.String isTechnology;
	/**是否备案入库*/
	@Excel(name = "是否备案入库", width = 15)
	@ApiModelProperty(value = "是否备案入库")
	private java.lang.String isRecord;
	/**附件*/
	@Excel(name = "附件", width = 15)
	@ApiModelProperty(value = "附件")
	private java.lang.String file;
	
	/**联系人*/
	@Excel(name = "联系人", width = 15)
    @ApiModelProperty(value = "联系人")
    private java.lang.String contact;
	/**联系地址*/
	@Excel(name = "联系地址", width = 15)
    @ApiModelProperty(value = "联系地址")
    private java.lang.String contactAddr;
	
	@Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    private java.lang.String contactTel;

	@ExcelCollection(name="业主联系人信息")
	@ApiModelProperty(value = "业主联系人信息")
	private List<XyParkOwnercontact> xyParkOwnercontactList;
	@ExcelCollection(name="业主工商信息")
	@ApiModelProperty(value = "业主工商信息")
	private List<XyParkOwnerbusiness> xyParkOwnerbusinessList;
	@ExcelCollection(name="业主开票信息表")
	@ApiModelProperty(value = "业主开票信息表")
	private List<XyParkOwnerbillinginfo> xyParkOwnerbillinginfoList;

}
