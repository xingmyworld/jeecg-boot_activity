package org.jeecg.community.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.autopoi.poi.excel.annotation.ExcelCollection;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecg.community.entity.XyParkHousecapital;
import org.jeecg.community.entity.XyParkHousestay;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.List;

/**
 * 房源合同扩展信息
 * @author: jeecg-boot
 * @version: V1.0
 */
@Data
@ApiModel(value="xy_park_house合同扩展信息对象", description="房源合同扩展信息")
public class XyParkHouseWithContractInfoVo {

	/**主键*/
	/*@ApiModelProperty(value = "主键")
	private String id;*/

	/**房源号*/
	@Excel(name = "房源号", width = 15)
	@ApiModelProperty(value = "房源号")
	private String houseCode;
	/**所属楼宇编码*/
	@Excel(name = "所属楼宇编码", width = 15)
	@ApiModelProperty(value = "所属楼宇编码")
	private String building;
	/**所属楼宇名称*/
	@Excel(name = "所属楼宇名称", width = 15)
	@ApiModelProperty(value = "所属楼宇名称")
	private String buildingName;
	/**所属项目编码*/
	@Excel(name = "所属项目编码", width = 15)
	@ApiModelProperty(value = "所属项目编码")
	private String project;
	/**所属项目名称*/
	@Excel(name = "所属项目名称", width = 15)
	@ApiModelProperty(value = "所属项目名称")
	private String projectName;
	/**楼层*/
	@Excel(name = "楼层", width = 15)
	@ApiModelProperty(value = "楼层")
	private String floor;
	/**建筑面积（㎡）*/
	@Excel(name = "建筑面积（㎡）", width = 15)
	@ApiModelProperty(value = "建筑面积（㎡）")
	private java.math.BigDecimal buildingArea;
	/**产权面积（㎡）*/
	/*@Excel(name = "产权面积（㎡）", width = 15)
	@ApiModelProperty(value = "产权面积（㎡）")
	private java.math.BigDecimal propertyArea;*/
	/**装修情况*/
	/*@Excel(name = "装修情况", width = 15, dicCode = "park_decoration")
    @Dict(dicCode = "park_decoration")
	@ApiModelProperty(value = "装修情况")
	private String decoration;*/
	/**房源类型*/
	/*@Excel(name = "房源类型", width = 15, dicCode = "park_housing_types")
    @Dict(dicCode = "park_housing_types")
	@ApiModelProperty(value = "房源类型")
	private String houseType;*/
	/**房源备案号*/
	/*@Excel(name = "房源备案号", width = 15)
	@ApiModelProperty(value = "房源备案号")
	private String housingRegistration;*/
	/**房源状态*/
	@Excel(name = "房源状态", width = 15, dicCode = "park_housing_condition")
    @Dict(dicCode = "park_housing_condition")
	@ApiModelProperty(value = "房源状态")
	private String houseStatus;
	/**备注*/
	/*@Excel(name = "备注", width = 15)
	@ApiModelProperty(value = "备注")
	private String remarks;*/


	/**房源号*/
	@Excel(name = "合同号", width = 15)
	@ApiModelProperty(value = "合同号")
	private String contractCode;
	/**房源号*/
	@Excel(name = "业主名称", width = 15)
	@ApiModelProperty(value = "业主名称")
	private String contractOwnerName;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "签订日期")
	private java.util.Date contractSigningDate;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "合同有效期")
	private java.util.Date contractEndDate;

	@Excel(name = "合同金额", width = 15)
	@ApiModelProperty(value = "合同金额")
	private java.math.BigDecimal contractAmount;

	//日期检索条件
	@ApiModelProperty(value = "签订日期从")
	private java.util.Date contractSigningDate_begin;

	@ApiModelProperty(value = "签订日期至")
	private java.util.Date contractSigningDate_end;

	@ApiModelProperty(value = "合同有效期从")
	private java.util.Date contractEndDate_begin;

	@ApiModelProperty(value = "合同有效期至")
	private java.util.Date contractEndDate_end;
}
