package org.jeecg.community.vo;

import java.util.List;
import org.jeecg.community.entity.XyParkFloor;
import lombok.Data;
import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.autopoi.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 楼宇信息
 * @author: jeecg-boot
 * @version: V1.0
 */
@Data
@ApiModel(value="xy_park_buildlingPage对象", description="楼宇信息")
public class XyParkBuildlingPage {

	/**主键*/
	@ApiModelProperty(value = "主键")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**楼宇编码*/
	@Excel(name = "楼宇编码", width = 15)
	@ApiModelProperty(value = "楼宇编码")
	private java.lang.String buildingCode;
	/**楼宇名称*/
	@Excel(name = "楼宇名称", width = 15)
	@ApiModelProperty(value = "楼宇名称")
	private java.lang.String buildingName;
	/**楼宇地址*/
	@Excel(name = "楼宇地址", width = 15)
	@ApiModelProperty(value = "楼宇地址")
	private java.lang.String address;
	/**所属项目*/
	@Excel(name = "所属项目", width = 15, dictTable = "xy_park_project", dicText = "project_name", dicCode = "project_code")
    @Dict(dictTable = "xy_park_project", dicText = "project_name", dicCode = "project_code")
	@ApiModelProperty(value = "所属项目")
	private java.lang.String buildingProject;
	/**行政区域*/
	@Excel(name = "行政区域", width = 15)
	@ApiModelProperty(value = "行政区域")
	private java.lang.String administrativeArea;
	/**产权性质*/
	@Excel(name = "产权性质", width = 15, dicCode = "park_nature_property")
    @Dict(dicCode = "park_nature_property")
	@ApiModelProperty(value = "产权性质")
	private java.lang.String propertyRight;
	/**建筑面积（㎡）*/
	@Excel(name = "建筑面积（㎡）", width = 15)
	@ApiModelProperty(value = "建筑面积（㎡）")
	private java.math.BigDecimal buildingArea;
	/**产权面积（㎡）*/
	@Excel(name = "产权面积（㎡）", width = 15)
	@ApiModelProperty(value = "产权面积（㎡）")
	private java.math.BigDecimal propertyArea;
	/**房源数量*/
	@Excel(name = "房源数量", width = 15)
	@ApiModelProperty(value = "房源数量")
	private java.lang.Integer houseingQty;
	/**首层*/
	@Excel(name = "首层", width = 15)
	@ApiModelProperty(value = "首层")
	private java.lang.Integer firstFloor;
	/**顶层*/
	@Excel(name = "顶层", width = 15)
	@ApiModelProperty(value = "顶层")
	private java.lang.Integer lastFloor;
	/**楼高（m）*/
	@Excel(name = "楼高（m）", width = 15)
	@ApiModelProperty(value = "楼高（m）")
	private java.math.BigDecimal buildingHeight;
	/**备注*/
	@Excel(name = "备注", width = 15)
	@ApiModelProperty(value = "备注")
	private java.lang.String remarks;

	@ExcelCollection(name="楼层信息")
	@ApiModelProperty(value = "楼层信息")
	private List<XyParkFloor> xyParkFloorList;

}
