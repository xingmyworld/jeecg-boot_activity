package org.jeecg.community.vo;

import java.math.BigDecimal;

import org.jeecg.community.entity.XyParkBill;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@ApiModel(value="XyParkBillVo对象", description="账单信息查询")
public class XyParkBillVo extends XyParkBill {
	
	private static final long serialVersionUID = 1L;
	
	private BigDecimal sumCheckAmount;
	
	private BigDecimal surplusAmount;
	
	private BigDecimal planAmount;

}
