package org.jeecg.community.vo;

import java.util.List;
import org.jeecg.community.entity.XyParkHousecapital;
import org.jeecg.community.entity.XyParkHousestay;
import lombok.Data;
import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.autopoi.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 房源信息
 * @author: jeecg-boot
 * @version: V1.0
 */
@Data
@ApiModel(value="xy_park_housePage对象", description="房源信息")
public class XyParkHousePage {

	/**主键*/
	@ApiModelProperty(value = "主键")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**房源号*/
	@Excel(name = "房源号", width = 15)
	@ApiModelProperty(value = "房源号")
	private java.lang.String houseCode;
	/**所属楼宇编码*/
	@Excel(name = "所属楼宇编码", width = 15)
	@ApiModelProperty(value = "所属楼宇编码")
	private java.lang.String building;
	/**所属楼宇名称*/
	@Excel(name = "所属楼宇名称", width = 15)
	@ApiModelProperty(value = "所属楼宇名称")
	private java.lang.String buildingName;
	/**所属项目编码*/
	@Excel(name = "所属项目编码", width = 15)
	@ApiModelProperty(value = "所属项目编码")
	private java.lang.String project;
	/**所属项目名称*/
	@Excel(name = "所属项目名称", width = 15)
	@ApiModelProperty(value = "所属项目名称")
	private java.lang.String projectName;
	/**楼层*/
	@Excel(name = "楼层", width = 15)
	@ApiModelProperty(value = "楼层")
	private java.lang.String floor;
	/**建筑面积（㎡）*/
	@Excel(name = "建筑面积（㎡）", width = 15)
	@ApiModelProperty(value = "建筑面积（㎡）")
	private java.math.BigDecimal buildingArea;
	/**产权面积（㎡）*/
	@Excel(name = "产权面积（㎡）", width = 15)
	@ApiModelProperty(value = "产权面积（㎡）")
	private java.math.BigDecimal propertyArea;
	/**装修情况*/
	@Excel(name = "装修情况", width = 15, dicCode = "park_decoration")
    @Dict(dicCode = "park_decoration")
	@ApiModelProperty(value = "装修情况")
	private java.lang.String decoration;
	/**房源类型*/
	@Excel(name = "房源类型", width = 15, dicCode = "park_housing_types")
    @Dict(dicCode = "park_housing_types")
	@ApiModelProperty(value = "房源类型")
	private java.lang.String houseType;
	/**房源备案号*/
	@Excel(name = "房源备案号", width = 15)
	@ApiModelProperty(value = "房源备案号")
	private java.lang.String housingRegistration;
	/**房源状态*/
	@Excel(name = "房源状态", width = 15, dicCode = "park_housing_condition")
    @Dict(dicCode = "park_housing_condition")
	@ApiModelProperty(value = "房源状态")
	private java.lang.String houseStatus;
	/**备注*/
	@Excel(name = "备注", width = 15)
	@ApiModelProperty(value = "备注")
	private java.lang.String remarks;

	@ExcelCollection(name="房源配属资产")
	@ApiModelProperty(value = "房源配属资产")
	private List<XyParkHousecapital> xyParkHousecapitalList;
	@ExcelCollection(name="房源住宿信息")
	@ApiModelProperty(value = "房源住宿信息")
	private List<XyParkHousestay> xyParkHousestayList;

}
