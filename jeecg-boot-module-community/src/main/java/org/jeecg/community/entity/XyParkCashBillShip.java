package org.jeecg.community.entity;

import java.math.BigDecimal;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("xy_park_cash_bill_ship")
public class XyParkCashBillShip extends JeecgEntity {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "收支编号")
	@Excel(name="收支编号",width=15)
	private String cashCode;
	
	@ApiModelProperty(value = "账单编号")
	@Excel(name="账单编号",width=15)
	private String billCode;
	
	@ApiModelProperty(value = "收支类型")
	@Excel(name="收支类型",width=15)
	private String cashType;
	
	@ApiModelProperty(value = "匹配金额")
	@Excel(name="匹配金额",width=15)
	private BigDecimal checkAmount;
	
	@TableField(exist = false)
	private XyParkBill xyParkBill;
	
}
