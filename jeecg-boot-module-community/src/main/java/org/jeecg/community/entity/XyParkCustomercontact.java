package org.jeecg.community.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 客户联系人
 * @author: jeecg-boot
 * @version: V1.0
 */
@ApiModel(value="xy_park_customer对象", description="客户信息表")
@Data
@TableName("xy_park_customercontact")
public class XyParkCustomercontact implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
	@ApiModelProperty(value = "主键")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**客户编码*/
	@ApiModelProperty(value = "客户编码")
	private java.lang.String customerCode;
	/**联系人*/
	@ApiModelProperty(value = "联系人")
	private java.lang.String contacts;
	/**联系电话*/
	@ApiModelProperty(value = "联系电话")
	private java.lang.String phone;
	/**邮箱*/
	@ApiModelProperty(value = "邮箱")
	private java.lang.String email;
	/**职务*/
	@ApiModelProperty(value = "职务")
	private java.lang.String position;
}
