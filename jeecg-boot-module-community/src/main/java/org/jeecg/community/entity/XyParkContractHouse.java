package org.jeecg.community.entity;

import java.math.BigDecimal;
import java.util.Date;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;
import org.springframework.format.annotation.DateTimeFormat;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("xy_park_contract_house")
public class XyParkContractHouse extends JeecgEntity {

	private static final long serialVersionUID = 1L;
	
	/**合同编号*/
	@Excel(name = "合同编号", width = 15)
    @ApiModelProperty(value = "合同编号")
	private String contractCode;
	
	/**合同类别*/
	@Excel(name = "合同类别", width = 15)
    @ApiModelProperty(value = "合同类别")
	private String contractType;
	
	/**合同日期*/
	@Excel(name = "合同日期", width = 15)
    @ApiModelProperty(value = "合同日期")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	private Date contractDate;
	
	/**房源号*/
	@Excel(name = "房源号", width = 15)
    @ApiModelProperty(value = "房源号")
	private String houseCode;
	
	/**房源号*/
	@Excel(name = "项目名称", width = 15)
    @ApiModelProperty(value = "项目名称")
	private String projectName;
	
	/**房源号*/
	@Excel(name = "楼宇名称", width = 15)
    @ApiModelProperty(value = "楼宇名称")
	private String buildingName;
	
	/**楼层*/
	@Excel(name = "楼层", width = 15)
    @ApiModelProperty(value = "楼层")
    private String floor;
	
	/**建筑面积（㎡）*/
	@Excel(name = "建筑面积（㎡）", width = 15)
    @ApiModelProperty(value = "建筑面积（㎡）")
    private BigDecimal buildingArea;
	
	/**产权面积（㎡）*/
	@Excel(name = "产权面积（㎡）", width = 15)
    @ApiModelProperty(value = "产权面积（㎡）")
    private BigDecimal propertyArea;

}
