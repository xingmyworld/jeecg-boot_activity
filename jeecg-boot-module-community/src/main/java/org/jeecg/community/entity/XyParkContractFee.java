package org.jeecg.community.entity;

import java.math.BigDecimal;
import java.util.Date;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;
import org.springframework.format.annotation.DateTimeFormat;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("xy_park_contract_fee")
public class XyParkContractFee extends JeecgEntity {

	private static final long serialVersionUID = 1L;
	
	/**合同编号*/
	@Excel(name = "合同编号", width = 15)
    @ApiModelProperty(value = "合同编号")
	private String contractCode;
	
	@ApiModelProperty(value = "费项类型")
	@Excel(name="费项类型",width=15)
	private String costType;
	
	@ApiModelProperty(value = "费项名称")
	@Excel(name="费项名称",width=15)
	private String costName;
	
	@ApiModelProperty(value = "金额")
	@Excel(name="金额",width=15)
	private BigDecimal price;
	
	@ApiModelProperty(value = "开始时间")
	@Excel(name="开始时间",width=15)
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	private Date startDate;
	
	@ApiModelProperty(value = "结束时间")
	@Excel(name="结束时间",width=15)
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	private Date endDate;
	
	@ApiModelProperty(value = "收款日")
	@Excel(name="收款日",width=15)
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	private Date collectionDate;

}
