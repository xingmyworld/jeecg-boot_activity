package org.jeecg.community.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;
import org.springframework.format.annotation.DateTimeFormat;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("xy_park_bill")
public class XyParkBill extends JeecgEntity {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "账单编号")
	@Excel(name="账单编号",width=15)
	private String billCode;
	
	@ApiModelProperty(value = "发生对象")
	@Excel(name="发生对象",width=15)
	private String ownerCode;
	
	@ApiModelProperty(value = "收支账户")
	@Excel(name="收支账户",width=15)
	private String customerAccount;
	
	@ApiModelProperty(value = "账单类型")
	@Excel(name="账单类型",width=15, dicCode="xy_park_bill_type")
	private String billType;
	
	@ApiModelProperty(value = "合同编号")
	@Excel(name="合同编号",width=15)
	private String contractCode;
	
	@ApiModelProperty(value = "账单来源")
	@Excel(name="账单来源",width=15, dicCode="xy_park_bill_source")
	private String billSource;
	
	@ApiModelProperty(value = "账单状态")
	@Excel(name="账单状态",width=15, dicCode="xy_park_bill_status")
	private String billStatus;
	
	@ApiModelProperty(value = "结清状态")
	@Excel(name="结清状态",width=15, dicCode="xy_park_bill_clear")
	private String clearStatus;
	
	@ApiModelProperty(value = "开始日期")
	@Excel(name="开始日期",width=15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	private Date startDate;
	
	@ApiModelProperty(value = "结束日期")
	@Excel(name="结束日期",width=15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	private Date endDate;
	
	@ApiModelProperty(value = "应收日期")
	@Excel(name="应收日期",width=15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	private Date dueDate;
	
	@ApiModelProperty(value = "费项类型")
	@Excel(name="费项类型",width=15)
	private String costType;
	
	@ApiModelProperty(value = "发生金额")
	@Excel(name="发生金额",width=15)
	private BigDecimal amount;
	
	@ApiModelProperty(value = "违约金比例")
	@Excel(name="违约金比例",width=15)
	private BigDecimal breachFee;
	
	@ApiModelProperty(value = "备注")
	@Excel(name="备注",width=15)
	private String remark;
	
	@TableField(exist = false)
	private XyParkContract xyParkContract;
	
	@TableField(exist = false)
	private XyParkCostType xyParkCostType;
	
	@TableField(exist = false)
	private List<XyParkBillHouse> houses;
	
	@TableField(exist = false)
	private XyParkOwner xyParkOwner;
	
	@TableField(exist = false)
	private BigDecimal checkAmount;

}
