package org.jeecg.hr.vo;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class AssessmentResultVo {

	private String username;
	
	private BigDecimal assessmentScore;
	
	private String levelDesc;
}
