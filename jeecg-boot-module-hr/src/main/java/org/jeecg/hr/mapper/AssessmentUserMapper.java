package org.jeecg.hr.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.hr.entity.AssessmentUser;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface AssessmentUserMapper extends BaseMapper<AssessmentUser> {

	public boolean deleteByPlanCode(@Param("planCode") String planCode);
    
	public List<AssessmentUser> selectByPlanCode(@Param("planCode") String planCode);

}
