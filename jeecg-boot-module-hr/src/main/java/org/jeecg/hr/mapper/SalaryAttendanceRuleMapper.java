package org.jeecg.hr.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.hr.entity.SalaryAttendanceRule;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface SalaryAttendanceRuleMapper extends BaseMapper<SalaryAttendanceRule> {

	void deleteByItemCode(@Param("itemCode") String itemCode);

}
