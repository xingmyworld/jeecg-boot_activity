package org.jeecg.hr.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.hr.entity.AssessmentTaskResultSummary;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface AssessmentTaskResultSummaryMapper extends BaseMapper<AssessmentTaskResultSummary> {

}
