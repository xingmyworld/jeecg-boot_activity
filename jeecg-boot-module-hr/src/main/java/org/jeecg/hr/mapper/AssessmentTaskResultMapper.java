package org.jeecg.hr.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.hr.entity.AssessmentTaskResult;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface AssessmentTaskResultMapper extends BaseMapper<AssessmentTaskResult> {

	List<AssessmentTaskResult> selectByTaskCode(@Param("taskCode") String taskCode);

}
