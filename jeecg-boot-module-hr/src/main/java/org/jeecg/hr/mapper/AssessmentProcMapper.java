package org.jeecg.hr.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.hr.entity.AssessmentProc;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface AssessmentProcMapper extends BaseMapper<AssessmentProc> {

	public boolean deleteByPlanCode(@Param("planCode") String planCode);
    
	public List<AssessmentProc> selectByPlanCode(@Param("planCode") String planCode);

}
