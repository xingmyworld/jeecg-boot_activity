package org.jeecg.hr.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.hr.entity.SalaryTemplateDetail;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface SalaryTemplateDetailMapper extends BaseMapper<SalaryTemplateDetail> {

	public boolean deleteByTemplateCode(@Param("templateCode") String templateCode);
    
	public List<SalaryTemplateDetail> selectByTemplateCode(@Param("templateCode") String templateCode);

}
