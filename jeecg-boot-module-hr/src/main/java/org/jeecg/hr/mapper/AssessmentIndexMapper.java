package org.jeecg.hr.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.hr.entity.AssessmentIndex;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface AssessmentIndexMapper extends BaseMapper<AssessmentIndex> {

	public boolean deleteByPlanCode(@Param("planCode") String planCode);
    
	public List<AssessmentIndex> selectByPlanCode(@Param("planCode") String planCode);

}
