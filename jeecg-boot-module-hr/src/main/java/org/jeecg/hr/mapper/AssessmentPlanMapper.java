package org.jeecg.hr.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.hr.entity.AssessmentPlan;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface AssessmentPlanMapper extends BaseMapper<AssessmentPlan> {

	AssessmentPlan selectByPlanCode(@Param("planCode") String planCode);

}
