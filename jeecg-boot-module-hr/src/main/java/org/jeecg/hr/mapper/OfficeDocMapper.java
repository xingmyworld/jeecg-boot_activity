package org.jeecg.hr.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.hr.entity.OfficeDoc;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface OfficeDocMapper extends BaseMapper<OfficeDoc> {

}
