package org.jeecg.hr.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.hr.entity.AssessmentTask;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface AssessmentTaskMapper extends BaseMapper<AssessmentTask> {

	List<AssessmentTask> selectByInstanceCode(@Param("instanceCode") String instanceCode);

}
