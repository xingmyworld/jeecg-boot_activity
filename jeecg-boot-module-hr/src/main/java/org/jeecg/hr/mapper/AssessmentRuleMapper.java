package org.jeecg.hr.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.hr.entity.AssessmentRule;
import org.jeecg.hr.vo.AssessmentResultVo;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface AssessmentRuleMapper extends BaseMapper<AssessmentRule> {

	public boolean deleteByPlanCode(@Param("planCode") String planCode);
    
	public List<AssessmentRule> selectByPlanCode(@Param("planCode") String planCode);

	public List<AssessmentResultVo> selectResult(@Param("planCode") String planCode,@Param("instanceCode") String instanceCode);
}
