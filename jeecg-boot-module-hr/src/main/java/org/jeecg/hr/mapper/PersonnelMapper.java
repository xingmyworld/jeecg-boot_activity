package org.jeecg.hr.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.jeecg.hr.entity.Personnel;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

@Mapper
public interface PersonnelMapper extends BaseMapper<Personnel> {

}
