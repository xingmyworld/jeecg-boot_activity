package org.jeecg.hr.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.jeecg.autopoi.poi.excel.ExcelImportUtil;
import org.jeecg.autopoi.poi.excel.def.NormalExcelConstants;
import org.jeecg.autopoi.poi.excel.entity.ExportParams;
import org.jeecg.autopoi.poi.excel.entity.ImportParams;
import org.jeecg.autopoi.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.hr.entity.AssessmentIndex;
import org.jeecg.hr.entity.AssessmentPlan;
import org.jeecg.hr.entity.AssessmentProc;
import org.jeecg.hr.entity.AssessmentRule;
import org.jeecg.hr.entity.AssessmentTaskResultSummary;
import org.jeecg.hr.entity.AssessmentUser;
import org.jeecg.hr.service.AssessmentIndexService;
import org.jeecg.hr.service.AssessmentPlanService;
import org.jeecg.hr.service.AssessmentProcService;
import org.jeecg.hr.service.AssessmentRuleService;
import org.jeecg.hr.service.AssessmentTaskResultSummaryService;
import org.jeecg.hr.service.AssessmentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * 绩效考评计划
 * @author: MxpIO
 */
@Api(tags = "绩效考评计划")
@RestController
@RequestMapping("/hr/assessment/plan")
@Slf4j
public class AssessmentPlanController extends JeecgController<AssessmentPlan, AssessmentPlanService> {

	@Autowired
	private AssessmentPlanService assessmentPlanService;

	@Autowired
	private AssessmentIndexService assessmentIndexService;
	
	@Autowired
	private AssessmentProcService assessmentProcService;
	
	@Autowired
	private AssessmentRuleService assessmentRuleService;
	
	@Autowired
	private AssessmentUserService assessmentUserService;
	
	@Autowired
	private AssessmentTaskResultSummaryService assessmentTaskResultSummaryService;

	/*---------------------------------主表处理-begin-------------------------------------*/

	/**
	 * 分页列表查询
	 * 
	 * @param assessmentPlan
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "绩效考评计划-分页列表查询")
	@ApiOperation(value = "绩效考评计划-分页列表查询", notes = "绩效考评计划-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(AssessmentPlan assessmentPlan,
			@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize, HttpServletRequest req) {
		QueryWrapper<AssessmentPlan> queryWrapper = QueryGenerator.initQueryWrapper(assessmentPlan,
				req.getParameterMap());
		Page<AssessmentPlan> page = new Page<AssessmentPlan>(pageNo, pageSize);
		IPage<AssessmentPlan> pageList = assessmentPlanService.page(page, queryWrapper);
		assessmentPlanService.handleDetails(pageList);
		return Result.OK(pageList);
	}

	/**
	 * 添加
	 * 
	 * @param assessmentPlan
	 * @return
	 */
	@AutoLog(value = "绩效考评计划-添加")
	@ApiOperation(value = "绩效考评计划-添加", notes = "绩效考评计划-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody AssessmentPlan assessmentPlan) {
		assessmentPlanService.save(assessmentPlan);
		return Result.OK("添加成功！", assessmentPlan);
	}

	/**
	 * 编辑
	 * 
	 * @param assessmentPlan
	 * @return
	 */
	@AutoLog(value = "绩效考评计划-编辑")
	@ApiOperation(value = "绩效考评计划-编辑", notes = "绩效考评计划-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody AssessmentPlan assessmentPlan) {
		assessmentPlanService.updateById(assessmentPlan);
		return Result.OK("编辑成功!", assessmentPlan);
	}
	
	/**
	 * 编辑分数
	 * 
	 * @param assessmentTaskResultSummary
	 * @return
	 */
	@AutoLog(value = "绩效考评计划-编辑分数")
	@ApiOperation(value = "绩效考评计划-编辑分数", notes = "绩效考评计划-编辑分数")
	@PutMapping(value = "/editScore")
	public Result<?> editScore(@RequestBody AssessmentTaskResultSummary assessmentTaskResultSummary) {
		assessmentTaskResultSummaryService.updateById(assessmentTaskResultSummary);
		return Result.OK("编辑成功!", assessmentTaskResultSummary);
	}
	
	/**
	 * 批量编辑分数
	 * 
	 * @param assessmentTaskResultSummaryList
	 * @return
	 */
	@AutoLog(value = "绩效考评计划-批量编辑分数")
	@ApiOperation(value = "绩效考评计划-批量编辑分数", notes = "绩效考评计划-批量编辑分数")
	@PutMapping(value = "/editBatchScore")
	public Result<?> editBatchScore(@RequestBody List<AssessmentTaskResultSummary> assessmentTaskResultSummaryList) {
		assessmentTaskResultSummaryService.updateBatchById(assessmentTaskResultSummaryList);
		return Result.OK("编辑成功!", assessmentTaskResultSummaryList);
	}

	/**
	 * 添加
	 *
	 * @param assessmentPlan
	 * @return
	 */
	@AutoLog(value = "绩效考评计划-添加(主从)")
	@ApiOperation(value = "绩效考评计划-添加(主从)", notes = "绩效考评计划-添加(主从)")
	@PostMapping(value = "/addWithChildren")
	public Result<?> addWithChildren(@RequestBody AssessmentPlan assessmentPlan) {
		assessmentPlanService.saveAssessmentPlanWithDetail(assessmentPlan);
		return Result.OK("添加成功！", assessmentPlan);
	}

	/**
	 * 编辑
	 *
	 * @param assessmentPlan
	 * @return
	 */
	@AutoLog(value = "绩效考评计划-编辑(主从)")
	@ApiOperation(value = "绩效考评计划-编辑(主从)", notes = "绩效考评计划-编辑(主从)")
	@PutMapping(value = "/editWithChildren")
	public Result<?> editWithChildren(@RequestBody AssessmentPlan assessmentPlan) {
		AssessmentPlan assessmentPlanEntity = assessmentPlanService.getById(assessmentPlan.getId());
		if (assessmentPlanEntity == null) {
			return Result.error("未找到对应数据");
		}
		assessmentPlanService.updateAssessmentPlanWithDetail(assessmentPlan);
		return Result.OK("编辑成功!", assessmentPlan);
	}

	/**
	 * 通过id删除
	 * 
	 * @param id
	 * @return
	 */
	@AutoLog(value = "绩效考评计划-通过id删除")
	@ApiOperation(value = "绩效考评计划-通过id删除", notes = "绩效考评计划-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
		assessmentPlanService.delAssessmentPlan(id);
		return Result.OK("删除成功!", id);
	}

	/**
	 * 批量删除
	 * 
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "绩效考评计划-批量删除")
	@ApiOperation(value = "绩效考评计划-批量删除", notes = "绩效考评计划-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
		assessmentPlanService.delBatchMain(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}

	/**
	 * 导出
	 * 
	 * @return
	 */
	@RequestMapping(value = "/exportXls")
	public ModelAndView exportXls(HttpServletRequest request, AssessmentPlan assessmentPlan) {
		return super.exportXls(request, assessmentPlan, AssessmentPlan.class, "绩效考评计划");
	}

	/**
	 * 导入
	 * 
	 * @return
	 */
	@RequestMapping(value = "/importExcel", method = RequestMethod.POST)
	public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
		return super.importExcel(request, response, AssessmentPlan.class);
	}
	/*---------------------------------主表处理-end-------------------------------------*/

	/*--------------------------------子表处理-考评流程明细-begin----------------------------------------------*/
	/**
	 * 通过主表planCode查询
	 * 
	 * @return
	 */
	@AutoLog(value = "考评流程-通过planCode查询")
	@ApiOperation(value = "考评流程-通过planCode查询", notes = "考评流程-通过planCode查询")
	@GetMapping(value = "/listAssessmentProcByPlanCode")
	public Result<?> listAssessmentProcByMainId(AssessmentProc assessmentProc,
			@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize, HttpServletRequest req) {

		Object tamplate = req.getParameter("planCode");
		if(tamplate == null){
			return Result.OK();
		}
		QueryWrapper<AssessmentProc> queryWrapper = QueryGenerator.initQueryWrapper(assessmentProc,
				req.getParameterMap());
		Page<AssessmentProc> page = new Page<AssessmentProc>(pageNo, pageSize);
		IPage<AssessmentProc> pageList = assessmentProcService.page(page, queryWrapper);
		return Result.OK(pageList);
	}

	/**
	 * 添加
	 * 
	 * @param assessmentProc
	 * @return
	 */
	@AutoLog(value = "考评流程-添加")
	@ApiOperation(value = "考评流程-添加", notes = "考评流程-添加")
	@PostMapping(value = "/addAssessmentProc")
	public Result<?> addAssessmentProc(@RequestBody AssessmentProc assessmentProc) {
		assessmentProcService.save(assessmentProc);
		return Result.OK("添加成功！", assessmentProc);
	}

	/**
	 * 编辑
	 * 
	 * @param assessmentProc
	 * @return
	 */
	@AutoLog(value = "考评流程-编辑")
	@ApiOperation(value = "考评流程-编辑", notes = "考评流程-编辑")
	@PutMapping(value = "/editAssessmentProc")
	public Result<?> editAssessmentProc(@RequestBody AssessmentProc assessmentProc) {
		assessmentProcService.updateById(assessmentProc);
		return Result.OK("编辑成功!", assessmentProc);
	}

	/**
	 * 通过id删除
	 * 
	 * @param id
	 * @return
	 */
	@AutoLog(value = "考评流程-通过id删除")
	@ApiOperation(value = "考评流程-通过id删除", notes = "考评流程-通过id删除")
	@DeleteMapping(value = "/deleteAssessmentProc")
	public Result<?> deleteAssessmentProc(@RequestParam(name = "id", required = true) String id) {
		assessmentProcService.removeById(id);
		return Result.OK("删除成功!", id);
	}

	/**
	 * 批量删除
	 * 
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "考评流程-批量删除")
	@ApiOperation(value = "考评流程-批量删除", notes = "考评流程-批量删除")
	@DeleteMapping(value = "/deleteBatchAssessmentProc")
	public Result<?> deleteBatchAssessmentProc(@RequestParam(name = "ids", required = true) String ids) {
		assessmentProcService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!", null);
	}

	/**
	 * 导出
	 * 
	 * @return
	 */
	@RequestMapping(value = "/exportAssessmentProc")
	public ModelAndView exportAssessmentProc(HttpServletRequest request,
			AssessmentProc assessmentProc) {
		// Step.1 组装查询条件
		QueryWrapper<AssessmentProc> queryWrapper = QueryGenerator.initQueryWrapper(assessmentProc,
				request.getParameterMap());
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		// Step.2 获取导出数据
		List<AssessmentProc> pageList = assessmentProcService.list(queryWrapper);
		List<AssessmentProc> exportList = null;

		// 过滤选中数据
		String selections = request.getParameter("selections");
		if (oConvertUtils.isNotEmpty(selections)) {
			List<String> selectionList = Arrays.asList(selections.split(","));
			exportList = pageList.stream().filter(item -> selectionList.contains(item.getId()))
					.collect(Collectors.toList());
		} else {
			exportList = pageList;
		}

		// Step.3 AutoPoi 导出Excel
		ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		mv.addObject(NormalExcelConstants.FILE_NAME, "绩效指标"); // 此处设置的filename无效
																// ,前端会重更新设置一下
		mv.addObject(NormalExcelConstants.CLASS, AssessmentProc.class);
		mv.addObject(NormalExcelConstants.PARAMS,
				new ExportParams("绩效指标", "导出人:" + sysUser.getRealname(), "绩效指标"));
		mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		return mv;
	}

	/**
	 * 导入
	 * 
	 * @return
	 */
	@RequestMapping(value = "/importAssessmentProc/{planCode}")
	public Result<?> importAssessmentProc(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("planCode") String planCode) {
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			MultipartFile file = entity.getValue();// 获取上传文件对象
			ImportParams params = new ImportParams();
			params.setTitleRows(2);
			params.setHeadRows(1);
			params.setNeedSave(true);
			try {
				List<AssessmentProc> list = ExcelImportUtil.importExcel(file.getInputStream(),
						AssessmentProc.class, params);
				for (AssessmentProc temp : list) {
					temp.setPlanCode(planCode);
				}
				long start = System.currentTimeMillis();
				assessmentProcService.saveBatch(list);
				log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
				return Result.OK("文件导入成功！数据行数：" + list.size());
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				return Result.error("文件导入失败:" + e.getMessage());
			} finally {
				try {
					file.getInputStream().close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return Result.error("文件导入失败！");
	}

	/*--------------------------------子表处理-考评流程明细-end----------------------------------------------*/
	
	/*--------------------------------子表处理-绩效考评指标明细-begin----------------------------------------------*/
	/**
	 * 通过主表planCode查询
	 * 
	 * @return
	 */
	@AutoLog(value = "绩效指标-通过planCode查询")
	@ApiOperation(value = "绩效指标-通过planCode查询", notes = "绩效指标-通过planCode查询")
	@GetMapping(value = "/listAssessmentIndexByPlanCode")
	public Result<?> listAssessmentIndexByMainId(AssessmentIndex assessmentIndex,
			@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize, HttpServletRequest req) {

		Object tamplate = req.getParameter("planCode");
		if(tamplate == null){
			return Result.OK();
		}
		QueryWrapper<AssessmentIndex> queryWrapper = QueryGenerator.initQueryWrapper(assessmentIndex,
				req.getParameterMap());
		Page<AssessmentIndex> page = new Page<AssessmentIndex>(pageNo, pageSize);
		IPage<AssessmentIndex> pageList = assessmentIndexService.page(page, queryWrapper);
		return Result.OK(pageList);
	}

	/**
	 * 添加
	 * 
	 * @param assessmentIndex
	 * @return
	 */
	@AutoLog(value = "绩效指标-添加")
	@ApiOperation(value = "绩效指标-添加", notes = "绩效指标-添加")
	@PostMapping(value = "/addAssessmentIndex")
	public Result<?> addAssessmentIndex(@RequestBody AssessmentIndex assessmentIndex) {
		assessmentIndexService.save(assessmentIndex);
		return Result.OK("添加成功！", assessmentIndex);
	}

	/**
	 * 编辑
	 * 
	 * @param assessmentIndex
	 * @return
	 */
	@AutoLog(value = "绩效指标-编辑")
	@ApiOperation(value = "绩效指标-编辑", notes = "绩效指标-编辑")
	@PutMapping(value = "/editAssessmentIndex")
	public Result<?> editAssessmentIndex(@RequestBody AssessmentIndex assessmentIndex) {
		assessmentIndexService.updateById(assessmentIndex);
		return Result.OK("编辑成功!", assessmentIndex);
	}

	/**
	 * 通过id删除
	 * 
	 * @param id
	 * @return
	 */
	@AutoLog(value = "绩效指标-通过id删除")
	@ApiOperation(value = "绩效指标-通过id删除", notes = "绩效指标-通过id删除")
	@DeleteMapping(value = "/deleteAssessmentIndex")
	public Result<?> deleteAssessmentIndex(@RequestParam(name = "id", required = true) String id) {
		assessmentIndexService.removeById(id);
		return Result.OK("删除成功!", id);
	}

	/**
	 * 批量删除
	 * 
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "绩效指标-批量删除")
	@ApiOperation(value = "绩效指标-批量删除", notes = "绩效指标-批量删除")
	@DeleteMapping(value = "/deleteBatchAssessmentIndex")
	public Result<?> deleteBatchAssessmentIndex(@RequestParam(name = "ids", required = true) String ids) {
		assessmentIndexService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!", null);
	}

	/**
	 * 导出
	 * 
	 * @return
	 */
	@RequestMapping(value = "/exportAssessmentIndex")
	public ModelAndView exportAssessmentIndex(HttpServletRequest request,
			AssessmentIndex assessmentIndex) {
		// Step.1 组装查询条件
		QueryWrapper<AssessmentIndex> queryWrapper = QueryGenerator.initQueryWrapper(assessmentIndex,
				request.getParameterMap());
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		// Step.2 获取导出数据
		List<AssessmentIndex> pageList = assessmentIndexService.list(queryWrapper);
		List<AssessmentIndex> exportList = null;

		// 过滤选中数据
		String selections = request.getParameter("selections");
		if (oConvertUtils.isNotEmpty(selections)) {
			List<String> selectionList = Arrays.asList(selections.split(","));
			exportList = pageList.stream().filter(item -> selectionList.contains(item.getId()))
					.collect(Collectors.toList());
		} else {
			exportList = pageList;
		}

		// Step.3 AutoPoi 导出Excel
		ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		mv.addObject(NormalExcelConstants.FILE_NAME, "绩效指标"); // 此处设置的filename无效
																// ,前端会重更新设置一下
		mv.addObject(NormalExcelConstants.CLASS, AssessmentIndex.class);
		mv.addObject(NormalExcelConstants.PARAMS,
				new ExportParams("绩效指标", "导出人:" + sysUser.getRealname(), "绩效指标"));
		mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		return mv;
	}

	/**
	 * 导入
	 * 
	 * @return
	 */
	@RequestMapping(value = "/importAssessmentIndex/{planCode}")
	public Result<?> importAssessmentIndex(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("planCode") String planCode) {
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			MultipartFile file = entity.getValue();// 获取上传文件对象
			ImportParams params = new ImportParams();
			params.setTitleRows(2);
			params.setHeadRows(1);
			params.setNeedSave(true);
			try {
				List<AssessmentIndex> list = ExcelImportUtil.importExcel(file.getInputStream(),
						AssessmentIndex.class, params);
				for (AssessmentIndex temp : list) {
					temp.setPlanCode(planCode);
				}
				long start = System.currentTimeMillis();
				assessmentIndexService.saveBatch(list);
				log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
				return Result.OK("文件导入成功！数据行数：" + list.size());
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				return Result.error("文件导入失败:" + e.getMessage());
			} finally {
				try {
					file.getInputStream().close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return Result.error("文件导入失败！");
	}

	/*--------------------------------子表处理-绩效考评指标明细-end----------------------------------------------*/

/*--------------------------------子表处理-得分级别明细-begin----------------------------------------------*/
/**
 * 通过主表planCode查询
 * 
 * @return
 */
@AutoLog(value = "得分级别-通过planCode查询")
@ApiOperation(value = "得分级别-通过planCode查询", notes = "得分级别-通过planCode查询")
@GetMapping(value = "/listAssessmentRuleByPlanCode")
public Result<?> listAssessmentRuleByMainId(AssessmentRule assessmentRule,
		@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
		@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize, HttpServletRequest req) {

	Object tamplate = req.getParameter("planCode");
	if(tamplate == null){
		return Result.OK();
	}
	QueryWrapper<AssessmentRule> queryWrapper = QueryGenerator.initQueryWrapper(assessmentRule,
			req.getParameterMap());
	Page<AssessmentRule> page = new Page<AssessmentRule>(pageNo, pageSize);
	IPage<AssessmentRule> pageList = assessmentRuleService.page(page, queryWrapper);
	return Result.OK(pageList);
}

/**
 * 添加
 * 
 * @param assessmentRule
 * @return
 */
@AutoLog(value = "得分级别-添加")
@ApiOperation(value = "得分级别-添加", notes = "得分级别-添加")
@PostMapping(value = "/addAssessmentRule")
public Result<?> addAssessmentRule(@RequestBody AssessmentRule assessmentRule) {
	assessmentRuleService.save(assessmentRule);
	return Result.OK("添加成功！", assessmentRule);
}

/**
 * 编辑
 * 
 * @param assessmentRule
 * @return
 */
@AutoLog(value = "得分级别-编辑")
@ApiOperation(value = "得分级别-编辑", notes = "得分级别-编辑")
@PutMapping(value = "/editAssessmentRule")
public Result<?> editAssessmentRule(@RequestBody AssessmentRule assessmentRule) {
	assessmentRuleService.updateById(assessmentRule);
	return Result.OK("编辑成功!", assessmentRule);
}

/**
 * 通过id删除
 * 
 * @param id
 * @return
 */
@AutoLog(value = "得分级别-通过id删除")
@ApiOperation(value = "得分级别-通过id删除", notes = "得分级别-通过id删除")
@DeleteMapping(value = "/deleteAssessmentRule")
public Result<?> deleteAssessmentRule(@RequestParam(name = "id", required = true) String id) {
	assessmentRuleService.removeById(id);
	return Result.OK("删除成功!", id);
}

/**
 * 批量删除
 * 
 * @param ids
 * @return
 */
@AutoLog(value = "得分级别-批量删除")
@ApiOperation(value = "得分级别-批量删除", notes = "得分级别-批量删除")
@DeleteMapping(value = "/deleteBatchAssessmentRule")
public Result<?> deleteBatchAssessmentRule(@RequestParam(name = "ids", required = true) String ids) {
	assessmentRuleService.removeByIds(Arrays.asList(ids.split(",")));
	return Result.OK("批量删除成功!", null);
}

/**
 * 导出
 * 
 * @return
 */
@RequestMapping(value = "/exportAssessmentRule")
public ModelAndView exportAssessmentRule(HttpServletRequest request,
		AssessmentRule assessmentRule) {
	// Step.1 组装查询条件
	QueryWrapper<AssessmentRule> queryWrapper = QueryGenerator.initQueryWrapper(assessmentRule,
			request.getParameterMap());
	LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

	// Step.2 获取导出数据
	List<AssessmentRule> pageList = assessmentRuleService.list(queryWrapper);
	List<AssessmentRule> exportList = null;

	// 过滤选中数据
	String selections = request.getParameter("selections");
	if (oConvertUtils.isNotEmpty(selections)) {
		List<String> selectionList = Arrays.asList(selections.split(","));
		exportList = pageList.stream().filter(item -> selectionList.contains(item.getId()))
				.collect(Collectors.toList());
	} else {
		exportList = pageList;
	}

	// Step.3 AutoPoi 导出Excel
	ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
	mv.addObject(NormalExcelConstants.FILE_NAME, "得分级别"); // 此处设置的filename无效
															// ,前端会重更新设置一下
	mv.addObject(NormalExcelConstants.CLASS, AssessmentRule.class);
	mv.addObject(NormalExcelConstants.PARAMS,
			new ExportParams("得分级别", "导出人:" + sysUser.getRealname(), "得分级别"));
	mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
	return mv;
}

/**
 * 导入
 * 
 * @return
 */
@RequestMapping(value = "/importAssessmentRule/{planCode}")
public Result<?> importAssessmentRule(HttpServletRequest request, HttpServletResponse response,
		@PathVariable("planCode") String planCode) {
	MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
	Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
	for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
		MultipartFile file = entity.getValue();// 获取上传文件对象
		ImportParams params = new ImportParams();
		params.setTitleRows(2);
		params.setHeadRows(1);
		params.setNeedSave(true);
		try {
			List<AssessmentRule> list = ExcelImportUtil.importExcel(file.getInputStream(),
					AssessmentRule.class, params);
			for (AssessmentRule temp : list) {
				temp.setPlanCode(planCode);
			}
			long start = System.currentTimeMillis();
			assessmentRuleService.saveBatch(list);
			log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
			return Result.OK("文件导入成功！数据行数：" + list.size());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Result.error("文件导入失败:" + e.getMessage());
		} finally {
			try {
				file.getInputStream().close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	return Result.error("文件导入失败！");
}

/*--------------------------------子表处理-绩效考评指标明细-end----------------------------------------------*/

/*--------------------------------子表处理-被考评人明细-begin----------------------------------------------*/
/**
 * 通过主表planCode查询
 * 
 * @return
 */
@AutoLog(value = "被考评人-通过planCode查询")
@ApiOperation(value = "被考评人-通过planCode查询", notes = "被考评人-通过planCode查询")
@GetMapping(value = "/listAssessmentUserByPlanCode")
public Result<?> listAssessmentUserByMainId(AssessmentUser assessmentUser,
		@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
		@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize, HttpServletRequest req) {

	Object tamplate = req.getParameter("planCode");
	if(tamplate == null){
		return Result.OK();
	}
	QueryWrapper<AssessmentUser> queryWrapper = QueryGenerator.initQueryWrapper(assessmentUser,
			req.getParameterMap());
	Page<AssessmentUser> page = new Page<AssessmentUser>(pageNo, pageSize);
	IPage<AssessmentUser> pageList = assessmentUserService.page(page, queryWrapper);
	return Result.OK(pageList);
}

/**
 * 添加
 * 
 * @param assessmentUser
 * @return
 */
@AutoLog(value = "被考评人-添加")
@ApiOperation(value = "被考评人-添加", notes = "被考评人-添加")
@PostMapping(value = "/addAssessmentUser")
public Result<?> addAssessmentUser(@RequestBody AssessmentUser assessmentUser) {
	assessmentUserService.save(assessmentUser);
	return Result.OK("添加成功！", assessmentUser);
}

/**
 * 编辑
 * 
 * @param assessmentUser
 * @return
 */
@AutoLog(value = "被考评人-编辑")
@ApiOperation(value = "被考评人-编辑", notes = "被考评人-编辑")
@PutMapping(value = "/editAssessmentUser")
public Result<?> editAssessmentUser(@RequestBody AssessmentUser assessmentUser) {
	assessmentUserService.updateById(assessmentUser);
	return Result.OK("编辑成功!", assessmentUser);
}

/**
 * 通过id删除
 * 
 * @param id
 * @return
 */
@AutoLog(value = "被考评人-通过id删除")
@ApiOperation(value = "被考评人-通过id删除", notes = "被考评人-通过id删除")
@DeleteMapping(value = "/deleteAssessmentUser")
public Result<?> deleteAssessmentUser(@RequestParam(name = "id", required = true) String id) {
	assessmentUserService.removeById(id);
	return Result.OK("删除成功!", id);
}

/**
 * 批量删除
 * 
 * @param ids
 * @return
 */
@AutoLog(value = "被考评人-批量删除")
@ApiOperation(value = "被考评人-批量删除", notes = "被考评人-批量删除")
@DeleteMapping(value = "/deleteBatchAssessmentUser")
public Result<?> deleteBatchAssessmentUser(@RequestParam(name = "ids", required = true) String ids) {
	assessmentUserService.removeByIds(Arrays.asList(ids.split(",")));
	return Result.OK("批量删除成功!", null);
}

/**
 * 导出
 * 
 * @return
 */
@RequestMapping(value = "/exportAssessmentUser")
public ModelAndView exportAssessmentUser(HttpServletRequest request,
		AssessmentUser assessmentUser) {
	// Step.1 组装查询条件
	QueryWrapper<AssessmentUser> queryWrapper = QueryGenerator.initQueryWrapper(assessmentUser,
			request.getParameterMap());
	LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

	// Step.2 获取导出数据
	List<AssessmentUser> pageList = assessmentUserService.list(queryWrapper);
	List<AssessmentUser> exportList = null;

	// 过滤选中数据
	String selections = request.getParameter("selections");
	if (oConvertUtils.isNotEmpty(selections)) {
		List<String> selectionList = Arrays.asList(selections.split(","));
		exportList = pageList.stream().filter(item -> selectionList.contains(item.getId()))
				.collect(Collectors.toList());
	} else {
		exportList = pageList;
	}

	// Step.3 AutoPoi 导出Excel
	ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
	mv.addObject(NormalExcelConstants.FILE_NAME, "被考评人"); // 此处设置的filename无效
															// ,前端会重更新设置一下
	mv.addObject(NormalExcelConstants.CLASS, AssessmentUser.class);
	mv.addObject(NormalExcelConstants.PARAMS,
			new ExportParams("被考评人", "导出人:" + sysUser.getRealname(), "被考评人"));
	mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
	return mv;
}

/**
 * 导入
 * 
 * @return
 */
@RequestMapping(value = "/importAssessmentUser/{planCode}")
public Result<?> importAssessmentUser(HttpServletRequest request, HttpServletResponse response,
		@PathVariable("planCode") String planCode) {
	MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
	Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
	for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
		MultipartFile file = entity.getValue();// 获取上传文件对象
		ImportParams params = new ImportParams();
		params.setTitleRows(2);
		params.setHeadRows(1);
		params.setNeedSave(true);
		try {
			List<AssessmentUser> list = ExcelImportUtil.importExcel(file.getInputStream(),
					AssessmentUser.class, params);
			for (AssessmentUser temp : list) {
				temp.setPlanCode(planCode);
			}
			long start = System.currentTimeMillis();
			assessmentUserService.saveBatch(list);
			log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
			return Result.OK("文件导入成功！数据行数：" + list.size());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return Result.error("文件导入失败:" + e.getMessage());
		} finally {
			try {
				file.getInputStream().close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	return Result.error("文件导入失败！");
}

/*--------------------------------子表处理-绩效考评指标明细-end----------------------------------------------*/
}
