package org.jeecg.hr.entity;

import java.util.Date;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecg.common.system.base.entity.JeecgEntity;
import org.springframework.format.annotation.DateTimeFormat;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("hr_office_doc")
public class OfficeDoc extends JeecgEntity {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "文件编号")
	@Excel(name="文件编号",width=15)
	private String code;
	
	@ApiModelProperty(value = "文件标题")
	@Excel(name="文件标题",width=15)
	private String title;
	
	@ApiModelProperty(value = "发文日期")
	@Excel(name = "发文日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date outDate;
	
	@ApiModelProperty(value = "发往单位")
	@Excel(name="发往单位",width=15)
	@Dict(dictTable ="sys_depart",dicText = "depart_name",dicCode = "org_code")
	private String deptCode;
	
	@ApiModelProperty(value = "状态")
	@Excel(name="状态",width=15)
	private String status;
	
	@ApiModelProperty(value = "文件类型")
	@Excel(name="文件类型",width=15)
	private String type;
	
	@ApiModelProperty(value = "置顶")
	@Excel(name="置顶",width=15)
	private String topType;
	
	@ApiModelProperty(value = "文件")
	@Excel(name="文件",width=15)
	private String file;
	
	@ApiModelProperty(value = "附件")
	@Excel(name="附件",width=15)
	private String appendix;
	
	@ApiModelProperty(value = "签发人")
	@Excel(name="签发人",width=15)
	private String signer;
	
	@ApiModelProperty(value = "已读人员")
	private String readed;

}
