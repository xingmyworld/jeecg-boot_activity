package org.jeecg.hr.entity;

import java.math.BigDecimal;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("hr_insurance_template_detail")
public class InsuranceTemplateDetail extends JeecgEntity {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "方案编号")
	@Excel(name="方案编号",width=15)
	private String templateCode;
	
	@ApiModelProperty(value = "类型")
	@Excel(name="类型",width=15)
	private String insuranceType;
	
	@ApiModelProperty(value = "基数上限")
	@Excel(name="基数上限",width=15)
	private BigDecimal baseUpper;
	
	@ApiModelProperty(value = "基数下限")
	@Excel(name="基数下限",width=15)
	private BigDecimal baseLower;
	
	@ApiModelProperty(value = "个人比例")
	@Excel(name="个人比例",width=15)
	private BigDecimal personalScale;
	
	@ApiModelProperty(value = "公司比例")
	@Excel(name="公司比例",width=15)
	private BigDecimal companyScale;
	
	@ApiModelProperty(value = "个人常量")
	@Excel(name="个人常量",width=15)
	private BigDecimal personalConstant;
	
	@ApiModelProperty(value = "公司常量")
	@Excel(name="公司常量",width=15)
	private BigDecimal companyConstant;

}
