package org.jeecg.hr.entity;

import java.math.BigDecimal;
import java.util.List;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("hr_salary_item")
public class SalaryItem extends JeecgEntity {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "项目编号")
	@Excel(name="项目编号",width=15)
	private String itemCode;
	
	@ApiModelProperty(value = "项目名称")
	@Excel(name="项目名称",width=15)
	private String itemName;
	
	@ApiModelProperty(value = "核算周期")
	@Excel(name="核算周期",width=15)
	private String accountCycle;
	
	@ApiModelProperty(value = "考勤比例（>=）")
	@Excel(name="考勤比例（>=）",width=15)
	private BigDecimal attendanceScale;
	
	@ApiModelProperty(value = "是否计算试用期")
	@Excel(name="是否计算试用期",width=15)
	private String isProbation;
	
	@ApiModelProperty(value = "是否计算绩效")
	@Excel(name="是否计算绩效",width=15)
	private String isPerformance;
	
	@TableField(exist = false)
	private List<SalaryAttendanceRule> attendanceRules;
}