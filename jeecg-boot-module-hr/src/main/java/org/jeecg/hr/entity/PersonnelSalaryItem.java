package org.jeecg.hr.entity;

import java.math.BigDecimal;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("hr_personnel_salary_item")
public class PersonnelSalaryItem extends JeecgEntity {
	
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "系统账号")
	@Excel(name="系统账号",width=15)
	private String username;
	
	@ApiModelProperty(value = "模板明细ID")
	@Excel(name="模板明细ID",width=15)
	private String salaryTemplateDetailId;
	
	@ApiModelProperty(value = "执行金额")
	@Excel(name="执行金额",width=15)
	private BigDecimal amount;
	
	@TableField(exist = false)
	private SalaryTemplateDetail salaryTemplateDetail;
	
	@TableField(exist = false)
	private SalaryItem salaryItem;

}
