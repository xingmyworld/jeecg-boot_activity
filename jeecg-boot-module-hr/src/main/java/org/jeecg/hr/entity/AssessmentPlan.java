package org.jeecg.hr.entity;

import java.util.List;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("hr_assessment_plan")
public class AssessmentPlan extends JeecgEntity {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "计划编号")
	@Excel(name="计划编号",width=15)
	private String planCode;
	
	@ApiModelProperty(value = "计划名称")
	@Excel(name="计划名称",width=15)
	private String planName;
	
	@ApiModelProperty(value = "考核方式")
	@Excel(name="考核方式",width=15)
	private String planType;
	
	@ApiModelProperty(value = "核算周期")
	@Excel(name="核算周期",width=15)
	private String accountCycle;
	
	@ApiModelProperty(value = "是否自评")
	@Excel(name="是否自评",width=15)
	private Boolean selfAble;
	
	@ApiModelProperty(value = "说明")
	@Excel(name="说明",width=15)
	private String remark;
	
	@ApiModelProperty(value = "考评人数")
	@TableField(exist = false)
	private Integer planQuantity;
	
	@TableField(exist = false)
	private List<AssessmentIndex> indexes;
	
	@TableField(exist = false)
	private List<AssessmentProc> procs;
	
	@TableField(exist = false)
	private List<AssessmentUser> users;
	
	@TableField(exist = false)
	private List<AssessmentRule> rules;

}
