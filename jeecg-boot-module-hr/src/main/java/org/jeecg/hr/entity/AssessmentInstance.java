package org.jeecg.hr.entity;

import java.util.List;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("hr_assessment_instance")
public class AssessmentInstance extends JeecgEntity {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "计划编号")
	@Excel(name="计划编号",width=15)
	private String planCode;
	
	@ApiModelProperty(value = "任务编号")
	@Excel(name="任务编号",width=15)
	private String instanceCode;
	
	@ApiModelProperty(value = "任务名称")
	@Excel(name="任务名称",width=15)
	private String instanceName;
	
	@ApiModelProperty(value = "任务状态")
	@Excel(name="任务状态",width=15)
	private String instanceStatus;
	
	@ApiModelProperty(value = "说明")
	@Excel(name="说明",width=15)
	private String remark;
	
	@ApiModelProperty(value = "考评人数")
	@TableField(exist = false)
	private Integer planQuantity;
	
	@ApiModelProperty(value = "完成人数")
	@TableField(exist = false)
	private Integer completeQuantity;
	
	@TableField(exist = false)
	private List<AssessmentTask> tasks;

}
