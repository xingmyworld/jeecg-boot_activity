package org.jeecg.hr.entity;

import java.util.List;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@TableName("hr_insurance_template")
public class InsuranceTemplate extends JeecgEntity {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "方案编号")
	@Excel(name="方案编号",width=15)
	private String templateCode;
	
	@ApiModelProperty(value = "方案名称")
	@Excel(name="方案名称",width=15)
	private String templateName;
	
	@ApiModelProperty(value = "方案状态")
	@Excel(name="方案状态",width=15)
	private Boolean templateStatus;
	
	@ApiModelProperty(value = "缴纳城市")
	@Excel(name="缴纳城市",width=15)
	private String baseCity;
	
	@TableField(exist = false)
	private List<InsuranceTemplateDetail> insuranceTemplateDetails;

}
