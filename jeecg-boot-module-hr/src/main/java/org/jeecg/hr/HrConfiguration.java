package org.jeecg.hr;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@AutoConfigurationPackage
@MapperScan({"org.jeecg.hr.mapper"})
public class HrConfiguration {
	
}