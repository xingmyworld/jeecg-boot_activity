package org.jeecg.hr.service;

import java.util.List;

import org.jeecg.hr.entity.PersonnelInsuranceItem;

import com.baomidou.mybatisplus.extension.service.IService;

public interface PersonnelInsuranceItemService extends IService<PersonnelInsuranceItem> {

	public List<PersonnelInsuranceItem> selectByUsername(String username);

	public void handleTransientVariable(List<PersonnelInsuranceItem> records);
}
