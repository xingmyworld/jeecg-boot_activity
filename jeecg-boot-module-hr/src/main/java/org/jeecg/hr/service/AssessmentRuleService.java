package org.jeecg.hr.service;

import java.util.List;

import org.jeecg.hr.entity.AssessmentRule;

import com.baomidou.mybatisplus.extension.service.IService;

public interface AssessmentRuleService extends IService<AssessmentRule> {

	public List<AssessmentRule> selectByPlanCode(String planCode);

	public void saveResult(String planCode, String instanceCode);
}
