package org.jeecg.hr.service.impl;

import javax.transaction.Transactional;

import org.jeecg.hr.entity.SalaryAttendanceRule;
import org.jeecg.hr.entity.SalaryItem;
import org.jeecg.hr.mapper.SalaryAttendanceRuleMapper;
import org.jeecg.hr.mapper.SalaryItemMapper;
import org.jeecg.hr.service.SalaryItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class SalaryItemServiceImpl extends ServiceImpl<SalaryItemMapper, SalaryItem> implements SalaryItemService {
	
	@Autowired
	private SalaryItemMapper salaryItemMapper;
	
	@Autowired
	private SalaryAttendanceRuleMapper salaryAttendanceRuleMapper;

	@Override
	@Transactional
	public void saveWithDetail(SalaryItem salaryItem) {
		salaryItemMapper.insert(salaryItem);
		if(salaryItem.getAttendanceRules() != null || salaryItem.getAttendanceRules().size()>0){
			for(SalaryAttendanceRule entity : salaryItem.getAttendanceRules()){
				entity.setItemCode(salaryItem.getItemCode());
				salaryAttendanceRuleMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateByIdWithDetail(SalaryItem salaryItem) {
		salaryAttendanceRuleMapper.deleteByItemCode(salaryItem.getItemCode());
		
		salaryItemMapper.updateById(salaryItem);
		
		if(salaryItem.getAttendanceRules() != null || salaryItem.getAttendanceRules().size()>0){
			for(SalaryAttendanceRule entity : salaryItem.getAttendanceRules()){
				entity.setItemCode(salaryItem.getItemCode());
				salaryAttendanceRuleMapper.insert(entity);
			}
		}
	}
	
}
