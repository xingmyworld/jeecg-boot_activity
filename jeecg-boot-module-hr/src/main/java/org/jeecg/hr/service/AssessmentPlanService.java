package org.jeecg.hr.service;

import java.io.Serializable;
import java.util.Collection;

import org.jeecg.hr.entity.AssessmentPlan;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;

public interface AssessmentPlanService extends IService<AssessmentPlan> {

	/**
	 * 删除一对多
	 */
	public void delAssessmentPlan (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);

	/**
	 * 新增一对多
	 * @param assessmentPlan
	 */
	public void saveAssessmentPlanWithDetail(AssessmentPlan assessmentPlan);

	/**
	 * 修改一对多
	 * @param assessmentPlan
	 */
	public void updateAssessmentPlanWithDetail(AssessmentPlan assessmentPlan);

	public void handleDetails(IPage<AssessmentPlan> pageList);


}
