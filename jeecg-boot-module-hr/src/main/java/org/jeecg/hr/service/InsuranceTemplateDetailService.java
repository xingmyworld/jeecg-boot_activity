package org.jeecg.hr.service;

import java.util.List;

import org.jeecg.hr.entity.InsuranceTemplateDetail;

import com.baomidou.mybatisplus.extension.service.IService;

public interface InsuranceTemplateDetailService extends IService<InsuranceTemplateDetail> {

	public List<InsuranceTemplateDetail> selectByTemplateCode(String templateCode);
}
