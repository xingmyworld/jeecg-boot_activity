package org.jeecg.hr.service.impl;

import java.util.List;

import org.jeecg.hr.entity.InsuranceTemplateDetail;
import org.jeecg.hr.entity.PersonnelInsuranceItem;
import org.jeecg.hr.mapper.InsuranceTemplateDetailMapper;
import org.jeecg.hr.mapper.PersonnelInsuranceItemMapper;
import org.jeecg.hr.service.PersonnelInsuranceItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class PersonnelInsuranceItemServiceImpl extends ServiceImpl<PersonnelInsuranceItemMapper, PersonnelInsuranceItem> implements PersonnelInsuranceItemService {

	@Autowired
	private PersonnelInsuranceItemMapper personnelInsuranceItemMapper;
	
	@Autowired
	private InsuranceTemplateDetailMapper insuranceTemplateDetailMapper;
	
	@Override
	public List<PersonnelInsuranceItem> selectByUsername(String username) {
		return personnelInsuranceItemMapper.selectByUsername(username);
	}

	@Override
	public void handleTransientVariable(List<PersonnelInsuranceItem> records) {
		for(PersonnelInsuranceItem item : records){
			InsuranceTemplateDetail detail = insuranceTemplateDetailMapper.selectById(item.getInsuranceTemplateDetailId());
			item.setInsuranceTemplateDetail(detail);
		}		
	}

}
