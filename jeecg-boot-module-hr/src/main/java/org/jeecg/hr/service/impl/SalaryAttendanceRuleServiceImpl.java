package org.jeecg.hr.service.impl;

import org.jeecg.hr.entity.SalaryAttendanceRule;
import org.jeecg.hr.mapper.SalaryAttendanceRuleMapper;
import org.jeecg.hr.service.SalaryAttendanceRuleService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class SalaryAttendanceRuleServiceImpl extends ServiceImpl<SalaryAttendanceRuleMapper, SalaryAttendanceRule> implements SalaryAttendanceRuleService {

}
