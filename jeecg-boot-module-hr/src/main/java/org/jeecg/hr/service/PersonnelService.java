package org.jeecg.hr.service;

import java.io.Serializable;
import java.util.Collection;

import org.jeecg.common.system.base.service.JeecgService;
import org.jeecg.hr.entity.Personnel;

public interface PersonnelService extends JeecgService<Personnel> {

	void delPersonnel(String id);

	void delBatchMain(Collection<? extends Serializable> idList);

	void savePersonnelWithDetail(Personnel personnel);

	void updatePersonnelWithDetail(Personnel personnel);

}
