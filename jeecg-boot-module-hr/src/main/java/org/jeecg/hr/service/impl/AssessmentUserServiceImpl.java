package org.jeecg.hr.service.impl;

import java.util.List;

import org.jeecg.hr.entity.AssessmentUser;
import org.jeecg.hr.mapper.AssessmentUserMapper;
import org.jeecg.hr.service.AssessmentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class AssessmentUserServiceImpl extends ServiceImpl<AssessmentUserMapper, AssessmentUser> implements AssessmentUserService {

	@Autowired
	private AssessmentUserMapper assessmentUserMapper;
	
	@Override
	public List<AssessmentUser> selectByPlanCode(String planCode) {
		return assessmentUserMapper.selectByPlanCode(planCode);
	}

}
