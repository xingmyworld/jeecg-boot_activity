package org.jeecg.hr.service.impl;

import java.util.List;

import org.jeecg.hr.entity.InsuranceTemplateDetail;
import org.jeecg.hr.mapper.InsuranceTemplateDetailMapper;
import org.jeecg.hr.service.InsuranceTemplateDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class InsuranceTemplateDetailServiceImpl extends ServiceImpl<InsuranceTemplateDetailMapper, InsuranceTemplateDetail> implements InsuranceTemplateDetailService {

	@Autowired
	private InsuranceTemplateDetailMapper insuranceTemplateDetailMapper;
	
	@Override
	public List<InsuranceTemplateDetail> selectByTemplateCode(String templateCode) {
		return insuranceTemplateDetailMapper.selectByTemplateCode(templateCode);
	}

}
