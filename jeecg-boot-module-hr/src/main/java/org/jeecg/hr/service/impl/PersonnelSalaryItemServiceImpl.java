package org.jeecg.hr.service.impl;

import java.util.List;

import org.jeecg.hr.entity.PersonnelSalaryItem;
import org.jeecg.hr.entity.SalaryItem;
import org.jeecg.hr.entity.SalaryTemplateDetail;
import org.jeecg.hr.mapper.PersonnelSalaryItemMapper;
import org.jeecg.hr.mapper.SalaryItemMapper;
import org.jeecg.hr.mapper.SalaryTemplateDetailMapper;
import org.jeecg.hr.service.PersonnelSalaryItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class PersonnelSalaryItemServiceImpl extends ServiceImpl<PersonnelSalaryItemMapper, PersonnelSalaryItem> implements PersonnelSalaryItemService {

	@Autowired
	private PersonnelSalaryItemMapper personnelSalaryItemMapper;
	
	@Autowired
	private SalaryItemMapper salaryItemMapper;
	
	@Autowired
	private SalaryTemplateDetailMapper salaryTemplateDetailMapper;
	
	@Override
	public List<PersonnelSalaryItem> selectByUsername(String username) {
		return personnelSalaryItemMapper.selectByUsername(username);
	}

	@Override
	public void handleTransientVariable(List<PersonnelSalaryItem> records) {
		for(PersonnelSalaryItem item : records){
			SalaryTemplateDetail detail = salaryTemplateDetailMapper.selectById(item.getSalaryTemplateDetailId());
			QueryWrapper<SalaryItem> qw = new QueryWrapper<SalaryItem>();
			qw.eq("item_code", detail.getSalaryItemCode());
			SalaryItem salaryItem = salaryItemMapper.selectOne(qw);
			item.setSalaryItem(salaryItem);
			item.setSalaryTemplateDetail(detail);
		}
	}

}
