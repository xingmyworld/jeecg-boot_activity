package org.jeecg.weixin.common.util.http.jodd;

import jodd.http.HttpConnectionProvider;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import jodd.http.ProxyInfo;
import jodd.util.StringPool;
import org.jeecg.weixin.common.enums.WxType;
import org.jeecg.weixin.common.bean.result.WxMediaUploadResult;
import org.jeecg.weixin.common.error.WxError;
import org.jeecg.weixin.common.error.WxErrorException;
import org.jeecg.weixin.common.util.http.MediaUploadRequestExecutor;
import org.jeecg.weixin.common.util.http.RequestHttp;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * .
 *
 * @author ecoolper
 */
public class JoddHttpMediaUploadRequestExecutor extends MediaUploadRequestExecutor<HttpConnectionProvider, ProxyInfo> {
	public JoddHttpMediaUploadRequestExecutor(RequestHttp requestHttp) {
		super(requestHttp);
	}

	@Override
	public WxMediaUploadResult execute(String uri, File file, WxType wxType) throws WxErrorException, IOException {
		HttpRequest request = HttpRequest.post(uri);
		if (requestHttp.getRequestHttpProxy() != null) {
			requestHttp.getRequestHttpClient().useProxy(requestHttp.getRequestHttpProxy());
		}
		request.withConnectionProvider(requestHttp.getRequestHttpClient());
		request.form("media", file);
		HttpResponse response = request.send();
		response.charset(StandardCharsets.UTF_8.name());

		String responseContent = response.bodyText();
		WxError error = WxError.fromJson(responseContent, wxType);
		if (error.getErrorCode() != 0) {
			throw new WxErrorException(error);
		}
		return WxMediaUploadResult.fromJson(responseContent);
	}
}
