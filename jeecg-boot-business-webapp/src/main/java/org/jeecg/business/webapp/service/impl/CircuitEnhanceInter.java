package org.jeecg.business.webapp.service.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.modules.online.cgform.enhance.CgformEnhanceJavaInter;
import org.jeecg.modules.online.cgform.mapper.OnlCgformFieldMapper;
import org.jeecg.modules.online.config.exception.BusinessException;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

@Component("circuitEnhanceInter")
public class CircuitEnhanceInter implements CgformEnhanceJavaInter {

	@Override
	public int execute(String s, Map map) throws BusinessException {
		return 0;
	}

	@Override
	public int execute(String s, JSONObject jsonobject) throws BusinessException {
		System.out.println("--------java增强JSON："+ jsonobject.toJSONString());
		String lineCode = jsonobject.getString("line_code");
		String taskCode = jsonobject.getString("task_code");
		OnlCgformFieldMapper onlCgformFieldMapper = SpringContextUtils.getBean(OnlCgformFieldMapper.class);
		String selectSql = "Select * from xy_park_patrol_linedetail where line_code = '"+lineCode+"'";
		System.out.println("--------java增强Sql："+ jsonobject.toJSONString());
		List<Map<String, Object>> result = onlCgformFieldMapper.queryListBySql(selectSql);
		for(Map<String, Object> record : result){
			System.out.println("========================");
			String uuid = UUID.randomUUID().toString();
			StringBuffer insertSql = new StringBuffer("insert into xy_park_patrol_taskinfo (id, task_code, in_turn, patrol_code, patrol_name, zone, location) values ('"+uuid+"'");
			insertSql.append(",'"+taskCode+"'");
			insertSql.append(",'"+record.get("in_turn")+"'");
			insertSql.append(",'"+record.get("patrol_code")+"'");
			insertSql.append(",'"+record.get("patrol_name")+"'");
			insertSql.append(",'"+record.get("zone")+"'");
			insertSql.append(",'"+record.get("location")+"'");
			insertSql.append(")");
			System.out.println("----insertSql===" + insertSql.toString());
			onlCgformFieldMapper.saveFormData(insertSql.toString());
			System.out.println("========================");
		}
		
		return 1;
	}

}
