package org.jeecg.business.webapp.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.history.HistoricActivityInstance;
import org.jeecg.modules.jmreport.api.data.IDataSetFactory;
import org.jeecg.modules.jmreport.desreport.model.JmPage;
import org.springframework.stereotype.Component;

@Component("bpmnDataSet")
public class BpmnDataSet implements IDataSetFactory {
	
	ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

	@Override
	public List<Map<String, Object>> createData(Map<String, Object> param) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		List<HistoricActivityInstance> list = processEngine.getHistoryService()
                .createHistoricActivityInstanceQuery()
                .processInstanceId(param.get("processInstanceId").toString())
                .list();
		if (list != null && list.size() > 0) {
            for (HistoricActivityInstance hai : list) {
            	Map<String, Object> map = new HashMap<>();
            	map.put("taskId", hai.getTaskId());
            	map.put("activityName", hai.getActivityName());
            	map.put("assignee", hai.getAssignee());
            	result.add(map);
            }
        }

		return result;
	}

	@Override
	public JmPage createPageData(Map<String, Object> arg0) {
		return null;
	}

}
