package org.jeecg.business.webapp.service.impl;

import java.util.List;
import java.util.Map;

import org.jeecg.business.webapp.service.CustemService;
import org.jeecg.modules.online.cgform.mapper.OnlCgformFieldMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CustemServiceImpl implements CustemService {
	@Autowired
    private OnlCgformFieldMapper onlCgformFieldMapper;

	@Override
	public void updateFileDetail(String parentId) {
		String sql = "select file_code from xy_file_borrowdetail where borrow_code = '" + parentId +"'";
		log.info("借阅查询SQL===》"+sql);
		List<Map<String, Object>> codeMap = onlCgformFieldMapper.queryListBySql(sql);
		StringBuilder sb = new StringBuilder();
		codeMap.stream().forEach(action -> {
			sb.append("'" + action.get("file_code")+"',");
		});
		if(sb.length() > 0){
			sb.setLength(sb.length() - 1);
		}
		String updateSql = "update xy_file_detail set status = '2' where file_code in (" + sb.toString() + ")";
		log.info("借阅更新SQL===》"+updateSql);
		onlCgformFieldMapper.editFormData(updateSql);
	}

	@Override
	public void reBackFile(String code) {
		String updateSql = "update xy_file_detail set status = '1' where file_code ='"+ code +"'";
		log.info("归还更新SQL===》"+updateSql);
		onlCgformFieldMapper.editFormData(updateSql);
	}
}
