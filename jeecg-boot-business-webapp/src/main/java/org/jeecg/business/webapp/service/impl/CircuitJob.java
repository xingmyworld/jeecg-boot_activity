package org.jeecg.business.webapp.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.modules.online.cgform.mapper.OnlCgformFieldMapper;
import org.jeecg.modules.system.service.SnRuleService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;

public class CircuitJob implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		OnlCgformFieldMapper onlCgformFieldMapper = SpringContextUtils.getBean(OnlCgformFieldMapper.class);
		Date date = DateUtil.beginOfDay(DateUtil.offsetDay(new Date(), 1));
		String dateStr = DateUtil.format(date, DatePattern.NORM_DATE_PATTERN);
		SnRuleService snRuleService = SpringContextUtils.getBean(SnRuleService.class);
		String selectSql = "Select * from xy_park_patrol_plan where effective_date <= '"+dateStr+"' and expiration_date >= '"+dateStr+"'";
		List<Map<String, Object>> result = onlCgformFieldMapper.queryListBySql(selectSql);
		for(Map<String, Object> record : result){
			Long diffDay = DateUtil.betweenDay((Date) record.get("effective_date"), date, true);
			if(diffDay == 0 || diffDay%(int)record.get("plan_interval") == 0){
				String uuid = UUID.randomUUID().toString();
				String sn = snRuleService.execute("HNXYXG-${YYYY}${MM}${DD}-####", null) + "";
				StringBuffer insertSql = new StringBuffer("insert into xy_park_patrol_task (id, create_by, create_time, task_code, task_name, leading_cadre, line_code, line_name, is_inorder, patrol_personnel, estimate_time, allowable_error, remarks, plan_starttime, plan_code, plan_name) values ('"+uuid+"', '"+record.get("create_by") +"', '"+record.get("create_time")+"','"+sn+"'");
				insertSql.append(", '" + record.get("plan_name")+"'");
				insertSql.append(", '" + record.get("leading_cadre")+"'");
				insertSql.append(", '" + record.get("line_code")+"'");
				insertSql.append(", '" + record.get("line_name")+"'");
				insertSql.append(", '" + record.get("is_inorder")+"'");
				insertSql.append(", '" + record.get("patrol_personnel")+"'");
				insertSql.append(", '" + record.get("estimate_time")+"'");
				insertSql.append(", '" + record.get("allowable_error")+"'");
				insertSql.append(", '" + record.get("remarks")+"'");
				String starttime = DateUtil.format(DateUtil.offsetHour(date, (int) record.get("task_starttime")),DatePattern.NORM_DATETIME_PATTERN);
				insertSql.append(", '" + starttime+"'");
				insertSql.append(", '" + record.get("plan_code")+"'");
				insertSql.append(", '" + record.get("plan_name")+"'");
				insertSql.append(")");
				System.out.println("----insertSql===" + insertSql.toString());
				onlCgformFieldMapper.saveFormData(insertSql.toString());
				System.out.println("========================");
				
				String subSelectSql = "Select * from xy_park_patrol_linedetail where line_code = '"+record.get("line_code")+"'";
				List<Map<String, Object>> subresult = onlCgformFieldMapper.queryListBySql(subSelectSql);
				for(Map<String, Object> subRecord : subresult){
					System.out.println("========================");
					String subuuid = UUID.randomUUID().toString();
					StringBuffer subInsertSql = new StringBuffer("insert into xy_park_patrol_taskinfo (id, task_code, in_turn, patrol_code, patrol_name, zone, location) values ('"+subuuid+"'");
					subInsertSql.append(",'"+sn+"'");
					subInsertSql.append(",'"+subRecord.get("in_turn")+"'");
					subInsertSql.append(",'"+subRecord.get("patrol_code")+"'");
					subInsertSql.append(",'"+subRecord.get("patrol_name")+"'");
					subInsertSql.append(",'"+subRecord.get("zone")+"'");
					subInsertSql.append(",'"+subRecord.get("location")+"'");
					subInsertSql.append(")");
					System.out.println("----subInsertSql===" + subInsertSql.toString());
					onlCgformFieldMapper.saveFormData(subInsertSql.toString());
					System.out.println("========================");
				}
			}
		}
	}

}
