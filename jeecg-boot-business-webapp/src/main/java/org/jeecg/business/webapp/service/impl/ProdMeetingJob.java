package org.jeecg.business.webapp.service.impl;

import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.modules.online.cgform.mapper.OnlCgformFieldMapper;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class ProdMeetingJob implements Job {

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		OnlCgformFieldMapper onlCgformFieldMapper = SpringContextUtils.getBean(OnlCgformFieldMapper.class);
		String sql = "update qs_meet_info set meeting_status = '2' where meeting_status = '1' and start_time < NOW()";
		onlCgformFieldMapper.editFormData(sql);
	}

}
