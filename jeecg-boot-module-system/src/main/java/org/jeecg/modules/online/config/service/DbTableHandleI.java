package org.jeecg.modules.online.config.service;

import org.jeecg.modules.online.config.exception.DBException;
import org.jeecg.modules.online.config.util.ColumnMeta;

public interface DbTableHandleI {
    String getAddColumnSql(ColumnMeta columnMeta);

    String getReNameFieldName(ColumnMeta columnMeta);

    String getUpdateColumnSql(ColumnMeta cgformcolumnMeta, ColumnMeta datacolumnMeta) throws DBException;

    String getMatchClassTypeByDataType(String var1, int var2);

    String dropTableSQL(String tableName);

    String getDropColumnSql(String fieldName);

    String getCommentSql(ColumnMeta columnMeta);

    String getSpecialHandle(ColumnMeta cgformcolumnMeta, ColumnMeta datacolumnMeta);

    String dropIndexs(String indexName, String tableName);

    String countIndex(String indexName, String tableName);
}
