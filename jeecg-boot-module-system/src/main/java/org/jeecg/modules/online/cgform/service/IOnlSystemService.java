package org.jeecg.modules.online.cgform.service;

import org.jeecg.modules.online.cgform.entity.OnlSystem;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 子系统
 * @author: MxpIO
 * @version: V1.0
 */
public interface IOnlSystemService extends IService<OnlSystem> {

}
