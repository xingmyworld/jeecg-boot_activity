package org.jeecg.modules.online.cgform.converter.field;

import java.util.List;
import java.util.Map;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.vo.DictModel;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.online.cgform.converter.FieldCommentConverter;

import lombok.Data;

/**
 * @author wanheng
 */
@Data
public class TableFieldCommentConverter implements FieldCommentConverter {

    protected ISysBaseAPI sysBaseApi;
    protected String field;
    protected String table;
    protected String code;
    protected String text;

    public TableFieldCommentConverter() {
        this.sysBaseApi = SpringContextUtils.getBean(ISysBaseAPI.class);
    }

    public TableFieldCommentConverter(String table, String code, String text) {
        this();
        this.table = table;
        this.code = code;
        this.text = text;
    }


    @Override
    public String converterToVal(String txt) {
        if (oConvertUtils.isNotEmpty(txt)) {
            String s = this.text + "= '" + txt + "'";
            String s1;
            int where = this.table.indexOf("where");
            if (where > 0) {
                s1 = this.table.substring(0, where).trim();
                s = s + " and " + this.table.substring(where + 5);
            } else {
                s1 = this.table;
            }

            List<DictModel> filterTableDictInfo = this.sysBaseApi.queryFilterTableDictInfo(s1, this.text, this.code, s);
            if (filterTableDictInfo != null && filterTableDictInfo.size() > 0) {
                return filterTableDictInfo.get(0).getValue();
            }
        }

        return null;
    }

    @Override
    public String converterToTxt(String val) {
        if (oConvertUtils.isNotEmpty(val)) {
            String s = this.code + "= '" + val + "'";
            String s1;
            int where = this.table.indexOf("where");
            if (where > 0) {
                s1 = this.table.substring(0, where).trim();
                s = s + " and " + this.table.substring(where + 5);
            } else {
                s1 = this.table;
            }

            List<DictModel> queryFilterTableDictInfo = this.sysBaseApi.queryFilterTableDictInfo(s1, this.text, this.code, s);
            if (queryFilterTableDictInfo != null && queryFilterTableDictInfo.size() > 0) {
                return queryFilterTableDictInfo.get(0).getText();
            }
        }

        return null;
    }

    @Override
    public Map<String, String> getConfig() {
        return null;
    }
}

