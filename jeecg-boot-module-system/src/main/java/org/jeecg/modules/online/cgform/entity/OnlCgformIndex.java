package org.jeecg.modules.online.cgform.entity;

import java.io.Serializable;
import java.util.Date;

public class OnlCgformIndex
    implements Serializable
{

    public OnlCgformIndex()
    {
    }

    public String getId()
    {
        return id;
    }

    public String getCgformHeadId()
    {
        return cgformHeadId;
    }

    public String getIndexName()
    {
        return indexName;
    }

    public String getIndexField()
    {
        return indexField;
    }

    public String getIsDbSynch()
    {
        return isDbSynch;
    }

    public Integer getDelFlag()
    {
        return delFlag;
    }

    public String getIndexType()
    {
        return indexType;
    }

    public String getCreateBy()
    {
        return createBy;
    }

    public Date getCreateTime()
    {
        return createTime;
    }

    public String getUpdateBy()
    {
        return updateBy;
    }

    public Date getUpdateTime()
    {
        return updateTime;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setCgformHeadId(String cgformHeadId)
    {
        this.cgformHeadId = cgformHeadId;
    }

    public void setIndexName(String indexName)
    {
        this.indexName = indexName;
    }

    public void setIndexField(String indexField)
    {
        this.indexField = indexField;
    }

    public void setIsDbSynch(String isDbSynch)
    {
        this.isDbSynch = isDbSynch;
    }

    public void setDelFlag(Integer delFlag)
    {
        this.delFlag = delFlag;
    }

    public void setIndexType(String indexType)
    {
        this.indexType = indexType;
    }

    public void setCreateBy(String createBy)
    {
        this.createBy = createBy;
    }

    public void setCreateTime(Date createTime)
    {
        this.createTime = createTime;
    }

    public void setUpdateBy(String updateBy)
    {
        this.updateBy = updateBy;
    }

    public void setUpdateTime(Date updateTime)
    {
        this.updateTime = updateTime;
    }

    public boolean equals(Object o)
    {
        if(o == this)
            return true;
        if(!(o instanceof OnlCgformIndex))
            return false;
        OnlCgformIndex onlcgformindex = (OnlCgformIndex)o;
        if(!onlcgformindex.canEqual(this))
            return false;
        String s = getId();
        String s1 = onlcgformindex.getId();
        if(s != null ? !s.equals(s1) : s1 != null)
            return false;
        String s2 = getCgformHeadId();
        String s3 = onlcgformindex.getCgformHeadId();
        if(s2 != null ? !s2.equals(s3) : s3 != null)
            return false;
        String s4 = getIndexName();
        String s5 = onlcgformindex.getIndexName();
        if(s4 != null ? !s4.equals(s5) : s5 != null)
            return false;
        String s6 = getIndexField();
        String s7 = onlcgformindex.getIndexField();
        if(s6 != null ? !s6.equals(s7) : s7 != null)
            return false;
        String s8 = getIsDbSynch();
        String s9 = onlcgformindex.getIsDbSynch();
        if(s8 != null ? !s8.equals(s9) : s9 != null)
            return false;
        Integer integer = getDelFlag();
        Integer integer1 = onlcgformindex.getDelFlag();
        if(integer != null ? !integer.equals(integer1) : integer1 != null)
            return false;
        String s10 = getIndexType();
        String s11 = onlcgformindex.getIndexType();
        if(s10 != null ? !s10.equals(s11) : s11 != null)
            return false;
        String s12 = getCreateBy();
        String s13 = onlcgformindex.getCreateBy();
        if(s12 != null ? !s12.equals(s13) : s13 != null)
            return false;
        Date date = getCreateTime();
        Date date1 = onlcgformindex.getCreateTime();
        if(date != null ? !date.equals(date1) : date1 != null)
            return false;
        String s14 = getUpdateBy();
        String s15 = onlcgformindex.getUpdateBy();
        if(s14 != null ? !s14.equals(s15) : s15 != null)
            return false;
        Date date2 = getUpdateTime();
        Date date3 = onlcgformindex.getUpdateTime();
        return date2 != null ? date2.equals(date3) : date3 == null;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof OnlCgformIndex;
    }

    public int hashCode()
    {
        int i = 1;
        String s = getId();
        i = i * 59 + (s != null ? s.hashCode() : 43);
        String s1 = getCgformHeadId();
        i = i * 59 + (s1 != null ? s1.hashCode() : 43);
        String s2 = getIndexName();
        i = i * 59 + (s2 != null ? s2.hashCode() : 43);
        String s3 = getIndexField();
        i = i * 59 + (s3 != null ? s3.hashCode() : 43);
        String s4 = getIsDbSynch();
        i = i * 59 + (s4 != null ? s4.hashCode() : 43);
        Integer integer = getDelFlag();
        i = i * 59 + (integer != null ? integer.hashCode() : 43);
        String s5 = getIndexType();
        i = i * 59 + (s5 != null ? s5.hashCode() : 43);
        String s6 = getCreateBy();
        i = i * 59 + (s6 != null ? s6.hashCode() : 43);
        Date date = getCreateTime();
        i = i * 59 + (date != null ? date.hashCode() : 43);
        String s7 = getUpdateBy();
        i = i * 59 + (s7 != null ? s7.hashCode() : 43);
        Date date1 = getUpdateTime();
        i = i * 59 + (date1 != null ? date1.hashCode() : 43);
        return i;
    }

    public String toString()
    {
        return (new StringBuilder()).append("OnlCgformIndex(id=").append(getId()).append(", cgformHeadId=").append(getCgformHeadId()).append(", indexName=").append(getIndexName()).append(", indexField=").append(getIndexField()).append(", isDbSynch=").append(getIsDbSynch()).append(", delFlag=").append(getDelFlag()).append(", indexType=").append(getIndexType()).append(", createBy=").append(getCreateBy()).append(", createTime=").append(getCreateTime()).append(", updateBy=").append(getUpdateBy()).append(", updateTime=").append(getUpdateTime()).append(")").toString();
    }

    private static final long serialVersionUID = 1L;
    private String id;
    private String cgformHeadId;
    private String indexName;
    private String indexField;
    private String isDbSynch;
    private Integer delFlag;
    private String indexType;
    private String createBy;
    private Date createTime;
    private String updateBy;
    private Date updateTime;
}
