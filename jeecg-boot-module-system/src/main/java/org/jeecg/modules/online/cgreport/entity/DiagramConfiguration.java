package org.jeecg.modules.online.cgreport.entity;

import java.util.List;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 图表配置
 */
@Data
@TableName("CS_DIAGRAM_CONFIG")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "图表配置对象", description = "图表配置")
public class DiagramConfiguration extends JeecgEntity implements Cloneable {

	private static final long serialVersionUID = -8301271214331501163L;

	@Excel(name = "图表名称", width = 15)
	@ApiModelProperty(value = "图表名称")
	private String name;// "月度增长分析同比"

	@Excel(name = "编码", width = 15)
	@ApiModelProperty(value = "编码")
	private String code;// "monthly_growth_analysis"

	@Excel(name = "显示模板", width = 15)
	@ApiModelProperty(value = "显示模板")
	private String displayTemplate;// "double"

	@Excel(name = "X轴字段", width = 15)
	@ApiModelProperty(value = "X轴字段")
	@TableField(value = "X_AXIS_FIELD")
	private String xaxisField;// "month"

	@Excel(name = "Y轴字段", width = 15)
	@ApiModelProperty(value = "Y轴字段")
	@TableField(value = "Y_AXIS_FIELD")
	private String yaxisField; // "2018年月总收入,2019年月总收入"

	@Excel(name = "分组列", width = 15)
	@ApiModelProperty(value = "分组列")
	@TableField(value = "GROUP_FIELD")
	private String groupField;

	@Excel(name = "是否分组", width = 15)
	@ApiModelProperty(value = "是否分组")
	private Boolean isGroup;

	@Excel(name = "数据类型", width = 15)
	@ApiModelProperty(value = "数据类型")
	private String dataType;// "sql"

	@Excel(name = "图表类型", width = 15)
	@ApiModelProperty(value = "图表类型")
	private String graphType;// "table,line,bar,treeTable"

	@Excel(name = "描述", width = 15)
	@ApiModelProperty(value = "描述")
	private String content;// null

	@Excel(name = "查询SQL", width = 15)
	@ApiModelProperty(value = "查询SQL")
	private String cgrSql;// select * from dual

	@Excel(name = "JS增强", width = 15)
	@ApiModelProperty(value = "JS增强")
	private String extendJs;// null

	@Excel(name = "JS增强", width = 15)
	@ApiModelProperty(value = "JS增强")
	private String isCombination;// "combination"

	@Excel(name = "Y轴文字", width = 15)
	@ApiModelProperty(value = "Y轴文字")
	@TableField(value = "Y_AXIS_TEXT")
	private String yaxisText;// "-"
	
	@Excel(name = "是否根据X轴聚合", width = 15)
	@ApiModelProperty(value = "是否根据X轴聚合")
	@TableField(value = "AGGREGATE_")
	private Boolean aggregate;// "-"

	@Excel(name = "图表注释", width = 15)
	@ApiModelProperty(value = "图表注释")
	private String annotation;

	@Excel(name = "树形表格PID", width = 15)
	@ApiModelProperty(value = "树形表格PID")
	private String pid;
	
	@TableField(exist = false)
	private List<DiagramFieldConfiguration> diagramFieldConfigurationList;
	
	@Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
