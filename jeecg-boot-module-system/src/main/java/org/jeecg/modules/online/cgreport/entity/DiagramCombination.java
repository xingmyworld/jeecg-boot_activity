package org.jeecg.modules.online.cgreport.entity;

import java.util.List;

import org.jeecg.autopoi.poi.excel.annotation.Excel;
import org.jeecg.common.system.base.entity.JeecgEntity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 图表配置
 */

@Data
@TableName("CS_DIAGRAM_COMBINATION")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "图表组合对象", description = "图表组合")
public class DiagramCombination extends JeecgEntity {

	private static final long serialVersionUID = -321900087911168003L;

	@Excel(name = "组合报表名称", width = 15)
	@ApiModelProperty(value = "组合名称")
	private String templetName;

	@Excel(name = "组合报表编码", width = 15)
	@ApiModelProperty(value = "组合编码")
	private String templetCode;
	
	@Excel(name = "组合报表风格", width = 15)
	@ApiModelProperty(value = "组合报表风格")
	private String templetStyle;
	
	@TableField(exist = false)
	private List<DiagramCombinationDetail> diagramCombinationDetails;
	
	@TableField(exist = false)
	private List<DiagramCombinationQueryConfig> diagramCombinationQueryConfigs;

}
