package org.jeecg.modules.online.cgreport.service;


import java.util.List;

import org.jeecg.modules.online.cgreport.entity.DiagramCombination;

import com.baomidou.mybatisplus.extension.service.IService;

public interface IDiagramCombinationService extends IService<DiagramCombination> {
	
	void add(DiagramCombination diagramCombination);

	void edit(DiagramCombination diagramCombination);

	void deleteById(String id);

	void deleteBatch(String ids);
	
	List<DiagramCombination> selectBySql(String sql);

}
