package org.jeecg.modules.online.cgreport.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.online.cgreport.entity.DiagramCombinationDetail;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 组合图表配置
 */
public interface DiagramCombinationDetailMapper extends BaseMapper<DiagramCombinationDetail> {

	List<DiagramCombinationDetail> selectBySql(@Param("sql") String sql);

}
