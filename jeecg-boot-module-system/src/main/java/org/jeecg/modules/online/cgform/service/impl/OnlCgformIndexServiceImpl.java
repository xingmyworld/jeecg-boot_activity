package org.jeecg.modules.online.cgform.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import lombok.extern.slf4j.Slf4j;

import java.util.Iterator;
import java.util.List;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.modules.online.cgform.entity.OnlCgformIndex;
import org.jeecg.modules.online.cgform.mapper.OnlCgformHeadMapper;
import org.jeecg.modules.online.cgform.mapper.OnlCgformIndexMapper;
import org.jeecg.modules.online.cgform.service.IOnlCgformIndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("onlCgformIndexServiceImpl")
@Slf4j
public class OnlCgformIndexServiceImpl extends ServiceImpl<OnlCgformIndexMapper, OnlCgformIndex> implements IOnlCgformIndexService {
    @Autowired
    private OnlCgformHeadMapper onlCgformHeadMapper;

    public void createIndex(String code, String databaseType, String tbname) {
        LambdaQueryWrapper<OnlCgformIndex> var4 = new LambdaQueryWrapper<>();
        var4.eq(OnlCgformIndex::getCgformHeadId, code);
        List<OnlCgformIndex> var5 = this.list(var4);
        if (var5 != null && var5.size() > 0) {
            Iterator<OnlCgformIndex> var6 = var5.iterator();

            while(var6.hasNext()) {
                OnlCgformIndex var7 = (OnlCgformIndex)var6.next();
                if (!CommonConstant.DEL_FLAG_1.equals(var7.getDelFlag()) && "N".equals(var7.getIsDbSynch())) {
                    String var8 = "";
                    String var9 = var7.getIndexName();
                    String var10 = var7.getIndexField();
                    String var11 = "normal".equals(var7.getIndexType()) ? " index " : var7.getIndexType() + " index ";
                    byte var13 = -1;
                    switch(databaseType.hashCode()) {
                        case -1955532418:
                            if (databaseType.equals("ORACLE")) {
                                var13 = 1;
                            }
                            break;
                        case -1620389036:
                            if (databaseType.equals("POSTGRESQL")) {
                                var13 = 3;
                            }
                            break;
                        case 73844866:
                            if (databaseType.equals("MYSQL")) {
                                var13 = 0;
                            }
                            break;
                        case 912124529:
                            if (databaseType.equals("SQLSERVER")) {
                                var13 = 2;
                            }
                    }

                    switch(var13) {
                        case 0:
                            var8 = "create " + var11 + var9 + " on " + tbname + "(" + var10 + ")";
                            break;
                        case 1:
                            var8 = "create " + var11 + var9 + " on " + tbname + "(" + var10 + ")";
                            break;
                        case 2:
                            var8 = "create " + var11 + var9 + " on " + tbname + "(" + var10 + ")";
                            break;
                        case 3:
                            var8 = "create " + var11 + var9 + " on " + tbname + "(" + var10 + ")";
                            break;
                        default:
                            var8 = "create " + var11 + var9 + " on " + tbname + "(" + var10 + ")";
                    }

                    log.info(" 创建索引 executeDDL ：" + var8);
                    this.onlCgformHeadMapper.executeDDL(var8);
                    var7.setIsDbSynch("Y");
                    this.updateById(var7);
                }
            }
        }

    }

    public boolean isExistIndex(String countSql) {
        if (countSql == null) {
            return true;
        } else {
            Integer count = this.baseMapper.queryIndexCount(countSql);
            return count != null && count > 0;
        }
    }

    public List<OnlCgformIndex> getCgformIndexsByCgformId(String cgformId) {
        return this.baseMapper.selectList((new LambdaQueryWrapper<OnlCgformIndex>()).in(OnlCgformIndex::getCgformHeadId, new Object[]{cgformId}));
    }
}
