package org.jeecg.modules.online.cgform.enhance.demo;

import com.alibaba.fastjson.JSONObject;

import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import org.jeecg.modules.online.cgform.enhance.CgformEnhanceJavaInter;
import org.jeecg.modules.online.config.exception.BusinessException;
import org.springframework.stereotype.Component;

@Slf4j
@Component("cgformEnhanceImportDemo")
public class CgformEnhanceImportDemo implements CgformEnhanceJavaInter {

    public int execute(String tableName, JSONObject json) throws BusinessException {
        log.info("--------我是自定义java增强测试bean 导入示例-----------");
        log.info("--------当前tableName=>" + tableName);
        log.info("--------当前JSON数据=>" + json.toJSONString());
        if (json.get("ee") == null) {
            return 1;
        } else if (json.getString("ee").equals("hello")) {
            json.put("id", "testid123");
            json.put("ee", "java 增强导入测试修改");
            return 2;
        } else {
            return json.getString("ee").equals("ok") ? 0 : 1;
        }
    }

    public int execute(String tableName, Map<String, Object> map) throws BusinessException {
        return 1;
    }
}
