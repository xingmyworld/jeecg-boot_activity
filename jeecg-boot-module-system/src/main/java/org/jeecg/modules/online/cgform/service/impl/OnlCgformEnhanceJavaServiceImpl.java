package org.jeecg.modules.online.cgform.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.online.cgform.entity.OnlCgformEnhanceJava;
import org.jeecg.modules.online.cgform.mapper.OnlCgformEnhanceJavaMapper;
import org.jeecg.modules.online.cgform.service.IOnlCgformEnhanceJavaService;
import org.springframework.stereotype.Service;

@Service
public class OnlCgformEnhanceJavaServiceImpl extends ServiceImpl<OnlCgformEnhanceJavaMapper, OnlCgformEnhanceJava> implements IOnlCgformEnhanceJavaService {

}
