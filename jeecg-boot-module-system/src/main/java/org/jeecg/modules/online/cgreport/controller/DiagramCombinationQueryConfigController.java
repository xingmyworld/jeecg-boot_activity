package org.jeecg.modules.online.cgreport.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.online.cgreport.entity.DiagramCombinationQueryConfig;
import org.jeecg.modules.online.cgreport.service.IDiagramCombinationQueryConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * 查询配置
 * @author: jeecg-boot
 * @version: V1.0
 */
@Api(tags="查询配置")
@RestController
@RequestMapping("/diagram/diagramCombinationQueryConfig")
public class DiagramCombinationQueryConfigController extends JeecgController<DiagramCombinationQueryConfig, IDiagramCombinationQueryConfigService> {
	
	@Autowired
	private IDiagramCombinationQueryConfigService diagramCombinationQueryConfigService;
	
	/**
	 * 分页列表查询
	 *
	 * @param diagramCombinationQueryConfig
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "查询配置-分页列表查询")
	@ApiOperation(value="查询配置-分页列表查询", notes="查询配置-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(DiagramCombinationQueryConfig diagramCombinationQueryConfig,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<DiagramCombinationQueryConfig> queryWrapper = QueryGenerator.initQueryWrapper(diagramCombinationQueryConfig, req.getParameterMap());
		Page<DiagramCombinationQueryConfig> page = new Page<DiagramCombinationQueryConfig>(pageNo, pageSize);
		IPage<DiagramCombinationQueryConfig> pageList = diagramCombinationQueryConfigService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param diagramCombinationQueryConfig
	 * @return
	 */
	@AutoLog(value = "查询配置-添加")
	@ApiOperation(value="查询配置-添加", notes="查询配置-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody DiagramCombinationQueryConfig diagramCombinationQueryConfig) {
		diagramCombinationQueryConfigService.save(diagramCombinationQueryConfig);
		return Result.OK("添加成功！",null);
	}
	
	/**
	 *  编辑
	 *
	 * @param diagramCombinationQueryConfig
	 * @return
	 */
	@AutoLog(value = "查询配置-编辑")
	@ApiOperation(value="查询配置-编辑", notes="查询配置-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody DiagramCombinationQueryConfig diagramCombinationQueryConfig) {
		diagramCombinationQueryConfigService.updateById(diagramCombinationQueryConfig);
		return Result.OK("编辑成功!",null);
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "查询配置-通过id删除")
	@ApiOperation(value="查询配置-通过id删除", notes="查询配置-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		diagramCombinationQueryConfigService.removeById(id);
		return Result.OK("删除成功!",null);
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "查询配置-批量删除")
	@ApiOperation(value="查询配置-批量删除", notes="查询配置-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.diagramCombinationQueryConfigService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!",null);
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "查询配置-通过id查询")
	@ApiOperation(value="查询配置-通过id查询", notes="查询配置-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		DiagramCombinationQueryConfig diagramCombinationQueryConfig = diagramCombinationQueryConfigService.getById(id);
		if(diagramCombinationQueryConfig==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(diagramCombinationQueryConfig);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param diagramCombinationQueryConfig
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, DiagramCombinationQueryConfig diagramCombinationQueryConfig) {
        return super.exportXls(request, diagramCombinationQueryConfig, DiagramCombinationQueryConfig.class, "查询配置");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, DiagramCombinationQueryConfig.class);
    }

}
