package org.jeecg.modules.online.cgform.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import org.hibernate.HibernateException;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.online.cgform.entity.*;
import org.jeecg.modules.online.cgform.model.OnlGenerateModel;
import org.jeecg.modules.online.cgform.model.OnlCgformHeadModel;
import org.jeecg.modules.online.config.exception.BusinessException;
import org.jeecg.modules.online.config.exception.DBException;

public interface IOnlCgformHeadService
    extends IService<OnlCgformHead>
{

    public Result<?> addAll(OnlCgformHeadModel a);

    public Result<?> editAll(OnlCgformHeadModel a);

    public void doDbSynch(String code, String synMethod)
        throws HibernateException, IOException, TemplateException, SQLException, DBException;

    public void deleteRecordAndTable(String s)
        throws DBException, SQLException;

    public void deleteRecord(String s)
        throws DBException, SQLException;

    public List<Map<String, Object>> queryListData(String s);

    public OnlCgformEnhanceJs queryEnhance(String s, String s1);

    public void saveEnhance(OnlCgformEnhanceJs onlcgformenhancejs);

    public void editEnhance(OnlCgformEnhanceJs onlcgformenhancejs);

    public OnlCgformEnhanceSql queryEnhanceSql(String s, String s1);

    public void saveEnhance(OnlCgformEnhanceSql onlcgformenhancesql);

    public void editEnhance(OnlCgformEnhanceSql onlcgformenhancesql);

    public OnlCgformEnhanceJava queryEnhanceJava(OnlCgformEnhanceJava onlcgformenhancejava);

    public boolean checkOnlyEnhance(OnlCgformEnhanceJava onlcgformenhancejava);

    public void saveEnhance(OnlCgformEnhanceJava onlcgformenhancejava);

    public void editEnhance(OnlCgformEnhanceJava onlcgformenhancejava);

    public List<OnlCgformButton> queryButtonList(String code, boolean isListButton);

    public List<OnlCgformButton> queryButtonList(String code);

    public List<String> queryOnlinetables();

    public void saveDbTable2Online(String s);

    public JSONObject queryFormItem(OnlCgformHead onlcgformhead);

    public void saveManyFormData(String s, JSONObject jsonobject, String s1)
        throws DBException, BusinessException;

    public Map<String, Object> queryManyFormData(String code, String id)
        throws DBException;
    public Map<String, Object> queryManyFormDataToArr(String code, String id)
    	throws DBException ;

    public List<Map<String, Object>> queryManySubFormData(String table, String mainId)
        throws DBException;

    public Map<String, Object> querySubFormData(String table, String mainId)
        throws DBException;

    public void editManyFormData(String s, JSONObject jsonobject)
        throws DBException, BusinessException;

    public int executeEnhanceJava(String s, String s1, OnlCgformHead onlcgformhead, JSONObject jsonobject)
        throws BusinessException;

    public void executeEnhanceExport(OnlCgformHead onlcgformhead, List<Map<String,Object>> list)
        throws BusinessException;

    public void executeEnhanceList(OnlCgformHead onlcgformhead, String s, List<Map<String,Object>> list)
        throws BusinessException;

    public void executeEnhanceSql(String s, String s1, JSONObject jsonobject);

    public void executeCustomerButton(String s, String s1, String s2)
        throws BusinessException;

    public List<OnlCgformButton> queryValidButtonList(String headId);

    public OnlCgformEnhanceJs queryEnhanceJs(String s, String s1);

    public void deleteOneTableInfo(String s, String s1)
        throws BusinessException;

    public List<String> generateCode(OnlGenerateModel model)
        throws Exception;

    public List<String> generateOneToMany(OnlGenerateModel model)
        throws Exception;

    public void addCrazyFormData(String s, JSONObject jsonobject)
        throws DBException, UnsupportedEncodingException;

    public void editCrazyFormData(String s, JSONObject jsonobject)
        throws DBException, UnsupportedEncodingException;

    public Integer getMaxCopyVersion(String s);

    public void copyOnlineTableConfig(OnlCgformHead onlcgformhead)
        throws Exception;

    public void initCopyState(List<OnlCgformHead> list);

    public void deleteBatch(String s, String s1);

    public void updateParentNode(OnlCgformHead onlcgformhead, String s);

}
