package org.jeecg.modules.online.cgform.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;
import org.jeecg.modules.online.cgform.entity.OnlCgformHead;
import org.jeecg.modules.online.cgform.service.IOnlCgformFieldService;
import org.jeecg.modules.online.cgform.service.IOnlCgformHeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController("onlCgformFieldController")
@RequestMapping({"/online/cgform/field"})
@Slf4j
public class OnlCgformFieldController {
    @Autowired
    private IOnlCgformHeadService onlCgformHeadService;
    @Autowired
    private IOnlCgformFieldService onlCgformFieldService;

    public OnlCgformFieldController() {
    }

    @GetMapping({"/listByHeadCode"})
    public Result<?> listByHeadCode(@RequestParam("headCode") String headCode) {
        LambdaQueryWrapper<OnlCgformHead> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(OnlCgformHead::getTableName, headCode);
        OnlCgformHead onlCgformHead = (OnlCgformHead)this.onlCgformHeadService.getOne(wrapper);
        return onlCgformHead == null ? Result.error("不存在的code") : this.listByHeadId(onlCgformHead.getId());
    }

    @GetMapping({"/listByHeadId"})
    public Result<?> listByHeadId(@RequestParam("headId") String headId) {
        QueryWrapper<OnlCgformField> wrapper = new QueryWrapper<>();
        wrapper.eq("cgform_head_id", headId);
        wrapper.orderByAsc("order_num");
        List<OnlCgformField> var3 = this.onlCgformFieldService.list(wrapper);
        return Result.OK(var3);
    }

    @GetMapping({"/list"})
    public Result<IPage<OnlCgformField>> list(OnlCgformField entity, @RequestParam(name = "pageNo",defaultValue = "1") Integer pageNo, @RequestParam(name = "pageSize",defaultValue = "10") Integer pageSize, HttpServletRequest request) {
        Result<IPage<OnlCgformField>> result = new Result<>();
        QueryWrapper<OnlCgformField> wrapper = QueryGenerator.initQueryWrapper(entity, request.getParameterMap());
        Page<OnlCgformField> page = new Page<>(pageNo, pageSize);
        IPage<OnlCgformField> ipage = this.onlCgformFieldService.page(page, wrapper);
        result.setSuccess(true);
        result.setResult(ipage);
        return result;
    }

    @PostMapping({"/add"})
    public Result<OnlCgformField> add(@RequestBody OnlCgformField field) {
        Result<OnlCgformField> result = new Result<>();

        try {
            this.onlCgformFieldService.save(field);
            result.success("添加成功！");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.error500("操作失败");
        }

        return result;
    }

    @PutMapping({"/edit"})
    public Result<OnlCgformField> edit(@RequestBody OnlCgformField field) {
        Result<OnlCgformField> result = new Result<>();
        OnlCgformField onlCgformField = (OnlCgformField)this.onlCgformFieldService.getById(field.getId());
        if (onlCgformField == null) {
        	result.error500("未找到对应实体");
        } else {
            boolean b = this.onlCgformFieldService.updateById(field);
            if (b) {
            	result.success("修改成功!");
            }
        }

        return result;
    }

    @DeleteMapping({"/delete"})
    public Result<OnlCgformField> delete(@RequestParam(name = "id",required = true) String id) {
        Result<OnlCgformField> result = new Result<>();
        OnlCgformField var3 = (OnlCgformField)this.onlCgformFieldService.getById(id);
        if (var3 == null) {
        	result.error500("未找到对应实体");
        } else {
            boolean var4 = this.onlCgformFieldService.removeById(id);
            if (var4) {
            	result.success("删除成功!");
            }
        }

        return result;
    }

    @DeleteMapping({"/deleteBatch"})
    public Result<OnlCgformField> deleteBatch(@RequestParam(name = "ids",required = true) String ids) {
        Result<OnlCgformField> result = new Result<>();
        if (ids != null && !"".equals(ids.trim())) {
            this.onlCgformFieldService.removeByIds(Arrays.asList(ids.split(",")));
            result.success("删除成功!");
        } else {
        	result.error500("参数不识别！");
        }

        return result;
    }

    @GetMapping({"/queryById"})
    public Result<OnlCgformField> queryById(@RequestParam(name = "id",required = true) String id) {
        Result<OnlCgformField> result = new Result<>();
        OnlCgformField onlCgformField = (OnlCgformField)this.onlCgformFieldService.getById(id);
        if (onlCgformField == null) {
        	result.error500("未找到对应实体");
        } else {
        	result.setResult(onlCgformField);
        	result.setSuccess(true);
        }

        return result;
    }
}
