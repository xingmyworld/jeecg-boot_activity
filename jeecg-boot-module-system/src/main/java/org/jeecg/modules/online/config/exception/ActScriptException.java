package org.jeecg.modules.online.config.exception;

public class ActScriptException extends Exception {

    private static final long serialVersionUID = 1L;

    public ActScriptException(String msg) {
        super(msg);
    }
}
