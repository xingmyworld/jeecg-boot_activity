package org.jeecg.modules.online.cgform.converter.impl;

import org.jeecg.modules.online.cgform.converter.field.TableFieldCommentConverter;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;

public class SelSearchFieldCommentConverter extends TableFieldCommentConverter
{

    public SelSearchFieldCommentConverter(OnlCgformField onlcgformfield)
    {
        super(onlcgformfield.getDictTable(), onlcgformfield.getDictField(), onlcgformfield.getDictText());
    }
}
