package org.jeecg.modules.online.cgform.util;

import org.jeecg.codegenerate.generate.pojo.ColumnVo;

public class ColumnExpVo extends ColumnVo {

	private String readonly;

	public String getReadonly() {
		return readonly;
	}

	public void setReadonly(String readonly) {
		this.readonly = readonly;
	}
	
	
}
