package org.jeecg.modules.online.cgform.model;

import java.util.List;

import lombok.Data;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;
import org.jeecg.modules.online.cgform.entity.OnlCgformHead;
import org.jeecg.modules.online.cgform.entity.OnlCgformIndex;

@Data
public class OnlCgformHeadModel {
    private OnlCgformHead head;
    private List<OnlCgformField> fields;
    private List<String> deleteFieldIds;
    private List<OnlCgformIndex> indexs;
    private List<String> deleteIndexIds;
}
