package org.jeecg.modules.online.cgform.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class OnlCgformField
    implements Serializable
{
    private static final long serialVersionUID = 1L;
    private String id;
    private String cgformHeadId;
    private String dbFieldName;
    private String dbFieldTxt;
    private String dbFieldNameOld;
    private Integer dbIsKey;
    private Integer dbIsNull;
    private String dbType;
    private Integer dbLength;
    private Integer dbPointLength;
    private String dbDefaultVal;
    private String dictField;
    private String dictTable;
    private String dictText;
    private String fieldShowType;
    private String fieldHref;
    private Integer fieldLength;
    private String fieldValidType;
    private String fieldMustInput;
    private String fieldExtendJson;
    private String fieldDefaultValue;
    private Integer isQuery;
    private Integer isShowForm;
    private Integer isShowList;
    private Integer isReadOnly;
    private String queryMode;
    private String mainTable;
    private String mainField;
    private Integer orderNum;
    private String updateBy;
    private Date updateTime;
    private Date createTime;
    private String createBy;
    private String converter;
    private String queryConfigFlag;
    private String queryDefVal;
    private String queryDictText;
    private String queryDictField;
    private String queryDictTable;
    private String queryShowType;
    private String queryValidType;
    private String queryMustInput;
    private String sortFlag;

    /**
     * js增强
     */
    private String jsEnhance;
}
