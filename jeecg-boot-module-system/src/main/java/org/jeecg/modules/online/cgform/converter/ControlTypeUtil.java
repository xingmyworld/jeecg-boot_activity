package org.jeecg.modules.online.cgform.converter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jeecg.common.util.MyClassLoader;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.online.cgform.converter.impl.CatTreeFieldCommentConverter;
import org.jeecg.modules.online.cgform.converter.impl.CheckboxFieldCommentConverter;
import org.jeecg.modules.online.cgform.converter.impl.LinkDownFieldCommentConverter;
import org.jeecg.modules.online.cgform.converter.impl.PcaFieldCommentConverter;
import org.jeecg.modules.online.cgform.converter.impl.RadioFieldCommentConverter;
import org.jeecg.modules.online.cgform.converter.impl.SelDepartFieldCommentConverter;
import org.jeecg.modules.online.cgform.converter.impl.SelSearchFieldCommentConverter;
import org.jeecg.modules.online.cgform.converter.impl.SelTreeFieldCommentConverter;
import org.jeecg.modules.online.cgform.converter.impl.SelUserFieldCommentConverter;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NoArgsConstructor
public class ControlTypeUtil {

    public static FieldCommentConverter getFieldCommentConverter(OnlCgformField onlCgformField) {
        String fieldShowType = onlCgformField.getFieldShowType();
        FieldCommentConverter object = null;
        switch(fieldShowType) {
	        case "list":
            case "radio":
                object = new RadioFieldCommentConverter(onlCgformField);
                break;
            case "list_multi":
            case "checkbox":
                object = new CheckboxFieldCommentConverter(onlCgformField);
                break;
            case "sel_search":
                object = new SelSearchFieldCommentConverter(onlCgformField);
                break;
            case "sel_tree":
                object = new SelTreeFieldCommentConverter(onlCgformField);
                break;
            case "cat_tree":
                object = new CatTreeFieldCommentConverter(onlCgformField);
                break;
            case "link_down":
                object = new LinkDownFieldCommentConverter(onlCgformField);
                break;
            case "sel_depart":
                object = new SelDepartFieldCommentConverter(onlCgformField);
                break;
            case "sel_user":
                object = new SelUserFieldCommentConverter(onlCgformField);
                break;
            case "pca":
                object = new PcaFieldCommentConverter(onlCgformField);
                break;
            default:
        }

        return object;
    }

    public static Map<String, FieldCommentConverter> getFieldCommentConverters(List<OnlCgformField> onlCgformFields) {
        Map<String, FieldCommentConverter> hashMap = new HashMap<>();

        for (OnlCgformField onlCgformField : onlCgformFields) {
            FieldCommentConverter fieldCommentConverter;
            if (oConvertUtils.isNotEmpty(onlCgformField.getConverter())) {
                fieldCommentConverter = getFieldCommentConverter(onlCgformField.getConverter().trim());
            } else {
                fieldCommentConverter = getFieldCommentConverter(onlCgformField);
            }

            if (fieldCommentConverter != null) {
                hashMap.put(onlCgformField.getDbFieldName(), fieldCommentConverter);
            }
        }

        return hashMap;
    }

    private static FieldCommentConverter getFieldCommentConverter(String s) {
        Object object = null;
        if (s.indexOf(".") > 0) {
            try {
                object = MyClassLoader.getClassByScn(s).newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                log.error(e.getMessage(), e);
            }
        } else {
            object = SpringContextUtils.getBean(s);
        }

        if (object instanceof FieldCommentConverter) {
            return (FieldCommentConverter) object;
        } else {
            return null;
        }
    }
}
