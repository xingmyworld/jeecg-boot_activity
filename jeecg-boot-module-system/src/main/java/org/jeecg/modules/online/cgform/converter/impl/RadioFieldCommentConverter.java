package org.jeecg.modules.online.cgform.converter.impl;

import java.util.ArrayList;
import java.util.List;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.vo.DictModel;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.online.cgform.converter.field.FieldFieldCommentConverter;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;

public class RadioFieldCommentConverter extends FieldFieldCommentConverter {

	public RadioFieldCommentConverter(OnlCgformField onlCgformField) {
		ISysBaseAPI sysBaseApi = SpringContextUtils.getBean(ISysBaseAPI.class);
		String dictTable = onlCgformField.getDictTable();
		String dictText = onlCgformField.getDictText();
		String dictField = onlCgformField.getDictField();
		List<DictModel> arrayList = new ArrayList<>();
		if (oConvertUtils.isNotEmpty(dictTable)) {
			arrayList = sysBaseApi.queryTableDictItemsByCode(dictTable, dictText, dictField);
		} else if (oConvertUtils.isNotEmpty(dictField)) {
			arrayList = sysBaseApi.queryDictItemsByCode(dictField);
		}

		this.dictList = arrayList;
		this.filed = onlCgformField.getDbFieldName();
	}
}
