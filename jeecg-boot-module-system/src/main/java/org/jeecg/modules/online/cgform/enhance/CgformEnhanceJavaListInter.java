package org.jeecg.modules.online.cgform.enhance;

import java.util.List;
import java.util.Map;

import org.jeecg.modules.online.config.exception.BusinessException;

public interface CgformEnhanceJavaListInter
{

    public void execute(String s, List<Map<String,Object>> list)
        throws BusinessException;
}
