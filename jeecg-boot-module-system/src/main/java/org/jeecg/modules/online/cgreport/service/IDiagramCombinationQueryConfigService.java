package org.jeecg.modules.online.cgreport.service;

import org.jeecg.modules.online.cgreport.entity.DiagramCombinationQueryConfig;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 查询配置
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface IDiagramCombinationQueryConfigService extends IService<DiagramCombinationQueryConfig> {

}
