package org.jeecg.modules.online.cgform.service.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.hibernate.HibernateException;
import org.jeecg.codegenerate.generate.pojo.onetomany.ForeignKeyInfo;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.enums.CgformEnum;
import org.jeecg.common.util.MyClassLoader;
import org.jeecg.common.util.RestUtil;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.common.util.UUIDGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.online.cgform.enhance.CgformEnhanceJavaInter;
import org.jeecg.modules.online.cgform.enhance.CgformEnhanceJavaListInter;
import org.jeecg.modules.online.cgform.entity.OnlCgformButton;
import org.jeecg.modules.online.cgform.entity.OnlCgformEnhanceJava;
import org.jeecg.modules.online.cgform.entity.OnlCgformEnhanceJs;
import org.jeecg.modules.online.cgform.entity.OnlCgformEnhanceSql;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;
import org.jeecg.modules.online.cgform.entity.OnlCgformHead;
import org.jeecg.modules.online.cgform.entity.OnlCgformIndex;
import org.jeecg.modules.online.cgform.mapper.OnlCgformButtonMapper;
import org.jeecg.modules.online.cgform.mapper.OnlCgformEnhanceJavaMapper;
import org.jeecg.modules.online.cgform.mapper.OnlCgformEnhanceJsMapper;
import org.jeecg.modules.online.cgform.mapper.OnlCgformEnhanceSqlMapper;
import org.jeecg.modules.online.cgform.mapper.OnlCgformHeadMapper;
import org.jeecg.modules.online.cgform.model.FieldModel;
import org.jeecg.modules.online.cgform.model.OnlCgformHeadModel;
import org.jeecg.modules.online.cgform.model.OnlGenerateModel;
import org.jeecg.modules.online.cgform.service.IOnlCgformFieldService;
import org.jeecg.modules.online.cgform.service.IOnlCgformHeadService;
import org.jeecg.modules.online.cgform.service.IOnlCgformIndexService;
import org.jeecg.modules.online.cgform.util.ColumnExpVo;
import org.jeecg.modules.online.cgform.util.SqlSymbolUtil;
import org.jeecg.modules.online.config.db.DataBaseConfig;
import org.jeecg.modules.online.config.db.TableModel;
import org.jeecg.modules.online.config.exception.BusinessException;
import org.jeecg.modules.online.config.exception.DBException;
import org.jeecg.modules.online.config.service.DbTableHandleI;
import org.jeecg.modules.online.config.util.DbType;
import org.jeecg.modules.online.config.util.SqlHelper;
import org.jeecg.modules.online.config.util.TableUtil;
import org.jeecg.codegenerate.database.DbReadTableUtil;
import org.jeecg.codegenerate.generate.impl.CodeGenerateOne;
import org.jeecg.codegenerate.generate.impl.CodeGenerateOneToMany;
import org.jeecg.codegenerate.generate.pojo.ColumnVo;
import org.jeecg.codegenerate.generate.pojo.TableVo;
import org.jeecg.codegenerate.generate.pojo.onetomany.MainTableVo;
import org.jeecg.codegenerate.generate.pojo.onetomany.SubTableVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;

@Service("onlCgformHeadServiceImpl")
@Slf4j
public class OnlCgformHeadServiceImpl extends ServiceImpl<OnlCgformHeadMapper, OnlCgformHead> implements IOnlCgformHeadService {
    @Autowired
    private IOnlCgformFieldService fieldService;
    @Autowired
    private IOnlCgformIndexService indexService;
    @Autowired
    private OnlCgformEnhanceJsMapper onlCgformEnhanceJsMapper;
    @Autowired
    private OnlCgformButtonMapper onlCgformButtonMapper;
    @Autowired
    private OnlCgformEnhanceJavaMapper onlCgformEnhanceJavaMapper;
    @Autowired
    private OnlCgformEnhanceSqlMapper onlCgformEnhanceSqlMapper;
    @Autowired
    private DataBaseConfig dataBaseConfig;

    @Transactional(
            rollbackFor = {Exception.class}
    )
    public Result<?> addAll(OnlCgformHeadModel model) {
        String uuid = UUID.randomUUID().toString().replace("-", "");
        OnlCgformHead head = model.getHead();
        List<OnlCgformField> fields = model.getFields();
        List<OnlCgformIndex> indexs = model.getIndexs();
        head.setId(uuid);

        for(int i = 0; i < fields.size(); ++i) {
            OnlCgformField field = fields.get(i);
            field.setId((String)null);
            field.setCgformHeadId(uuid);
            if (field.getOrderNum() == null) {
            	field.setOrderNum(i);
            }

            this.a(field);
        }

        Iterator<OnlCgformIndex> iterator = indexs.iterator();

        while(iterator.hasNext()) {
            OnlCgformIndex index = iterator.next();
            index.setId(null);
            index.setCgformHeadId(uuid);
        }

        head.setIsDbSynch("N");
        head.setTableVersion(1);
        head.setCopyType(0);
        if (head.getTableType() == 3 && head.getTabOrderNum() == null) {
        	head.setTabOrderNum(1);
        }

        super.save(head);
        this.fieldService.saveBatch(fields);
        this.indexService.saveBatch(indexs);
        this.a(head, fields);
        return Result.OK("添加成功",null);
    }

    @Transactional(
            rollbackFor = {Exception.class}
    )
    public Result<?> editAll(OnlCgformHeadModel model) {
        OnlCgformHead onlCgformHead = model.getHead();
        OnlCgformHead var3 = (OnlCgformHead)super.getById(onlCgformHead.getId());
        if (var3 == null) {
            return Result.error("未找到对应实体");
        } else {
            String var4 = var3.getIsDbSynch();
            if (SqlSymbolUtil.onlCgformHeadEquals(var3, onlCgformHead)) {
                var4 = "N";
            }

            Integer var5 = var3.getTableVersion();
            if (var5 == null) {
                var5 = 1;
            }

            onlCgformHead.setTableVersion(var5 + 1);
            List<OnlCgformField> var6 = model.getFields();
            List<OnlCgformIndex> var7 = model.getIndexs();
            List<OnlCgformField> var8 = new ArrayList<>();
            List<OnlCgformField> var9 = new ArrayList<>();
            Iterator<OnlCgformField> var10 = var6.iterator();

            while(var10.hasNext()) {
                OnlCgformField var11 = (OnlCgformField)var10.next();
                String var12 = String.valueOf(var11.getId());
                this.a(var11);
                if (var12.length() == 32) {
                    var9.add(var11);
                } else {
                    String var13 = "_pk";
                    if (!var13.equals(var12)) {
                        var11.setId((String)null);
                        var11.setCgformHeadId(onlCgformHead.getId());
                        var8.add(var11);
                    }
                }
            }

            if (var8.size() > 0) {
                var4 = "N";
            }

            int var18 = 0;

            Iterator<OnlCgformField> var19;
            OnlCgformField var21;
            for(var19 = var9.iterator(); var19.hasNext(); this.fieldService.updateById(var21)) {
                var21 = var19.next();
                OnlCgformField var23 = (OnlCgformField)this.fieldService.getById(var21.getId());
                boolean var14 = SqlSymbolUtil.fieldEquals(var23, var21);
                if (var14) {
                    var4 = "N";
                }

                if ((var23.getOrderNum() == null ? 0 : var23.getOrderNum()) > var18) {
                    var18 = var23.getOrderNum();
                }
            }

            for(var19 = var8.iterator(); var19.hasNext(); this.fieldService.save(var21)) {
                var21 = (OnlCgformField)var19.next();
                if (var21.getOrderNum() == null) {
                    ++var18;
                    var21.setOrderNum(var18);
                }
            }

            List<OnlCgformIndex> var20 = this.indexService.getCgformIndexsByCgformId(onlCgformHead.getId());
            List<OnlCgformIndex> var22 = new ArrayList<>();
            List<OnlCgformIndex> var24 = new ArrayList<>();
            Iterator<OnlCgformIndex> var25 = var7.iterator();

            OnlCgformIndex var15;
            while(var25.hasNext()) {
                var15 = var25.next();
                String var16 = String.valueOf(var15.getId());
                if (var16.length() == 32) {
                    var24.add(var15);
                } else {
                    var15.setId((String)null);
                    var15.setCgformHeadId(onlCgformHead.getId());
                    var22.add(var15);
                }
            }

            var25 = var20.iterator();

            while(var25.hasNext()) {
                var15 = (OnlCgformIndex)var25.next();
                OnlCgformIndex finalVar1 = var15;
                boolean var26 = var7.stream().anyMatch((var1) -> {
                    return finalVar1.getId().equals(var1.getId());
                });
                if (!var26) {
                    var15.setDelFlag(CommonConstant.DEL_FLAG_1);
                    var24.add(var15);
                    var4 = "N";
                }
            }

            if (var22.size() > 0) {
                var4 = "N";
                this.indexService.saveBatch(var22);
            }

            for(var25 = var24.iterator(); var25.hasNext(); this.indexService.updateById(var15)) {
                var15 = (OnlCgformIndex)var25.next();
                OnlCgformIndex var27 = (OnlCgformIndex)this.indexService.getById(var15.getId());
                boolean var17 = SqlSymbolUtil.indexEquals(var27, var15);
                if (var17) {
                    var4 = "N";
                    var15.setIsDbSynch("N");
                }
            }

            if (model.getDeleteFieldIds().size() > 0) {
                var4 = "N";
                this.fieldService.removeByIds(model.getDeleteFieldIds());
            }

            onlCgformHead.setIsDbSynch(var4);
            super.updateById(onlCgformHead);
            this.a(onlCgformHead, var6);
            this.b(onlCgformHead, var6);
            return Result.OK("全部修改成功",null);
        }
    }

    public void doDbSynch(String code, String synMethod) throws HibernateException, IOException, TemplateException, SQLException, DBException {
        OnlCgformHead onlCgformHead = this.getById(code);
        if (onlCgformHead == null) {
            throw new DBException("实体配置不存在");
        } else {
            String tableName = onlCgformHead.getTableName();
            LambdaQueryWrapper<OnlCgformField> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(OnlCgformField::getCgformHeadId, code);
            wrapper.orderByAsc(OnlCgformField::getOrderNum);
            List<OnlCgformField> fields = this.fieldService.list(wrapper);
            TableModel tableModel = new TableModel();
            tableModel.setTableName(tableName);
            tableModel.setJformPkType(onlCgformHead.getIdType());
            tableModel.setJformPkSequence(onlCgformHead.getIdSequence());
            tableModel.setContent(onlCgformHead.getTableTxt());
            tableModel.setColumns(fields);
            tableModel.setDbConfig(this.dataBaseConfig);
            if ("normal".equals(synMethod)) {
                long t = System.currentTimeMillis();
                boolean isTableExist = TableUtil.isTableExist(tableName);
                log.info("==判断表是否存在消耗时间" + (System.currentTimeMillis() - t) + "毫秒");
                if (isTableExist) {
                    SqlHelper sqlHelper = new SqlHelper();
                    List<String> sqlList = sqlHelper.getUpdateTableSql(tableModel);
                    Iterator<String> sqlIterator = sqlList.iterator();

                    while(sqlIterator.hasNext()) {
                        String sql = sqlIterator.next();
                        if (!oConvertUtils.isEmpty(sql) && !oConvertUtils.isEmpty(sql.trim())) {
                            ((OnlCgformHeadMapper)this.baseMapper).executeDDL(sql);
                        }
                    }

                    List<OnlCgformIndex> indexes = this.indexService.list((new LambdaQueryWrapper<OnlCgformIndex>()).eq(OnlCgformIndex::getCgformHeadId, code));
                    Iterator<OnlCgformIndex> indexIterator = indexes.iterator();

                    label54:
                    while(true) {
                        OnlCgformIndex index;
                        do {
                            if (!indexIterator.hasNext()) {
                                break label54;
                            }

                            index = (OnlCgformIndex)indexIterator.next();
                        } while(!"N".equals(index.getIsDbSynch()) && !CommonConstant.DEL_FLAG_1.equals(index.getDelFlag()));

                        String countIndexSql = sqlHelper.getCountIndexSql(index.getIndexName(), tableName);
                        if (this.indexService.isExistIndex(countIndexSql)) {
                            String dropIndexsSql = sqlHelper.getDropIndexsSql(index.getIndexName(), tableName);

                            try {
                            	log.info("删除索引 executeDDL:" + dropIndexsSql);
                                ((OnlCgformHeadMapper)this.baseMapper).executeDDL(dropIndexsSql);
                                if (CommonConstant.DEL_FLAG_1.equals(index.getDelFlag())) {
                                    this.indexService.removeById(index.getId());
                                }
                            } catch (Exception e) {
                            	log.error("删除表【" + tableName + "】索引(" + index.getIndexName() + ")失败!", e);
                            }
                        } else if (CommonConstant.DEL_FLAG_1.equals(index.getDelFlag())) {
                            this.indexService.removeById(index.getId());
                        }
                    }
                } else {
                    SqlHelper.createTable(tableModel);
                }
            } else if ("force".equals(synMethod)) {
                DbTableHandleI dbTableHandle = TableUtil.getTableHandle();
                String dropTableSQL = dbTableHandle.dropTableSQL(tableName);
                ((OnlCgformHeadMapper)this.baseMapper).executeDDL(dropTableSQL);
                SqlHelper.createTable(tableModel);
            }

            this.indexService.createIndex(code, TableUtil.getDatabaseType(), tableName);
            onlCgformHead.setIsDbSynch("Y");
            this.updateById(onlCgformHead);
        }
    }

    public void deleteRecordAndTable(String id) throws DBException, SQLException {
        OnlCgformHead onlCgformHead = this.getById(id);
        if (onlCgformHead == null) {
            throw new DBException("实体配置不存在");
        } else {
            long t = System.currentTimeMillis();
            boolean isTableExist = TableUtil.isTableExist(onlCgformHead.getTableName());
            log.info("==判断表是否存在消耗时间 " + (System.currentTimeMillis() - t) + "毫秒");
            if (isTableExist) {
                String dropTableSQL = TableUtil.getTableHandle().dropTableSQL(onlCgformHead.getTableName());
                log.info(" 删除表  executeDDL： " + dropTableSQL);
                ((OnlCgformHeadMapper)this.baseMapper).executeDDL(dropTableSQL);
            }

            ((OnlCgformHeadMapper)this.baseMapper).deleteById(id);
            this.a(onlCgformHead);
        }
    }

    public void deleteRecord(String id) throws DBException, SQLException {
        OnlCgformHead onlCgformHead = this.getById(id);
        if (onlCgformHead == null) {
            throw new DBException("实体配置不存在");
        } else {
            ((OnlCgformHeadMapper)this.baseMapper).deleteById(id);
            this.a(onlCgformHead);
        }
    }

    private void a(OnlCgformHead onlCgformHead) {
        if (onlCgformHead.getTableType() == 3) {
            LambdaQueryWrapper<OnlCgformField> qw = (new LambdaQueryWrapper<OnlCgformField>()).eq(OnlCgformField::getCgformHeadId, onlCgformHead.getId());
            List<OnlCgformField> fields = this.fieldService.list(qw);
            String mainTable = null;
            Iterator<OnlCgformField> iterator = fields.iterator();
            if (iterator.hasNext()) {
                OnlCgformField field = iterator.next();
                mainTable = field.getMainTable();
            }

            if (oConvertUtils.isNotEmpty(mainTable)) {
                OnlCgformHead mainTableHead = ((OnlCgformHeadMapper)this.baseMapper).selectOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, mainTable));
                if (mainTableHead != null) {
                    String subTableStr = mainTableHead.getSubTableStr();
                    if (oConvertUtils.isNotEmpty(subTableStr)) {
                    	List<String> subTables = Arrays.asList(subTableStr.split(",")).stream().collect(Collectors.toList());
                    	subTables.remove(onlCgformHead.getTableName());
                        mainTableHead.setSubTableStr(String.join(",", subTables));
                        ((OnlCgformHeadMapper)this.baseMapper).updateById(mainTableHead);
                    }
                }
            }
        }

    }

    public List<Map<String, Object>> queryListData(String sql) {
        return this.baseMapper.queryList(sql);
    }

    public void saveEnhance(OnlCgformEnhanceJs onlCgformEnhanceJs) {
        this.onlCgformEnhanceJsMapper.insert(onlCgformEnhanceJs);
    }

    public OnlCgformEnhanceJs queryEnhance(String code, String type) {
        return this.onlCgformEnhanceJsMapper.selectOne(((LambdaQueryWrapper<OnlCgformEnhanceJs>)(new LambdaQueryWrapper<OnlCgformEnhanceJs>()).eq(OnlCgformEnhanceJs::getCgJsType, type)).eq(OnlCgformEnhanceJs::getCgformHeadId, code));
    }

    public void editEnhance(OnlCgformEnhanceJs onlCgformEnhanceJs) {
        this.onlCgformEnhanceJsMapper.updateById(onlCgformEnhanceJs);
    }

    public OnlCgformEnhanceSql queryEnhanceSql(String formId, String buttonCode) {
        return this.onlCgformEnhanceSqlMapper.selectOne(((LambdaQueryWrapper<OnlCgformEnhanceSql>)(new LambdaQueryWrapper<OnlCgformEnhanceSql>()).eq(OnlCgformEnhanceSql::getCgformHeadId, formId)).eq(OnlCgformEnhanceSql::getButtonCode, buttonCode));
    }

    public void saveEnhance(OnlCgformEnhanceSql onlCgformEnhanceSql) {
        this.onlCgformEnhanceSqlMapper.insert(onlCgformEnhanceSql);
    }

    public void editEnhance(OnlCgformEnhanceSql onlCgformEnhanceSql) {
        this.onlCgformEnhanceSqlMapper.updateById(onlCgformEnhanceSql);
    }

    public OnlCgformEnhanceJava queryEnhanceJava(OnlCgformEnhanceJava onlCgformEnhanceJava) {
        LambdaQueryWrapper<OnlCgformEnhanceJava> qw = new LambdaQueryWrapper<>();
        qw.eq(OnlCgformEnhanceJava::getButtonCode, onlCgformEnhanceJava.getButtonCode());
        qw.eq(OnlCgformEnhanceJava::getCgformHeadId, onlCgformEnhanceJava.getCgformHeadId());
        qw.eq(OnlCgformEnhanceJava::getCgJavaType, onlCgformEnhanceJava.getCgJavaType());
        qw.eq(OnlCgformEnhanceJava::getEvent, onlCgformEnhanceJava.getEvent());
        return (OnlCgformEnhanceJava)this.onlCgformEnhanceJavaMapper.selectOne(qw);
    }

    public void saveEnhance(OnlCgformEnhanceJava onlCgformEnhanceJava) {
        this.onlCgformEnhanceJavaMapper.insert(onlCgformEnhanceJava);
    }

    public void editEnhance(OnlCgformEnhanceJava onlCgformEnhanceJava) {
        this.onlCgformEnhanceJavaMapper.updateById(onlCgformEnhanceJava);
    }

    public List<OnlCgformButton> queryButtonList(String code, boolean isListButton) {
        LambdaQueryWrapper<OnlCgformButton> qw = new LambdaQueryWrapper<>();
        qw.eq(OnlCgformButton::getButtonStatus, "1");
        qw.eq(OnlCgformButton::getCgformHeadId, code);
        if (isListButton) {
        	qw.in(OnlCgformButton::getButtonStyle, new Object[]{"link", "button"});
        } else {
        	qw.eq(OnlCgformButton::getButtonStyle, "form");
        }

        qw.orderByAsc(OnlCgformButton::getOrderNum);
        return this.onlCgformButtonMapper.selectList(qw);
    }

    public List<OnlCgformButton> queryButtonList(String code) {
        LambdaQueryWrapper<OnlCgformButton> qw = new LambdaQueryWrapper<>();
        qw.eq(OnlCgformButton::getButtonStatus, "1");
        qw.eq(OnlCgformButton::getCgformHeadId, code);
        qw.orderByAsc(OnlCgformButton::getOrderNum);
        return this.onlCgformButtonMapper.selectList(qw);
    }

    public boolean checkOnlyEnhance(OnlCgformEnhanceJava onlCgformEnhanceJava) {
        LambdaQueryWrapper<OnlCgformEnhanceJava> qw = new LambdaQueryWrapper<>();
        qw.eq(OnlCgformEnhanceJava::getButtonCode, onlCgformEnhanceJava.getButtonCode());
        qw.eq(OnlCgformEnhanceJava::getCgformHeadId, onlCgformEnhanceJava.getCgformHeadId());
        Integer count = this.onlCgformEnhanceJavaMapper.selectCount(qw);
        if (count != null) {
            if (count == 1 && oConvertUtils.isEmpty(onlCgformEnhanceJava.getId())) {
                return false;
            }

            if (count == 2) {
                return false;
            }
        }

        return true;
    }

    public List<String> queryOnlinetables() {
        return this.baseMapper.queryOnlinetables();
    }

    @Transactional(
            rollbackFor = {Exception.class}
    )
    public void saveDbTable2Online(String tbname) {
        OnlCgformHead head = new OnlCgformHead();
        head.setTableType(1);
        head.setIsCheckbox("Y");
        head.setIsDbSynch("Y");
        head.setIsTree("N");
        head.setIsPage("Y");
        head.setQueryMode("group");
        head.setTableName(tbname.toLowerCase());
        head.setTableTxt(tbname);
        head.setTableVersion(1);
        head.setFormTemplate("1");
        head.setCopyType(0);
        head.setScroll(1);
        head.setThemeTemplate("normal");
        String uuid = UUIDGenerator.generate();
        head.setId(uuid);
        List<OnlCgformField> var4 = new ArrayList<>();

        try {
            List<ColumnVo> columns = DbReadTableUtil.getColumns(tbname);

            for(int i = 0; i < columns.size(); ++i) {
                ColumnVo column = columns.get(i);
                log.info("  columnt : " + column.toString());
                String fieldDbName = column.getFieldDbName();
                OnlCgformField field = new OnlCgformField();
                field.setCgformHeadId(uuid);
                field.setDbFieldNameOld(column.getFieldDbName().toLowerCase());
                field.setDbFieldName(column.getFieldDbName().toLowerCase());
                if (oConvertUtils.isNotEmpty(column.getFiledComment())) {
                	field.setDbFieldTxt(column.getFiledComment());
                } else {
                	field.setDbFieldTxt(column.getFieldName());
                }

                field.setDbIsKey(0);
                field.setIsShowForm(1);
                field.setIsQuery(0);
                field.setFieldMustInput("0");
                field.setIsShowList(1);
                field.setOrderNum(i + 1);
                field.setQueryMode("single");
                field.setDbLength(oConvertUtils.getInt(column.getPrecision()));
                field.setFieldLength(120);
                field.setDbPointLength(oConvertUtils.getInt(column.getScale()));
                field.setFieldShowType("text");
                field.setDbIsNull("Y".equals(column.getNullable()) ? 1 : 0);
                field.setIsReadOnly(0);
                if ("id".equalsIgnoreCase(fieldDbName)) {
                    String[] var10 = new String[]{"java.lang.Integer", "java.lang.Long"};
                    String var11 = column.getFieldType();
                    if (Arrays.asList(var10).contains(var11)) {
                    	head.setIdType("NATIVE");
                    } else {
                    	head.setIdType("UUID");
                    }

                    field.setDbIsKey(1);
                    field.setIsShowForm(0);
                    field.setIsShowList(0);
                    field.setIsReadOnly(1);
                }

                if ("java.lang.Integer".equalsIgnoreCase(column.getFieldType())) {
                	field.setDbType(DbType.INT);
                } else if ("java.lang.Long".equalsIgnoreCase(column.getFieldType())) {
                	field.setDbType(DbType.INT);
                } else if ("java.util.Date".equalsIgnoreCase(column.getFieldType())) {
                	field.setDbType(DbType.DATE);
                	field.setFieldShowType("date");
                } else if (!"java.lang.Double".equalsIgnoreCase(column.getFieldType()) && !"java.lang.Float".equalsIgnoreCase(column.getFieldType())) {
                    if (!"java.math.BigDecimal".equalsIgnoreCase(column.getFieldType()) && !"BigDecimal".equalsIgnoreCase(column.getFieldType())) {
                        if (!"byte[]".equalsIgnoreCase(column.getFieldType()) && !column.getFieldType().contains("blob")) {
                            if (!"java.lang.Object".equals(column.getFieldType()) || !"text".equalsIgnoreCase(column.getFieldDbType()) && !"ntext".equalsIgnoreCase(column.getFieldDbType())) {
                                if ("java.lang.Object".equals(column.getFieldType()) && "image".equalsIgnoreCase(column.getFieldDbType())) {
                                	field.setDbType(DbType.BLOB);
                                } else {
                                	field.setDbType(DbType.STRING);
                                }
                            } else {
                            	field.setDbType(DbType.TEXT);
                            	field.setFieldShowType(DbType.TEXT_AREA);
                            }
                        } else {
                        	field.setDbType(DbType.BLOB);
                            column.setCharmaxLength((String)null);
                        }
                    } else {
                    	field.setDbType(DbType.BIG_DECIMAL);
                    }
                } else {
                	field.setDbType(DbType.DOUBLE);
                }

                if (oConvertUtils.isEmpty(column.getPrecision()) && oConvertUtils.isNotEmpty(column.getCharmaxLength())) {
                    if (Long.valueOf(column.getCharmaxLength()) >= 3000L) {
                    	field.setDbType(DbType.TEXT);
                    	field.setFieldShowType("textarea");

                        try {
                        	field.setDbLength(Integer.valueOf(column.getCharmaxLength()));
                        } catch (Exception var12) {
                        	log.error(var12.getMessage(), var12);
                        }
                    } else {
                    	field.setDbLength(Integer.valueOf(column.getCharmaxLength()));
                    }
                } else {
                    if (oConvertUtils.isNotEmpty(column.getPrecision())) {
                    	field.setDbLength(Integer.valueOf(column.getPrecision()));
                    } else if (field.getDbType().equals(DbType.INT)) {
                    	field.setDbLength(10);
                    }

                    if (oConvertUtils.isNotEmpty(column.getScale())) {
                    	field.setDbPointLength(Integer.valueOf(column.getScale()));
                    }
                }

                if (oConvertUtils.getInt(column.getPrecision()) == -1 && oConvertUtils.getInt(column.getScale()) == 0) {
                	field.setDbType(DbType.TEXT);
                }

                if (DbType.BLOB.equals(field.getDbType()) || DbType.TEXT.equals(field.getDbType()) || DbType.DATE.equals(field.getDbType())) {
                	field.setDbLength(0);
                	field.setDbPointLength(0);
                }

                var4.add(field);
            }
        } catch (Exception e) {
        	log.error(e.getMessage(), e);
        }

        if (oConvertUtils.isEmpty(head.getFormCategory())) {
        	head.setFormCategory("bdfl_include");
        }

        this.save(head);
        this.fieldService.saveBatch(var4);
    }

    private void a(OnlCgformHead head, List<OnlCgformField> fields) {
        if (head.getTableType() == 3) {
        	head = ((OnlCgformHeadMapper)this.baseMapper).selectById(head.getId());

            for(int i = 0; i < fields.size(); ++i) {
                OnlCgformField field = fields.get(i);
                String mainTable = field.getMainTable();
                if (!oConvertUtils.isEmpty(mainTable)) {
                    OnlCgformHead var6 = ((OnlCgformHeadMapper)this.baseMapper).selectOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, mainTable));
                    if (var6 != null) {
                        String var7 = var6.getSubTableStr();
                        if (oConvertUtils.isEmpty(var7)) {
                            var7 = head.getTableName();
                        } else if (var7.indexOf(head.getTableName()) < 0) {
                            List<String> var8 = new ArrayList<>(Arrays.asList(var7.split(",")));

                            for(int var9 = 0; var9 < var8.size(); ++var9) {
                                String var10 = (String)var8.get(var9);
                                OnlCgformHead var11 = ((OnlCgformHeadMapper)this.baseMapper).selectOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, var10));
                                if (var11 != null && head.getTabOrderNum() < oConvertUtils.getInt(var11.getTabOrderNum(), 0)) {
                                    var8.add(var9, head.getTableName());
                                    break;
                                }
                            }

                            if (var8.indexOf(head.getTableName()) < 0) {
                                var8.add(head.getTableName());
                            }

                            var7 = String.join(",", var8);
                        }

                        var6.setSubTableStr(var7);
                        this.baseMapper.updateById(var6);
                        break;
                    }
                }
            }
        } else {
            List<OnlCgformHead> var12 = this.baseMapper.selectList((new LambdaQueryWrapper<OnlCgformHead>()).like(OnlCgformHead::getSubTableStr, head.getTableName()));
            if (var12 != null && var12.size() > 0) {
                Iterator<OnlCgformHead> var13 = var12.iterator();

                while(var13.hasNext()) {
                    OnlCgformHead var14 = var13.next();
                    String var15 = var14.getSubTableStr();
                    if (var14.getSubTableStr().equals(head.getTableName())) {
                        var15 = "";
                    } else if (var14.getSubTableStr().startsWith(head.getTableName() + ",")) {
                        var15 = var15.replace(head.getTableName() + ",", "");
                    } else if (var14.getSubTableStr().endsWith("," + head.getTableName())) {
                        var15 = var15.replace("," + head.getTableName(), "");
                    } else if (var14.getSubTableStr().indexOf("," + head.getTableName() + ",") != -1) {
                        var15 = var15.replace("," + head.getTableName() + ",", ",");
                    }

                    var14.setSubTableStr(var15);
                    ((OnlCgformHeadMapper)this.baseMapper).updateById(var14);
                }
            }
        }

    }

    @Transactional(
            rollbackFor = {Exception.class}
    )
    public void saveManyFormData(String code, JSONObject json, String xAccessToken) throws DBException, BusinessException {
        OnlCgformHead head = this.getById(code);
        if (head == null) {
            throw new DBException("数据库主表ID[" + code + "]不存在");
        } else {
            String buttonCode = "add";
            this.executeEnhanceJava(buttonCode, "start", head, json);
            String substring = SqlSymbolUtil.getSubstring(head.getTableName());
            if (head.getTableType() == 2) {
                String subTableStr = head.getSubTableStr();
                if (oConvertUtils.isNotEmpty(subTableStr)) {
                    String[] var8 = subTableStr.split(",");
                    String[] var9 = var8;
                    int var10 = var8.length;

                    for(int i = 0; i < var10; ++i) {
                        String var12 = var9[i];
                        JSONArray var13 = json.getJSONArray(var12);
                        if (var13 != null && var13.size() != 0) {
                            OnlCgformHead var14 = (OnlCgformHead)((OnlCgformHeadMapper)this.baseMapper).selectOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, var12));
                            if (var14 != null) {
                                List<OnlCgformField> var15 = this.fieldService.list((new LambdaQueryWrapper<OnlCgformField>()).eq(OnlCgformField::getCgformHeadId, var14.getId()));
                                String var16 = "";
                                String var17 = null;
                                Iterator<OnlCgformField> var18 = var15.iterator();

                                while(var18.hasNext()) {
                                    OnlCgformField var19 = (OnlCgformField)var18.next();
                                    if (!oConvertUtils.isEmpty(var19.getMainField())) {
                                        var16 = var19.getDbFieldName();
                                        String var20 = var19.getMainField();
                                        if (json.get(var20.toLowerCase()) != null) {
                                            var17 = json.getString(var20.toLowerCase());
                                        }

                                        if (json.get(var20.toUpperCase()) != null) {
                                            var17 = json.getString(var20.toUpperCase());
                                        }
                                    }
                                }

                                for(int var27 = 0; var27 < var13.size(); ++var27) {
                                    JSONObject var28 = var13.getJSONObject(var27);
                                    if (var17 != null) {
                                        var28.put(var16, var17);
                                    }

                                    this.fieldService.saveFormData(var15, var12, var28);
                                }
                            }
                        }
                    }
                }
            }

            if ("Y".equals(head.getIsTree())) {
                this.fieldService.saveTreeFormData(code, substring, json, head.getTreeIdField(), head.getTreeParentIdField());
            } else {
                this.fieldService.saveFormData(code, substring, json, false);
            }

            this.executeEnhanceSql(buttonCode, code, json);
            this.executeEnhanceJava(buttonCode, "end", head, json);
            if (oConvertUtils.isNotEmpty(json.get("bpm_status")) || oConvertUtils.isNotEmpty(json.get("bpm_status".toUpperCase()))) {
                try {
                    HttpHeaders var22 = new HttpHeaders();
                    var22.setContentType(MediaType.parseMediaType("application/json;charset=UTF-8"));
                    var22.set("Accept", "application/json;charset=UTF-8");
                    var22.set("X-Access-Token", xAccessToken);
                    JSONObject var23 = new JSONObject();
                    var23.put("flowCode", "onl_" + head.getTableName());
                    var23.put("id", json.get("id"));
                    var23.put("formUrl", "modules/bpm/task/form/OnlineFormDetail");
                    var23.put("formUrlMobile", "online/OnlineDetailForm");
                    String var24 = RestUtil.getBaseUrl() + "/process/extActProcess/saveMutilProcessDraft";
                    JSONObject var25 = (JSONObject)RestUtil.request(var24, HttpMethod.POST, var22, (JSONObject)null, var23, JSONObject.class).getBody();
                    if (var25 != null) {
                        String var26 = var25.getString("result");
                        log.info("保存流程草稿 dataId : " + var26);
                    }
                } catch (Exception var21) {
                	log.error("保存流程草稿异常, " + var21.getMessage(), var21);
                }
            }

        }
    }

    public Map<String, Object> querySubFormData(String table, String mainId) throws DBException {
        OnlCgformHead subHead = this.getOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, table));
        if (subHead == null) {
            throw new DBException("数据库子表[" + table + "]不存在");
        } else {
            List<OnlCgformField> subFields = this.fieldService.queryFormFields(subHead.getId(), false);
            String dbFieldName = null;
            Iterator<OnlCgformField> subFieldsIterator = subFields.iterator();

            while(subFieldsIterator.hasNext()) {
                OnlCgformField subField = subFieldsIterator.next();
                if (oConvertUtils.isNotEmpty(subField.getMainField())) {
                	dbFieldName = subField.getDbFieldName();
                    break;
                }
            }

            List<Map<String, Object>>  var9 = this.fieldService.querySubFormData(subFields, table, dbFieldName, mainId);
            if (var9 != null && var9.size() == 0) {
                throw new DBException("数据库子表[" + table + "]未找到相关信息,主表ID为" + mainId);
            } else if (var9.size() > 1) {
                throw new DBException("数据库子表[" + table + "]存在多条记录,主表ID为" + mainId);
            } else {
                Map<String, Object> var3 = var9.get(0);
                return var3;
            }
        }
    }

    public List<Map<String, Object>> queryManySubFormData(String table, String mainId) throws DBException {
        OnlCgformHead subHead = this.getOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, table));
        if (subHead == null) {
            throw new DBException("数据库子表[" + table + "]不存在");
        } else {
            List<OnlCgformField> subFields = this.fieldService.queryFormFields(subHead.getId(), false);
            String dbFieldName = null;
            String mainTable = null;
            String mainField = null;

            OnlCgformField linkField;
            
            for(OnlCgformField subField : subFields) {
            	linkField = subField;
            	if (oConvertUtils.isNotEmpty(subField.getMainField())) {
            		dbFieldName = subField.getDbFieldName();
            		mainTable = subField.getMainTable();
            		mainField = subField.getMainField();
                    break;
                }
            }

            List<OnlCgformField> fieldList = new ArrayList<>();
            linkField = new OnlCgformField();
            linkField.setDbFieldName(mainField);
            fieldList.add(linkField);
            Map<String, Object> formData = this.fieldService.queryFormData(fieldList, mainTable, mainId);
            String linkValue = formData.get(mainField).toString();
            List<Map<String, Object>> subFormData = this.fieldService.querySubFormData(subFields, table, dbFieldName, linkValue);
            if (subFormData != null && subFormData.size() == 0) {
                throw new DBException("数据库子表[" + table + "]未找到相关信息,主表ID为" + mainId);
            } else {
                List<Map<String, Object>> var13 = new ArrayList<>(subFormData.size());
                Iterator<Map<String, Object>> var14 = subFormData.iterator();

                while(var14.hasNext()) {
                    Map<String, Object> var15 = var14.next();
                    var13.add(SqlSymbolUtil.getValueType(var15));
                }

                return var13;
            }
        }
    }

    public Map<String, Object> queryManyFormData(String code, String id) throws DBException {
        OnlCgformHead head = this.getById(code);
        if (head == null) {
            throw new DBException("数据库主表ID[" + code + "]不存在");
        } else {
            List<OnlCgformField> fields = this.fieldService.queryFormFields(code, true);
            Map<String, Object> formData = this.fieldService.queryFormData(fields, head.getTableName(), id);
            if (head.getTableType() == 2) {
                String subTableStr = head.getSubTableStr();
                if (oConvertUtils.isNotEmpty(subTableStr)) {
                    String[] subTableArr = subTableStr.split(",");

                    for(int i = 0; i < subTableArr.length; ++i) {
                        String subTable = subTableArr[i];
                        OnlCgformHead subHead = this.baseMapper.selectOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, subTable));
                        if (subHead != null) {
                            List<OnlCgformField> subFields = this.fieldService.queryFormFields(subHead.getId(), false);
                            String linkField = "";
                            String mainFieldValue = null;
                            
                            for(OnlCgformField subField : subFields) {
                            	if (!oConvertUtils.isEmpty(subField.getMainField())) {
                            		linkField = subField.getDbFieldName();
                                    String mainField = subField.getMainField();
                                    if (null == formData.get(mainField)) {
                                    	mainFieldValue = formData.get(mainField.toUpperCase()).toString();
                                    } else {
                                    	mainFieldValue = formData.get(mainField).toString();
                                    }
                                }
                            }

                            List<Map<String, Object>> subFormData = this.fieldService.querySubFormData(subFields, subTable, linkField, mainFieldValue);
                            if (subFormData != null && subFormData.size() != 0) {
                            	formData.put(subTable, SqlSymbolUtil.transforRecords(subFormData));
                            } else {
                            	formData.put(subTable, new String[0]);
                            }
                        }
                    }
                }
            }

            return formData;
        }
    }
    
    public Map<String, Object> queryManyFormDataToArr(String code, String id) throws DBException {
        OnlCgformHead head = this.getById(code);
        if (head == null) {
            throw new DBException("数据库主表ID[" + code + "]不存在");
        } else {
            List<OnlCgformField> fields = this.fieldService.queryFormFields(code, false);
            Map<String, Object> formData = this.fieldService.queryFormData(fields, head.getTableName(), id);
            if (head.getTableType() == 2) {
                String subTableStr = head.getSubTableStr();
                if (oConvertUtils.isNotEmpty(subTableStr)) {
                    String[] subTableArr = subTableStr.split(",");

                    for(int i = 0; i < subTableArr.length; ++i) {
                        String subTable = subTableArr[i];
                        OnlCgformHead subHead = this.baseMapper.selectOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, subTable));
                        if (subHead != null) {
                            List<OnlCgformField> subFields = this.fieldService.queryFormFields(subHead.getId(), false);
                            String linkField = "";
                            String mainFieldValue = null;
                            
                            for(OnlCgformField subField : subFields) {
                            	if (!oConvertUtils.isEmpty(subField.getMainField())) {
                            		linkField = subField.getDbFieldName();
                                    String mainField = subField.getMainField();
                                    if (null == formData.get(mainField)) {
                                    	mainFieldValue = formData.get(mainField.toUpperCase()).toString();
                                    } else {
                                    	mainFieldValue = formData.get(mainField).toString();
                                    }
                                }
                            }

                            List<Map<String, Object>> subFormData = this.fieldService.querySubFormData(subFields, subTable, linkField, mainFieldValue);
                            if(subFormData == null) {
                            	subFormData = new ArrayList<Map<String, Object>>();
                            }
                        	subFormData = SqlSymbolUtil.transforRecords(subFormData);
                        	for(OnlCgformField field : subFields) {
                        		String fieldName = field.getDbFieldName();
                        		String key = subHead.getTableName()+"$"+fieldName;
                        		List<Object> value = new ArrayList<>();
                        		for(Map<String, Object> subFormRecord : subFormData) {
                        			value.add(subFormRecord.get(fieldName));
                            	}
                        		formData.put(key, value);
                        	}
                        }
                    }
                }
            }

            return formData;
        }
    }

    @Transactional(
            rollbackFor = {Exception.class}
    )
    public void editManyFormData(String code, JSONObject json) throws DBException, BusinessException {
        OnlCgformHead head = this.getById(code);
        if (head == null) {
            throw new DBException("数据库主表ID[" + code + "]不存在");
        } else {
            String buttonCode = "edit";
            this.executeEnhanceJava(buttonCode, "start", head, json);
            String tableName = head.getTableName();
            if ("Y".equals(head.getIsTree())) {
                this.fieldService.editTreeFormData(code, tableName, json, head.getTreeIdField(), head.getTreeParentIdField());
            } else {
                this.fieldService.editFormData(code, tableName, json, false);
            }

            if (head.getTableType() == 2) {
                String subTableStr = head.getSubTableStr();
                if (oConvertUtils.isNotEmpty(subTableStr)) {
                    String[] subTables = subTableStr.split(",");
                    String[] subTablesTemp = subTables;
                    int tableCount = subTables.length;

                    for(int i = 0; i < tableCount; ++i) {
                        String var11 = subTablesTemp[i];
                        OnlCgformHead var12 = (OnlCgformHead)((OnlCgformHeadMapper)this.baseMapper).selectOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, var11));
                        if (var12 != null) {
                            List<OnlCgformField> var13 = this.fieldService.list((new LambdaQueryWrapper<OnlCgformField>()).eq(OnlCgformField::getCgformHeadId, var12.getId()));
                            String var14 = "";
                            String var15 = null;
                            Iterator<OnlCgformField> var16 = var13.iterator();

                            while(var16.hasNext()) {
                                OnlCgformField var17 = var16.next();
                                if (!oConvertUtils.isEmpty(var17.getMainField())) {
                                    var14 = var17.getDbFieldName();
                                    String var18 = var17.getMainField();
                                    if (json.get(var18.toLowerCase()) != null) {
                                        var15 = json.getString(var18.toLowerCase());
                                    }

                                    if (json.get(var18.toUpperCase()) != null) {
                                        var15 = json.getString(var18.toUpperCase());
                                    }
                                }
                            }

                            if (!oConvertUtils.isEmpty(var15)) {
                                this.fieldService.deleteAutoList(var11, var14, var15);
                                JSONArray var19 = json.getJSONArray(var11);
                                if (var19 != null && var19.size() != 0) {
                                    for(int var20 = 0; var20 < var19.size(); ++var20) {
                                        JSONObject var21 = var19.getJSONObject(var20);
                                        if (var15 != null) {
                                            var21.put(var14, var15);
                                        }

                                        this.fieldService.saveFormData(var13, var11, var21);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            this.executeEnhanceJava(buttonCode, "end", head, json);
            this.executeEnhanceSql(buttonCode, code, json);
        }
    }

    public int executeEnhanceJava(String buttonCode, String eventType, OnlCgformHead head, JSONObject json) throws BusinessException {
        LambdaQueryWrapper<OnlCgformEnhanceJava> var5 = new LambdaQueryWrapper<>();
        var5.eq(OnlCgformEnhanceJava::getActiveStatus, "1");
        var5.eq(OnlCgformEnhanceJava::getButtonCode, buttonCode);
        var5.eq(OnlCgformEnhanceJava::getCgformHeadId, head.getId());
        var5.eq(OnlCgformEnhanceJava::getEvent, eventType);
        OnlCgformEnhanceJava var6 = (OnlCgformEnhanceJava)this.onlCgformEnhanceJavaMapper.selectOne(var5);
        Object var7 = this.a(var6);
        if (var7 != null && var7 instanceof CgformEnhanceJavaInter) {
            CgformEnhanceJavaInter var8 = (CgformEnhanceJavaInter)var7;
            return var8.execute(head.getTableName(), json);
        } else {
            return 1;
        }
    }

    public void executeEnhanceExport(OnlCgformHead head, List<Map<String,Object>> dataList) throws BusinessException {
        this.executeEnhanceList(head, "export", dataList);
    }

    public void executeEnhanceList(OnlCgformHead head, String buttonCode, List<Map<String,Object>> dataList) throws BusinessException {
        LambdaQueryWrapper<OnlCgformEnhanceJava> var4 = new LambdaQueryWrapper<>();
        var4.eq(OnlCgformEnhanceJava::getActiveStatus, "1");
        var4.eq(OnlCgformEnhanceJava::getButtonCode, buttonCode);
        var4.eq(OnlCgformEnhanceJava::getCgformHeadId, head.getId());
        List<OnlCgformEnhanceJava> var5 = this.onlCgformEnhanceJavaMapper.selectList(var4);
        if (var5 != null && var5.size() > 0) {
            Object var6 = this.a((OnlCgformEnhanceJava)var5.get(0));
            if (var6 != null && var6 instanceof CgformEnhanceJavaListInter) {
                CgformEnhanceJavaListInter var7 = (CgformEnhanceJavaListInter)var6;
                var7.execute(head.getTableName(), dataList);
            }
        }

    }

    private Object a(OnlCgformEnhanceJava var1) {
        if (var1 != null) {
            String var2 = var1.getCgJavaType();
            String var3 = var1.getCgJavaValue();
            if (oConvertUtils.isNotEmpty(var3)) {
                Object var4 = null;
                if ("class".equals(var2)) {
                    try {
                        var4 = MyClassLoader.getClassByScn(var3).newInstance();
                    } catch (InstantiationException var6) {
                    	log.error(var6.getMessage(), var6);
                    } catch (IllegalAccessException var7) {
                    	log.error(var7.getMessage(), var7);
                    }
                } else if ("spring".equals(var2)) {
                    var4 = SpringContextUtils.getBean(var3);
                }

                return var4;
            }
        }

        return null;
    }

    public void executeEnhanceSql(String buttonCode, String formId, JSONObject json) {
        LambdaQueryWrapper<OnlCgformEnhanceSql> var4 = new LambdaQueryWrapper<>();
        var4.eq(OnlCgformEnhanceSql::getButtonCode, buttonCode);
        var4.eq(OnlCgformEnhanceSql::getCgformHeadId, formId);
        OnlCgformEnhanceSql var5 = (OnlCgformEnhanceSql)this.onlCgformEnhanceSqlMapper.selectOne(var4);
        if (var5 != null && oConvertUtils.isNotEmpty(var5.getCgbSql())) {
            String var6 = org.jeecg.modules.online.cgform.util.SqlSymbolUtil.a(var5.getCgbSql(), json);
            String[] var7 = var6.split(";");
            String[] var8 = var7;
            int var9 = var7.length;

            for(int var10 = 0; var10 < var9; ++var10) {
                String var11 = var8[var10];
                if (var11 != null && !var11.toLowerCase().trim().equals("")) {
                	log.info(" online sql 增强： " + var11);
                    ((OnlCgformHeadMapper)this.baseMapper).executeDDL(var11);
                }
            }
        }

    }

    public void executeCustomerButton(String buttonCode, String formId, String dataId) throws BusinessException {
        OnlCgformHead var4 = (OnlCgformHead)this.getById(formId);
        if (var4 == null) {
            throw new BusinessException("未找到表配置信息");
        } else {
            Map<String, Object>  var5 = this.baseMapper.queryOneByTableNameAndId(SqlSymbolUtil.getSubstring(var4.getTableName()), dataId);
            JSONObject var6 = JSONObject.parseObject(JSON.toJSONString(var5));
            this.executeEnhanceJava(buttonCode, "start", var4, var6);
            this.executeEnhanceSql(buttonCode, formId, var6);
            this.executeEnhanceJava(buttonCode, "end", var4, var6);
        }
    }

    public List<OnlCgformButton> queryValidButtonList(String headId) {
        LambdaQueryWrapper<OnlCgformButton> var2 = new LambdaQueryWrapper<>();
        var2.eq(OnlCgformButton::getCgformHeadId, headId);
        var2.eq(OnlCgformButton::getButtonStatus, "1");
        var2.orderByAsc(OnlCgformButton::getOrderNum);
        return this.onlCgformButtonMapper.selectList(var2);
    }

    public OnlCgformEnhanceJs queryEnhanceJs(String formId, String cgJsType) {
        LambdaQueryWrapper<OnlCgformEnhanceJs> var3 = new LambdaQueryWrapper<>();
        var3.eq(OnlCgformEnhanceJs::getCgformHeadId, formId);
        var3.eq(OnlCgformEnhanceJs::getCgJsType, cgJsType);
        return (OnlCgformEnhanceJs)this.onlCgformEnhanceJsMapper.selectOne(var3);
    }

    @Transactional(
            rollbackFor = {Exception.class}
    )
    public void deleteOneTableInfo(String formId, String dataId) throws BusinessException {
        OnlCgformHead var3 = (OnlCgformHead)this.getById(formId);
        if (var3 == null) {
            throw new BusinessException("未找到表配置信息");
        } else {
            String var4 = SqlSymbolUtil.getSubstring(var3.getTableName());
            Map<String, Object> var5 = this.baseMapper.queryOneByTableNameAndId(var4, dataId);
            if (var5 == null) {
                throw new BusinessException("未找到数据信息");
            } else {
                String var6 = "delete";
                JSONObject var7 = JSONObject.parseObject(JSON.toJSONString(var5));
                this.executeEnhanceJava(var6, "start", var3, var7);
                this.updateParentNode(var3, dataId);
                if (var3.getTableType() == 2) {
                    this.fieldService.deleteAutoListMainAndSub(var3, dataId);
                } else {
                    String var8 = "delete from " + var4 + " where id = '" + dataId + "'";
                    ((OnlCgformHeadMapper)this.baseMapper).deleteOne(var8);
                }

                this.executeEnhanceSql(var6, formId, var7);
                this.executeEnhanceJava(var6, "end", var3, var7);
            }
        }
    }

    public JSONObject queryFormItem(OnlCgformHead head) {
        List<OnlCgformField> var2 = this.fieldService.queryAvailableFields(head.getId(), head.getTableName(), head.getTaskId(), false);
        List<String> var3 = new ArrayList<>();
        List<String> var4;
        if (oConvertUtils.isEmpty(head.getTaskId())) {
            var4 = this.fieldService.queryDisabledFields(head.getTableName());
            if (var4 != null && var4.size() > 0 && var4.get(0) != null) {
                var3.addAll(var4);
            }
        } else {
            var4 = this.fieldService.queryDisabledFields(head.getTableName(), head.getTaskId());
            if (var4 != null && var4.size() > 0 && var4.get(0) != null) {
                var3.addAll(var4);
            }
        }

        JSONObject var14 = SqlSymbolUtil.getFiledJson(var2, var3, (FieldModel)null);
        if (head.getTableType() == 2) {
            String var5 = head.getSubTableStr();
            if (oConvertUtils.isNotEmpty(var5)) {
                String[] var6 = var5.split(",");
                int var7 = var6.length;

                for(int var8 = 0; var8 < var7; ++var8) {
                    String var9 = var6[var8];
                    OnlCgformHead var10 = (OnlCgformHead)((OnlCgformHeadMapper)this.baseMapper).selectOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, var9));
                    if (var10 != null) {
                        List<OnlCgformField> fields = this.fieldService.queryAvailableFields(var10.getId(), var10.getTableName(), null, false);
                        List<String> hiddenFields = this.fieldService.queryDisabledFields(var10.getTableName());
                        JSONObject var13 = new JSONObject();
                        if (1 == var10.getRelationType()) {
                            var13 = SqlSymbolUtil.getFiledJson(fields, hiddenFields, null);
                        } else {
                            var13.put("columns", SqlSymbolUtil.getColumns(fields, hiddenFields));
                        }

                        var13.put("relationType", var10.getRelationType());
                        var13.put("view", "tab");
                        var13.put("order", var10.getTabOrderNum());
                        var13.put("formTemplate", var10.getFormTemplate());
                        var13.put("describe", var10.getTableTxt());
                        var13.put("key", var10.getTableName());
                        var14.getJSONObject("properties").put(var10.getTableName(), var13);
                    }
                }
            }
        }

        return var14;
    }

    public List<String> generateCode(OnlGenerateModel model) throws Exception {
        TableVo var2 = new TableVo();
        var2.setEntityName(model.getEntityName());
        var2.setEntityPackage(model.getEntityPackage());
        var2.setFtlDescription(model.getFtlDescription());
        var2.setTableName(model.getTableName());
        var2.setSearchFieldNum(-1);
        List<ColumnVo> var3 = new ArrayList<>();
        List<ColumnVo> var4 = new ArrayList<>();
        this.a(model.getCode(), var3, var4);
        OnlCgformHead var5 = (OnlCgformHead)((OnlCgformHeadMapper)this.baseMapper).selectOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getId, model.getCode()));
        Map<String,Object> var6 = new HashMap<>();
        var6.put("scroll", var5.getScroll() == null ? "0" : var5.getScroll().toString());
        String var7 = var5.getFormTemplate();
        if (oConvertUtils.isEmpty(var7)) {
            var2.setFieldRowNum(1);
        } else {
            var2.setFieldRowNum(Integer.parseInt(var7));
        }

        if ("Y".equals(var5.getIsTree())) {
            var6.put("pidField", var5.getTreeParentIdField());
            var6.put("hasChildren", var5.getTreeIdField());
            var6.put("textField", var5.getTreeFieldname());
        }

        var2.setExtendParams(var6);
        CgformEnum var8 = CgformEnum.getCgformEnumByConfig(model.getJspMode());
        List<String> var9 = (new CodeGenerateOne(var2, var3, var4)).generateCodeFile(model.getProjectPath(), var8.getTemplatePath(), var8.getStylePath());
        return var9;
    }

    public List<String> generateOneToMany(OnlGenerateModel model) throws Exception {
        MainTableVo mainTableVo = new MainTableVo();
        mainTableVo.setEntityName(model.getEntityName());
        mainTableVo.setEntityPackage(model.getEntityPackage());
        mainTableVo.setFtlDescription(model.getFtlDescription());
        mainTableVo.setTableName(model.getTableName());
        OnlCgformHead onlCgformHead = (OnlCgformHead)((OnlCgformHeadMapper)this.baseMapper).selectOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getId, model.getCode()));
        String formTemplate = onlCgformHead.getFormTemplate();
        if (oConvertUtils.isEmpty(formTemplate)) {
            mainTableVo.setFieldRowNum(1);
        } else {
            mainTableVo.setFieldRowNum(Integer.parseInt(formTemplate));
        }

        List<ColumnVo> showColumnVos = new ArrayList<>();
        List<ColumnVo> allColumnVoArrayList = new ArrayList<>();
        this.a(model.getCode(), showColumnVos, allColumnVoArrayList);//返回maintable
        List<OnlGenerateModel> subOnlGenerateModels = model.getSubList();
        List<SubTableVo> subTableVos = new ArrayList<>();
        Iterator<OnlGenerateModel> onlGenerateModelIterator = subOnlGenerateModels.iterator();

        List<ForeignKeyInfo> foreignKeyInfoList = new ArrayList<>(1);//todo 此处需要优化
        while(onlGenerateModelIterator.hasNext()) {
            OnlGenerateModel subOnlGenerateModel = onlGenerateModelIterator.next();
            OnlCgformHead subOnlCgformHeadEntity = ((OnlCgformHeadMapper)this.baseMapper).selectOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, subOnlGenerateModel.getTableName()));
            if (subOnlCgformHeadEntity != null) {
                SubTableVo subTableVo = new SubTableVo();
                subTableVo.setEntityName(subOnlGenerateModel.getEntityName());
                subTableVo.setEntityPackage(model.getEntityPackage());
                subTableVo.setTableName(subOnlGenerateModel.getTableName());
                subTableVo.setFtlDescription(subOnlGenerateModel.getFtlDescription());
                Integer relationType = subOnlCgformHeadEntity.getRelationType();
                subTableVo.setForeignRelationType(relationType == 1 ? "1" : "0");
                List<ColumnVo> showSubColumnVosList = new ArrayList<>();
                List<ColumnVo> allSubColumnVosArrayList = new ArrayList<>();
                OnlCgformField onlCgformField = this.a(subOnlCgformHeadEntity.getId(), showSubColumnVosList, allSubColumnVosArrayList);
                if (onlCgformField != null) {
                    String dbFieldName = onlCgformField.getDbFieldName();
                    subTableVo.setOriginalForeignKeys(new String[]{dbFieldName});
                    //subTableVo.setForeignKeys(new String[]{var16.getDbFieldName()});
                    ForeignKeyInfo foreignKeyInfo = new ForeignKeyInfo();
                    String mainTableName = onlCgformField.getMainTable();
                    String mainField = onlCgformField.getMainField();

                    if(mainTableName.equals(mainTableVo.getTableName())){
                        mainTableVo.setPkColumn(DbReadTableUtil.humpConver(mainField,true));
                    }

                    foreignKeyInfo.setColumnName(dbFieldName);
                    foreignKeyInfo.setMainTableName(mainTableName);
                    foreignKeyInfo.setMainTableColumn(DbReadTableUtil.humpConver(mainField,true));
                    foreignKeyInfo.setDbColumnName(dbFieldName);

                    foreignKeyInfoList.add(foreignKeyInfo);
                    subTableVo.setForeignKeyInfoList(foreignKeyInfoList);
                    subTableVo.setColums(showSubColumnVosList);
                    subTableVo.setOriginalColumns(allSubColumnVosArrayList);
                    subTableVos.add(subTableVo);
                }
            }
        }

        CgformEnum var17 = CgformEnum.getCgformEnumByConfig(model.getJspMode());
        List<String> var18 = (new CodeGenerateOneToMany(mainTableVo, showColumnVos, allColumnVoArrayList, subTableVos)).generateCodeFile(model.getProjectPath(), var17.getTemplatePath(), var17.getStylePath());
        return var18;
    }

    private OnlCgformField a(String code, List<ColumnVo> showColumnVos, List<ColumnVo> allColumnVoList) {
        LambdaQueryWrapper<OnlCgformField> onlCgformFieldLambdaQueryWrapper = new LambdaQueryWrapper<>();
        onlCgformFieldLambdaQueryWrapper.eq(OnlCgformField::getCgformHeadId, code);
        onlCgformFieldLambdaQueryWrapper.orderByAsc(OnlCgformField::getOrderNum);
        List<OnlCgformField> onlCgformFields = this.fieldService.list(onlCgformFieldLambdaQueryWrapper);
        OnlCgformField onlCgformField = null;
        Iterator<OnlCgformField> onlCgformFieldIterator = onlCgformFields.iterator();

        while(true) {
            OnlCgformField cgformField;
            ColumnExpVo columnExpVo;
            do {
                if (!onlCgformFieldIterator.hasNext()) {
                    return onlCgformField;
                }

                cgformField = onlCgformFieldIterator.next();
                if (oConvertUtils.isNotEmpty(cgformField.getMainTable())) {
                    onlCgformField = cgformField;
                }

                columnExpVo = new ColumnExpVo();
                columnExpVo.setFieldLength(cgformField.getFieldLength());
                columnExpVo.setFieldHref(cgformField.getFieldHref());
                columnExpVo.setFieldValidType(cgformField.getFieldValidType());
                columnExpVo.setFieldDefault(cgformField.getDbDefaultVal());
                columnExpVo.setFieldShowType(cgformField.getFieldShowType());
                columnExpVo.setFieldOrderNum(cgformField.getOrderNum());
                columnExpVo.setIsKey(cgformField.getDbIsKey() == 1 ? "Y" : "N");
                columnExpVo.setIsShow(cgformField.getIsShowForm() == 1 ? "Y" : "N");
                columnExpVo.setIsShowList(cgformField.getIsShowList() == 1 ? "Y" : "N");
                columnExpVo.setIsQuery(cgformField.getIsQuery() == 1 ? "Y" : "N");
                columnExpVo.setQueryMode(cgformField.getQueryMode());
                columnExpVo.setDictField(cgformField.getDictField());
                if (oConvertUtils.isNotEmpty(cgformField.getDictTable()) && cgformField.getDictTable().indexOf("where") > 0) {
                    columnExpVo.setDictTable(cgformField.getDictTable().split("where")[0].trim());
                } else {
                    columnExpVo.setDictTable(cgformField.getDictTable());
                }

                columnExpVo.setDictText(cgformField.getDictText());
                columnExpVo.setFieldDbName(cgformField.getDbFieldName());
                columnExpVo.setFieldName(oConvertUtils.camelName(cgformField.getDbFieldName()));
                columnExpVo.setFiledComment(cgformField.getDbFieldTxt());
                columnExpVo.setFieldDbType(cgformField.getDbType());
                columnExpVo.setFieldType(this.getJavaType(cgformField.getDbType()));
                columnExpVo.setClassType(cgformField.getFieldShowType());
                columnExpVo.setClassType_row(cgformField.getFieldShowType());
                columnExpVo.setSort("1".equals(cgformField.getSortFlag()) ? "Y" : "N");
                columnExpVo.setReadonly(cgformField.getIsReadOnly() == 1 ? "Y" : "N");
                if (cgformField.getDbIsNull() != 0 && !"*".equals(cgformField.getFieldValidType()) && !"1".equals(cgformField.getFieldMustInput())) {
                    columnExpVo.setNullable("Y");
                } else {
                    columnExpVo.setNullable("N");
                }

                if ("switch".equals(cgformField.getFieldShowType())) {
                    columnExpVo.setDictField(cgformField.getFieldExtendJson());
                }
                allColumnVoList.add(columnExpVo);
            } while(cgformField.getIsShowForm() != 1 && cgformField.getIsShowList() != 1);

            showColumnVos.add(columnExpVo);
        }
    }

    private String getJavaType(String var1) {
        var1 = var1.toLowerCase();
        if (var1.indexOf("int") >= 0) {
            return "java.lang.Integer";
        } else if (var1.indexOf("double") >= 0) {
            return "java.lang.Double";
        } else if (var1.indexOf("decimal") >= 0) {
            return "java.math.BigDecimal";
        } else {
            return var1.indexOf("date") >= 0 ? "java.util.Date" : "java.lang.String";
        }
    }

    public void addCrazyFormData(String tbname, JSONObject json) throws DBException, UnsupportedEncodingException {
        OnlCgformHead head = this.getOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, tbname));
        if (head == null) {
            throw new DBException("数据库主表[" + tbname + "]不存在");
        } else {
            if (head.getTableType() == 2) {
                String subTableStr = head.getSubTableStr();
                if (subTableStr != null) {
                    String[] subTableStrs = subTableStr.split(",");

                    for(int i = 0; i < subTableStrs.length; ++i) {
                        String subTableName = subTableStrs[i];
                        String subTableNameClass = json.getString("sub-table-design_" + subTableName);
                        if (!oConvertUtils.isEmpty(subTableNameClass)) {
                            JSONArray subTableJSON = JSONArray.parseArray(URLDecoder.decode(subTableNameClass, "UTF-8"));
                            if (subTableJSON != null && subTableJSON.size() != 0) {
                                OnlCgformHead subTableHead = ((OnlCgformHeadMapper)this.baseMapper).selectOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, subTableName));
                                if (subTableHead != null) {
                                    List<OnlCgformField> subTableFields = this.fieldService.list((new LambdaQueryWrapper<OnlCgformField>()).eq(OnlCgformField::getCgformHeadId, subTableHead.getId()));
                                    String subTableDbFieldName = "";
                                    String var15 = null;
                                    Iterator<OnlCgformField> subTableFieldsIterator = subTableFields.iterator();

                                    while(subTableFieldsIterator.hasNext()) {
                                        OnlCgformField subTableField = subTableFieldsIterator.next();
                                        if (!oConvertUtils.isEmpty(subTableField.getMainField())) {
                                        	subTableDbFieldName = subTableField.getDbFieldName();
                                            String subTableMainField = subTableField.getMainField();
                                            var15 = json.getString(subTableMainField);
                                        }
                                    }

                                    for(int j = 0; j < subTableJSON.size(); ++j) {
                                        JSONObject var20 = subTableJSON.getJSONObject(j);
                                        if (var15 != null) {
                                            var20.put(subTableDbFieldName, var15);
                                        }

                                        this.fieldService.executeInsertSQL(org.jeecg.modules.online.cgform.util.SqlSymbolUtil.getCrazyInsertSql(subTableName, subTableFields, var20));
                                    }
                                }
                            }
                        }
                    }
                }
            }

            this.fieldService.saveFormData(head.getId(), tbname, json, true);
        }
    }

    public void editCrazyFormData(String tbname, JSONObject json) throws DBException, UnsupportedEncodingException {
        OnlCgformHead var3 = this.getOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, tbname));
        if (var3 == null) {
            throw new DBException("数据库主表[" + tbname + "]不存在");
        } else {
            if (var3.getTableType() == 2) {
                String var4 = var3.getSubTableStr();
                String[] var5 = var4.split(",");
                String[] var6 = var5;
                int var7 = var5.length;

                for(int var8 = 0; var8 < var7; ++var8) {
                    String var9 = var6[var8];
                    OnlCgformHead var10 = ((OnlCgformHeadMapper)this.baseMapper).selectOne((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getTableName, var9));
                    if (var10 != null) {
                        List<OnlCgformField> var11 = this.fieldService.list((new LambdaQueryWrapper<OnlCgformField>()).eq(OnlCgformField::getCgformHeadId, var10.getId()));
                        String var12 = "";
                        String var13 = null;
                        Iterator<OnlCgformField> var14 = var11.iterator();

                        while(var14.hasNext()) {
                            OnlCgformField var15 = var14.next();
                            if (!oConvertUtils.isEmpty(var15.getMainField())) {
                                var12 = var15.getDbFieldName();
                                String var16 = var15.getMainField();
                                var13 = json.getString(var16);
                            }
                        }

                        if (!oConvertUtils.isEmpty(var13)) {
                            this.fieldService.deleteAutoList(var9, var12, var13);
                            String var18 = json.getString("sub-table-design_" + var9);
                            if (!oConvertUtils.isEmpty(var18)) {
                                JSONArray var19 = JSONArray.parseArray(URLDecoder.decode(var18, "UTF-8"));
                                if (var19 != null && var19.size() != 0) {
                                    for(int var20 = 0; var20 < var19.size(); ++var20) {
                                        JSONObject var17 = var19.getJSONObject(var20);
                                        if (var13 != null) {
                                            var17.put(var12, var13);
                                        }

                                        this.fieldService.executeInsertSQL(org.jeecg.modules.online.cgform.util.SqlSymbolUtil.getCrazyInsertSql(var9, var11, var17));
                                    }
                                }
                            }
                        }
                    }
                }
            }

            this.fieldService.editFormData(var3.getId(), tbname, json, true);
        }
    }

    public Integer getMaxCopyVersion(String physicId) {
        Integer var2 = ((OnlCgformHeadMapper)this.baseMapper).getMaxCopyVersion(physicId);
        return var2 == null ? 0 : var2;
    }

    public void copyOnlineTableConfig(OnlCgformHead physicTable) throws Exception {
        String var2 = physicTable.getId();
        int var3 = this.getMaxCopyVersion(var2);
        OnlCgformHead var4 = new OnlCgformHead();
        String var5 = UUIDGenerator.generate();
        var4.setId(var5);
        var4.setPhysicId(var2);
        var4.setCopyType(1);
        var4.setCopyVersion(physicTable.getTableVersion());
        var4.setTableVersion(1);
        var4.setTableName(physicTable.getTableName() + "$" + var3);
        var4.setTableTxt(physicTable.getTableTxt());
        var4.setFormCategory(physicTable.getFormCategory());
        var4.setFormTemplate(physicTable.getFormTemplate());
        var4.setFormTemplateMobile(physicTable.getFormTemplateMobile());
        var4.setIdSequence(physicTable.getIdSequence());
        var4.setIdType(physicTable.getIdType());
        var4.setIsCheckbox(physicTable.getIsCheckbox());
        var4.setIsPage(physicTable.getIsPage());
        var4.setIsTree(physicTable.getIsTree());
        var4.setQueryMode(physicTable.getQueryMode());
        var4.setTableType(1);
        var4.setIsDbSynch("N");
        var4.setTreeParentIdField(physicTable.getTreeParentIdField());
        var4.setTreeFieldname(physicTable.getTreeFieldname());
        var4.setTreeIdField(physicTable.getTreeIdField());
        var4.setRelationType((Integer)null);
        var4.setTabOrderNum((Integer)null);
        var4.setSubTableStr((String)null);
        var4.setThemeTemplate(physicTable.getThemeTemplate());
        var4.setScroll(physicTable.getScroll());
        List<OnlCgformField> var6 = this.fieldService.list((new LambdaQueryWrapper<OnlCgformField>()).eq(OnlCgformField::getCgformHeadId, var2));
        Iterator<OnlCgformField> var7 = var6.iterator();

        while(var7.hasNext()) {
            OnlCgformField var8 = var7.next();
            OnlCgformField var9 = new OnlCgformField();
            var9.setCgformHeadId(var5);
            this.a(var8, var9);
            this.fieldService.save(var9);
        }

        ((OnlCgformHeadMapper)this.baseMapper).insert(var4);
    }

    public void initCopyState(List<OnlCgformHead> headList) {
        List<String> var2 = this.baseMapper.queryCopyPhysicId();
        Iterator<OnlCgformHead> var3 = headList.iterator();

        while(var3.hasNext()) {
            OnlCgformHead var4 = var3.next();
            if (var2.contains(var4.getId())) {
                var4.setHascopy(1);
            } else {
                var4.setHascopy(0);
            }
        }

    }

    public void deleteBatch(String ids, String flag) {
        String[] var3 = ids.split(",");
        if ("1".equals(flag)) {
            String[] var4 = var3;
            int var5 = var3.length;

            for(int var6 = 0; var6 < var5; ++var6) {
                String var7 = var4[var6];

                try {
                    this.deleteRecordAndTable(var7);
                } catch (DBException var9) {
                    var9.printStackTrace();
                } catch (SQLException var10) {
                    var10.printStackTrace();
                }
            }
        } else {
            this.removeByIds(Arrays.asList(var3));
        }

    }

    public void updateParentNode(OnlCgformHead head, String dataId) {
        if ("Y".equals(head.getIsTree())) {
            String var3 = SqlSymbolUtil.getSubstring(head.getTableName());
            String var4 = head.getTreeParentIdField();
            Map<String, Object> var5 = ((OnlCgformHeadMapper)this.baseMapper).queryOneByTableNameAndId(var3, dataId);
            String var6 = null;
            if (var5.get(var4) != null && !"0".equals(var5.get(var4))) {
                var6 = var5.get(var4).toString();
            } else if (var5.get(var4.toUpperCase()) != null && !"0".equals(var5.get(var4.toUpperCase()))) {
                var6 = var5.get(var4.toUpperCase()).toString();
            }

            if (var6 != null) {
                Integer var7 = ((OnlCgformHeadMapper)this.baseMapper).queryChildNode(var3, var4, var6);
                if (var7 == 1) {
                    String var8 = head.getTreeIdField();
                    this.fieldService.updateTreeNodeNoChild(var3, var8, var6);
                }
            }
        }

    }

    private void b(OnlCgformHead var1, List<OnlCgformField> var2) {
        List<OnlCgformHead> var3 = this.list((new LambdaQueryWrapper<OnlCgformHead>()).eq(OnlCgformHead::getPhysicId, var1.getId()));
        if (var3 != null && var3.size() > 0) {
            Iterator<OnlCgformHead> var4 = var3.iterator();

            while(true) {
                List<OnlCgformField> var6;
                String var13;
                List<String> var19;
                Iterator var22;
                label108:
                do {
                    while(var4.hasNext()) {
                        OnlCgformHead var5 = var4.next();
                        var6 = this.fieldService.list((new LambdaQueryWrapper<OnlCgformField>()).eq(OnlCgformField::getCgformHeadId, var5.getId()));
                        OnlCgformField var9;
                        if (var6 != null && var6.size() != 0) {
                            Map<String,Integer> var15 = new HashMap<>();
                            Iterator<OnlCgformField> var16 = var6.iterator();

                            while(var16.hasNext()) {
                                var9 = var16.next();
                                var15.put(var9.getDbFieldName(), 1);
                            }

                            Map<String,Integer> var17 = new HashMap<>();
                            Iterator<OnlCgformField> var18 = var2.iterator();

                            while(var18.hasNext()) {
                                OnlCgformField var10 = (OnlCgformField)var18.next();
                                var17.put(var10.getDbFieldName(), 1);
                            }

                            var19 = new ArrayList<>();
                            List<String> var20 = new ArrayList<>();
                            Iterator<String> var11 = var17.keySet().iterator();

                            while(var11.hasNext()) {
                                String var12 = var11.next();
                                if (var15.get(var12) == null) {
                                    var20.add(var12);
                                } else {
                                    var19.add(var12);
                                }
                            }

                            List<String> var21 = new ArrayList<>();
                            var22 = var15.keySet().iterator();

                            while(var22.hasNext()) {
                                var13 = (String)var22.next();
                                if (var17.get(var13) == null) {
                                    var21.add(var13);
                                }
                            }

                            OnlCgformField var23;
                            if (var21.size() > 0) {
                                var22 = var6.iterator();

                                while(var22.hasNext()) {
                                    var23 = (OnlCgformField)var22.next();
                                    if (var21.contains(var23.getDbFieldName())) {
                                        this.fieldService.removeById(var23.getId());
                                    }
                                }
                            }

                            if (var20.size() > 0) {
                                var22 = var2.iterator();

                                while(var22.hasNext()) {
                                    var23 = (OnlCgformField)var22.next();
                                    if (var20.contains(var23.getDbFieldName())) {
                                        OnlCgformField var14 = new OnlCgformField();
                                        var14.setCgformHeadId(var5.getId());
                                        this.a(var23, var14);
                                        this.fieldService.save(var14);
                                    }
                                }
                            }
                            continue label108;
                        }

                        Iterator<OnlCgformField> var7 = var2.iterator();

                        while(var7.hasNext()) {
                            OnlCgformField var8 = (OnlCgformField)var7.next();
                            var9 = new OnlCgformField();
                            var9.setCgformHeadId(var5.getId());
                            this.a(var8, var9);
                            this.fieldService.save(var9);
                        }
                    }

                    return;
                } while(var19.size() <= 0);

                var22 = var19.iterator();

                while(var22.hasNext()) {
                    var13 = (String)var22.next();
                    this.b(var13, var2, var6);
                }
            }
        }
    }

    private void b(String var1, List<OnlCgformField> var2, List<OnlCgformField> var3) {
        OnlCgformField var4 = null;
        Iterator<OnlCgformField> var5 = var2.iterator();

        while(var5.hasNext()) {
            OnlCgformField var6 = var5.next();
            if (var1.equals(var6.getDbFieldName())) {
                var4 = var6;
            }
        }

        OnlCgformField var8 = null;
        Iterator<OnlCgformField> var9 = var3.iterator();

        while(var9.hasNext()) {
            OnlCgformField var7 = var9.next();
            if (var1.equals(var7.getDbFieldName())) {
                var8 = var7;
            }
        }

        if (var4 != null && var8 != null) {
            boolean var10 = false;
            if (!var4.getDbType().equals(var8.getDbType())) {
                var8.setDbType(var4.getDbType());
                var10 = true;
            }

            if (!var4.getDbDefaultVal().equals(var8.getDbDefaultVal())) {
                var8.setDbDefaultVal(var4.getDbDefaultVal());
                var10 = true;
            }

            if (var4.getDbLength() != var8.getDbLength()) {
                var8.setDbLength(var4.getDbLength());
                var10 = true;
            }

            if (var4.getDbIsNull() != var8.getDbIsNull()) {
                var8.setDbIsNull(var4.getDbIsNull());
                var10 = true;
            }

            if (var10) {
                this.fieldService.updateById(var8);
            }
        }

    }

    private void a(OnlCgformField var1, OnlCgformField var2) {
        var2.setDbDefaultVal(var1.getDbDefaultVal());
        var2.setDbFieldName(var1.getDbFieldName());
        var2.setDbFieldNameOld(var1.getDbFieldNameOld());
        var2.setDbFieldTxt(var1.getDbFieldTxt());
        var2.setDbIsKey(var1.getDbIsKey());
        var2.setDbIsNull(var1.getDbIsNull());
        var2.setDbLength(var1.getDbLength());
        var2.setDbPointLength(var1.getDbPointLength());
        var2.setDbType(var1.getDbType());
        var2.setDictField(var1.getDictField());
        var2.setDictTable(var1.getDictTable());
        var2.setDictText(var1.getDictText());
        var2.setFieldExtendJson(var1.getFieldExtendJson());
        var2.setFieldHref(var1.getFieldHref());
        var2.setFieldLength(var1.getFieldLength());
        var2.setFieldMustInput(var1.getFieldMustInput());
        var2.setFieldShowType(var1.getFieldShowType());
        var2.setFieldValidType(var1.getFieldValidType());
        var2.setFieldDefaultValue(var1.getFieldDefaultValue());
        var2.setIsQuery(var1.getIsQuery());
        var2.setIsShowForm(var1.getIsShowForm());
        var2.setIsShowList(var1.getIsShowList());
        var2.setMainField((String)null);
        var2.setMainTable((String)null);
        var2.setOrderNum(var1.getOrderNum());
        var2.setQueryMode(var1.getQueryMode());
        var2.setIsReadOnly(var1.getIsReadOnly());
        var2.setSortFlag(var1.getSortFlag());
        var2.setQueryDefVal(var1.getQueryDefVal());
        var2.setQueryConfigFlag(var1.getQueryConfigFlag());
        var2.setQueryDictField(var1.getQueryDictField());
        var2.setQueryDictTable(var1.getQueryDictTable());
        var2.setQueryDictText(var1.getQueryDictText());
        var2.setQueryMustInput(var1.getQueryMustInput());
        var2.setQueryShowType(var1.getQueryShowType());
        var2.setQueryValidType(var1.getQueryValidType());
        var2.setConverter(var1.getConverter());
    }

    private void a(OnlCgformField var1) {
        if (DbType.TEXT.equals(var1.getDbType()) || DbType.BLOB.equals(var1.getDbType())) {
            var1.setDbLength(0);
            var1.setDbPointLength(0);
        }

    }
}
