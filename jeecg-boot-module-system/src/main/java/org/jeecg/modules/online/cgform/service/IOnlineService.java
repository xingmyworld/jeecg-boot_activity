package org.jeecg.modules.online.cgform.service;

import com.alibaba.fastjson.JSONObject;
import org.jeecg.modules.online.cgform.entity.OnlCgformEnhanceJs;
import org.jeecg.modules.online.cgform.entity.OnlCgformHead;
import org.jeecg.modules.online.cgform.model.OnlineConfigModel;

public interface IOnlineService
{

    public OnlineConfigModel queryOnlineConfig(OnlCgformHead onlcgformhead);

    public JSONObject queryOnlineFormObj(OnlCgformHead onlcgformhead, OnlCgformEnhanceJs onlcgformenhancejs);

    public JSONObject queryOnlineFormObj(OnlCgformHead onlcgformhead);
}
