package org.jeecg.modules.online.cgform.converter.impl;

import com.alibaba.fastjson.JSONObject;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

import org.jeecg.modules.online.cgform.common.CommonEntity;
import org.jeecg.modules.online.cgform.converter.field.TableFieldCommentConverter;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;

@Data
@EqualsAndHashCode(callSuper = false)
public class LinkDownFieldCommentConverter extends TableFieldCommentConverter {

    private String linkField;

    public LinkDownFieldCommentConverter(OnlCgformField onlCgformField) {
        String dictTable = onlCgformField.getDictTable();
        CommonEntity linkDown = JSONObject.parseObject(dictTable, CommonEntity.class);
        this.setTable(linkDown.getTable());
        this.setCode(linkDown.getKey());
        this.setText(linkDown.getTxt());
        this.linkField = linkDown.getLinkField();
    }

    @Override
    public Map<String, String> getConfig() {
        Map<String, String> hashMap = new HashMap<>();
        hashMap.put("linkField", this.linkField);
        return hashMap;
    }
}