package org.jeecg.modules.online.cgform.converter;

import java.util.Map;

public interface FieldCommentConverter
{

    public abstract String converterToVal(String s);

    public abstract String converterToTxt(String s);

    public abstract Map<String, String> getConfig();
}
