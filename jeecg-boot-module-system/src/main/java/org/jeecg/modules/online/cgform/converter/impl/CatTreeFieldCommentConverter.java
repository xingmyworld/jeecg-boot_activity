package org.jeecg.modules.online.cgform.converter.impl;

import java.util.HashMap;
import java.util.Map;

import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.online.cgform.converter.field.TableFieldCommentConverter;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class CatTreeFieldCommentConverter extends TableFieldCommentConverter {
    private String treeText;


    public CatTreeFieldCommentConverter(OnlCgformField onlCgformField) {
        super("SYS_CATEGORY", "ID", "NAME");
        this.treeText = onlCgformField.getDictText();
        this.field = onlCgformField.getDbFieldName();
    }

    @Override
    public Map<String, String> getConfig() {
        if (oConvertUtils.isEmpty(this.treeText)) {
            return null;
        } else {
            Map<String, String> hashMap = new HashMap<>();
            hashMap.put("treeText", this.treeText);
            hashMap.put("field", this.field);
            return hashMap;
        }
    }
}

