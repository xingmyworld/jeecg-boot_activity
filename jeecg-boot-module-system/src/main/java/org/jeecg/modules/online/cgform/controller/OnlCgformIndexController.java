package org.jeecg.modules.online.cgform.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.online.cgform.entity.OnlCgformIndex;
import org.jeecg.modules.online.cgform.service.IOnlCgformIndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController("onlCgformIndexController")
@RequestMapping({"/online/cgform/index"})
@Slf4j
public class OnlCgformIndexController {
    @Autowired
    private IOnlCgformIndexService onlCgformIndexService;

    public OnlCgformIndexController() {
    }

    @GetMapping({"/listByHeadId"})
    public Result<List<OnlCgformIndex>> listByHeadId(@RequestParam("headId") String headId) {
        QueryWrapper<OnlCgformIndex> wrapper = new QueryWrapper<>();
        wrapper.eq("cgform_head_id", headId);
        wrapper.eq("del_flag", CommonConstant.DEL_FLAG_0);
        wrapper.orderByDesc("create_time");
        List<OnlCgformIndex> list = this.onlCgformIndexService.list(wrapper);
        return Result.OK(list);
    }

    @GetMapping({"/list"})
    public Result<IPage<OnlCgformIndex>> list(OnlCgformIndex index, @RequestParam(name = "pageNo",defaultValue = "1") Integer pageNo, @RequestParam(name = "pageSize",defaultValue = "10") Integer pageSize, HttpServletRequest request) {
        Result<IPage<OnlCgformIndex>> reult = new Result<>();
        QueryWrapper<OnlCgformIndex> wrapper = QueryGenerator.initQueryWrapper(index, request.getParameterMap());
        Page<OnlCgformIndex> page = new Page<OnlCgformIndex>(pageNo, pageSize);
        IPage<OnlCgformIndex> ipage = this.onlCgformIndexService.page(page, wrapper);
        reult.setSuccess(true);
        reult.setResult(ipage);
        return reult;
    }

    @PostMapping({"/add"})
    public Result<OnlCgformIndex> add(@RequestBody OnlCgformIndex index) {
        Result<OnlCgformIndex> result = new Result<>();

        try {
            this.onlCgformIndexService.save(index);
            result.success("添加成功！");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            result.error500("操作失败");
        }

        return result;
    }

    @PutMapping({"/edit"})
    public Result<OnlCgformIndex> edit(@RequestBody OnlCgformIndex index) {
        Result<OnlCgformIndex> result = new Result<>();
        OnlCgformIndex onlCgformIndex = this.onlCgformIndexService.getById(index.getId());
        if (onlCgformIndex == null) {
        	result.error500("未找到对应实体");
        } else {
            boolean b = this.onlCgformIndexService.updateById(index);
            if (b) {
            	result.success("修改成功!");
            }
        }

        return result;
    }

    @DeleteMapping({"/delete"})
    public Result<OnlCgformIndex> delete(@RequestParam(name = "id",required = true) String id) {
        Result<OnlCgformIndex> result = new Result<>();
        OnlCgformIndex onlCgformIndex = (OnlCgformIndex)this.onlCgformIndexService.getById(id);
        if (onlCgformIndex == null) {
        	result.error500("未找到对应实体");
        } else {
            boolean b = this.onlCgformIndexService.removeById(id);
            if (b) {
            	result.success("删除成功!");
            }
        }

        return result;
    }

    @DeleteMapping({"/deleteBatch"})
    public Result<OnlCgformIndex> deleteBatch(@RequestParam(name = "ids",required = true) String ids) {
        Result<OnlCgformIndex> result = new Result<OnlCgformIndex>();
        if (ids != null && !"".equals(ids.trim())) {
            this.onlCgformIndexService.removeByIds(Arrays.asList(ids.split(",")));
            result.success("删除成功!");
        } else {
        	result.error500("参数不识别！");
        }

        return result;
    }

    @GetMapping({"/queryById"})
    public Result<OnlCgformIndex> queryById(@RequestParam(name = "id",required = true) String id) {
        Result<OnlCgformIndex> result = new Result<>();
        OnlCgformIndex onlCgformIndex = this.onlCgformIndexService.getById(id);
        if (onlCgformIndex == null) {
        	result.error500("未找到对应实体");
        } else {
        	result.setResult(onlCgformIndex);
        	result.setSuccess(true);
        }

        return result;
    }
}
