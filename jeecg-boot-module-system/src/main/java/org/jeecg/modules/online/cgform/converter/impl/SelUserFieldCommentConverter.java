package org.jeecg.modules.online.cgform.converter.impl;

import java.util.ArrayList;
import java.util.List;

import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.online.cgform.converter.field.FieldFieldCommentConverter;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;

public class SelUserFieldCommentConverter extends FieldFieldCommentConverter {

    public SelUserFieldCommentConverter(OnlCgformField onlCgformField) {
        ISysBaseAPI sysBaseApi = SpringContextUtils.getBean(ISysBaseAPI.class);
        String sysUser = "SYS_USER";
        String realname = "REALNAME";
        String username = "USERNAME";
        this.dictList = sysBaseApi.queryTableDictItemsByCode(sysUser, realname, username);
        this.filed = onlCgformField.getDbFieldName();
    }

    @Override
    public String converterToVal(String txt) {
        if (oConvertUtils.isEmpty(txt)) {
            return null;
        } else {
            List<String> arrayList = new ArrayList<>();
            String[] strings = txt.split(",");

            for (String s : strings) {
                String s1 = super.converterToVal(s);
                if (s1 != null) {
                    arrayList.add(s1);
                }
            }

            return String.join(",", arrayList);
        }
    }

    @Override
    public String converterToTxt(String val) {
        if (oConvertUtils.isEmpty(val)) {
            return null;
        } else {
            List<String> arrayList = new ArrayList<>();
            String[] strings = val.split(",");

            for (String s : strings) {
                String s1 = super.converterToTxt(s);
                if (s1 != null) {
                    arrayList.add(s1);
                }
            }

            return String.join(",", arrayList);
        }
    }
}
