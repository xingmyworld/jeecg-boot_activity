package org.jeecg.modules.online.cgform.enhance;

import com.alibaba.fastjson.JSONObject;
import java.util.Map;
import org.jeecg.modules.online.config.exception.BusinessException;

public interface CgformEnhanceJavaInter
{

    /**
     * @deprecated Method execute is deprecated
     */

    int execute(String s, Map<String, Object> map)
        throws BusinessException;

    int execute(String s, JSONObject jsonobject)
        throws BusinessException;
}
