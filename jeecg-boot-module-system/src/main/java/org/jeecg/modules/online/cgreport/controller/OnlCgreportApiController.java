package org.jeecg.modules.online.cgreport.controller;

import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecg.autopoi.poi.excel.ExcelExportUtil;
import org.jeecg.autopoi.poi.excel.entity.ExportParams;
import org.jeecg.autopoi.poi.excel.entity.params.ExcelExportEntity;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.PermissionData;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.vo.DictModel;
import org.jeecg.common.system.vo.DynamicDataSourceModel;
import org.jeecg.common.util.BrowserUtils;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.online.cgreport.entity.OnlCgreportHead;
import org.jeecg.modules.online.cgreport.entity.OnlCgreportItem;
import org.jeecg.modules.online.cgreport.entity.OnlCgreportParam;
import org.jeecg.modules.online.cgreport.mapper.OnlCgreportHeadMapper;
import org.jeecg.modules.online.cgreport.service.IOnlCgreportHeadService;
import org.jeecg.modules.online.cgreport.service.IOnlCgreportItemService;
import org.jeecg.modules.online.cgreport.service.IOnlCgreportParamService;
import org.jeecg.modules.online.cgreport.util.SqlUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping({"/online/cgreport/api"})
public class OnlCgreportApiController {
    @Autowired
    private IOnlCgreportHeadService onlCgreportHeadService;
    @Autowired
    private IOnlCgreportItemService onlCgreportItemService;
    @Autowired
    private ISysBaseAPI sysBaseAPI;
    @Autowired
    private IOnlCgreportParamService onlCgreportParamService;

    @GetMapping({"/getColumnsAndData/{code}"})
    @PermissionData
    public Result<?> getColumnsAndData(@PathVariable("code") String code, HttpServletRequest req) {
        OnlCgreportHead head = this.onlCgreportHeadService.getById(code);
        if (head == null) {
            return Result.error("实体不存在");
        } else {
            Result data = this.getData(code, req);
            if (data.getCode().equals(200)) {
                JSONObject var5 = JSON.parseObject(JSONObject.toJSONString(data.getResult()));
                JSONArray var6 = var5.getJSONArray("records");
                QueryWrapper<OnlCgreportItem> qw = new QueryWrapper<OnlCgreportItem>();
                qw.eq("cgrhead_id", code).eq("is_show", 1).orderByAsc("order_num");
                List<OnlCgreportItem> items = this.onlCgreportItemService.list(qw);
                HashMap var9 = new HashMap();
                JSONArray var10 = new JSONArray();
                JSONArray var11 = new JSONArray();
                Iterator<OnlCgreportItem> iterator = items.iterator();

                while(iterator.hasNext()) {
                    OnlCgreportItem item = iterator.next();
                    JSONObject var14 = new JSONObject(4);
                    var14.put("title", item.getFieldTxt());
                    var14.put("dataIndex", item.getFieldName());
                    var14.put("align", "center");
                    var14.put("sorter", "true");
                    String var15;
                    if (StringUtils.isNotBlank(item.getFieldHref())) {
                        var15 = "fieldHref_" + item.getFieldName();
                        JSONObject var16 = new JSONObject();
                        var16.put("customRender", var15);
                        var14.put("scopedSlots", var16);
                        JSONObject var17 = new JSONObject();
                        var17.put("slotName", var15);
                        var17.put("href", item.getFieldHref());
                        var10.add(var17);
                    }

                    var11.add(var14);
                    var15 = item.getDictCode();
                    List var19 = this.getDictModels(var15, var6, item.getFieldName());
                    if (var19 != null) {
                        var9.put(item.getFieldName(), var19);
                        var14.put("customRender", item.getFieldName());
                    }
                }

                Map<String, Object> var18 = new HashMap<String, Object>(3);
                var18.put("data", data.getResult());
                var18.put("columns", var11);
                var18.put("dictOptions", var9);
                var18.put("fieldHrefSlots", var10);
                var18.put("cgreportHeadName", head.getName());
                return Result.OK(var18);
            } else {
                return data;
            }
        }
    }

    private List<DictModel> getDictModels(String var1, JSONArray var2, String var3) {
        List var4 = null;
        if (oConvertUtils.isNotEmpty(var1)) {
            if (var1.trim().toLowerCase().indexOf("select ") == 0 && (var3 == null || var2.size() > 0)) {
                var1 = var1.trim();
                int var5 = var1.lastIndexOf(";");
                if (var5 == var1.length() - 1) {
                    var1 = var1.substring(0, var5);
                }

                String var6 = "SELECT * FROM (" + var1 + ") temp ";
                String var12;
                if (var2 != null) {
                    List var7 = new ArrayList();

                    for(int var8 = 0; var8 < var2.size(); ++var8) {
                        JSONObject var9 = var2.getJSONObject(var8);
                        String var10 = var9.getString(var3);
                        if (StringUtils.isNotBlank(var10)) {
                            var7.add(var10);
                        }
                    }

                    var12 = "'" + StringUtils.join(var7, "','") + "'";
                    var6 = var6 + "WHERE temp.value IN (" + var12 + ")";
                }

                List var11 = ((OnlCgreportHeadMapper)this.onlCgreportHeadService.getBaseMapper()).executeSelete(var6);
                if (var11 != null && var11.size() != 0) {
                    var12 = JSON.toJSONString(var11);
                    var4 = JSON.parseArray(var12, DictModel.class);
                }
            } else {
                var4 = this.sysBaseAPI.queryDictItemsByCode(var1);
            }
        }

        return var4;
    }

    /** @deprecated */
    @Deprecated
    @GetMapping({"/getColumns/{code}"})
    public Result<?> getColumns(@PathVariable("code") String var1) {
        OnlCgreportHead var2 = (OnlCgreportHead)this.onlCgreportHeadService.getById(var1);
        if (var2 == null) {
            return Result.error("实体不存在");
        } else {
            QueryWrapper var3 = new QueryWrapper();
            var3.eq("cgrhead_id", var1);
            var3.eq("is_show", 1);
            var3.orderByAsc("order_num");
            List var4 = this.onlCgreportItemService.list(var3);
            ArrayList var5 = new ArrayList();
            HashMap var6 = new HashMap();
            Iterator var7 = var4.iterator();

            while(var7.hasNext()) {
                OnlCgreportItem var8 = (OnlCgreportItem)var7.next();
                HashMap var9 = new HashMap(3);
                var9.put("title", var8.getFieldTxt());
                var9.put("dataIndex", var8.getFieldName());
                var9.put("align", "center");
                var9.put("sorter", "true");
                var5.add(var9);
                String var10 = var8.getDictCode();
                if (oConvertUtils.isNotEmpty(var10)) {
                    List var11 = null;
                    if (var10.toLowerCase().indexOf("select ") == 0) {
                        List var12 = ((OnlCgreportHeadMapper)this.onlCgreportHeadService.getBaseMapper()).executeSelete(var10);
                        if (var12 != null && var12.size() != 0) {
                            String var13 = JSON.toJSONString(var12);
                            var11 = JSON.parseArray(var13, DictModel.class);
                        }
                    } else {
                        var11 = this.sysBaseAPI.queryDictItemsByCode(var10);
                    }

                    if (var11 != null) {
                        var6.put(var8.getFieldName(), var11);
                        var9.put("customRender", var8.getFieldName());
                    }
                }
            }

            HashMap var14 = new HashMap(1);
            var14.put("columns", var5);
            var14.put("dictOptions", var6);
            var14.put("cgreportHeadName", var2.getName());
            return Result.OK(var14);
        }
    }

    @GetMapping({"/getData/{code}"})
    @PermissionData
    public Result<?> getData(@PathVariable("code") String code, HttpServletRequest request) {
        OnlCgreportHead reportHead = this.onlCgreportHeadService.getById(code);
        if (reportHead == null) {
            return Result.error("实体不存在");
        } else {
            String cgrSql = reportHead.getCgrSql().trim();
            String dbSource = reportHead.getDbSource();

            try {
                Map<String, Object> params = SqlUtil.getParams(request);
                params.put("getAll", request.getAttribute("getAll"));
                Map<String, Object> result;
                if (StringUtils.isNotBlank(dbSource)) {
                    log.info("Online报表: 走了多数据源逻辑");
                    result = this.onlCgreportHeadService.executeSelectSqlDynamic(dbSource, cgrSql, params, reportHead.getId());
                } else {
                	log.info("Online报表: 走了稳定逻辑");
                	result = this.onlCgreportHeadService.executeSelectSql(cgrSql, reportHead.getId(), params);
                }

                return Result.OK(result);
            } catch (Exception e) {
            	log.error(e.getMessage(), e);
                return Result.error("SQL执行失败：" + e.getMessage());
            }
        }
    }

    @GetMapping({"/getQueryInfo/{code}"})
    public Result<?> getQueryInfo(@PathVariable("code") String var1) {
        try {
            List var2 = this.onlCgreportItemService.getAutoListQueryInfo(var1);
            return Result.OK(var2);
        } catch (Exception var3) {
        	log.info(var3.getMessage(), var3);
            return Result.error("查询失败");
        }
    }

    @GetMapping({"/getParamsInfo/{code}"})
    public Result<?> getParamsInfo(@PathVariable("code") String code) {
        try {
            LambdaQueryWrapper<OnlCgreportParam> qw = new LambdaQueryWrapper<>();
            qw.eq(OnlCgreportParam::getCgrheadId, code);
            List<OnlCgreportParam> params = this.onlCgreportParamService.list(qw);
            return Result.OK(params);
        } catch (Exception e) {
        	log.info(e.getMessage(), e);
            return Result.error("查询失败");
        }
    }

    @PermissionData
    @RequestMapping({"/exportXls/{reportId}"})
    public void exportXls(@PathVariable("reportId") String var1, HttpServletRequest var2, HttpServletResponse var3) {
        String var4 = "报表";
        String var5 = "导出信息";
        if (!oConvertUtils.isNotEmpty(var1)) {
            throw new JeecgBootException("参数错误");
        } else {
            Map var6 = null;

            try {
                var6 = this.onlCgreportHeadService.queryCgReportConfig(var1);
            } catch (Exception var31) {
                throw new JeecgBootException("动态报表配置不存在!");
            }

            List var7 = (List)var6.get("items");
            var2.setAttribute("getAll", true);
            Result var8 = this.getData(var1, var2);
            List var9 = null;
            if (var8.getCode().equals(200)) {
                Map var10 = (Map)var8.getResult();
                var9 = (List)var10.get("records");
            }

            ArrayList var32 = new ArrayList();

            String var12;
            for(int var11 = 0; var11 < var7.size(); ++var11) {
                if ("1".equals(oConvertUtils.getString(((Map)var7.get(var11)).get("is_show")))) {
                    var12 = ((Map)var7.get(var11)).get("field_name").toString();
                    ExcelExportEntity var13 = new ExcelExportEntity(((Map)var7.get(var11)).get("field_txt").toString(), var12, 15);
                    Object var14 = ((Map)var7.get(var11)).get("dict_code");
                    JSONArray var15 = JSONObject.parseArray(JSONObject.toJSONString(var9));
                    List var16 = this.getDictModels(oConvertUtils.getString(var14), var15, var12);
                    if (var16 != null && var16.size() > 0) {
                        ArrayList var17 = new ArrayList();
                        Iterator var18 = var16.iterator();

                        while(var18.hasNext()) {
                            DictModel var19 = (DictModel)var18.next();
                            var17.add(var19.getText() + "_" + var19.getValue());
                        }

                        var13.setReplace((String[])var17.toArray(new String[var17.size()]));
                    }

                    Object var36 = ((Map)var7.get(var11)).get("replace_val");
                    if (oConvertUtils.isNotEmpty(var36)) {
                        var13.setReplace(var36.toString().split(","));
                    }

                    var32.add(var13);
                }
            }

            var3.setContentType("application/vnd.ms-excel");
            ServletOutputStream var33 = null;

            try {
                var12 = BrowserUtils.checkBrowse(var2);
                if ("MSIE".equalsIgnoreCase(var12.substring(0, 4))) {
                    var3.setHeader("content-disposition", "attachment;filename=" + URLEncoder.encode(var4, "UTF-8") + ".xls");
                } else {
                    String var34 = new String(var4.getBytes("UTF-8"), "ISO8859-1");
                    var3.setHeader("content-disposition", "attachment;filename=" + var34 + ".xls");
                }

                Workbook var35 = ExcelExportUtil.exportExcel(new ExportParams((String)null, var5), var32, var9);
                var33 = var3.getOutputStream();
                var35.write(var33);
            } catch (Exception var29) {
            } finally {
                try {
                    var33.flush();
                    var33.close();
                } catch (Exception var28) {
                }

            }

        }
    }

    @GetMapping({"/getRpColumns/{code}"})
    public Result<?> getRpColumns(@PathVariable("code") String code) {
        LambdaQueryWrapper<OnlCgreportHead> qw = new LambdaQueryWrapper<>();
        qw.eq(OnlCgreportHead::getCode, code);
        OnlCgreportHead reportHead = this.onlCgreportHeadService.getOne(qw);
        if (reportHead == null) {
            return Result.error("实体不存在");
        } else {
            QueryWrapper<OnlCgreportItem> reportItem = new QueryWrapper<>();
            reportItem.eq("cgrhead_id", reportHead.getId());
            reportItem.eq("is_show", 1);
            reportItem.orderByAsc("order_num");
            List<OnlCgreportItem> items = this.onlCgreportItemService.list(reportItem);
            ArrayList<Map<String, String>> columns = new ArrayList<>();
            HashMap<String, Object> dictOptions = new HashMap<>();

            HashMap<String, String> column;
            for(Iterator<OnlCgreportItem> iterator = items.iterator(); iterator.hasNext(); columns.add(column)) {
                OnlCgreportItem item = iterator.next();
                column = new HashMap<>();
                column.put("title", item.getFieldTxt());
                column.put("dataIndex", item.getFieldName());
                column.put("align", "center");
                String var11 = item.getFieldType();
                if ("Integer".equals(var11) || "Date".equals(var11) || "Long".equals(var11)) {
                	column.put("sorter", "true");
                }

                String var12 = item.getDictCode();
                if (oConvertUtils.isNotEmpty(var12)) {
                    List<DictModel> dictModels = this.getDictModels(item.getDictCode(), null, null);
                    dictOptions.put(item.getFieldName(), dictModels);
                    column.put("customRender", item.getFieldName());
                }
            }

            HashMap<String, Object> result = new HashMap<>();
            result.put("columns", columns);
            result.put("dictOptions", dictOptions);
            result.put("cgRpConfigId", reportHead.getId());
            result.put("cgRpConfigName", reportHead.getName());
            return Result.ok(result);
        }
    }

    @PostMapping({"/testConnection"})
    public Result a(@RequestBody DynamicDataSourceModel var1) {
        Connection var2 = null;

        Result var4;
        try {
            Class.forName(var1.getDbDriver());
            var2 = DriverManager.getConnection(var1.getDbUrl(), var1.getDbUsername(), var1.getDbPassword());
            Result var3;
            if (var2 != null) {
                var3 = Result.ok("数据库连接成功");
                return var3;
            }

            var3 = Result.ok("数据库连接失败：错误未知");
            return var3;
        } catch (ClassNotFoundException var17) {
        	log.error(var17.toString());
            var4 = Result.error("数据库连接失败：驱动类不存在");
        } catch (Exception var18) {
        	log.error(var18.toString());
            var4 = Result.error("数据库连接失败：" + var18.getMessage());
            return var4;
        } finally {
            try {
                if (var2 != null && !var2.isClosed()) {
                    var2.close();
                }
            } catch (SQLException var16) {
            	log.error(var16.toString());
            }

        }

        return var4;
    }
}
