package org.jeecg.modules.online.cgform.converter.impl;

import java.util.ArrayList;
import java.util.List;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.vo.DictModel;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.online.cgform.converter.field.FieldFieldCommentConverter;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;

public class CheckboxFieldCommentConverter extends FieldFieldCommentConverter {

    public CheckboxFieldCommentConverter(OnlCgformField onlCgformField) {
        ISysBaseAPI sysBaseAPI = SpringContextUtils.getBean(ISysBaseAPI.class);
        String dictTable = onlCgformField.getDictTable();
        String dictText = onlCgformField.getDictText();
        String dictField = onlCgformField.getDictField();
        List<DictModel> arrayList = new ArrayList<>();
        if (oConvertUtils.isNotEmpty(dictTable)) {
            arrayList = sysBaseAPI.queryTableDictItemsByCode(dictTable, dictText, dictField);
        } else if (oConvertUtils.isNotEmpty(dictField)) {
            arrayList = sysBaseAPI.queryDictItemsByCode(dictField);
        }

        this.dictList = arrayList;
        this.filed = onlCgformField.getDbFieldName();
    }

    @Override
    public String converterToVal(String txt) {
        if (oConvertUtils.isEmpty(txt)) {
            return null;
        } else {
            List<String> arrayList = new ArrayList<>();
            String[] strings = txt.split(",");

            for (String s : strings) {
                String s1 = super.converterToVal(s);
                if (s1 != null) {
                    arrayList.add(s1);
                }
            }

            return String.join(",", arrayList);
        }
    }

    @Override
    public String converterToTxt(String val) {
        if (oConvertUtils.isEmpty(val)) {
            return null;
        } else {
            List<String> arrayList = new ArrayList<>();
            String[] strings = val.split(",");
            int length = strings.length;

            for(int i = 0; i < length; ++i) {
                String s = strings[i];
                String s1 = super.converterToTxt(s);
                if (s1 != null) {
                    arrayList.add(s1);
                }
            }

            return String.join(",", arrayList);
        }
    }
}
