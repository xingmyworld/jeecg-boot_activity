package org.jeecg.modules.online.cgform.mapper;

import org.jeecg.modules.online.cgform.entity.OnlSystem;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 子系统
 * @author: MxpIO
 * @version: V1.0
 */
public interface OnlSystemMapper extends BaseMapper<OnlSystem> {

}
