package org.jeecg.modules.online.cgform.enums;


public interface CgformConstant
{

    public static final Integer ONLINE_TABLE_TYPE_SUB = Integer.valueOf(3);
    public static final Integer ONLINE_TABLE_TYPE_ONE = Integer.valueOf(1);
    public static final Integer ONLINE_TABLE_TYPE_MAIN = Integer.valueOf(2);
    public static final String ONLINE_JS_CHANGE_FUNCTION_NAME = "onlChange";

}
