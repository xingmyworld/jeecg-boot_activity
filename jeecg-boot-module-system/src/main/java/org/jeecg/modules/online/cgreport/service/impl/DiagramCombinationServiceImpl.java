package org.jeecg.modules.online.cgreport.service.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.jeecg.modules.online.cgreport.entity.DiagramCombination;
import org.jeecg.modules.online.cgreport.entity.DiagramCombinationQueryConfig;
import org.jeecg.modules.online.cgreport.mapper.DiagramCombinationMapper;
import org.jeecg.modules.online.cgreport.service.IDiagramCombinationDetailService;
import org.jeecg.modules.online.cgreport.service.IDiagramCombinationQueryConfigService;
import org.jeecg.modules.online.cgreport.service.IDiagramCombinationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

@Service
public class DiagramCombinationServiceImpl extends ServiceImpl<DiagramCombinationMapper, DiagramCombination>
		implements IDiagramCombinationService {
	
	@Autowired
	private IDiagramCombinationDetailService diagramCombinationDetailService;
	@Autowired
	private IDiagramCombinationQueryConfigService diagramCombinationQueryConfigService;
	
	@Resource
	private DiagramCombinationMapper diagramCombinationMapper;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void add(DiagramCombination diagramCombination) {
		// 保存主表信息
		this.save(diagramCombination);
		// 保存子表信息
		if (diagramCombination != null && diagramCombination.getDiagramCombinationDetails() != null
				&& !diagramCombination.getDiagramCombinationDetails().isEmpty()) {
			diagramCombination.getDiagramCombinationDetails()
					.forEach(dcd -> dcd.setGraphreportTempletId(diagramCombination.getId()));
			this.diagramCombinationDetailService.saveBatch(diagramCombination.getDiagramCombinationDetails());

			// 设置查询配置所属组合报表ID
			List<DiagramCombinationQueryConfig> diagramCombinationQueryConfigs = diagramCombination.getDiagramCombinationQueryConfigs();
			diagramCombinationQueryConfigs.forEach(item -> {
				item.setGraphreportTempletId(diagramCombination.getId());
			});

			this.diagramCombinationQueryConfigService.saveBatch(diagramCombination.getDiagramCombinationQueryConfigs());
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void edit(DiagramCombination diagramCombination) {
		this.updateById(diagramCombination);
		// 更新子表信息
		Map<String, Object> columnMap = new HashMap<>();
		columnMap.put("GRAPHREPORT_TEMPLET_ID", diagramCombination.getId());
		this.diagramCombinationDetailService.removeByMap(columnMap);
		this.diagramCombinationDetailService.saveBatch(diagramCombination.getDiagramCombinationDetails());
		// 更新查询配置子表信息
		this.diagramCombinationQueryConfigService.removeByMap(columnMap);

		// 设置所属组合报表模板ID
		List<DiagramCombinationQueryConfig> diagramCombinationQueryConfigs = diagramCombination.getDiagramCombinationQueryConfigs();
		diagramCombinationQueryConfigs.forEach(item -> {
			item.setGraphreportTempletId(diagramCombination.getId());
		});

		this.diagramCombinationQueryConfigService.saveBatch(diagramCombination.getDiagramCombinationQueryConfigs());
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteById(String id) {
		DiagramCombination diagramCombination = super.getById(id);
		this.removeById(id);
		// 删除子表信息
		Map<String, Object> columnMap = new HashMap<>();
		columnMap.put("GRAPHREPORT_TEMPLET_ID", diagramCombination.getId());
		this.diagramCombinationDetailService.removeByMap(columnMap);
		this.diagramCombinationQueryConfigService.removeByMap(columnMap);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteBatch(String ids) {
		List<String> idList = Arrays.asList(ids.split(","));
		QueryWrapper<DiagramCombination> queryWrapper = new QueryWrapper<>();
		queryWrapper.in("ID", idList);
		List<DiagramCombination> diagramCombinationList = super.list(queryWrapper);
		this.removeByIds(idList);
		// 删除子表信息
		if (diagramCombinationList != null && !diagramCombinationList.isEmpty()) {
			Map<String, Object> columnMap = new HashMap<>();
			for (DiagramCombination diagramCombination : diagramCombinationList) {
				columnMap.clear();
				columnMap.put("GRAPHREPORT_TEMPLET_ID", diagramCombination.getId());
				this.diagramCombinationDetailService.removeByMap(columnMap);
				this.diagramCombinationQueryConfigService.removeByMap(columnMap);
			}
		}
	}

	@Override
	public List<DiagramCombination> selectBySql(String sql) {
		return this.diagramCombinationMapper.selectBySql(sql);
	}

}
