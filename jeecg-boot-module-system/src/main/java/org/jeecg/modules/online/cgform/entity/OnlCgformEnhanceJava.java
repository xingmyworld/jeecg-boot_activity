package org.jeecg.modules.online.cgform.entity;

import java.io.Serializable;

public class OnlCgformEnhanceJava
    implements Serializable
{

    public OnlCgformEnhanceJava()
    {
    }

    public String getId()
    {
        return id;
    }

    public String getCgformHeadId()
    {
        return cgformHeadId;
    }

    public String getButtonCode()
    {
        return buttonCode;
    }

    public String getCgJavaType()
    {
        return cgJavaType;
    }

    public String getCgJavaValue()
    {
        return cgJavaValue;
    }

    public String getActiveStatus()
    {
        return activeStatus;
    }

    public String getEvent()
    {
        return event;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setCgformHeadId(String cgformHeadId)
    {
        this.cgformHeadId = cgformHeadId;
    }

    public void setButtonCode(String buttonCode)
    {
        this.buttonCode = buttonCode;
    }

    public void setCgJavaType(String cgJavaType)
    {
        this.cgJavaType = cgJavaType;
    }

    public void setCgJavaValue(String cgJavaValue)
    {
        this.cgJavaValue = cgJavaValue;
    }

    public void setActiveStatus(String activeStatus)
    {
        this.activeStatus = activeStatus;
    }

    public void setEvent(String event)
    {
        this.event = event;
    }

    public boolean equals(Object o)
    {
        if(o == this)
            return true;
        if(!(o instanceof OnlCgformEnhanceJava))
            return false;
        OnlCgformEnhanceJava onlcgformenhancejava = (OnlCgformEnhanceJava)o;
        if(!onlcgformenhancejava.canEqual(this))
            return false;
        String s = getId();
        String s1 = onlcgformenhancejava.getId();
        if(s != null ? !s.equals(s1) : s1 != null)
            return false;
        String s2 = getCgformHeadId();
        String s3 = onlcgformenhancejava.getCgformHeadId();
        if(s2 != null ? !s2.equals(s3) : s3 != null)
            return false;
        String s4 = getButtonCode();
        String s5 = onlcgformenhancejava.getButtonCode();
        if(s4 != null ? !s4.equals(s5) : s5 != null)
            return false;
        String s6 = getCgJavaType();
        String s7 = onlcgformenhancejava.getCgJavaType();
        if(s6 != null ? !s6.equals(s7) : s7 != null)
            return false;
        String s8 = getCgJavaValue();
        String s9 = onlcgformenhancejava.getCgJavaValue();
        if(s8 != null ? !s8.equals(s9) : s9 != null)
            return false;
        String s10 = getActiveStatus();
        String s11 = onlcgformenhancejava.getActiveStatus();
        if(s10 != null ? !s10.equals(s11) : s11 != null)
            return false;
        String s12 = getEvent();
        String s13 = onlcgformenhancejava.getEvent();
        return s12 != null ? s12.equals(s13) : s13 == null;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof OnlCgformEnhanceJava;
    }

    public int hashCode()
    {
        int i = 1;
        String s = getId();
        i = i * 59 + (s != null ? s.hashCode() : 43);
        String s1 = getCgformHeadId();
        i = i * 59 + (s1 != null ? s1.hashCode() : 43);
        String s2 = getButtonCode();
        i = i * 59 + (s2 != null ? s2.hashCode() : 43);
        String s3 = getCgJavaType();
        i = i * 59 + (s3 != null ? s3.hashCode() : 43);
        String s4 = getCgJavaValue();
        i = i * 59 + (s4 != null ? s4.hashCode() : 43);
        String s5 = getActiveStatus();
        i = i * 59 + (s5 != null ? s5.hashCode() : 43);
        String s6 = getEvent();
        i = i * 59 + (s6 != null ? s6.hashCode() : 43);
        return i;
    }

    public String toString()
    {
        return (new StringBuilder()).append("OnlCgformEnhanceJava(id=").append(getId()).append(", cgformHeadId=").append(getCgformHeadId()).append(", buttonCode=").append(getButtonCode()).append(", cgJavaType=").append(getCgJavaType()).append(", cgJavaValue=").append(getCgJavaValue()).append(", activeStatus=").append(getActiveStatus()).append(", event=").append(getEvent()).append(")").toString();
    }

    private static final long serialVersionUID = 1L;
    private String id;
    private String cgformHeadId;
    private String buttonCode;
    private String cgJavaType;
    private String cgJavaValue;
    private String activeStatus;
    private String event;
}
