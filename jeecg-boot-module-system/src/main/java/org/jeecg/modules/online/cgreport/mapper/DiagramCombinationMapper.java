package org.jeecg.modules.online.cgreport.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.online.cgreport.entity.DiagramCombination;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 组合图表配置
 */
public interface DiagramCombinationMapper extends BaseMapper<DiagramCombination> {

	List<DiagramCombination> selectBySql(@Param("sql") String sql);

}
