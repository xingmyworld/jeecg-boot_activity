package org.jeecg.modules.online.cgform.entity;

import java.io.Serializable;

public class OnlCgformEnhanceSql
    implements Serializable
{

    public OnlCgformEnhanceSql()
    {
    }

    public String getId()
    {
        return id;
    }

    public String getCgformHeadId()
    {
        return cgformHeadId;
    }

    public String getButtonCode()
    {
        return buttonCode;
    }

    public String getCgbSql()
    {
        return cgbSql;
    }

    public String getCgbSqlName()
    {
        return cgbSqlName;
    }

    public String getContent()
    {
        return content;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setCgformHeadId(String cgformHeadId)
    {
        this.cgformHeadId = cgformHeadId;
    }

    public void setButtonCode(String buttonCode)
    {
        this.buttonCode = buttonCode;
    }

    public void setCgbSql(String cgbSql)
    {
        this.cgbSql = cgbSql;
    }

    public void setCgbSqlName(String cgbSqlName)
    {
        this.cgbSqlName = cgbSqlName;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public boolean equals(Object o)
    {
        if(o == this)
            return true;
        if(!(o instanceof OnlCgformEnhanceSql))
            return false;
        OnlCgformEnhanceSql onlcgformenhancesql = (OnlCgformEnhanceSql)o;
        if(!onlcgformenhancesql.canEqual(this))
            return false;
        String s = getId();
        String s1 = onlcgformenhancesql.getId();
        if(s != null ? !s.equals(s1) : s1 != null)
            return false;
        String s2 = getCgformHeadId();
        String s3 = onlcgformenhancesql.getCgformHeadId();
        if(s2 != null ? !s2.equals(s3) : s3 != null)
            return false;
        String s4 = getButtonCode();
        String s5 = onlcgformenhancesql.getButtonCode();
        if(s4 != null ? !s4.equals(s5) : s5 != null)
            return false;
        String s6 = getCgbSql();
        String s7 = onlcgformenhancesql.getCgbSql();
        if(s6 != null ? !s6.equals(s7) : s7 != null)
            return false;
        String s8 = getCgbSqlName();
        String s9 = onlcgformenhancesql.getCgbSqlName();
        if(s8 != null ? !s8.equals(s9) : s9 != null)
            return false;
        String s10 = getContent();
        String s11 = onlcgformenhancesql.getContent();
        return s10 != null ? s10.equals(s11) : s11 == null;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof OnlCgformEnhanceSql;
    }

    public int hashCode()
    {
        int i = 1;
        String s = getId();
        i = i * 59 + (s != null ? s.hashCode() : 43);
        String s1 = getCgformHeadId();
        i = i * 59 + (s1 != null ? s1.hashCode() : 43);
        String s2 = getButtonCode();
        i = i * 59 + (s2 != null ? s2.hashCode() : 43);
        String s3 = getCgbSql();
        i = i * 59 + (s3 != null ? s3.hashCode() : 43);
        String s4 = getCgbSqlName();
        i = i * 59 + (s4 != null ? s4.hashCode() : 43);
        String s5 = getContent();
        i = i * 59 + (s5 != null ? s5.hashCode() : 43);
        return i;
    }

    public String toString()
    {
        return (new StringBuilder()).append("OnlCgformEnhanceSql(id=").append(getId()).append(", cgformHeadId=").append(getCgformHeadId()).append(", buttonCode=").append(getButtonCode()).append(", cgbSql=").append(getCgbSql()).append(", cgbSqlName=").append(getCgbSqlName()).append(", content=").append(getContent()).append(")").toString();
    }

    private static final long serialVersionUID = 1L;
    private String id;
    private String cgformHeadId;
    private String buttonCode;
    private String cgbSql;
    private String cgbSqlName;
    private String content;
}
