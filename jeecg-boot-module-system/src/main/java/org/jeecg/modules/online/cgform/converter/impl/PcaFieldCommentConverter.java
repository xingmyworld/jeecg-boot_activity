package org.jeecg.modules.online.cgform.converter.impl;

import org.jeecg.common.constant.ProvinceCityArea;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.online.cgform.converter.field.FieldFieldCommentConverter;
import org.jeecg.modules.online.cgform.entity.OnlCgformField;

public class PcaFieldCommentConverter extends FieldFieldCommentConverter {

    ProvinceCityArea provinceCityArea;

    public PcaFieldCommentConverter(OnlCgformField onlCgformField) {
        this.filed = onlCgformField.getDbFieldName();
        this.provinceCityArea = SpringContextUtils.getBean(ProvinceCityArea.class);
    }

    @Override
    public String converterToVal(String txt) {
        return oConvertUtils.isEmpty(txt) ? null : this.provinceCityArea.getCode(txt);
    }

    @Override
    public String converterToTxt(String val) {
        return oConvertUtils.isEmpty(val) ? null : this.provinceCityArea.getText(val);
    }
}
