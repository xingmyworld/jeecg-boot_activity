package org.jeecg.modules.online.cgform.entity;

import java.io.Serializable;

public class OnlCgformButton
    implements Serializable
{

    public OnlCgformButton()
    {
    }

    public String getId()
    {
        return id;
    }

    public String getCgformHeadId()
    {
        return cgformHeadId;
    }

    public String getButtonCode()
    {
        return buttonCode;
    }

    public String getButtonName()
    {
        return buttonName;
    }

    public String getButtonStyle()
    {
        return buttonStyle;
    }

    public String getOptType()
    {
        return optType;
    }

    public String getExp()
    {
        return exp;
    }

    public String getButtonStatus()
    {
        return buttonStatus;
    }

    public Integer getOrderNum()
    {
        return orderNum;
    }

    public String getButtonIcon()
    {
        return buttonIcon;
    }

    public String getOptPosition()
    {
        return optPosition;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setCgformHeadId(String cgformHeadId)
    {
        this.cgformHeadId = cgformHeadId;
    }

    public void setButtonCode(String buttonCode)
    {
        this.buttonCode = buttonCode;
    }

    public void setButtonName(String buttonName)
    {
        this.buttonName = buttonName;
    }

    public void setButtonStyle(String buttonStyle)
    {
        this.buttonStyle = buttonStyle;
    }

    public void setOptType(String optType)
    {
        this.optType = optType;
    }

    public void setExp(String exp)
    {
        this.exp = exp;
    }

    public void setButtonStatus(String buttonStatus)
    {
        this.buttonStatus = buttonStatus;
    }

    public void setOrderNum(Integer orderNum)
    {
        this.orderNum = orderNum;
    }

    public void setButtonIcon(String buttonIcon)
    {
        this.buttonIcon = buttonIcon;
    }

    public void setOptPosition(String optPosition)
    {
        this.optPosition = optPosition;
    }

    public boolean equals(Object o)
    {
        if(o == this)
            return true;
        if(!(o instanceof OnlCgformButton))
            return false;
        OnlCgformButton onlcgformbutton = (OnlCgformButton)o;
        if(!onlcgformbutton.canEqual(this))
            return false;
        String s = getId();
        String s1 = onlcgformbutton.getId();
        if(s != null ? !s.equals(s1) : s1 != null)
            return false;
        String s2 = getCgformHeadId();
        String s3 = onlcgformbutton.getCgformHeadId();
        if(s2 != null ? !s2.equals(s3) : s3 != null)
            return false;
        String s4 = getButtonCode();
        String s5 = onlcgformbutton.getButtonCode();
        if(s4 != null ? !s4.equals(s5) : s5 != null)
            return false;
        String s6 = getButtonName();
        String s7 = onlcgformbutton.getButtonName();
        if(s6 != null ? !s6.equals(s7) : s7 != null)
            return false;
        String s8 = getButtonStyle();
        String s9 = onlcgformbutton.getButtonStyle();
        if(s8 != null ? !s8.equals(s9) : s9 != null)
            return false;
        String s10 = getOptType();
        String s11 = onlcgformbutton.getOptType();
        if(s10 != null ? !s10.equals(s11) : s11 != null)
            return false;
        String s12 = getExp();
        String s13 = onlcgformbutton.getExp();
        if(s12 != null ? !s12.equals(s13) : s13 != null)
            return false;
        String s14 = getButtonStatus();
        String s15 = onlcgformbutton.getButtonStatus();
        if(s14 != null ? !s14.equals(s15) : s15 != null)
            return false;
        Integer integer = getOrderNum();
        Integer integer1 = onlcgformbutton.getOrderNum();
        if(integer != null ? !integer.equals(integer1) : integer1 != null)
            return false;
        String s16 = getButtonIcon();
        String s17 = onlcgformbutton.getButtonIcon();
        if(s16 != null ? !s16.equals(s17) : s17 != null)
            return false;
        String s18 = getOptPosition();
        String s19 = onlcgformbutton.getOptPosition();
        return s18 != null ? s18.equals(s19) : s19 == null;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof OnlCgformButton;
    }

    public int hashCode()
    {
        int i = 1;
        String s = getId();
        i = i * 59 + (s != null ? s.hashCode() : 43);
        String s1 = getCgformHeadId();
        i = i * 59 + (s1 != null ? s1.hashCode() : 43);
        String s2 = getButtonCode();
        i = i * 59 + (s2 != null ? s2.hashCode() : 43);
        String s3 = getButtonName();
        i = i * 59 + (s3 != null ? s3.hashCode() : 43);
        String s4 = getButtonStyle();
        i = i * 59 + (s4 != null ? s4.hashCode() : 43);
        String s5 = getOptType();
        i = i * 59 + (s5 != null ? s5.hashCode() : 43);
        String s6 = getExp();
        i = i * 59 + (s6 != null ? s6.hashCode() : 43);
        String s7 = getButtonStatus();
        i = i * 59 + (s7 != null ? s7.hashCode() : 43);
        Integer integer = getOrderNum();
        i = i * 59 + (integer != null ? integer.hashCode() : 43);
        String s8 = getButtonIcon();
        i = i * 59 + (s8 != null ? s8.hashCode() : 43);
        String s9 = getOptPosition();
        i = i * 59 + (s9 != null ? s9.hashCode() : 43);
        return i;
    }

    public String toString()
    {
        return (new StringBuilder()).append("OnlCgformButton(id=").append(getId()).append(", cgformHeadId=").append(getCgformHeadId()).append(", buttonCode=").append(getButtonCode()).append(", buttonName=").append(getButtonName()).append(", buttonStyle=").append(getButtonStyle()).append(", optType=").append(getOptType()).append(", exp=").append(getExp()).append(", buttonStatus=").append(getButtonStatus()).append(", orderNum=").append(getOrderNum()).append(", buttonIcon=").append(getButtonIcon()).append(", optPosition=").append(getOptPosition()).append(")").toString();
    }

    private static final long serialVersionUID = 1L;
    private String id;
    private String cgformHeadId;
    private String buttonCode;
    private String buttonName;
    private String buttonStyle;
    private String optType;
    private String exp;
    private String buttonStatus;
    private Integer orderNum;
    private String buttonIcon;
    private String optPosition;
}
