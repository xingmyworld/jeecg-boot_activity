package org.jeecg.modules.online.cgreport.entity;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class GroupData<T> {
	
	private List<T> charts = new ArrayList<>();

	private Integer groupNum;
	
	private String groupStyle;
	
	private String groupTxt;
	
	private String templetGrid;
}
