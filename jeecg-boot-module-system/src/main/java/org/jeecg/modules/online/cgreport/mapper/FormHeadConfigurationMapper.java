package org.jeecg.modules.online.cgreport.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.jeecg.modules.online.cgreport.entity.FormHeadConfiguration;

@Mapper
public interface FormHeadConfigurationMapper extends BaseMapper<FormHeadConfiguration> {
}
