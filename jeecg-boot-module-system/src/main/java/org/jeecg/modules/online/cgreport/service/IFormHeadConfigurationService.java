package org.jeecg.modules.online.cgreport.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.online.cgreport.entity.FormHeadConfiguration;

public interface IFormHeadConfigurationService extends IService<FormHeadConfiguration> {
}
