package org.jeecg.modules.system.mapper;

import org.jeecg.modules.system.entity.SysHomePageLayout;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 首页配置
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface SysHomePageLayoutMapper extends BaseMapper<SysHomePageLayout> {

}
