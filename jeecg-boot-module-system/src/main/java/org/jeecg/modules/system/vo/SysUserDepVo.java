package org.jeecg.modules.system.vo;

import lombok.Data;

/**
 * @author qinfeng
 * @version 1.0
 */
@Data
public class SysUserDepVo {
    private String userId;
    private String departName;
}
