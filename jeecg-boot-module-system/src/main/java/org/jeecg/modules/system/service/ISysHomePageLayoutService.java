package org.jeecg.modules.system.service;

import java.util.Map;

import org.jeecg.modules.system.entity.SysHomePageLayout;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 首页配置
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface ISysHomePageLayoutService extends IService<SysHomePageLayout> {

	/**
	 * 通过当前登录人查询
	 * @return
	 */
	SysHomePageLayout getSysHomePageLayoutByMap(Map<String, Object> parameter);

}
