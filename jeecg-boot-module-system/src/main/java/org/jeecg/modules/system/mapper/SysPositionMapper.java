package org.jeecg.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.system.entity.SysPosition;

/**
 * 职务表
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface SysPositionMapper extends BaseMapper<SysPosition> {

}
