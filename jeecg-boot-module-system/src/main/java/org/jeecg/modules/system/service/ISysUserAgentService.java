package org.jeecg.modules.system.service;

import org.jeecg.modules.system.entity.SysUserAgent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 用户代理人设置
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface ISysUserAgentService extends IService<SysUserAgent> {

}
