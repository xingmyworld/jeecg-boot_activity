package org.jeecg.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.system.entity.SysDataSource;

/**
 * 多数据源管理
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface SysDataSourceMapper extends BaseMapper<SysDataSource> {

}
