package org.jeecg.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.system.entity.SysUserClient;

public interface SysUserClientMapper extends BaseMapper<SysUserClient> {
}
