package org.jeecg.modules.system.service;

import org.jeecg.modules.system.entity.SysParameter;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 系统参数
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface ISysParameterService extends IService<SysParameter> {

}
