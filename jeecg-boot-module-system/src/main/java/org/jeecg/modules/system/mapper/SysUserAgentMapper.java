package org.jeecg.modules.system.mapper;

import org.jeecg.modules.system.entity.SysUserAgent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户代理人设置
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface SysUserAgentMapper extends BaseMapper<SysUserAgent> {

}
