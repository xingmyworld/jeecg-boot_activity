package org.jeecg.modules.system.mapper;

import org.jeecg.modules.system.entity.SysFieldValueChange;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 记录字段值的变化
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface SysFieldValueChangeMapper extends BaseMapper<SysFieldValueChange> {

}
