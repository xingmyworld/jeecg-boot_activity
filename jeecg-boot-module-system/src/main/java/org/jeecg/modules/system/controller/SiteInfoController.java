package org.jeecg.modules.system.controller;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.modules.config.InitSiteDataConfig;
import org.jeecg.modules.system.model.SiteInfoModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/site")
@Api(tags="站点信息")
@Slf4j
public class SiteInfoController {
	
	@GetMapping("siteinfo")
	@ApiOperation(value = "站点信息-获取站点信息", notes = "站点信息-获取站点信息")
	public Result<SiteInfoModel> siteinfo() {
		Result<SiteInfoModel> r = new Result<SiteInfoModel>();
		r.setSuccess(true);
		r.setCode(CommonConstant.SC_OK_200);
		r.setResult((SiteInfoModel) InitSiteDataConfig.SITE_PARAM.get("siteInfo"));
		log.info("");
		return r;
	}
}