package org.jeecg.modules.system.controller;

import java.util.Arrays;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.system.entity.SysHomePageLayout;
import org.jeecg.modules.system.service.ISysHomePageLayoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * 首页布局
 * @author: jeecg-boot
 * @version: V1.0
 */
@Api(tags="首页布局")
@RestController
@RequestMapping("/sys/sysHomePageLayout")
public class SysHomePageLayoutController extends JeecgController<SysHomePageLayout, ISysHomePageLayoutService> {
	@Autowired
	private ISysHomePageLayoutService sysHomePageLayoutService;
	
	/**
	 * 分页列表查询
	 *
	 * @param sysHomePageLayout
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "首页布局-分页列表查询")
	@ApiOperation(value="首页布局-分页列表查询", notes="首页布局-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(SysHomePageLayout sysHomePageLayout,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<SysHomePageLayout> queryWrapper = QueryGenerator.initQueryWrapper(sysHomePageLayout, req.getParameterMap());
		Page<SysHomePageLayout> page = new Page<SysHomePageLayout>(pageNo, pageSize);
		IPage<SysHomePageLayout> pageList = sysHomePageLayoutService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param sysHomePageLayout
	 * @return
	 */
	@AutoLog(value = "首页布局-添加")
	@ApiOperation(value="首页布局-添加", notes="首页布局-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody SysHomePageLayout sysHomePageLayout) {
		sysHomePageLayoutService.save(sysHomePageLayout);
		return Result.OK(sysHomePageLayout);
	}
	
	/**
	 *  编辑
	 *
	 * @param sysHomePageLayout
	 * @return
	 */
	@AutoLog(value = "首页布局-编辑")
	@ApiOperation(value="首页布局-编辑", notes="首页布局-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody SysHomePageLayout sysHomePageLayout) {
		sysHomePageLayoutService.updateById(sysHomePageLayout);
		return Result.OK(sysHomePageLayout);
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "首页布局-通过id删除")
	@ApiOperation(value="首页布局-通过id删除", notes="首页布局-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		sysHomePageLayoutService.removeById(id);
		return Result.OK("删除成功!",null);
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "首页布局-批量删除")
	@ApiOperation(value="首页布局-批量删除", notes="首页布局-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.sysHomePageLayoutService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!",null);
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "首页布局-通过id查询")
	@ApiOperation(value="首页布局-通过id查询", notes="首页布局-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		SysHomePageLayout sysHomePageLayout = sysHomePageLayoutService.getById(id);
		if(sysHomePageLayout==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(sysHomePageLayout);
	}
	
	/**
	 * 通过当前登录人查询
	 *
	 * @param parameter
	 * @return
	 */
	@AutoLog(value = "首页布局-通过当前登录人查询")
	@ApiOperation(value="首页布局-通过当前登录人查询", notes="首页布局-通过当前登录人查询")
	@RequestMapping(value = "/getSysHomePageLayout", method = RequestMethod.POST)
	public Result<?> getSysHomePageLayout(HttpServletRequest request, @RequestBody Map<String, Object> parameter) {
		SysHomePageLayout sysHomePageLayout = this.sysHomePageLayoutService.getSysHomePageLayoutByMap(parameter);
		if(sysHomePageLayout==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(sysHomePageLayout);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param sysHomePageLayout
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, SysHomePageLayout sysHomePageLayout) {
        return super.exportXls(request, sysHomePageLayout, SysHomePageLayout.class, "首页布局");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, SysHomePageLayout.class);
    }

}
