package org.jeecg.modules.system.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * TODO 移动设备码与用户关系实体类
 *
 * @author dousw
 * @version 1.0
 */

@Data
@TableName("sys_user_client")
public class SysUserClient {

    private String id;

    private String userId;

    private String clientId;
}
