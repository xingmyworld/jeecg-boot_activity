package org.jeecg.modules.message.mapper;

import org.jeecg.modules.message.entity.SysMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 消息
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface SysMessageMapper extends BaseMapper<SysMessage> {

}
