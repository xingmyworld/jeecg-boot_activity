package org.jeecg.modules.message.service;

import org.jeecg.common.system.base.service.JeecgService;
import org.jeecg.modules.message.entity.SysMessage;

/**
 * 消息
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface ISysMessageService extends JeecgService<SysMessage> {

}
