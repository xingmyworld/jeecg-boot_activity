package org.jeecg.modules.demo.test.mapper;

import org.jeecg.modules.demo.test.entity.JoaDemo;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 流程测试
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface JoaDemoMapper extends BaseMapper<JoaDemo> {

}
