package org.jeecg.modules.demo.test.service;

import org.jeecg.modules.demo.test.entity.JoaDemo;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 流程测试
 * @author: jeecg-boot
 * @version: V1.0
 */
public interface IJoaDemoService extends IService<JoaDemo> {

}
