package org.jeecg.weixin.web.handler;

import java.util.Map;

import org.springframework.stereotype.Component;

import org.jeecg.weixin.web.builder.TextBuilder;
import org.jeecg.weixin.web.utils.JsonUtils;
import org.jeecg.weixin.common.session.WxSessionManager;
import org.jeecg.weixin.cp.api.WxCpService;
import org.jeecg.weixin.cp.bean.message.WxCpXmlMessage;
import org.jeecg.weixin.cp.bean.message.WxCpXmlOutMessage;

/**
 * 通讯录变更事件处理器.
 *
 * @author Binary Wang(https://github.com/binarywang)
 */
@Component
public class ContactChangeHandler extends AbstractHandler {

	@Override
	public WxCpXmlOutMessage handle(WxCpXmlMessage wxMessage, Map<String, Object> context, WxCpService cpService,
			WxSessionManager sessionManager) {
		String content = "收到通讯录变更事件，内容：" + JsonUtils.toJson(wxMessage);
		this.logger.info(content);

		return new TextBuilder().build(content, wxMessage, cpService);
	}

}
