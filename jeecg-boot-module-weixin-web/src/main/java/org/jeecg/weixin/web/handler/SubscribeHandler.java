package org.jeecg.weixin.web.handler;

import java.util.Map;

import org.springframework.stereotype.Component;

import org.jeecg.weixin.web.builder.TextBuilder;
import org.jeecg.weixin.common.error.WxErrorException;
import org.jeecg.weixin.common.session.WxSessionManager;
import org.jeecg.weixin.cp.api.WxCpService;
import org.jeecg.weixin.cp.bean.WxCpUser;
import org.jeecg.weixin.cp.bean.message.WxCpXmlMessage;
import org.jeecg.weixin.cp.bean.message.WxCpXmlOutMessage;

/**
 * @author Binary Wang(https://github.com/binarywang)
 */
@Component
public class SubscribeHandler extends AbstractHandler {

	@Override
	public WxCpXmlOutMessage handle(WxCpXmlMessage wxMessage, Map<String, Object> context, WxCpService cpService,
			WxSessionManager sessionManager) throws WxErrorException {

		this.logger.info("新关注用户 OPENID: " + wxMessage.getFromUserName());

		// 获取微信用户基本信息
		WxCpUser userWxInfo = cpService.getUserService().getById(wxMessage.getFromUserName());

		if (userWxInfo != null) {
			// TODO 可以添加关注用户到本地
		}

		WxCpXmlOutMessage responseResult = null;
		try {
			responseResult = handleSpecial(wxMessage);
		} catch (Exception e) {
			this.logger.error(e.getMessage(), e);
		}

		if (responseResult != null) {
			return responseResult;
		}

		try {
			return new TextBuilder().build("感谢关注", wxMessage, cpService);
		} catch (Exception e) {
			this.logger.error(e.getMessage(), e);
		}

		return null;
	}

	/**
	 * 处理特殊请求，比如如果是扫码进来的，可以做相应处理
	 */
	private WxCpXmlOutMessage handleSpecial(WxCpXmlMessage wxMessage) {
		// TODO
		return null;
	}

}
