package org.jeecg.weixin.web.handler;

import org.jeecg.weixin.web.builder.TextBuilder;
import org.jeecg.weixin.web.utils.JsonUtils;
import org.jeecg.weixin.common.api.WxConsts;
import org.jeecg.weixin.common.session.WxSessionManager;
import org.jeecg.weixin.cp.api.WxCpService;
import org.jeecg.weixin.cp.bean.message.WxCpXmlMessage;
import org.jeecg.weixin.cp.bean.message.WxCpXmlOutMessage;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author Binary Wang(https://github.com/binarywang)
 */
@Component
public class MsgHandler extends AbstractHandler {

	@Override
	public WxCpXmlOutMessage handle(WxCpXmlMessage wxMessage, Map<String, Object> context, WxCpService cpService,
			WxSessionManager sessionManager) {
		final String msgType = wxMessage.getMsgType();
		if (msgType == null) {
			// 如果msgType没有，就自己根据具体报文内容做处理
		}

		if (!msgType.equals(WxConsts.XmlMsgType.EVENT)) {
			// TODO 可以选择将消息保存到本地
		}

		// TODO 组装回复消息
		String content = "收到信息内容：" + JsonUtils.toJson(wxMessage);

		return new TextBuilder().build(content, wxMessage, cpService);

	}

}
