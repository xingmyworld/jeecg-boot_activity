package org.jeecg.weixin.web.handler;

import java.util.Map;

import org.springframework.stereotype.Component;

import org.jeecg.weixin.web.utils.JsonUtils;
import org.jeecg.weixin.common.session.WxSessionManager;
import org.jeecg.weixin.cp.api.WxCpService;
import org.jeecg.weixin.cp.bean.message.WxCpXmlMessage;
import org.jeecg.weixin.cp.bean.message.WxCpXmlOutMessage;

/**
 * @author Binary Wang(https://github.com/binarywang)
 */
@Component
public class LogHandler extends AbstractHandler {
	@Override
	public WxCpXmlOutMessage handle(WxCpXmlMessage wxMessage, Map<String, Object> context, WxCpService cpService,
			WxSessionManager sessionManager) {
		this.logger.info("\n接收到请求消息，内容：{}", JsonUtils.toJson(wxMessage));
		return null;
	}

}
