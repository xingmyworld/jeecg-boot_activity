package org.jeecg.weixin.web.controller;

import org.jeecg.common.api.vo.Result;
import org.jeecg.weixin.common.bean.WxJsapiSignature;
import org.jeecg.weixin.common.error.WxErrorException;
import org.jeecg.weixin.cp.api.WxCpService;
import org.jeecg.weixin.web.config.WxCpConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/community/wx/auth")
public class XyParkWxAuthController {
	
	@Value("${jeecg.wx.community.agentId}")
	private Integer agentId;

	@GetMapping(value="/createJsapiSignature")
	public Result<Object> createJsapiSignature(
			@RequestParam(name = "url", required = true) String url) {
		WxCpService wxCpService =  WxCpConfiguration.getCpService(agentId);
		WxJsapiSignature jsapi;
		try {
			jsapi = wxCpService.createJsapiSignature(url);
			log.info("Signature url===>"+jsapi.getUrl());
		} catch (WxErrorException e) {
			e.printStackTrace();
			return Result.error(e.getMessage());
		}
		return Result.OK(jsapi);
	}
}
