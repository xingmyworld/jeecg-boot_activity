package org.jeecg.weixin.web.builder;

import org.jeecg.weixin.cp.api.WxCpService;
import org.jeecg.weixin.cp.bean.message.WxCpXmlMessage;
import org.jeecg.weixin.cp.bean.message.WxCpXmlOutMessage;
import org.jeecg.weixin.cp.bean.message.WxCpXmlOutTextMessage;

/**
 * @author Binary Wang(https://github.com/binarywang)
 */
public class TextBuilder extends AbstractBuilder {

	@Override
	public WxCpXmlOutMessage build(String content, WxCpXmlMessage wxMessage, WxCpService service) {
		WxCpXmlOutTextMessage m = WxCpXmlOutMessage.TEXT().content(content).fromUser(wxMessage.getToUserName())
				.toUser(wxMessage.getFromUserName()).build();
		return m;
	}

}
