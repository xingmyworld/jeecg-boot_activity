package org.jeecg.weixin.web.handler;

import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import org.jeecg.weixin.common.error.WxErrorException;
import org.jeecg.weixin.common.session.WxSessionManager;
import org.jeecg.weixin.cp.api.WxCpService;
import org.jeecg.weixin.cp.bean.message.WxCpXmlMessage;
import org.jeecg.weixin.cp.bean.message.WxCpXmlOutMessage;

/**
 * <pre>
 *
 * Created by Binary Wang on 2018/8/27.
 * </pre>
 *
 * @author <a href="https://github.com/binarywang">Binary Wang</a>
 */
@Slf4j
public class EnterAgentHandler extends AbstractHandler {
	private static final int TEST_AGENT = 1000002;

	@Override
	public WxCpXmlOutMessage handle(WxCpXmlMessage wxMessage, Map<String, Object> context, WxCpService wxCpService,
			WxSessionManager sessionManager) throws WxErrorException {
		// do something
		return null;
	}
}
