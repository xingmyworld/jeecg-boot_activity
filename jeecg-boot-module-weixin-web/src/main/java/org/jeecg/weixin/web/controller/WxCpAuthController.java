package org.jeecg.weixin.web.controller;

import org.jeecg.common.api.vo.Result;
import org.jeecg.weixin.common.error.WxErrorException;
import org.jeecg.weixin.cp.api.WxCpService;
import org.jeecg.weixin.web.config.WxCpConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/wx/cp/auth")
@Api(tags="企业微信-权限")
public class WxCpAuthController {
	
	@ApiOperation(value = "获取微信权限", notes = "获取微信权限")
    @GetMapping(value = "/getAuthUrl")
    public Result<Object> getAuthUrl(
    		@ApiParam(name = "url", value = "菜单", required = true) @RequestParam(value = "url", required = true) String url) throws WxErrorException{
		log.info("getAuthUrl url===>"+url);
		WxCpService wxCpService =  WxCpConfiguration.getCpService(1000011);
		String result = wxCpService.getOauth2Service().buildAuthorizationUrl(wxCpService.getWxCpConfigStorage().getDomain()+url,null);
		log.info("getAuthUrl result===>"+result);
		return Result.OK(result);
    }

}
