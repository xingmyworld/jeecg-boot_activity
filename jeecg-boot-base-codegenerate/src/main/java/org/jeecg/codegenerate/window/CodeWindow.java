package org.jeecg.codegenerate.window;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.jeecg.codegenerate.database.DbReadTableUtil;
import org.jeecg.codegenerate.generate.impl.CodeGenerateOne;
import org.jeecg.codegenerate.generate.pojo.TableVo;

public class CodeWindow extends JFrame
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -2388439999496817664L;
	private static String packageName;
    private static String className;
    private static String tableName;
    private static String description;
    private static Integer fieldRowNum;
    private static String identifier;
    private static String sequenceCode;
    String[] identifiers;
    
    /*public CodeWindow() {
        this.identifiers = new String[] { "uuid", "identity", "sequence" };
        final JPanel contentPane = new JPanel();
        this.setContentPane(contentPane);
        contentPane.setLayout(new GridLayout(14, 2));
        final JLabel label = new JLabel("提示:");
        final JLabel label2 = new JLabel();
        final JLabel label3 = new JLabel("包名（小写）：");
        final JTextField textField = new JTextField();
        final JLabel label4 = new JLabel("实体类名（首字母大写）：");
        final JTextField textField2 = new JTextField();
        final JLabel label5 = new JLabel("表名：");
        final JTextField textField3 = new JTextField(20);
        final JLabel label6 = new JLabel("主键生成策略：");
        final JComboBox<String> comboBox = new JComboBox<String>(identifiers);
        comboBox.setEnabled(false);
        final JLabel label7 = new JLabel("主键SEQUENCE：(oracle序列名)");
        final JTextField textField4 = new JTextField(20);
        final JLabel label8 = new JLabel("功能描述：");
        final JTextField textField5 = new JTextField();
        final JLabel label9 = new JLabel("行字段数目：");
        final JTextField textField6 = new JTextField();
        textField6.setText(CodeWindow.fieldRowNum + "");
        final ButtonGroup buttonGroup = new ButtonGroup();
        final JRadioButton radioButton = new JRadioButton("抽屉风格表单");
        radioButton.setSelected(true);
        final JRadioButton radioButton2 = new JRadioButton("弹窗风格表单");
        buttonGroup.add(radioButton);
        buttonGroup.add(radioButton2);
        final JCheckBox checkBox = new JCheckBox("Control");
        checkBox.setSelected(true);
        final JCheckBox checkBox2 = new JCheckBox("Vue");
        checkBox2.setSelected(true);
        final JCheckBox checkBox3 = new JCheckBox("Service");
        checkBox3.setSelected(true);
        final JCheckBox checkBox4 = new JCheckBox("Mapper.xml");
        checkBox4.setSelected(true);
        final JCheckBox checkBox5 = new JCheckBox("Dao");
        checkBox5.setSelected(true);
        final JCheckBox checkBox6 = new JCheckBox("Entity");
        checkBox6.setSelected(true);
        contentPane.add(label);
        contentPane.add(label2);
        contentPane.add(label3);
        contentPane.add(textField);
        contentPane.add(label4);
        contentPane.add(textField2);
        contentPane.add(label5);
        contentPane.add(textField3);
        contentPane.add(label6);
        contentPane.add(comboBox);
        contentPane.add(label7);
        contentPane.add(textField4);
        contentPane.add(label8);
        contentPane.add(textField5);
        contentPane.add(label9);
        contentPane.add(textField6);
        contentPane.add(checkBox);
        contentPane.add(checkBox2);
        contentPane.add(checkBox3);
        contentPane.add(checkBox4);
        contentPane.add(checkBox5);
        contentPane.add(checkBox6);
        contentPane.add(radioButton);
        contentPane.add(radioButton2);
        final JButton button = new JButton("生成");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                if ("".equals(textField.getText())) {
                    label2.setForeground(Color.red);
                    label2.setText("包名不能为空！");
                    return;
                }
                CodeWindow.packageName = textField.getText();
                if ("".equals(textField2.getText())) {
                    label2.setForeground(Color.red);
                    label2.setText("实体类名不能为空！");
                    return;
                }
                CodeWindow.className = textField2.getText();
                if ("".equals(textField5.getText())) {
                    label2.setForeground(Color.red);
                    label2.setText("描述不能为空！");
                    return;
                }
                CodeWindow.description = textField5.getText();
                if (!"".equals(textField3.getText())) {
                    CodeWindow.tableName = textField3.getText();
                    CodeWindow.identifier = (String)comboBox.getSelectedItem();
                    if (CodeWindow.identifier.equals("sequence")) {
                        if ("".equals(textField4.getText())) {
                            label2.setForeground(Color.red);
                            label2.setText("主键生成策略为sequence时，序列号不能为空！");
                            return;
                        }
                        CodeWindow.sequenceCode = textField4.getText();
                    }
                    try {
                        if (DbReadTableUtil.isTableExist(CodeWindow.tableName)) {
                            final TableVo tableVo = new TableVo();
                            tableVo.setTableName(CodeWindow.tableName);
                            tableVo.setPrimaryKeyPolicy(CodeWindow.identifier);
                            tableVo.setEntityPackage(CodeWindow.packageName);
                            tableVo.setEntityName(CodeWindow.className);
                            tableVo.setFieldRowNum(CodeWindow.fieldRowNum);
                            tableVo.setSequenceCode(CodeWindow.sequenceCode);
                            tableVo.setFtlDescription(CodeWindow.description);
                            new CodeGenerateOne(tableVo).generateCodeFile(null);
                            label2.setForeground(Color.red);
                            label2.setText("成功生成增删改查->功能：" + CodeWindow.description);
                        }
                        else {
                            label2.setForeground(Color.red);
                            label2.setText("表[" + CodeWindow.tableName + "] 在数据库中，不存在");
                            System.err.println(" ERROR ：   表 [ " + CodeWindow.tableName + " ] 在数据库中，不存在 ！请确认数据源配置是否配置正确、表名是否填写正确~");
                        }
                    }
                    catch (Exception ex) {
                        label2.setForeground(Color.red);
                        label2.setText(ex.getMessage());
                    }
                    return;
                }
                label2.setForeground(Color.red);
                label2.setText("表名不能为空！");
            }
        });
        final JButton button2 = new JButton("退出");
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                CodeWindow.this.dispose();
                System.exit(0);
            }
        });
        contentPane.add(button);
        contentPane.add(button2);
        this.setTitle("代码生成器[单表模型]");
        this.setVisible(true);
        this.setDefaultCloseOperation(3);
        this.setSize(new Dimension(600, 400));
        this.setResizable(false);
        this.setLocationRelativeTo(this.getOwner());
    }
    
    public static void main(final String[] args) {
        try {
            new CodeWindow().pack();
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    static {
        CodeWindow.packageName = "test";
        CodeWindow.className = "TestEntity";
        CodeWindow.tableName = "t00_company";
        CodeWindow.description = "分公司";
        CodeWindow.fieldRowNum = 1;
        CodeWindow.identifier = "uuid";
        CodeWindow.sequenceCode = "";
    }*/
}
